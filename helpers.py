# -*- coding: utf-8 -*-

"""
	helpers
	~~~~~~~

	Implements useful helpers.

	:copyright: (c) 2012 by Roman Semirook.
	:license: BSD, see LICENSE for more details.
"""

import os
from flask import Flask
from werkzeug.utils import import_string
import inspect,jinjahelpers

class AnonymousUserMixin(object):
	'''
	This is the default object for representing an anonymous user.
	'''
	def is_authenticated(self):
		return False

	def is_active(self):
		return False

	def is_anonymous(self):
		return True

	def get_id(self):
		return

	def is_admin(self):
		return False

	role = -1
	subRole = -1
		

class NoContextProcessorException(Exception):
	pass


class NoBlueprintException(Exception):
	pass


class NoExtensionException(Exception):
	pass

from ext import loginManager

class AppFactory(object):

	def __init__(self, config = 'PROJECT_SETTINGS', envvar = 'PROJECT_CREDENTIALS'):
		config = os.getenv(config, 'config.TestingConfig')
		self.silent = not (config == 'config.ProductionConfig')
		module, config_name = self._get_imported_stuff_by_path(config)
		if not hasattr(module, config_name):
			raise Exception('No {config_name} config found'.format(config_name=config_name))
		
		self.app_config = getattr(module, config_name)
		self.app_envvar = envvar
		
	def get_app(self, app_module_name, **kwargs):
		basedir = os.path.abspath(os.path.dirname(__file__))
		self.app = Flask(app_module_name, template_folder = basedir + "/templates")#TODO **kwargs missing from Flask()
		self.app.config.from_object(self.app_config)
		self.app.config.from_envvar(self.app_envvar, self.silent)

		self._bind_extensions()
		self._register_blueprints()
		self._register_context_processors()
		self._register_signals()
		self._config_jinja()
		self._config_loginManger()

		self._add_loggers()

		return self.app

	def _config_jinja(self):
		self.app.jinja_env.line_statement_prefix = '#'
		self.app.jinja_env.line_comment_prefix = '##'
		jinja_functions = inspect.getmembers(jinjahelpers,inspect.isfunction)
		for jinja_function in jinja_functions:
			self.app.jinja_env.globals[jinja_function[0]] = jinja_function[1]


	def _config_loginManger(self):
		loginManager.login_view = "basePortal.login"
		loginManager.refresh_view = "reauthenticate"#TODO view
		loginManager.needs_refresh_message = (u"To protect your account, please reauthenticate to access this page.")
		loginManager.needs_refresh_message_category = "info"
		loginManager.anonymous_user = AnonymousUserMixin


	def _add_loggers(self):
		
		for handler in self.app.config.get('HANDLERS', []):
			module, h_name = self._get_imported_stuff_by_path(handler)
			if hasattr(module, h_name):
				
				handler = getattr(module, h_name)(self.app)
				if handler: 
					self.app.logger.addHandler(handler)#mailHandler returns None in test/debugging environment
					for logger in self.app.config.get('LOGGERS', []):#Loggers of differnt extensions
						logger.addHandler(handler)
		
	def _get_imported_stuff_by_path(self, path):
		module_name, object_name = path.rsplit('.', 1)
		module = import_string(module_name)

		return module, object_name

	def _bind_extensions(self):
		for ext_path in self.app.config.get('EXTENSIONS', []):
			module, e_name = self._get_imported_stuff_by_path(ext_path)
			if not hasattr(module, e_name):
				raise NoExtensionException('No {e_name} extension found'.format(e_name=e_name))
			ext = getattr(module, e_name)
			if getattr(ext, 'init_app', False):
				ext.init_app(self.app)
			else:
				self.app.signer = ext(self.app)#TODO change it to support other extensions as well self.app.e_name is not working
				#One solution is to assign the variable based on e_name

	def _register_context_processors(self):
		for processor_path in self.app.config.get('CONTEXT_PROCESSORS', []):
			module, p_name = self._get_imported_stuffhon_by_path(processor_path)
			if hasattr(module, p_name):
				self.app.context_processor(getattr(module, p_name))
			else:
				raise NoContextProcessorException('No {cp_name} context processor found'.format(cp_name=p_name))

	def _register_blueprints(self):
		for blueprint_path in self.app.config.get('BLUEPRINTS', []):
			module, b_name = self._get_imported_stuff_by_path(blueprint_path)
			if hasattr(module, b_name):
				self.app.register_blueprint(getattr(module, b_name))
			else:
				raise NoBlueprintException('No {bp_name} blueprint found'.format(bp_name=b_name))

	def _register_signals(self):
		for k,v in self.app.config.get('SIGNALS', {}).items():
			module, attr = self._get_imported_stuff_by_path(k)			
			id = getattr(module, attr)
			module, attr = self._get_imported_stuff_by_path(v)
			signal = getattr(module, attr)
			id.connect(signal, self.app)
