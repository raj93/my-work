## Project's configuration

**Config.py** contains different configuration settings based on the run levels

* `BLUEPRINTS` is a list of registered blueprints.  
* `CONTEXT_PROCESSORS` is a list of registered context processors.  
* `EXTENSIONS` is a list of registered extensions.  
* `LOGGERS` is list of loggers apart from core logger of the flask application
* `HANDLERS` is list of logging handlers
* `SIGNALS` is list of signals, in our case, required for Flask principal

Following configurations are used by the application internally.

### Folder paths  

Define the following paths:
* `TEMP_FOLDER` Contains Customer's signatures of a particular transaction and transaction receipts temporarily
* `UPLOAD_FOLDER` Merchant identity proof/registration documents
* `TEMPLATE_FOLDER`: Application template files are stored here
* `LOG_FILENAME` File where application logs are stored. Includes the path. Used for logging by FileHandler 

### File Extensions allowed
* `SIGNATURE_EXTENSIONS_ALLOWED` File extensions allowed for Signature
* `PROOFS_EXTENSION_ALLOWED` File extensions allowed for merchant documents


### Amazon Cedentials for SES, S3
Note that the following two values will be set through ~/.boto or /etc/boto during development. Or using the IAM roles. They can also be set by setting the following configuration:
* `AWS_ACCESS_KEY`
* `AWS_SECRET_ACCESS_KEY`


### Contact Email
* `DEVELOPERS` is list of developers to whom mail is sent when a `CRITICAL` error occurs in the application
* `SALES` used to contact merchants for promotional events


### S3 Buckets
Objects such as signatures, transaction receipts, merchant proofs are stored in S3 buckets.
* `SIGNATURES_BUCKET` 
* `RECEIPTS_BUCKET`
* `UPLOADS_BUCKET`

### Session/Cookie Management
* `SECRET_KEY` Signs the sesson keys
* `SESSION_PROTECTION` Deteremines level of sesion key protection
* `ITS_DANGEROUS` Used to generate session keys for Mobile POS REST APIs
* `REMEMBER_COOKIE_NAME` Cookie name of 'Remember Me' functionality
* `SESSION_COOKIE_NAME` Sessions in Flask are stored on Client side by default in the form of Cookies. This determines the name of the client side session.
* `POS_SESSION_TIME` Duration of Session validity
* `REMEMBER_COOKIE_DURATION` Duration of Cookie validity
* `SESSION_COOKIE_HTTPONLY` controls if the cookie should be set with the httponly flag. Defaults to True.
* `SESSION_COOKIE_SECURE` Ensures that session cookie is exchanged only during a HTTPS connection.


### SqlAlchemy
* `SQLALCHEMY_DATABASE_URI` The database URI that should be used for the connection. Examples:

    1. sqlite:////tmp/test.db
    2. mysql://username:password@server/db
* `SQLALCHEMY_BINDS` A dictionary that maps bind keys to SQLAlchemy connection URIs
* `SQLALCHEMY_POOL_SIZE` The size of the database pool. Defaults to the engine’s default (usually 5)
* `SQLALCHEMY_POOL_RECYCLE` Number of seconds after which a connection is automatically recycled. This is required for MySQL, which removes connections after 8 hours idle by default. Note that Flask-SQLAlchemy automatically sets this to 2 hours if MySQL is used.
* `SQLALCHEMY_POOL_TIMEOUT` Specifies the connection timeout for the pool. Defaults to 10.
* `SQLALCHEMY_ECHO` If set to True SQLAlchemy will log all the statements issued to stderr which can be useful for debugging.


### Request Caps/Timings
* `TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST` Minimum time required between two otp requests from a POS
* `OTP_REQUEST_NUMTRIES_CAP` Maximum OTP Requests that can be made from a POS
* `ACTIVATION_REQUEST_NUMTRIES_CAP` This is actually intended to be maximum number of activation requests that can be made for a OTP so as to prevent brute force activation for a OTP. But changed to maximum activation requests from a POS to tighten the security

.
### Flask-WTF
* `CSRF_ENABLED` Enable/Disable CSRF @ WTF forms


### Misc
* `DEBUG` Enable/Disable debug mode
* `TESTING` Enable/Disable Testing mode. It should be set to ``True`` during unittests
* `PLUTUS_STATUS` (Not used anymore). Used to control is transaction should go to Plutus or be taken care by AasaanPay's simulator. 
* `STRIPE_LIVE` Controls if transaction should be treated as live/demo by Stripe. Should be True in production for live transactions
* `ALLOWED_SMS_LIST` Restricts access from developer mobiles during development


### Third party application details
* `SENTRY_DSN` 
* `FRESH_DESK_API_KEY`
* `KEY_TIER_URL` 


## Production Configuration
Some configuration variables like `SESSION_KEY` are sensitive and shouldn't be shared between development and production environment. These values are to be kept secret and are to be disclosed only with business justification. Following configuration are to be defined in creds.py for usage in ``Production`` environment:
* `DEVELOPERS`
* `SALES`
* `SIGNATURES_BUCKET` (Optional)
* `RECEIPTS_BUCKET` (Optional)
* `UPLOADS_BUCKET` (Optional)
* `SECRET_KEY`
* `ITS_DANGEROUS`
* `POS_SESSION_TIME` (Optional)
* `SQLALCHEMY_DATABASE_URI`
* `SQLALCHEMY_BINDS`
* `FORGOT_PASSWORD_FROM_EMAIL`
* `FORGOT_PASSWORD_TOKEN_KEY`
* `RECAPTCHA_PUBLIC_KEY`
* `RECAPTCHA_PRIVATE_KEY`
* `SENTRY_DSN`
* `FRESH_DESK_API_KEY`
* `KEY_TIER_URL`



