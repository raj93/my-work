## What Backend is 

**Backend** contains the server side code of AasaanPay. It covers the following functionality:

1. ```Merchant Portal```
2. ```Admin Portal```
3. ```Customer care Portal```
4. ```Payment application```

## Installation

Backend installation process is trivial. 

1. ```git clone git://github.com/Asaan/backend.git```
2. ```cd backend```
3. Install dependencies mentioned in `dependencies.<os>`
4. ```pip install -r reqs.pip```
5. ```apt-get install python-mysqldb```
6. ```apt-get install wkhtmltopdf```
7. Change database engine type to `InnoDB`, if necessary
8. Copy `deploy/backend.wsgi` to apache(or httpd) root directory
9. Copy VirtualHost configuration from `deploy/http_backend.conf' to apache(or httpd) conf
10. Give appropriate AWS permissions/credentials using either boto.cfg or via IAM to instances

  * Email services (SES)
  * Signatures/receipts in S3 buckets
11. Set environment variables PROJECT_SETTINGS and PROJECT_CREDENTIALS to choose configuration and credentials respectively


## Run

There is some demo data for you. Create it.  
1. ```./manage.py init_data```

Run development server, and go to http://127.0.0.1:5000  
2. ```./manage.py runserver```

Demo admin's login/password is admin/develop.


## Configuration

Application architecture is based on Flask Kit and consists of some helpers, blueprints,
`app.py`, `ext.py`, `helpers.py`, `manage.py`, `config.py` and `testing.py` modules.

Let's take a look at each of them.

### app.py

There is your main app instance, created by `AppFactory`. Note, it's just a point for blueprints,
context processors and extensions binding. But don't bind them explicit, as usual. And don't bind any views
to the main app. Why?

You have at least two apps in your project – one as the basic app and one for testing.
Each of them is created at runtime with some individual settings for database, debug level etc.
And each of them has to have access to any views or extensions with their individual settings.
So dynamical application binding is much more flexible solution.


### ext.py

I've found it neat to define all extensions separately and bind them to the application at runtime.
Unfortunately, it's possible if extension provides init_app() method only. But for some not-so-smart
extensions there is some workaround. Look into the file for details.


### helpers.py

There is the application factory and, maybe, something else (in the future) to avoid routine.


### testing.py

Simple basic TestCase for your tests. Note, that `nose` test runner is used (it's really good).

```
(flaskit)MacBook-Pro-Roman:flaskit semirook$ nosetests
...
----------------------------------------------------------------------
Ran 3 tests in 0.476s

OK
```


### config.py

Contains settings specific to different run levels.

`BLUEPRINTS` is a list of registered blueprints.  
`CONTEXT_PROCESSORS` is a list of registered context processors.  
`EXTENSIONS` is a list of registered extensions.  

Helper script will automatically register blueprints specified in the `BLUEPRINTS`
list for you. Behaviour for `CONTEXT_PROCESSORS` and `EXTENSIONS` lists is the same.

The notation is `package.module.object` or `package.object` if object is in the `__init__.py`.
Look into the file for examples.

Check out details of other configuration varibales at [Config](config.md)

### manage.py

A set of scripts that you may find useful. Amount of commands will constantly
grow. By now, there are:

**Command**           | **Result**                                             |
----------------------|--------------------------------------------------------|
runserver             | Runs the Flask development server i.e. app.run()       |
shell                 | Runs interactive shell, ipython if installed           |
init_db               | Creates some demo DB-tables and data                   |
refresh_db			  | Purge db and Initilise with demo data	 
clean_pyc             | Removes all *.pyc files from the project folder        |

Run `./manage.py -h` for the list of all available commands.

Run `./manage.py command_name -h` for the list of command arguments.


## Bug Fixes/Hacks with Python Packages

### Werkzeug==0.9.3(dependency package)

Replace with werkzeug==0.8.3 if any error such as "TypeError: decoding Unicode is not supported"

### Alembic

1. Append the os path in alembic/env.py to import the database for it to work.

`import os, sys`
`sys.path.append(os.getcwd())`  
2. Replace the server_default = 'adsfa' with server_default = sa.text('adsfa')


### Sqlalchemy.

Change the line 
```from sqlalchemy import exceptions as sa_exceptions``` @
File "/usr/local/lib/python2.7/dist-packages/migrate/versioning/schema.py", line 10, in <module> to 
```from sqlalchemy import exc as sa_exceptions```


### Mysql

MyISAM was the default storage engine for the MySQL relational database management system versions prior to 5.5. The major deficiency of MyISAM is the absence of transactions support. Also, foreign keys are not supported. In normal use cases, InnoDB seems to be faster than MyISAM.

`CentOS` has MySQL 5.1 version. Make sure to change default engine to innodb if used. Amazon RDS use 5.6+ versions and doesn't pose this problem. 


### Twill==1.8.0

Is used during testing(py.test). Gives the following error in testing mode

```Error in twillBrowser at /usr/local/lib/python2.7/site-packages/twill/browser: ConnectionCls is not an attribute of HTTPConnectionPool```

Comment the following line

```cpl.HTTPConnectionPool.old_http = cpl.HTTPConnectionPool.ConnectionCls``` in browser.py

### Xhtml2Pdf==3.0.33 

If reportlab==3.0+ is installed, following error is given due to package error. 
```Reportlab Version 2.1+ is needed```

The problem is with the installation. The init checks for a variable REPORTLAB22. This is imported from util.py(of package xhtml2pdf).

The code in util.py is using the following logic

```if not (reportlab.Version[0] == "2" and reportlab.Version[2] >= "1"):
raise ImportError("Reportlab Version 2.1+ is needed!")
REPORTLAB22 = (reportlab.Version[0] == "2" and reportlab.Version[2] >= "2")```

This will fail as the reportlab version is now 3.0.

Just replace this code segment with the following:

```if not (reportlab.Version[:3]>="2.1"):
	raise ImportError("Reportlab Version 2.1+ is needed!")
REPORTLAB22 = (reportlab.Version[:3]>="2.1")```

### Pillow

If dependencies are not installed, following error is displayed. 
```IOError: decoder jpeg not available```

Refer [this](http://pillow.readthedocs.org/en/latest/installation.html#external-libraries "Pillow's external libraries") for resolving. Note that pillow has to be reinstalled after installing these dependencies.

```pip install -I pillow```

### Python 2.7

Some latest pip packages like ```reportlab v3.0``` require ```python2.7```. Centos only supports python2.6 currently. It can't be upgraded as yum depends on python.

In case python2.7+ is absolutely required, make use of  ```makealt install``` to install python2.7 alongside python2.6. Refer [this](http://toomuchdata.com/2014/02/16/how-to-install-python-on-centos/) if required.