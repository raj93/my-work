# -*- coding: utf-8 -*-

"""
	testing
	~~~~~~~

	More useful TestCase for tests.

	:copyright: (c) 2012 by Roman Semirook.
	:license: see, BSD LICENSE for more details.
"""

from flask.ext.testing import TestCase
from helpers import AppFactory
from ext import db
from src.portal import ADMIN, COMPANY, AGENT, MERCHANT , MAIN_ROLE,ACCOUNTS_MANAGER,CUSTOMER_CARE,SALES_AGENT
from src.utils.crypto import otp_generator1, otp_generator, hash_passwd
import random, string, hashlib
from datetime import datetime	
from src.models import Admin,AdminEmployee,Company,CompanyEmployee,Agent,AgentEmployee,Merchant,MerchantEmployee,\
			portalLogin,payment
from src.models import posDetails, posUsers


PASSWORD = "password9"
CHANGED_PASSWORD = "changepassword9"
FORGOT_PASSWORD = "forgotpassword9"
SHOULD_CHANGE_PASSWORD = "shouldchange9"


class KitTestCase(TestCase):
	appVersion = '1.0.7'#TODO change it after adding constraints on it
		
	def create_app(self):
		return AppFactory('PROJECT_TEST_SETTINGS').get_app(__name__)

	def assertContains(self, response, text, count=None,
					   status_code=200, msg_prefix=''):
		"""
		Asserts that a response indicates that some content was retrieved
		successfully, (i.e., the HTTP status code was as expected), and that
		``text`` occurs ``count`` times in the content of the response.
		If ``count`` is None, the count doesn't matter - the assertion is true
		if the text occurs at least once in the response.
		"""

		if msg_prefix:
			msg_prefix += ": "

		self.assertEqual(response.status_code, status_code,
			msg_prefix + "Couldn't retrieve content: Response code was %d"
						 " (expected %d)" % (response.status_code, status_code))

		real_count = response.data.count(text)
		if count is not None:
			self.assertEqual(real_count, count,
				msg_prefix + "Found %d instances of '%s' in response"
							 " (expected %d)" % (real_count, text, count))
		else:
			self.assertTrue(real_count != 0,
				msg_prefix + "Couldn't find '%s' in response" % text)


"""
	Merchant,Agent and Company subclasses User
	This function returns the common part i.e, (personalDetails,bankDetails,address)->User payload
"""
def getCommonPayload(user):
	payload = {
	'username': user.username,
	'fullname': user.fullname,
	'mobileNumber': user.mobileNumber,
	'email': user.email,

	'bankName':user.bankName,
	'bankBranch':user.bankBranch,
	'ifsc':user.ifsc,
	'acNumber':user.acNumber,
	'MDR_Debit':user.MDR_Debit,
	'MDR_Credit':user.MDR_Credit,
	'MDR_CreditGold':user.MDR_CreditGold,

	'address1':user.address1,
	'address2':user.address2,
	'city':user.city,
	'district':user.district,
	'state':user.state,
	'pincode':user.pincode,
	'country':user.country
	}
	return payload

def getUserPayload():
	bank = random.choice(['SBH', 'SBI', 'HDFC', 'ICICI', 'YES', 'KOTAK'])
	MDR_DEBIT = random.randint(0, 100)
	MDR_CREDIT = MDR_DEBIT + random.randint(0, 50)
	MDR_CREDIT_GOLD = MDR_CREDIT + random.randint(0, 30)

	payload = {
	'username': otp_generator(random.randint(6,10), string.letters),
	'fullname': otp_generator(random.randint(15,30), string.letters + ' '),
	'mobileNumber': otp_generator(10, string.digits),
	'email': otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com',

	'bankName':bank,
	'bankBranch':otp_generator(random.randint(8,12), string.letters),
	'ifsc': bank + otp_generator(12 - len(bank), string.digits),
	'acNumber':otp_generator(10, string.digits),
	'MDR_Debit':MDR_DEBIT,
	'MDR_Credit':MDR_CREDIT,
	'MDR_CreditGold':MDR_CREDIT_GOLD,

	'address1':'Vindhya C5-' + str(random.randint(100, 300)),
	'address2':random.choice(['CIE', 'NBH', 'OBH']),
	'city':random.choice(['Hyderabad', 'Delhi', 'Mumbai']),
	'district':random.choice(['Hyderabad', 'Delhi', 'Mumbai']),
	'state':random.choice(['stateone', 'statetwo', 'statethree']),
	'pincode':random.randint(500000, 700000),
	'country':'India'
	}
	return payload

def getPaymentPayload():
	payload = {
		'paymentMode':random.choice([0,1]),
		'micr': otp_generator(random.randint(8,12), string.letters),
		'paymentAcNumber': otp_generator(10, string.digits),
		'serial': otp_generator(10, string.digits),
		'amount': random.randint(1,2000)
	}
	return payload

"""
	Generates form payload for OnBoardMerchant functionality
"""
def getOnBoardMerchantPayload(companyName = None,agentName = None):

	# generate payload for a merchant under a company
	payload = getUserPayload()
	payload.update(getPaymentPayload())

	# merchant specific payload
	merchSpecific_paload = {
	'currency' : 'INR',
	'dob':otp_generator(8,string.digits),
	'idProof': otp_generator(8) # TODO
	}

	payload.update(merchSpecific_paload)

	# posData payload
	numPOS = random.randint(1,3)
	posData = ''
	for i in range(numPOS):
		posData += otp_generator(10,string.digits)+';'

	posData_payload = {'posData':posData}

	payload.update(posData_payload)

	if companyName != None:
		hierarchy_payload = None
		if agentName == None:
			# hierarchy payload
			hierarchy_payload = {
			'companies':companyName,
			'agents':'-1'
			}
		else:
			# hierarchy payload
			hierarchy_payload = {
			'companies':companyName,
			'forAgent':'on',
			'agents':agentName
			}

		payload.update(hierarchy_payload)
		
	return payload
		
"""
	Creates and returns a new company.
"""
def getNewCompany(nameOnReceipt = None):
	username = otp_generator(random.randint(6,10), string.letters)
	password = PASSWORD
	companyName = otp_generator(random.randint(15,30), string.letters + ' ')
	mobile = otp_generator(10, string.digits)
	email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'
	bank = random.choice(['SBH', 'SBI', 'HDFC', 'ICICI', 'YES', 'KOTAK'])
	bankBranch = otp_generator(random.randint(8,12), string.letters)
	ifsc = bank + otp_generator(12 - len(bank), string.digits)
	acNumber = otp_generator(10, string.digits)
	MDR_DEBIT = random.randint(0, 100)
	MDR_CREDIT = MDR_DEBIT + random.randint(0, 50)
	MDR_CREDIT_GOLD = MDR_CREDIT + random.randint(0, 30)
	address1 = 'Vindhya C5-' + str(random.randint(100, 300))
	address2 = random.choice(['CIE', 'NBH', 'OBH'])
	city = random.choice(['Hyderabad', 'Delhi', 'Mumbai'])
	district = city
	state = random.choice(['stateone', 'statetwo', 'statethree'])
	pincode = random.randint(500000, 700000)
	country = 'India'

	hashedPasswd, salt = hash_passwd(hashlib.sha256(PASSWORD).hexdigest())

	company1 = Company(username, companyName, datetime.now(), mobile, email, bank, bankBranch, ifsc, acNumber,\
		MDR_DEBIT/100.0, MDR_CREDIT/100.0, MDR_CREDIT_GOLD/100.0, address1, address2, city, district, state, \
		pincode,country, nameOnReceipt)
	company_login = portalLogin(username, hashedPasswd, salt, COMPANY, 0, True)#TODO test case with portalLogin isActivated = False
	return company1, company_login

"""
	Creates and returns an agent under company(companyName)
"""
def getNewAgent(companyName,nameOnReceipt = None):

	username = otp_generator(random.randint(6,10), string.letters)
	password = PASSWORD
	agentName = otp_generator(random.randint(15,30), string.letters + ' ')
	mobile = otp_generator(10, string.digits)
	email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'
	bank = random.choice(['SBH', 'SBI', 'HDFC', 'ICICI', 'YES', 'KOTAK'])
	bankBranch = otp_generator(random.randint(8,12), string.letters)
	ifsc = bank + otp_generator(12 - len(bank), string.digits)
	acNumber = otp_generator(10, string.digits)
	MDR_DEBIT = random.randint(0, 100)
	MDR_CREDIT = MDR_DEBIT + random.randint(0, 50)
	MDR_CREDIT_GOLD = MDR_CREDIT + random.randint(0, 30)
	address1 = 'Vindhya C5-' + str(random.randint(100, 300))
	address2 = random.choice(['CIE', 'NBH', 'OBH'])
	city = random.choice(['Hyderabad', 'Delhi', 'Mumbai'])
	district = city
	state = random.choice(['stateone', 'statetwo', 'statethree'])
	pincode = random.randint(500000, 700000)
	country = 'India'

	hashedPasswd, salt = hash_passwd(hashlib.sha256(PASSWORD).hexdigest())

	agent = Agent(username, agentName, datetime.now(), mobile, email, bank, bankBranch, ifsc, acNumber,\
		MDR_DEBIT/100.0, MDR_CREDIT/100.0, MDR_CREDIT_GOLD/100.0, address1, address2, city, district, state, \
		pincode,country, companyName, nameOnReceipt)
	agent_login = portalLogin(username, hashedPasswd, salt, AGENT, 0, True)#TODO test case with portalLogin isActivated = False
	return agent,agent_login

"""
	Creates and returns a new merchant
	* For args:(company,None,None), creates under a company.
	* For args: (company,agent,None), creates under an agent.
"""
def getNewMerchant(company, agent = None, nameOnReceipt = None):

	username = otp_generator(random.randint(6,10), string.letters)
	password = PASSWORD
	merchantCompany = otp_generator(random.randint(15,30), string.letters + ' ')
	mobile = otp_generator(10, string.digits)
	email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'
	bank = random.choice(['SBH', 'SBI', 'HDFC', 'ICICI', 'YES', 'KOTAK'])
	bankBranch = otp_generator(random.randint(8,12), string.letters)
	ifsc = bank + otp_generator(12 - len(bank), string.digits)
	acNumber = otp_generator(10, string.digits)
	MDR_DEBIT = random.randint(0, 100)#ignoring the cap of company's MDR rates as such cases are possible in real-life scenarios
	MDR_CREDIT = MDR_DEBIT + random.randint(0, 50)
	MDR_CREDIT_GOLD = MDR_CREDIT + random.randint(0, 30)
	dob = str(random.randint(1, 30)) + str(random.randint(1,12)) + str(random.randint(1970, 2000))
	filename = otp_generator(random.randint(8, 12), string.letters)
	address1 = 'Vindhya C5-' + str(random.randint(100, 300))
	address2 = random.choice(['CIE', 'NBH', 'OBH'])
	city = random.choice(['Hyderabad', 'Delhi', 'Mumbai'])
	district = city
	state = random.choice(['stateone', 'statetwo', 'statethree'])
	pincode = random.randint(500000, 700000)
	country = 'India'
	currency = 'INR'

	hashedPasswd, salt = hash_passwd(hashlib.sha256(password).hexdigest())

	merchant1 = Merchant(username, merchantCompany, datetime.now(), mobile, email, bank, bankBranch, ifsc, acNumber,\
		MDR_DEBIT/100.0, MDR_CREDIT/100.0, MDR_CREDIT_GOLD/100.0, dob, filename, address1, address2, city, district, state, \
		pincode, country,currency,company, agent, nameOnReceipt)
	merchant_login = portalLogin(username, hashedPasswd, salt, MERCHANT, 0, True)#TODO test case with portalLogin isActivated = False

	# for testing we are making the merchant verified and mid alloted
	merchant1.bitmap = Merchant.BITMAP['MERCHANT_VERIFICATION'] + Merchant.BITMAP['MERCHANT_MID_ALLOTED']

	return merchant1, merchant_login

"""
	creates and returns a new admin
"""
def createNewAdmin():
	username = otp_generator(random.randint(6,10), string.letters)
	password = PASSWORD
	fullname = otp_generator(random.randint(6,14), string.letters)
	registrationTime = datetime.now()
	mobileNumber = otp_generator(10, string.digits)
	email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'

	hashedPasswd, salt = hash_passwd(hashlib.sha256(PASSWORD).hexdigest())

	entity = Admin(username,fullname,registrationTime,mobileNumber,email)
	entity_login = portalLogin(username, hashedPasswd, salt,ADMIN,0, True)

	entity_login.save()
	entity.save()

	return entity,entity_login

"""
	Creates and returns a new Company.
"""

def createNewCompany(nameOnReceipt = None):
	company,company_login = getNewCompany()
	company_login.save()
	company.save()

	return company,company_login

"""
	Creates and returns a new Agent.
	If company is provided it creates under it.
	Other wise a company is created to fulfill the requirement.
"""
def createNewAgent(companyName = None,nameOnReceipt = None):
	if companyName == None:
		company,company_login = createNewCompany()
		agent,agent_login = getNewAgent(company.username)
	else:
		agent,agent_login = getNewAgent(companyName)

	agent_login.save()
	agent.save()

	return agent,agent_login

"""
	Creates a merchant whose parent is company.
	If company is provided the merchant is created under him.
	Other wise a company is created to fulfill the requirement.
"""
def createNewCompanyMerchant(companyName = None):
	if companyName == None:
		company,company_login = createNewCompany()
		merchant,merchant_login = getNewMerchant(company.username)
	else:
		merchant,merchant_login = getNewMerchant(companyName)

	merchant_login.save()
	merchant.save()

	return merchant,merchant_login

"""
	Creates a merchant whose parent is agent.
	If agent is provided the merchant is created under him.
	Other wise an agent is created to fulfill the requirement.
"""
def createNewAgentMerchant(agentName=None):
	if agentName == None:
		agent,agent_login = createNewAgent()
		merchant,merchant_login = getNewMerchant(agent.company,agent.username)
	else:
		entry = db.session.query(Agent).filter_by(username = agentName).first()
		if entry is None:
			print 'error in testing code'
			pass
		merchant,merchant_login = getNewMerchant(entry.company,entry.username)


	merchant_login.save()
	merchant.save()

	return merchant,merchant_login

def getNewPayment(merchant):
	'''
	Creates a new payment for given merchant. It is used during creation of new POS.
	'''
	micr = otp_generator(random.randint(4,9), string.letters)
	acNumber = otp_generator(random.randint(8,15), string.digits)
	serial = otp_generator(random.randint(4, 6), string.digits)
	amount = random.randint(1, 1000)
	numPOS = random.randint(1, 3)
	# all pos devices will be under 'merchant'
	p1 = payment(merchant.username,0, micr, datetime.now(), acNumber, serial, amount, numPOS)
	p1.isVerified = True
	p1.save() # we need id for posDetails.
	return p1

def getNewPOS(merchant, paymentEntry = None):
	'''
	Creates and returns new terminal for the given merchant.
	Default processor = DEMO
	Params:
	merchant -- Merchant instance
	Returns: details, payment
	'''

	deviceId = ''
	dongleSerial = otp_generator(4, string.letters) + otp_generator(6, string.digits)
	
	model = random.choice(['samsung', 'htc', 'lg', 'nexus'])
	mobile = otp_generator(10, string.digits)
	if paymentEntry is None:
		paymentEntry = getNewPayment(merchant)

	details = posDetails(merchant.username, merchant.agent, merchant.company, paymentEntry.id, mobile, model, 'INR')

	# for testing purposes we are verifying,alloting tid,verifying payment
	details.bitmap = (1 << len(posDetails.PRE_ACTIVATION_BITMAP)) - 1
	#details.bitmap += posDetails.BITMAP['SKIP_OTP_VERIFICATION']
	details.save()
	details.clientId = details.id
	details.update()
	return details, paymentEntry

"""
	When we get clientId's from an external source.
	In this case PLUTUS we have to allocate them to our terminal

"""
def allocatePOS(posList):
	'''
	Params:
	posList -- List of posDetails instances(Maximum of 4)
	'''
	plutusClients = [#[78328, '{9F312C87-A5EF-4155-9593-84A7BE135CB0}', 1],#Ignoring 78332 clientId for testing, 78328 responding with Request XML 
					[78329, '{2025941C-F577-4330-BC13-2CB56BA5B10A}', 1],
					[78330, '{91CB180D-F1AB-4F82-A69D-6D8D9CF50220}', 1],
					[78331, '{5ED6E1ED-2421-4077-87E8-30A18D5D59B8}', 1]]
	a = len(posList)
	for i in range(a):
		pos = posList[i]
		pos.plutusClientId = plutusClients[i][0]
		pos.plutusSecretKey = plutusClients[i][1]

"""
Given the terminal's clientId it creates and returns a new posUser 
for that terminal
"""
def getNewPOSUser(clientId):
	'''
	Params:
	pos -- POS clientId
	@returns posUser, password
	'''
	details = db.session.query(posDetails).filter_by(clientId = clientId).first();
	username = details.mobileNumber #default Pos User
	password = PASSWORD#This password is set automatically during activation. Don't use random passwd here
	hashedPasswd, salt = hash_passwd(hashlib.sha256(password).hexdigest())
	newUser = posUsers(username, hashedPasswd, salt, clientId)
	return newUser, password

"""
	creates a user with (baseRole,subRole) ordered pair.
"""
def createEntryForRole(baseRole,subRole,companyMerchant = True):

	# These are there for inserting in to employee
	username = otp_generator(6)

	# this protocol should be so because while doing a requesting we need to send
	# sha256 of password which cannot be recovered from hashedPasswd
	password = PASSWORD

	hashedPasswd, salt = hash_passwd(hashlib.sha256(password).hexdigest())

	fullname = otp_generator(10)
	mobile = otp_generator(10, string.digits)
	email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'
	
	# createing entity details entry.
	if baseRole == ADMIN:
		if subRole == MAIN_ROLE:
			return createNewAdmin()
		else:
			admin,admin_login = createNewAdmin()

			# portal login and details entries
			entity_login = portalLogin(username, hashedPasswd, salt , baseRole, subRole, True)
			entity = AdminEmployee(username,fullname, datetime.now(),mobile,email,admin.username)

			# save the entries
			entity_login.save()
			entity.save()
			return entity,entity_login

	if baseRole == COMPANY:
		if subRole == MAIN_ROLE:
			return createNewCompany()
		else:
			company,company_login = createNewCompany()

			# portal login and details entry
			entity_login = portalLogin(username, hashedPasswd, salt , baseRole, subRole, True)
			entity = CompanyEmployee(username,fullname, datetime.now(),mobile,email,company.username)

			# save the entries
			entity_login.save()
			entity.save()
			return entity,entity_login

	if baseRole == AGENT:
		if subRole == MAIN_ROLE:
			return createNewAgent()
		else:
			agent,agent_login = createNewAgent()

			# portal login and details entry
			entity_login = portalLogin(username, hashedPasswd, salt , baseRole, subRole, True)
			entity = AgentEmployee(username,fullname, datetime.now(),mobile,email,agent.username)

			# save the entries
			entity_login.save()
			entity.save()
			return entity,entity_login

	if baseRole == MERCHANT:
		if subRole == MAIN_ROLE:
			if companyMerchant:
				return createNewCompanyMerchant()
			else:
				return createNewAgentMerchant()
		else:
			if companyMerchant:
				merchant,merchant_login = createNewCompanyMerchant()
			else:
				merchant,merchant_login = createNewAgentMerchant()

			# portal login and details entry
			entity_login = portalLogin(username, hashedPasswd, salt , baseRole, subRole, True)
			entity = MerchantEmployee(username,fullname, datetime.now(),mobile,email,merchant.username)

			# save the entries
			entity_login.save()
			entity.save()
			return entity,entity_login

"""
	Deletes the trace of that username in db.
"""

def cleanUpEntryOfRole(username,l_entry=None):
	
	if l_entry != None:
		login_entry = l_entry
	else:
		login_entry = db.session.query(portalLogin).filter_by(username = username).first()

	baseRole = login_entry.role
	subRole = login_entry.subRole

	if baseRole == ADMIN:
		if subRole == MAIN_ROLE:
			entity = db.session.query(Admin).filter_by(username = username).first()
			entity_login = entity.loginDetails

			entity.delete() #dlt admin details
			entity_login.delete() #dlt admin login

		else:
			entity = db.session.query(AdminEmployee).filter_by(username = username).first()
			entity_login = entity.loginDetails

			parent = entity.parentDetails
			parent_login = parent.loginDetails

			entity.delete() #dlt empl details
			entity_login.delete() #dlt empl login

			parent.delete() #dlt parent details
			parent_login.delete() #dlt parent login

	elif baseRole == COMPANY:
		if subRole == MAIN_ROLE:
			entity = db.session.query(Company).filter_by(username = username).first()
			entity_login = entity.loginDetails

			entity.delete() #dlt cmpny details
			entity_login.delete() #dlt cmpny login
		
		else:
			entity = db.session.query(CompanyEmployee).filter_by(username = username).first()
			entity_login = entity.loginDetails

			parent = entity.parentDetails
			parent_login = parent.loginDetails

			entity.delete() #dlt empl details
			entity_login.delete() #dlt empl login

			parent.delete() #dlt parent details
			parent_login.delete() #dlt parent login

	elif baseRole == AGENT:
		if subRole == MAIN_ROLE:
			entity = db.session.query(Agent).filter_by(username = username).first()
			entity_login = entity.loginDetails

			company = entity.companyDetails
			company_login = company.loginDetails

			entity.delete() #dlt agent details
			entity_login.delete() #dlt agent login		

			company.delete() #dlt cmpny details
			company_login.delete() #dlt cmpny login

		else:
			entity = db.session.query(AgentEmployee).filter_by(username = username).first()
			entity_login = entity.loginDetails

			agent = entity.parentDetails
			agent_login = agent.loginDetails

			company = agent.companyDetails
			company_login = company.loginDetails

			entity.delete() #dlt empl details
			entity_login.delete() #dlt empl login

			agent.delete() #dlt agent details
			agent_login.delete() #dlt agent login

			company.delete() #dlt company details
			company_login.delete() #dlt company login

	elif baseRole == MERCHANT:
		if subRole == MAIN_ROLE:
			entity = db.session.query(Merchant).filter_by(username = username).first()
			entity_login = entity.loginDetails

			if entity.agent != None:
				agent = db.session.query(Agent).filter_by(username = entity.agent).first()
				agent_login = agent.loginDetails

			company = entity.companyDetails
			company_login = company.loginDetails

			#dlt merchant
			entity.delete()
			entity_login.delete()

			#dlt agent
			if entity.agent != None:
				agent.delete()
				agent_login.delete()

			# remove company
			company.delete()
			company_login.delete()

		else:

			entity = db.session.query(MerchantEmployee).filter_by(username = username).first()
			entity_login = entity.loginDetails

			merchant = entity.parentDetails
			merchant_login = merchant.loginDetails

			if merchant.agent != None:
				agent = db.session.query(Agent).filter_by(username = merchant.agent).first()
				agent_login = agent.loginDetails

			company = merchant.companyDetails
			company_login = company.loginDetails

			#dlt merchant employee
			entity.delete()
			entity_login.delete()

			#dlt merchant
			merchant.delete()
			merchant_login.delete()

			#dlt agent
			if merchant.agent != None:
				agent.delete()
				agent_login.delete()

			# remove company
			company.delete()
			company_login.delete()

def getDeviceId():
	return otp_generator(16, string.hexdigits)
	
def getNewPassword():
	'''Returns a string with atleast 1 digit and 1 letter'''
	s= random.choice(string.digits) + random.choice(string.letters) + otp_generator1(8)
	return ''.join(random.sample(s,len(s)))

def getUserName():
	chars = string.lowercase + string.digits + '_'
	return random.choice(string.lowercase + string.digits) + \
		''.join(random.choice(chars) for x in range(random.randint(5,19)))

def getTransID():
	return ''.join(random.choice(string.hexdigits+'_-:') for x in range(random.randint(8,30)))