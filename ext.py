"""
    Good place for pluggable extensions.

"""

from flask.ext.login import LoginManager
from flask.ext.sqlalchemy import SQLAlchemy
from itsdangerous import TimestampSigner
from flask.ext.principal import Principal
from flask_wtf.csrf import CsrfProtect


db = SQLAlchemy()
db1 = SQLAlchemy()
loginManager = LoginManager()
principals = Principal(skip_static=True)
csrf = CsrfProtect()

# Almost any modern Flask extension has special init_app()
# method for deferred app binding. But there are a couple of
# popular extensions that no nothing about such use case.

#toolbar = lambda app: DebugToolbarExtension(app)  # has no init_app()
signer = lambda app: TimestampSigner(app.config['ITS_DANGEROUS']) #has no init_app()
# Note write all other extensions which has no init_app() above it. So that self.app.signer will be equal to this object(@ helpers.py:_bind_extensions())