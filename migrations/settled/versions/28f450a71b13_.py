"""
Changes column size for cardNumber in purchase. It will be used to store tokens

Revision ID: 28f450a71b13
Revises: 227413e272e4
Create Date: 2014-05-29 20:44:07.448617

"""

# revision identifiers, used by Alembic.
revision = '28f450a71b13'
down_revision = '227413e272e4'

from alembic import op
import sqlalchemy as sa
from sqlalchemy import String


def upgrade():
	op.alter_column('settledPurchase','cardNumber', existing_type=String(22), type_= String(512))


def downgrade():
   op.alter_column('settledPurchase','cardNumber', existing_type=String(512), type_= String(22))
    #Data might be lost