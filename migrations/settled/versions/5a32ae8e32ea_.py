"""
* Add ttype column to Transaction table
* Add a state cashback to bitmap
* Adds cashbackAmount field to Transaction (as amount2) . So that it can be used for pre-auth transactions also.
* Remove purchase state from bitmap


Revision ID: 5a32ae8e32ea
Revises: 901fcb7860e
Create Date: 2014-07-01 13:17:19.400673

"""

# revision identifiers, used by Alembic.
revision = '5a32ae8e32ea'
down_revision = '901fcb7860e'

from alembic import op
import sqlalchemy as sa
from flask import current_app
from ext import db

engine = db.get_engine(current_app, bind='settled')

PURCHASE_STATUS = {
		'ENABLED' : 1 << 0,
		'APPROVED' : 1 << 1,
		'ONLINE' : 1 << 2,#Signifies if transaction is online or offline
		#Transaction type bits
		'PURCHASE' : 1 << 8,
		'VOID' : 1 << 9,
		'TIP' : 1 << 10,
		'SETTLED' : 1 << 11
	}

TTYPE = {
		'INVALID': 0,
		'PURCHASE': 1,
		'REFUND': 2,
		'PREAUTH': 3,
		}

def upgrade():
	op.create_table('capture',
	sa.Column('id', sa.Integer(), nullable=False),
	sa.Column('username', sa.String(length=16), nullable=True),
	sa.Column('clientId', sa.Integer(), nullable=True),
	sa.Column('sequenceCode', sa.Integer(), nullable=True),
	sa.Column('plutusSequenceCode', sa.Integer(), nullable=True),
	sa.Column('authId', sa.Integer(), nullable=True),
	sa.Column('transId', sa.String(length=40), nullable=True),
	sa.Column('transTime', sa.TIMESTAMP(), nullable=True),
	sa.Column('invoice', sa.String(length=12), nullable=True),
	sa.Column('apprCode', sa.String(length=12), nullable=True),
	sa.Column('rrn', sa.String(length=12), nullable=True),
	sa.Column('receiptId', sa.String(length=20), nullable=True),
	sa.Column('receiptMobile', sa.String(length=13), nullable=True),
	sa.Column('receiptEmail', sa.String(length=30), nullable=True),
	sa.Column('status', sa.Integer(), nullable=True),
	sa.ForeignKeyConstraint(['authId'], ['settledTransaction.unSettledTransactionId'], ),
	sa.PrimaryKeyConstraint('id')
	)
	op.add_column(u'settledTransaction', sa.Column('amount2', sa.Integer(), nullable=False, server_default=sa.text('0')))
	op.add_column(u'settledTransaction', sa.Column('ttype', sa.Integer(), nullable=False, server_default=sa.text('0')))
	op.alter_column(u'settledTransaction', 'status',
			   existing_type=sa.INTEGER(),
			   nullable=False)

	with engine.begin() as conn:
		conn.execute('update settledTransaction set ttype = ' + str(TTYPE['PURCHASE']) + \
						' where status & ' + str(PURCHASE_STATUS['PURCHASE']) + ' = ' + str(PURCHASE_STATUS['PURCHASE']))
		conn.execute('update settledTransaction set status = status & ~' + str(PURCHASE_STATUS['PURCHASE']))


def downgrade():

	with engine.begin() as conn:
		conn.execute('update settledTransaction set status = status | ' + str(PURCHASE_STATUS['PURCHASE']) + \
						' where ttype = ' + str(TTYPE['PURCHASE']))

	op.alter_column(u'settledTransaction', 'status',
			   existing_type=sa.INTEGER(),
			   nullable=True)
	op.drop_column(u'settledTransaction', 'ttype')
	op.drop_column(u'settledTransaction', 'amount2')
	op.drop_table('capture')
