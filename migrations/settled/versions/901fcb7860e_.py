"""
* Renames purchase table to transaction
* Changes `unsettledPurchaseId` to `unSettledTransactionId`
Update is done so that next upgrades can easily store refund or pre-auth transactions in this.

Revision ID: 901fcb7860e
Revises: 28f450a71b13
Create Date: 2014-07-01 09:30:45.299281

"""

# revision identifiers, used by Alembic.
revision = '901fcb7860e'
down_revision = '28f450a71b13'
from flask import current_app
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql
from sqlalchemy import Integer
from ext import db

#Worst hack ever. Flask Migration and alembic sucks with multiple dbs
engine = db.get_engine(current_app, bind='settled')

def upgrade():
	op.rename_table('settledPurchase', 'settledTransaction')

	#To handle foreign key constraints
	op.drop_constraint('settledTip_ibfk_1', 'settledTip', type_='foreignkey')
	op.drop_constraint('settledVoid_ibfk_1', 'settledVoid', type_='foreignkey')

	op.alter_column('settledTransaction', 'unSettledPurchaseId', existing_type=Integer, new_column_name='unSettledTransactionId')

	op.create_index('ix_settledTransaction_clientId', 'settledTransaction', ['clientId'], unique=False)
	op.create_index('ix_settledTransaction_plutusSequenceCode', 'settledTransaction', ['plutusSequenceCode'], unique=False)
	op.create_index('ix_settledTransaction_sequenceCode', 'settledTransaction', ['sequenceCode'], unique=False)
	op.create_index('ix_settledTransaction_unSettledTransactionId', 'settledTransaction', ['unSettledTransactionId'], unique=False)
	op.create_index('ix_settledTransaction_username', 'settledTransaction', ['username'], unique=False)
	op.drop_index('ix_settledPurchase_clientId', table_name='settledTransaction')
	op.drop_index('ix_settledPurchase_plutusSequenceCode', table_name='settledTransaction')
	op.drop_index('ix_settledPurchase_sequenceCode', table_name='settledTransaction')
	op.drop_index('ix_settledPurchase_unSettledPurchaseId', table_name='settledTransaction')
	op.drop_index('ix_settledPurchase_username', table_name='settledTransaction')

	with engine.begin() as conn:
		conn.execute('alter table settledVoid add foreign key(ppurchaseId) references settledTransaction(unSettledTransactionId)')
		conn.execute('alter table settledTip add foreign key(ppurchaseId) references settledTransaction(unSettledTransactionId)')

def downgrade():
	op.drop_constraint('settledTip_ibfk_1', 'settledTip', type_='foreignkey')
	op.drop_constraint('settledVoid_ibfk_1', 'settledVoid', type_='foreignkey')

	op.create_index('ix_settledPurchase_username', 'settledTransaction', ['username'], unique=False)
	op.create_index('ix_settledPurchase_unSettledPurchaseId', 'settledTransaction', ['unSettledTransactionId'], unique=False)
	op.create_index('ix_settledPurchase_sequenceCode', 'settledTransaction', ['sequenceCode'], unique=False)
	op.create_index('ix_settledPurchase_plutusSequenceCode', 'settledTransaction', ['plutusSequenceCode'], unique=False)
	op.create_index('ix_settledPurchase_clientId', 'settledTransaction', ['clientId'], unique=False)
	op.drop_index('ix_settledTransaction_username', table_name='settledTransaction')
	op.drop_index('ix_settledTransaction_unSettledTransactionId', table_name='settledTransaction')
	op.drop_index('ix_settledTransaction_sequenceCode', table_name='settledTransaction')
	op.drop_index('ix_settledTransaction_plutusSequenceCode', table_name='settledTransaction')
	op.drop_index('ix_settledTransaction_clientId', table_name='settledTransaction')

	op.rename_table('settledTransaction', 'settledPurchase')

	op.alter_column('settledPurchase', 'unSettledTransactionId', existing_type=Integer, new_column_name='unSettledPurchaseId')

	with engine.begin() as conn:
		conn.execute('alter table settledVoid add foreign key(ppurchaseId) references settledPurchase(unSettledPurchaseId)')
		conn.execute('alter table settledTip add foreign key(ppurchaseId) references settledPurchase(unSettledPurchaseId)')
