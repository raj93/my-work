"""
Marking default value of `amount` and `tipAmount` in `transaction` table to 0
NOTE: Keep it the same in downgrade as well!

Revision ID: 84e9bd02a0
Revises: 988d761d81e
Create Date: 2014-07-07 16:27:07.848499

"""

# revision identifiers, used by Alembic.
revision = '84e9bd02a0'
down_revision = '988d761d81e'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
    op.alter_column('transaction', 'amount',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=False, server_default=sa.text('0'))
    op.alter_column('transaction', 'tipAmount',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=False, server_default=sa.text('0'))


def downgrade():
    op.alter_column('transaction', 'tipAmount',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=True, server_default=sa.text('0'))
    op.alter_column('transaction', 'amount',
               existing_type=mysql.INTEGER(display_width=11),
               nullable=True, server_default=sa.text('0'))
