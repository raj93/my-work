"""
Changes column size for cardNumber in purchase. It will be used to store tokens

Revision ID: 48a6c5504d6e
Revises: 1abfa4ba7df0
Create Date: 2014-05-29 20:34:17.582187

"""

# revision identifiers, used by Alembic.
revision = '48a6c5504d6e'
down_revision = '1abfa4ba7df0'

from alembic import op
import sqlalchemy as sa
from sqlalchemy import String

def upgrade():
	op.alter_column('purchase','cardNumber', existing_type=String(22), type_= String(512))


def downgrade():
    op.alter_column('purchase','cardNumber', existing_type=String(512), type_= String(22))
    #Data might be lost
   