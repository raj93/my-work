"""Adds `numPreAuths` and `preAuthAmount` columns to `settle` to store `preAuth` summary during settlement

Revision ID: 988d761d81e
Revises: 59c4847fae8b
Create Date: 2014-07-02 19:58:19.761553

"""

# revision identifiers, used by Alembic.
revision = '988d761d81e'
down_revision = '59c4847fae8b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('settle', sa.Column('numPreAuths', sa.Integer(), nullable=True))
    op.add_column('settle', sa.Column('preAuthAmount', sa.Integer(), nullable=True))


def downgrade():
    op.drop_column('settle', 'preAuthAmount')
    op.drop_column('settle', 'numPreAuths')
