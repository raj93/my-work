""" Merges posStatus into posDetails
* Adds misc columns like country, currency
* Adds columns to posUser to support shouldChangePassword

Revision ID: 429b8c4210b6
Revises: 40b3c29ea444
Create Date: 2014-04-03 14:12:49.433872

"""

# revision identifiers, used by Alembic.
revision = '429b8c4210b6'
down_revision = '40b3c29ea444'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

from ext import db


#This is the bitmap used in merchant table. 
#Saving it here as policy prevents using external objects
MERCHANT_BITMAP = {
		'MERCHANT_VERIFICATION' : 1,
		'MERCHANT_MID_ALLOTED' : 2,
		'MERCHANT_LOCK' : 4,
		'MERCHANT_DISABLE' : 8
	}

PRE_ACTIVATION_BITMAP = ['MERCHANT_VERIFICATION', 'POS_VERIFICATION', \
	'PAYMENT_VERIFICATION', 'MID_ALLOTED', 'TID_ALLOTED']
PRE_ACTIVATION_BITMAP = ['MERCHANT_VERIFICATION', 'POS_VERIFICATION', \
	'PAYMENT_VERIFICATION', 'MID_ALLOTED', 'TID_ALLOTED']
PRE_LOGIN_BITMAP = PRE_ACTIVATION_BITMAP + ['POS_ACTIVATION']
POSITIVE_BITMAP = PRE_LOGIN_BITMAP + ['LOGIN_STATUS']
NEUTRAL_BITMAP = ['SKIP_OTP_VERIFICATION', 'FRAUD']
NEGATIVE_BITMAP = ['MERCHANT_LOCK', 'MERCHANT_DISABLE', 'POS_LOCK', \
	'POS_DISABLE']
TOTAL_BITMAP = POSITIVE_BITMAP + NEUTRAL_BITMAP + NEGATIVE_BITMAP

POS_BITMAP = {}
for i in range(len(TOTAL_BITMAP)):
	POS_BITMAP[TOTAL_BITMAP[i]] = (1 << i)

VALID_PRE_ACTIVATION_VALUE = 0
for bitName in PRE_ACTIVATION_BITMAP:
	VALID_PRE_ACTIVATION_VALUE |= POS_BITMAP[bitName]

def upgradeData():
	'''
		Upgrades db data as per new structure
	'''
	db.session.execute('update company set country="India"')
	db.session.execute('update agent  set country="India"')
	db.session.execute('update merchant set country="India", currency="INR", '
		'bitmap=' + str(MERCHANT_BITMAP['MERCHANT_VERIFICATION'] + \
			MERCHANT_BITMAP['MERCHANT_MID_ALLOTED']))
	# giving posUser should change password functionality
	db.session.execute('update posUsers set rn=1234, shouldChangePassword=False')

	db.session.execute('update posDetails, posStatus set bitmap = ' + 
		str(VALID_PRE_ACTIVATION_VALUE) + ', ' +
		'posDetails.lastActivity = posStatus.lastActivity, '
		'posDetails.loggedInUser = posStatus.isLoggedIn, '
		'posDetails.loginTime = posStatus.loginTime, '
		'posDetails.transactionStatus = posStatus.transactionStatus, '
		'posDetails.transactionType = posStatus.transactionType'
		' where posDetails.clientId = posStatus.clientId')	
	# get the data inscribed in posStatus to posDetails
	
	db.session.execute('update posDetails set bitmap = bitmap | ' + \
		str(POS_BITMAP['SKIP_OTP_VERIFICATION']) + ' where isDemo = True')
	db.session.execute('update posDetails set transactionStatus = '
		'transactionStatus & 3 | 4 where inSettleConflict = True')
	db.session.execute('update posDetails, posStatus set posDetails.bitmap = posDetails.bitmap | ' + \
		str(POS_BITMAP['POS_ACTIVATION']) + ' where posStatus.clientId = posDetails.clientId and '
		'posStatus.isActivated = True')

	db.session.commit()	# for changes to take place.

	# dropping after using the table
	op.drop_table('posStatus')
	op.drop_column('posDetails', 'isDemo')
	op.drop_column('posDetails', 'inSettleConflict')
	
	db.session.commit()

def downgradeData():
	db.session.execute('insert into posStatus (merchant, clientId, transactionStatus, '
		'transactionType, appVersion, lastActivity, loginTime, isLoggedIn) select '
		'merchant, clientId, transactionStatus & 3, transactionType, "0.0", lastActivity, loginTime, '
		'loggedInUser from posDetails')

	db.session.execute('update posDetails set isDemo = bitmap & ' + \
		str(POS_BITMAP['SKIP_OTP_VERIFICATION']) + ' != 0')
	
	# transfering activation status
	db.session.execute('update posStatus, posDetails set posStatus.isActivated = posDetails.bitmap'
	 '& ' + str(POS_BITMAP['POS_ACTIVATION']) + ' != 0 where posStatus.clientId = posDetails.clientId')

	# setting inSettleConflict        
	db.session.execute('update posDetails set inSettleConflict = transactionStatus & 4 != 0')
	db.session.execute('update posDetails set bitmap = 0')
	db.session.commit()

def upgrade():
	
	# updating to new schema
	op.add_column('agent', sa.Column('country', sa.String(length=20), nullable=False))
	op.add_column('company', sa.Column('country', sa.String(length=20), nullable=False))
	op.add_column('merchant', sa.Column('bitmap', sa.Integer(), nullable=True))
	op.add_column('merchant', sa.Column('country', sa.String(length=20), nullable=False))
	op.add_column('merchant', sa.Column('currency', sa.String(length=5), nullable=False))
	op.drop_column('merchant', 'numPos')
	op.drop_column('merchant', 'status')
	op.add_column('posDetails', sa.Column('lastActivity', sa.TIMESTAMP(), nullable=True))
	op.add_column('posDetails', sa.Column('loggedInUser', sa.String(length=16), nullable=True))
	op.add_column('posDetails', sa.Column('loginTime', sa.TIMESTAMP(), nullable=True))
	op.add_column('posDetails', sa.Column('transactionStatus', sa.Integer(), nullable=True))
	op.add_column('posDetails', sa.Column('transactionType', sa.Integer(), nullable=True))
	
	op.alter_column('posDetails', 'mobileModel',
			   existing_type=mysql.VARCHAR(length=25),
			   nullable=True)

	op.add_column('posUsers', sa.Column('rn', sa.String(length=32), nullable=True))
	op.add_column('posUsers', sa.Column('shouldChangePassword', sa.Boolean(), nullable=True))

	upgradeData()

	### end Alembic commands ###


def downgrade():

	### commands auto generated by Alembic - please adjust! ###
	op.drop_column('posUsers', 'shouldChangePassword')
	op.drop_column('posUsers', 'rn')
	
	op.add_column('merchant', sa.Column('status', mysql.INTEGER(display_width=11), nullable=True))
	op.add_column('merchant', sa.Column('numPos', mysql.INTEGER(display_width=11), nullable=True))
	op.drop_column('merchant', 'currency')
	op.drop_column('merchant', 'country')
	op.drop_column('merchant', 'bitmap')
	op.drop_column('company', 'country')
	op.drop_column('agent', 'country')

	op.alter_column('posDetails', 'mobileModel',
			   existing_type=mysql.VARCHAR(length=25),
			   nullable=False)
	op.add_column('posDetails', sa.Column('inSettleConflict', mysql.TINYINT(display_width=1), nullable=True))
	op.add_column('posDetails', sa.Column('isDemo', mysql.TINYINT(display_width=1), nullable=True))

	op.create_table('posStatus',
	sa.Column('id', mysql.INTEGER(display_width=11), nullable=False),
	sa.Column('merchant', mysql.VARCHAR(length=16), nullable=True),
	sa.Column('clientId', mysql.INTEGER(display_width=11), autoincrement=False, nullable=True),
	sa.Column('isLoggedIn', mysql.VARCHAR(length=16), nullable=True),
	sa.Column('lastActivity', mysql.TIMESTAMP(), nullable=True),
	sa.Column('loginTime', mysql.TIMESTAMP(), nullable=True),
	sa.Column('numFailures', mysql.SMALLINT(display_width=6), autoincrement=False, nullable=True),
	sa.Column('transactionStatus', mysql.TINYINT(display_width=1), autoincrement=False, nullable=True),
	sa.Column('transactionType', mysql.INTEGER(display_width=11), autoincrement=False, nullable=True),
	sa.Column('isActivated', mysql.TINYINT(display_width=1), autoincrement=False, nullable=True),
	sa.Column('appVersion', mysql.VARCHAR(length=20), nullable=False),
	sa.ForeignKeyConstraint(['clientId'], [u'posDetails.clientId'], name=u'posStatus_ibfk_2', onupdate=u'CASCADE'),
	sa.ForeignKeyConstraint(['merchant'], [u'merchant.username'], name=u'posStatus_ibfk_1', onupdate=u'CASCADE'),
	sa.PrimaryKeyConstraint('id'),
	mysql_default_charset=u'latin1',
	mysql_engine=u'InnoDB'
	)

	downgradeData()

	# changing after using data
	
	op.drop_column('posDetails', 'transactionType')
	op.drop_column('posDetails', 'transactionStatus')
	op.drop_column('posDetails', 'loginTime')
	op.drop_column('posDetails', 'loggedInUser')
	op.drop_column('posDetails', 'lastActivity')

	db.session.commit()

	### end Alembic commands ###
