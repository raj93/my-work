"""
* Renames purchase table to transaction
Update is done so that next upgrades can easily store refund or pre-auth transactions in this.

Revision ID: 2635e747b5a
Revises: 535b2e68a884
Create Date: 2014-07-01 08:31:34.157822

"""

# revision identifiers, used by Alembic.
revision = '2635e747b5a'
down_revision = 'd3e625c668'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

def upgrade():
	op.rename_table('purchase', 'transaction')

def downgrade():
	op.rename_table('transaction', 'purchase')
