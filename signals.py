from flask.ext.login import current_user
from flask.ext.principal import RoleNeed, UserNeed
from src.portal import ADMIN, COMPANY, AGENT, MERCHANT,\
 ADMIN_ROLE,COMPANY_ROLE,MERCHANT_ROLE,AGENT_ROLE
from src.portal.principalConstants import *


def on_identity_loaded(sender, identity):
	# Set the identity user object
	identity.user = current_user
	# Add the UserNeed to the identity
	if hasattr(current_user, 'id'):
		identity.provides.add(UserNeed(current_user.id))

	# Assuming the User model has a list of roles, update the
	# identity with the roles that the user provides
	if current_user.role == ADMIN:
		identity.provides.add(admin_need)
		if current_user.subRole == 0:
			
			identity.provides.update([\
				add_company_need, edit_company_need,view_company_need, \
				add_agent_need,edit_agent_need,view_agent_need,\
				add_merchant_need,edit_merchant_need,view_merchant_need,search_merchant_need,\
				add_employee_need, edit_employee_need,view_employee_need,\
				add_terminal_need,view_terminal_need,search_terminal_need,\
				add_pos_user_need,view_pos_users_need,delete_pos_user_need,change_pos_user_password_need,lock_pos_user_need,unlock_pos_user_need,\
				add_clientid_need,add_dongle_need,\
				view_transactions_need,\
				get_agents_need,get_merchants_of_company_need,get_merchants_of_agent_need,\
				get_payments_need,verify_payment_need,
				lock_merchant_need,lock_terminal_need,
				verify_merchant_need,verify_pos_need,
				allot_mid_need,allot_tid_need,
				allot_dongle_need,unallot_dongle_need])

		elif current_user.subRole == ADMIN_ROLE['ACCOUNTS_MANAGER']:
			
			identity.provides.update([\
				view_company_need, \
				view_agent_need,\
				add_merchant_need,view_merchant_need,\
				view_employee_need,\
				view_transactions_need,\
				get_payments_need,verify_payment_need,
				add_employee_need])
			
			
		elif current_user.subRole == ADMIN_ROLE['CUSTOMER_CARE']:

			identity.provides.update([\
				view_company_need, \
				view_agent_need,\
				view_terminal_need,\
				search_terminal_need,\
				view_merchant_need,search_merchant_need,\
				view_employee_need])

		else:
			#least one i.e,sales
			identity.provides.update([])
			pass

	elif current_user.role == COMPANY:
		identity.provides.add(RoleNeed('company'))
		if current_user.subRole == 0:

			identity.provides.update([\
				add_agent_need,edit_agent_need,view_agent_need,\
				add_merchant_need,edit_merchant_need,view_merchant_need,search_merchant_need,\
				add_employee_need, edit_employee_need,view_employee_need,\
				add_terminal_need,view_terminal_need,search_terminal_need,\
				add_pos_user_need,view_pos_users_need,delete_pos_user_need,change_pos_user_password_need,\
				view_transactions_need,\
				get_agents_need,get_merchants_of_company_need,get_merchants_of_agent_need,
				get_payments_need,verify_payment_need,
				lock_merchant_need,lock_terminal_need,
				verify_merchant_need,verify_pos_need,
				allot_mid_need,allot_tid_need,
				allot_dongle_need,unallot_dongle_need])
			
		elif current_user.subRole == COMPANY_ROLE['ACCOUNTS_MANAGER']:

			identity.provides.update([\
				view_agent_need,\
				add_merchant_need,view_merchant_need,\
				view_employee_need,\
				view_transactions_need,\
				add_employee_need])
			
		elif current_user.subRole == COMPANY_ROLE['CUSTOMER_CARE']:

			identity.provides.update([\
				view_agent_need,\
				view_merchant_need,search_merchant_need,\
				view_employee_need,search_terminal_need])

		else:
			#permissions
			pass

	elif current_user.role == AGENT:
		identity.provides.add(RoleNeed('agent'))
		if current_user.subRole == 0:

			identity.provides.update([\
				add_merchant_need,edit_merchant_need,view_merchant_need,search_merchant_need,\
				search_terminal_need,\
				add_employee_need, edit_employee_need,view_employee_need,\
				view_transactions_need,\
				get_merchants_of_agent_need])

		elif current_user.subRole == AGENT_ROLE['ACCOUNTS_MANAGER']:

			identity.provides.update([\
				add_merchant_need,view_merchant_need,\
				view_employee_need,\
				view_transactions_need])

		elif current_user.subRole == AGENT_ROLE['CUSTOMER_CARE']:

			identity.provides.update([\
				view_merchant_need,\
				view_employee_need])
		else:
			#permissions
			pass

	elif current_user.role == MERCHANT:
		if current_user.subRole == 0:

			identity.provides.update([\
				add_employee_need, edit_employee_need,view_employee_need,\
				view_terminal_need,search_terminal_need,\
				view_transactions_need,\
				add_pos_user_need,view_pos_users_need,delete_pos_user_need,change_pos_user_password_need])
			
		elif current_user.subRole == MERCHANT_ROLE['ACCOUNTS_MANAGER']:

			identity.provides.update([\
				view_employee_need,\
				view_transactions_need])
		else:
			#permissions
			pass


