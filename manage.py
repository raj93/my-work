#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
	manage
	~~~~~~

	Set of some useful management commands.

"""

import subprocess
from flask.ext.script import Shell, Manager
from app import app
from ext import db
from mobileAPISimulator import mobsim
from flask.ext.migrate import Migrate, MigrateCommand
import os

manager = Manager(app)
if app.config['DEBUG'] or app.config['TESTING']:
	manager.add_command('db', MigrateCommand)

@manager.command
def simulate():
	mobsim.simulate('mobileAPISimulator/preferences.json', False)

	
@manager.command
def clean_pyc():
	"""Removes all *.pyc files from the project folder"""
	clean_command = "find . -name *.pyc -delete".split()
	subprocess.call(clean_command)


@manager.command
def drop_db():
	''' Drops all db models'''

	if not app.config['DEBUG'] and not app.config['TESTING']:
		print 'Error: Trying to meddle with database on production runlevel'
		return
	
	from flask.ext.migrate import downgrade
	downgrade(directory='migrations/transactions', revision='base')
	downgrade(directory='migrations/settled', revision='base')


@manager.command
def upgrade_db():
	'''
		This command should be run when code is updated. It updates db models to latest version if present
	'''
	if not app.config['DEBUG'] and not app.config['TESTING']:
		print 'Error: Trying to meddle with database on production runlevel'
		return
	
	from flask.ext.migrate import upgrade
	upgrade(directory='migrations/transactions')
	upgrade(directory='migrations/settled')


@manager.command
def recreate_db():
	"""
	Drops all tables and creates

	Following code can also be used. Migrations preferred. 
		```
		db.drop_all()
		db.create_all()
		```
	"""
	if not app.config['DEBUG'] and not app.config['TESTING']:
		print 'Error: Trying to meddle with database on production runlevel'
		return

	db.engine.echo = True
	drop_db()
	upgrade_db()


@manager.command
def init_admin():
	''' To be used only for the first time during setup '''

	import hashlib
	from src.utils.crypto import hash_passwd, generateRandomPassword
	from src.models import portalLogin, Admin
	from src.portal import ADMIN
	from datetime import datetime
	from src.utils.ses_email import send1

	password = generateRandomPassword(10)
	hashedPasswd, salt = hash_passwd(hashlib.sha256(password).hexdigest())

	login2 = portalLogin('admin', hashedPasswd, salt, ADMIN, 0, True, True)
	login2.save()
		
	admin1 = Admin(login2.username, 'Teja', datetime.now(), '9908306120', 'pruthvi@aasaanpay.com');
	admin1.save()

	# send password to email
	subject = "Admin credentials with temporary password"
	to = "developers@aasaanpay.com"
		
	body = "admin username and password are "+"\n"+ \
			"username: "+ login2.username + "\n"+ \
			"password: "+ password

	print body		
	print send1(subject, to, body,None,None)
	db.session.commit()

@manager.option('-e', '--echo', default = False)
def init_db(echo):
	"""Fish data for project"""
	
	recreate_db()
	populateDB()
	
	
@manager.command
def populateDB():
	
	if not app.config['DEBUG'] and not app.config['TESTING']:
		print 'Error: Trying to clean db data on production runlevel'
		return
	
	from src.models import portalLogin, Admin, Company, Agent, Merchant, AdminEmployee, CompanyEmployee, MerchantEmployee
	from src.models import posDetails, posUsers, payment, clientIdInventory, dongleInventory
	from src.portal import ADMIN, COMPANY, AGENT, MERCHANT, ADMIN_ROLE, COMPANY_ROLE, MERCHANT_ROLE
	from src.utils.crypto import hash_passwd
	try:
		
		import hashlib
		from datetime import datetime


		hashedPasswd, salt = hash_passwd(hashlib.sha256('develop').hexdigest())
		login1 = portalLogin('admin', hashedPasswd, salt , ADMIN, 0, True);
		login1.save()

		portalLogin('harish', hashedPasswd, salt , ADMIN, 0, True).save();
		portalLogin('harika', hashedPasswd, salt , ADMIN, 0, True).save();
		portalLogin('harsha', hashedPasswd, salt , ADMIN, 0, True).save();
		portalLogin('harsani', hashedPasswd, salt , ADMIN, 0, True).save();
		portalLogin('harshit', hashedPasswd, salt , ADMIN, 0, True).save();
		portalLogin('harendhra', hashedPasswd, salt , ADMIN, 0, True).save();


		
		admin = Admin(login1.username, 'Pruthvi', datetime.now(), '9908306120','pruthvi@aasaanpay.com');
		admin.save()


		#company details
		#AasaanPay
		company1 = portalLogin('aasaanpay', hashedPasswd, salt , COMPANY, 0, True);
		company1.save()
		
		c1 = Company('aasaanpay', 'AasaanPay Solutions Ind. Pvt. Ltd.' , datetime.now(), '9908306120', 'pruthvi@aasaanpay.com', \
			'SBH','IIIT Gachibowli','SBH0102210','010101010101','0.75', '1.25', '1.50', \
			'Vindhya C5-204', 'CIE', 'Hyderabad', 'Hyderabad', 'Andhra Pradesh', '500032','India' 'AasaanPay')
		c1.save()


		#Agent details of AsaanPay
		login5 = portalLogin('a_aasaanpay', hashedPasswd, salt , AGENT, 0, True);
		login5.save()



		a1 = Agent('a_aasaanpay', 'AasaanPay Solutions Ind. Pvt. Ltd.' , datetime.now(), '9908306120', 'pruthvi3@aasaanpay.com', \
			'SBH','IIIT Gachibowli','SBH0102210','010101010101','0.85', '1.35', '1.60', \
			'Vindhya C5-204', 'CIE', 'Hyderabad', 'Hyderabad', 'Andhra Pradesh', '500032','India','aasaanpay')
		a1.save()

		
		
		#merchant is gaurav
		hashedPasswd, salt = hash_passwd(hashlib.sha256('password').hexdigest())
		merchant_login = portalLogin('merchant', hashedPasswd, salt , MERCHANT, 0, True);
		merchant_login.save()

		merchant1 = Merchant(merchant_login.username, 'OnMobilePay Solutions Ind. Pvt. Ltd.' , datetime.now(), '9908306120', 'pruthvi@aasaanpay.com', \
			'SBH','IIIT Gachibowli','SBH0102210','010101010101','0.90', '1.40', '1.60', \
			'21-10-1980', 'hello.pdf','Vindhya C5-204', 'CIE', 'Hyderabad', 'Hyderabad', 'Andhra Pradesh', '500032','India','INR',company1.username,\
			 a1.username)
		merchant1.save()        

		# all pos devices will be under 'merchant'
		p1 = payment(merchant1.username,0,'micr','end of universe','12345678','1233',12,2)
		p1.save()

		pos_de = posDetails(merchant1.username,merchant1.agent,merchant1.company,p1.id,'9030997944','samsung s7252','INR')
		pos_de.save()

		pos_de.clientId = pos_de.id
		pos_de.update()


		# pos user under
		hashedPasswd, salt = hash_passwd(hashlib.sha256('password').hexdigest())
		pos_user = posUsers(pos_de.mobileNumber, hashedPasswd, salt, pos_de.clientId,True);
		pos_user.save()

					
		# sainath's

		merchant1 = db.session.query(Merchant).filter_by(username = "merchant").first()

		p5 = payment(merchant1.username,0,'micr','end of universe','12345678','1233',12,1)
		p5.save()

		pos_de5 = posDetails(merchant1.username,merchant1.agent,merchant1.company,p5.id,'9908306120','motog','INR')
		pos_de5.save()

		pos_de5.clientId = pos_de5.id
		pos_de5.update()


		# sainath's
		hashedPasswd, salt = hash_passwd(hashlib.sha256('password').hexdigest())
		pos_user5 = posUsers(pos_de5.mobileNumber, hashedPasswd, salt, pos_de5.clientId,True);
		pos_user5.save()
		

		# bangladesh's
		mNUmber = "9049269079"
		currency = "BDT"

		p6 = payment(merchant1.username,0,'micr','end of universe','12345678','1233',12,1)
		p6.save()

		pos_de6 = posDetails(merchant1.username,merchant1.agent,merchant1.company,p6.id,mNUmber,'motog',currency)
		pos_de6.save()

		pos_de6.clientId = pos_de6.id
		pos_de6.update()


		# sainath's
		hashedPasswd, salt = hash_passwd(hashlib.sha256('password').hexdigest())
		pos_user6 = posUsers(pos_de6.mobileNumber, hashedPasswd, salt, pos_de6.clientId,True);
		pos_user6.save()
		
		print 'added entries successfully'

		for i in range(10):dongleInventory(str(i)).save()

	except Exception as e:
		print e
		
	
manager.add_command('shell', Shell(make_context=lambda:{'app': app, 'db': db}))


if __name__ == '__main__':
	manager.run()
