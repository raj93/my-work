#!/usr/bin/env python
import simplejson as json
import requests, os, datetime
import hashlib

# from src.transactions.tipWorker import TipWorker
'''

'''
class MobileSimulator:

	base = 'http://54.251.106.159/'

	#mobile parameters
	clientId = 1
	sessionToken = 'user.BQC9GA.wmkZMdch4hneABN-rEENVftPEBQ'
	sequenceCode = 0
	mobileNumber = '9908306120'
	email = 'teja@apay.com'
	deviceId = 'DEVICE10'
	deviceModel = 'Nexus'
	appVersion = '1.0.7'
	timeStamp = 'sdfsdf'
	registered = True
	conflict = False
	otp = ''
	username = 'user'
	passwd = '0d6be69b264717f2dd33652e212b173104b4a647b7c11ae72e9885f11cd312fb'
	transId = ''

	def __init__(self,localHost=True):
		if localHost:
			self.base = 'http://127.0.0.1:5000'

	def populateEntries(self,filename):
		json_data = open(filename)

		data = json.load(json_data)
		self.clientId = data['clientId']
		self.sessionToken = data['sessionToken']
		self.sequenceCode = data['sequenceCode']
		self.mobileNumber = data['mobileNumber']
		self.email = data['email']
		self.deviceId = data['deviceId']
		self.deviceModel = data['deviceModel']
		self.appVersion = data['appVersion']

		self.timeStamp = datetime.datetime.utcnow().strftime("%Y-%m-%d %X")

		self.registered = data['registered']
		self.conflict = data['conflict']
		self.otp = data['otp']
		self.username = data['username']
		self.passwd = data['passwd']
		self.transId = data['transId']

		json_data.close()

	def dumpEntries(self, filename):
		f = open(filename, 'w')
		data = {'clientId': self.clientId, \
		'sessionToken': self.sessionToken, \
		'sequenceCode': self.sequenceCode, \
		'mobileNumber': self.mobileNumber, \
		'email': self.email, \
		'deviceId': self.deviceId, \
		'deviceModel' : self.deviceModel, \
		'appVersion': self.appVersion, \
		'timeStamp': self.timeStamp, \
		'registered': self.registered, \
		'conflict': self.conflict, \
		'otp': self.otp, \
		'username': self.username, \
		'passwd': self.passwd,
		'transId': self.transId}
		f.write(json.dumps(data, indent = 4))
		f.close()
		return

	def doPurchase(self, amount, cbAmount=0):
		authentication = {'sequenceCode': self.sequenceCode, 'clientId': self.clientId, 'sessionToken': self.sessionToken}

		terminalInfo = {'deviceId': self.deviceId, 'timeStamp': self.timeStamp, 'appVersion': self.appVersion}

		#Plain text payload
		#card = {'track1': '%B4242424242424242^gautama buddha    /^14091010010000000772000000?',\
			#'track2': ';5176521005587557=10091010017720?'}

		#payload = {'deviceType': 0, 'conflict': self.conflict, 'card': card, 'amount': amount}

		#Miura swipe payload
		# tlv = 'E181B448020007DFAE0278682DC744CF3078F7720A1531A8741228B866CDC313FBA92E356F0599E054A06E85AEBDC19528058B50CFBB34D67CDC6F394AF89927B115F51BDD06444B2AF105F56960B7806ADBA38321B95B3CB7E03C3D480CE048C8F95B382D7EC819C81437040D9C05A5D653A0F71E058ACECE46AFA77C92944D66C895DFAE030A0000090171808C200071DFAE22223B3534353232362A2A2A2A2A2A323235383D31353130323031303031363339303F3F'
		# card = {'inputType': 1, 'tlv' : tlv}
		# payload = {'deviceType': 1, 'conflict': self.conflict, 'card': card, 'amount': amount}
		#pin = {'block' : 'FDC48216A18049CB', 'ksn' : '00000A0171808C200004'}

		#Miura chip payload
		tlv = 'e4820146500a4d6173746572436172645f201a5341494e415448204755505441204b202020202020202020202f5f24031510315f280203565f2a0203565f300202015f340101820258008407a0000000041010950540000080009a031501219b02e8009c01009f02060000000005009f03060000000000009f0607a00000000410109f090200029f10120110a00000240000000000000000000000ff9f120c484446432042414e4b204d439f1a0208269f1c0830363035333939379f1e0830363035333939379f2608da6f750a1d0502689f2701809f3303e0f8c89f34035e03009f3501229f360202859f3704169425c49f410400000111dfae0220d4d2f513dbb7a91fd6b3a46098e4b54dab1777bc3a53bc27dacecbc9791ed02cdfae030a000001017181b400007ddfae5710545226ffffff2258d15102010016390fdfae5a08545226ffffff2258'
		card = {'inputType': 2, 'tlv': tlv}
		payload = {'deviceType': 1, 'conflict': self.conflict, 'card': card, 'amount': amount}


		#QPOS swipe payload
		'''
		swipeJson = {
			'expiryDate': '1510',
			'chName': 'SAINATH GUPTA K          /',
			'trackKsn': '00000332100300E00027',
			'track1Length': 71,
			'track2Length': 31,
			'encTrack1': '742703AAD5202D6C2187A321BDFE7895EF8DEEBC01C682F4DC65EE6CA1FCB8950D11B57AC215AC859959D37A82D6A96C9016E3C0CC571D1E98520742D8B6286A',
			'encTrack2': 'DF78E9B90304D2EC98E9736A887F7C627522CEF2703A73ED',
			}

		pin = {
			'ksn': '00000332100300E00031',
			'block': 'C5901D061FC36CA2',
			}

		card = {'inputType': 1, 'swipe': swipeJson, 'pin': pin}
		payload = {'deviceType': 3, 'conflict': self.conflict, 'card': card, 'amount': amount}
		'''

		if cbAmount != 0:
			payload['amount2'] = cbAmount

		purchase_data = {'terminalInfo': terminalInfo, 'authentication': authentication, 'payload': payload}

		purchase_url = 	self.base + '/pos/purchase'
		purchase_json = json.dumps(purchase_data)
		headers = {'content-type': 'application/json'}
		r = requests.post(purchase_url, data=purchase_json, headers=headers, verify=False)
		try:
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				self.sequenceCode = response_data['sequenceCode']
				self.transId = response_data['transactionInfo']['transId']
				if response_data.has_key('settleCode'):
					if response_data['settleCode'] != 'd':
						self.conflict = False
			else:
				if response_data.has_key('sequenceCode'):
					self.sequenceCode = response_data['sequenceCode']
		except Exception, e:
			print e

	def doRefund(self, amount):
		authentication = {'sequenceCode': self.sequenceCode, 'clientId': self.clientId, 'sessionToken': self.sessionToken}

		terminalInfo = {'deviceId': self.deviceId, 'timeStamp': self.timeStamp, 'appVersion': self.appVersion}

		card = {'track1': '%B4000056655665556^gautama buddha    /^14091010010000000772000000?',\
				'track2': ';5176521005587557=10091010017720?'}#Stripe Visa(debit)
		#payload = {'eType': 1, 'conflict': self.conflict, 'card': card, 'amount': amount}
		#tlv = 'E181B448020007DFAE0278682DC744CF3078F7720A1531A8741228B866CDC313FBA92E356F0599E054A06E85AEBDC19528058B50CFBB34D67CDC6F394AF89927B115F51BDD06444B2AF105F56960B7806ADBA38321B95B3CB7E03C3D480CE048C8F95B382D7EC819C81437040D9C05A5D653A0F71E058ACECE46AFA77C92944D66C895DFAE030A0000090171808C200071DFAE22223B3534353232362A2A2A2A2A2A323235383D31353130323031303031363339303F3F'
		#card = {'inputType': 0, 'tlv' : tlv}

		#pin = {'block' : 'FDC48216A18049CB', 'ksn' : '00000A0171808C200004'}
		payload = {'deviceType': 0, 'conflict': self.conflict, 'card': card, 'amount': amount}

		purchase_data = {'terminalInfo': terminalInfo, 'authentication': authentication, 'payload': payload}

		purchase_url = 	self.base + '/pos/refund'
		purchase_json = json.dumps(purchase_data)
		headers = {'content-type': 'application/json'}
		r = requests.post(purchase_url, data=purchase_json, headers=headers, verify=False)
		try:
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				self.sequenceCode = response_data['sequenceCode']
				self.transId = response_data['transactionInfo']['transId']
				if response_data.has_key('settleCode'):
					if response_data['settleCode'] != 'd':
						self.conflict = False
			else:
				if response_data.has_key('sequenceCode'):
					self.sequenceCode = response_data['sequenceCode']
		except Exception, e:
			print e

	def doEnquiry(self):
		authentication = {'sequenceCode': self.sequenceCode, 'clientId': self.clientId, 'sessionToken': self.sessionToken}

		terminalInfo = {'deviceId': self.deviceId, 'timeStamp': self.timeStamp, 'appVersion': self.appVersion}

		card = {'track1': '%B4242424242424242^gautama buddha    /^14091010010000000772000000?',\
				'track2': ';5176521005587557=10091010017720?'}

		#payload = {'eType': 1, 'conflict': self.conflict, 'card': card, 'amount': amount}
		#tlv = 'E181B448020007DFAE0278682DC744CF3078F7720A1531A8741228B866CDC313FBA92E356F0599E054A06E85AEBDC19528058B50CFBB34D67CDC6F394AF89927B115F51BDD06444B2AF105F56960B7806ADBA38321B95B3CB7E03C3D480CE048C8F95B382D7EC819C81437040D9C05A5D653A0F71E058ACECE46AFA77C92944D66C895DFAE030A0000090171808C200071DFAE22223B3534353232362A2A2A2A2A2A323235383D31353130323031303031363339303F3F'
		#card = {'inputType': 0, 'tlv' : tlv}

		#pin = {'block' : 'FDC48216A18049CB', 'ksn' : '00000A0171808C200004'}
		payload = {'deviceType': 0, 'conflict': self.conflict, 'card': card}

		purchase_data = {'terminalInfo': terminalInfo, 'authentication': authentication, 'payload': payload}

		purchase_url = 	self.base + '/pos/enquireBalance'
		purchase_json = json.dumps(purchase_data)
		headers = {'content-type': 'application/json'}
		r = requests.post(purchase_url, data=purchase_json, headers=headers, verify=False)
		try:
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				self.sequenceCode = response_data['sequenceCode']
				self.transId = response_data['transactionInfo']['transId']
				if response_data.has_key('settleCode'):
					if response_data['settleCode'] != 'd':
						self.conflict = False
			else:
				if response_data.has_key('sequenceCode'):
					self.sequenceCode = response_data['sequenceCode']
		except Exception, e:
			print e

	def doPreAuth(self, amount):
		authentication = {'sequenceCode': self.sequenceCode, 'clientId': self.clientId, 'sessionToken': self.sessionToken}

		terminalInfo = {'deviceId': self.deviceId, 'timeStamp': self.timeStamp, 'appVersion': self.appVersion}

		card = {'track1': '%B4242424242424242^gautama buddha    /^14091010010000000772000000?',\
				'track2': ';5176521005587557=10091010017720?'}

		#payload = {'eType': 1, 'conflict': self.conflict, 'card': card, 'amount': amount}
		#tlv = 'E181B448020007DFAE0278682DC744CF3078F7720A1531A8741228B866CDC313FBA92E356F0599E054A06E85AEBDC19528058B50CFBB34D67CDC6F394AF89927B115F51BDD06444B2AF105F56960B7806ADBA38321B95B3CB7E03C3D480CE048C8F95B382D7EC819C81437040D9C05A5D653A0F71E058ACECE46AFA77C92944D66C895DFAE030A0000090171808C200071DFAE22223B3534353232362A2A2A2A2A2A323235383D31353130323031303031363339303F3F'
		#card = {'inputType': 0, 'tlv' : tlv}

		#pin = {'block' : 'FDC48216A18049CB', 'ksn' : '00000A0171808C200004'}
		payload = {'deviceType': 0, 'conflict': self.conflict, 'card': card, 'amount': amount}

		preAuthData = {'terminalInfo': terminalInfo, 'authentication': authentication, 'payload': payload}

		preAuth_url = 	self.base + '/pos/preAuth'
		preAuth_json = json.dumps(preAuthData)
		headers = {'content-type': 'application/json'}
		r = requests.post(preAuth_url, data=preAuth_json, headers=headers, verify=False)
		try:
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				self.sequenceCode = response_data['sequenceCode']
				self.transId = response_data['transactionInfo']['transId']
				if response_data.has_key('settleCode'):
					if response_data['settleCode'] != 'd':
						self.conflict = False
			else:
				if response_data.has_key('sequenceCode'):
					self.sequenceCode = response_data['sequenceCode']
		except Exception, e:
			print e

	def doCaptureAuth(self, amount):
		terminalInfo = {'deviceId':self.deviceId,'timeStamp':self.timeStamp,'appVersion':self.appVersion}
		authentication = {'sequenceCode':self.sequenceCode,'clientId':self.clientId,'sessionToken':self.sessionToken}
		payload = {'transId':self.transId, 'amount': amount}
		capture_data = {'terminalInfo':terminalInfo,'authentication':authentication,'payload':payload}

		capture_url = self.base+'/pos/captureAuth'
		capture_json = json.dumps(capture_data)
		headers = {'content-type':'application/json'}
		r = requests.post(capture_url, data=capture_json, headers=headers, verify=False)

		try:
			print r
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				self.sequenceCode = response_data['sequenceCode']
			else:
				#show error code
				pass
		except Exception,e:
			print e

	def uploadSignature(self, type=0):

		terminalInfo = {'deviceId':self.deviceId,'timeStamp':self.timeStamp,'appVersion':self.appVersion}
		payload = {'clientId' : self.clientId, 'sessionToken' : self.sessionToken, 'transId' : self.transId, 'transType': type, 'sms':'9908306120'}
		upload_data = {'terminalInfo':terminalInfo, 'payload':payload}
		f=open('mobileAPISimulator/uploadSignature.json', 'w')
		f.write(json.dumps(upload_data))
		f.close()
		upload_url = self.base + '/pos/signatureUpload'
		files = {'json':open('mobileAPISimulator/uploadSignature.json'), 'signature':open('mobileAPISimulator/a.png')}
		r = requests.post(upload_url, files=files, verify=False)
		try:
			print r.json()
		except Exception as e:
			print e

	def doOTPRequest(self):
		terminalInfo = {'deviceId':self.deviceId,'timeStamp':self.timeStamp,'appVersion':self.appVersion}
		payload = {'mobileNumber':self.mobileNumber,'email':self.email,'registered':self.registered}
		verification_data = {'terminalInfo':terminalInfo,'payload':payload}
		verification_json = json.dumps(verification_data)

		verification_url = 	self.base +'/pos/otprequest'
		headers = {'content-type': 'application/json'}
		r = requests.post(verification_url, data=verification_json, headers=headers, verify=False)
		try:
			response_data = r.json()
			if not response_data.has_key('errorCode'):
				self.otp = response_data['mesg']
				print response_data
			else:
				print response_data

		except Exception as e:
			print e

	def doActivation(self):
		terminalInfo = {'deviceId':self.deviceId,'timeStamp':self.timeStamp,'appVersion':self.appVersion, 'deviceModel' : self.deviceModel}
		payload = {'mobileNumber':self.mobileNumber,'otp':self.otp,'registered':self.registered}
		activation_data = {'terminalInfo':terminalInfo,'payload':payload}
		activation_json = json.dumps(activation_data)

		activation_url = self.base+'/pos/activation'
		headers = {'content-type': 'application/json'}
		r = requests.post(activation_url, data=activation_json, headers=headers, verify=False)

		try:
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				self.sequenceCode = response_data['sequenceCode']
				self.clientId = response_data['clientId']
		except Exception, e:
			print e

	def doLogin(self):
		terminalInfo = {'deviceId':self.deviceId,'timeStamp':self.timeStamp,'appVersion':self.appVersion}
		payload = {'clientId':self.clientId,'username':self.username,'passwd':self.passwd}
		login_data = {'terminalInfo':terminalInfo,'payload':payload}
		login_json = json.dumps(login_data)

		login_url = self.base+'/pos/login'
		headers = {'content-type': 'application/json'}
		r = requests.post(login_url, data=login_json, headers=headers, verify=False)

		try:
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				self.sessionToken = response_data['sessionKey']
			else:
				#show error code
				pass
		except Exception, e:
			print e

	def doLogout(self):

		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : self.timeStamp}
		payload = {'clientId' : self.clientId, 'sessionToken' : self.sessionToken}

		logout_data = {"terminalInfo" : terminalInfo, "payload" : payload}

		url = self.base + '/pos/logout'
		headers = {'content-type': 'application/json'}

		r = requests.post(url, data=json.dumps(logout_data), headers=headers, verify=False)

		try:
			print r.json()
		except Exception, e:
			print r
			print e

	def changePasswd(self,newPasswd):

		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : self.timeStamp}
		payload = {'clientId' : self.clientId, 'sessionToken' : self.sessionToken, 'oldPasswd' : self.passwd, 'newPasswd' : newPasswd}

		request = {"terminalInfo" : terminalInfo, "payload" : payload}

		url = self.base + '/pos/changePasswd'
		headers = {'content-type': 'application/json'}
		r = requests.post(url, data=json.dumps(request), headers=headers, verify=False)

		try:
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				self.passwd = hashlib.sha256(newPasswd).hexdigest()

		except Exception, e:
			print e

	def shouldChangePasswd(self,newPasswd):
		#client, deviceId, username, clientId, sessionToken, oldPasswd, newPasswd):

		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : self.timeStamp}
		payload = {'clientId' : self.clientId, 'username' : self.username, 'sessionToken' : self.sessionToken, 'oldPasswd' : self.passwd, 'newPasswd' : newPasswd}

		request = {"terminalInfo" : terminalInfo, "payload" : payload}
		print json.dumps(request)

		url = self.base + '/pos/shouldChangePasswd'
		headers = {'content-type': 'application/json'}
		r = requests.post(url, data=json.dumps(request), headers=headers, verify=False)

		try:
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				self.passwd = hashlib.sha256(newPasswd).hexdigest()
				self.sessionToken = response_data['sessionKey']
			else:
				#show error code
				pass
		except Exception, e:
			print e

	def doVoid(self):
		terminalInfo = {'deviceId':self.deviceId,'timeStamp':self.timeStamp,'appVersion':self.appVersion}
		authentication = {'sequenceCode':self.sequenceCode,'clientId':self.clientId,'sessionToken':self.sessionToken}
		payload = {'transId':self.transId}
		void_data = {'terminalInfo':terminalInfo,'authentication':authentication,'payload':payload}

		void_url = self.base+'/pos/void'
		void_json = json.dumps(void_data)
		headers = {'content-type':'application/json'}
		r = requests.post(void_url, data=void_json, headers=headers, verify=False)

		try:
			print r
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				self.sequenceCode = response_data['sequenceCode']
			else:
				#show error code
				pass
		except Exception,e:
			print e

	def doSettlementReport(self):
		authentication = {'sequenceCode':self.sequenceCode,'clientId':self.clientId,'sessionToken':self.sessionToken}
		terminalInfo = {'deviceId':self.deviceId,'timeStamp':self.timeStamp,'appVersion':self.appVersion}
		settlementReport_data = {'terminalInfo':terminalInfo,'authentication':authentication,'conflict':self.conflict}

		settlementReport_url = self.base+'/pos/settlement_report'
		settlementReport_json = json.dumps(settlementReport_data)
		headers = {'content-type':'application/json'}
		r = requests.post(settlementReport_url, data=settlementReport_json, headers=headers,
			verify=False)

		try:
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				self.sequenceCode = response_data['sequenceCode']
				if response_data.has_key('settleCode'):
					if response_data['settleCode'] != 'd':
						self.conflict = False
			else:
				self.sequenceCode = response_data['sequenceCode']
				pass
		except Exception, e:
			print e,'settlement report failed'

	def doSettlement(self):
		authentication = {'sequenceCode':self.sequenceCode,'clientId':self.clientId,'sessionToken':self.sessionToken}
		terminalInfo = {'deviceId':self.deviceId,'timeStamp':self.timeStamp,'appVersion':self.appVersion}
		settle_data = {'terminalInfo':terminalInfo,'authentication':authentication}

		settle_url = self.base+'/pos/settle'
		settle_json = json.dumps(settle_data)
		headers = {'content-type':'application/json'}

		r = requests.post(settle_url, data=settle_json, headers=headers, verify=False)

		try:
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				self.sequenceCode = response_data['sequenceCode']
			else:
				#show that settlement has failed
				self.sequenceCode = response_data['sequenceCode']
				if response_data['errorCode'] == 'SETTLEMENT_CONFLICT':
					self.conflict = True

				#conflict = true when exception occured in plutus request
		except Exception, e:
			print e
	"""
	def doPendingTip(self, amount):
		terminalInfo = {'deviceId': self.deviceId, 'timeStamp': self.timeStamp, 'appVersion': self.appVersion}
		authentication = {'clientId': self.clientId,'sessionToken': self.sessionToken}
		payload = {'transId': self.transId, 'tipAmount': amount, 'sequenceCode' : self.sequenceCode}
		pendingTip_data = {'terminalInfo': terminalInfo, 'authentication': authentication, 'payload': payload}

		pendingTip_url = self.base + '/pos/pendingtip'
		pendingTip_json = json.dumps(pendingTip_data)
		headers = {'content-type': 'application/json'}
		r = requests.post(pendingTip_url, data = pendingTip_json, headers = headers)

		try:
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				print 'pending tip successful'
			else:
				pass
				#don't mark it as successful
			#there won't be any sequence code modifications
		except Exception, e:
			print e

	def processPendingTips(self):
		t1 = TipWorker(self.clientId)
		t1.start()
		print 'waiting from TipWorker thread to join'
		t1.join()
	"""
	def doVoidSearch(self):
		terminalInfo = {'deviceId':self.deviceId, 'timeStamp':self.timeStamp, 'appVersion':self.appVersion}
		authentication = {'sequenceCode':self.sequenceCode, 'clientId':self.clientId,'sessionToken':self.sessionToken}
		void_search_data = {'terminalInfo':terminalInfo, 'authentication':authentication, 'conflict':self.conflict}
		void_search_url = self.base + '/pos/history'
		void_search_json = json.dumps(void_search_data)

		headers = {'content-type':'application/json'}
		r = requests.post(void_search_url, data=void_search_json, headers=headers, verify=False)

		try:
			response_data = r.json()
			print response_data
			if not response_data.has_key('errorCode'):
				if response_data.has_key('settleCode'):
					if response_data['settleCode'] != 'd':
						self.conflict = False
			else:
				#show error
				pass
		except Exception,e:
			print e

	def doHelp(self):
		terminalInfo = {'deviceId':self.deviceId,'timeStamp':self.timeStamp}
		payload = {'clientId':self.clientId, 'query':'this is a query from mobile end', 'contact': self.mobileNumber}
		help_data = {'terminalInfo':terminalInfo,'payload':payload}

		help_url = self.base+'/pos/help'
		help_json = json.dumps(help_data)

		headers = {'content-type':'application/json'}
		r = requests.post(help_url, data=help_json, headers=headers,verify=False)
		try:
			response_data = r.json()
			print response_data
		except Exception,e:
			print e


def simulate(prefPath = 'preferences.json', localHost = True):
	""" Simulates like a mobile"""
	ms = MobileSimulator(localHost)

	ms.populateEntries(os.path.join(os.getcwd(), prefPath))

	#ms.doOTPRequest()
	#ms.doActivation()
	#ms.passwd = hashlib.sha256("password").hexdigest()
	#ms.doLogin()
	#ms.shouldChangePasswd("test1234")
	#ms.doLogout()

	#ms.doLogin()
	#ms.doPurchase('100')
	#ms.doVoid()

	#ms.doPurchase('2000')
	#ms.doPurchase('2000')
	#ms.doPendingTip('100')
	#ms.processPendingTips()
	#ms.sequenceCode -= 1
	#ms.doVoidSearch()
	#ms.doVoid()
	#ms.doPurchase('1300')
	#ms.doSettlementReport()
	#ms.doSettlement()
	# ms.conflict = True
	# ms.sequenceCode -= 1
	# ms.doSettlementReport()
	#ms.doSettlement()
	#ms.doLogout()

	ms.dumpEntries(os.path.join(os.getcwd(), prefPath))

if __name__ == "__main__":
	ms = MobileSimulator(True)

	ms.populateEntries(os.path.join(os.getcwd(), "preferences.json"))

	#ms.doOTPRequest()
	#ms.doActivation()
#	ms.passwd = hashlib.sha256("test7890").hexdigest()
	ms.doLogin()
	ms.doPurchase(1000)
#	ms.doRefund(1110)
#	ms.doVoid()
	#ms.doLogout()
	# ms.doLogin()
	#ms.doPreAuth(10000)
	#ms.doCaptureAuth(5000)


	# import hashlib
	#ms.passwd = hashlib.sha256('test1234').hexdigest()
	#ms.doLogin()
	#ms.doPurchase('1300')
#	ms.doPurchase('1000')
	# ms.doVoid()
	#ms.doSettlementReport()
	#ms.doEnquiry()
	#ms.doSettlement()
	#ms.doSettlement()
	# ms.passwd1 = 'test1234'
	# ms.changePasswd()
	# ms.passwd1 = 'test1235'
	# ms.changePasswd()
	# ms.passwd = hashlib.sha256(ms.passwd1).hexdigest()
	# ms.doLogin()
	# ms.doHelp()
	#ms.doLogout()
	# ms.doHelp()

	ms.dumpEntries(os.path.join(os.getcwd(), "preferences.json"))

