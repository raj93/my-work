#Requests should reach every part of the code, then we can be confident that it works

#Resolving settlement conflicts at server
"""
write the following lines of code after the plutus request in settle request
#simulating server conflict (settle is success at plutus)
details_entry.inSettleConflict = True
output = {'errorCode':'server busy'}
return output

"""
#Resolving server conflict by purchase
	ms.doPurchase('1504')
	ms.doVoid()
	ms.doPurchase('1403')
	ms.doPendingTip('1504')
	ms.processPendingTips()
	ms.doSettlementReport()
	ms.doSettlement()
	ms.doPurchase('1345')
#Resolving server conflict by void search
	ms.doPurchase('1504')
	ms.doVoid()
	ms.doPurchase('1403')
	ms.doPendingTip('1504')
	ms.processPendingTips()
	ms.doSettlementReport()
	ms.doSettlement()
	ms.doVoidSearch()
#Resolving server conflict by settlement report
	ms.doPurchase('1504')
	ms.doVoid()
	ms.doPurchase('1403')
	ms.doPendingTip('1504')
	ms.processPendingTips()
	ms.doSettlementReport()
	ms.doSettlement()
	ms.doSettlementReport()


#cancelling last transaction works
#last transactions are cancelled from purchase,void,settle-report
#transactions that can be rolled back are purchase,void,[tip-supported but not used]
	#cancelling a purchase
	ms.doPurchase('2000')
	ms.sequenceCode -= 1
	ms.doPurchase('1300')
	ms.doSettlementReport()
	ms.doSettlement()

	#cancelling a void
	ms.doPurchase('2000')
	ms.doPurchase('1300')
	ms.doVoid()
	ms.sequenceCode -= 1
	ms.doPurchase('3000')
	ms.doSettlementReport()
	ms.doSettlement()

#Resolving settlement conficts at mobile
#Resolving mobile conflict by purchase
	ms.doPurchase('1234')
	ms.doPurchase('2234')
	ms.doSettlementReport()
	ms.doSettlement()
	ms.sequenceCode -= 1
	ms.conflict = True
	ms.doPurchase('1567')

#Resolving conflict by void search
	ms.doPurchase('1456')
	ms.doSettlementReport()
	ms.doSettlement()
	ms.sequenceCode -= 1
	ms.conflict = True
	ms.doVoidSearch()

#Resolving conflict by settlement report
	ms.doPurchase('2456')
	ms.doSettlementReport()
	ms.doSettlement()
	ms.sequenceCode -= 1
	ms.conflict = True
	ms.doSettlementReport()

