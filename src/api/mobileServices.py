from flask import Blueprint
from src.mAuthenticate import activation, login, logout, changePasswd, upload_signature, otprequest, help , shouldChangePasswd
from ext import csrf

mServices = Blueprint('mServices', __name__, url_prefix = '/pos')

#Contains authentication related APIs. 
#Registering them under different blueprint for future flexibility in prefix URLs ec..
@csrf.exempt	
@mServices.route('/login', methods=['POST'])
def mLogin():
	obj = login.Login()
	return obj.process()

@csrf.exempt
@mServices.route('/logout', methods=['POST'])
def mLogout():
	obj = logout.Logout()
	return obj.process()

@csrf.exempt
@mServices.route('/changePasswd', methods=['POST'])
def changePassword():
	obj = changePasswd.ChangePasswd()
	return obj.process()

@csrf.exempt
@mServices.route('/shouldChangePasswd', methods=['POST'])
def shouldChangePassword():
	obj = shouldChangePasswd.ShouldChangePasswd()
	return obj.process()

@csrf.exempt
@mServices.route('/help', methods=['POST'])
def mHelp():
	obj = help.Help()
	return obj.process()

@csrf.exempt
@mServices.route('/signatureUpload', methods=['POST'])
def signature():
	obj = upload_signature.Upload()
	return obj.process()

@csrf.exempt
@mServices.route('/otprequest',methods=['POST'])
def otpRequest():
	obj = otprequest.OtpRequest()
	return obj.process()

@csrf.exempt
@mServices.route('/activation',methods=['POST'])
def activationProcess():
	obj = activation.ActivatePOS()
	return obj.process()
