from flask import Blueprint
from src.transactions import purchase, settleReport, settleRequest, tip, void, transactionHistory, pendingTip, \
    refund, enquiryBalance, preAuth, captureAuth
from ext import csrf

transactions = Blueprint('transactions', __name__, url_prefix = '/pos')

@csrf.exempt
@transactions.route('/purchase', methods=['POST'])
def Purchase():
    obj = purchase.Purchase()
    return obj.process()

@csrf.exempt
@transactions.route('/void',methods=['POST'])
def Void():
    obj = void.Void()
    return obj.process()

#@csrf.exempt
#@transactions.route('/tip',methods=['POST'])
def tipHandler():
    obj = tip.Tip()
    return obj.process()

'''Batch report gives detailed report of the batch. It doesn't settle the batch. Just shows the history of transactions in the open batch.'''
#@csrf.exempt
#@transactions.route('/batch_report', methods=['POST'])
def batch_report():
    obj = batchReport.Batch()
    return obj.process()

@transactions.route('/refund', methods=['POST'])
@csrf.exempt
def Refund():
    obj = refund.Refund()
    return obj.process()

#@transactions.route('/query_previous')
#@csrf.exempt
def query_previous():
    pass

@csrf.exempt
@transactions.route('/settlement_report', methods=['POST', 'GET'])
def settlement_report():
    obj = settleReport.SettleReport()
    return obj.process()

@csrf.exempt
@transactions.route('/settle', methods = ['GET', 'POST'])
def settlement_request():
    obj = settleRequest.SettleRequest()
    return obj.process()

@csrf.exempt
@transactions.route('/pendingtip', methods = ['GET', 'POST'])
def pending_tip():
    obj = pendingTip.PendingTip()
    return obj.process()

@csrf.exempt
@transactions.route('/history', methods=['POST'])
def transaction_history():
    obj = transactionHistory.TransactionHistory()
    return obj.process()

@transactions.route('/preAuth', methods=['POST'])
@csrf.exempt
def pre_auth():
    obj = preAuth.PreAuth()
    return obj.process()

@transactions.route('/captureAuth', methods=['POST'])
@csrf.exempt
def capture_auth():
    obj = captureAuth.CaptureAuth()
    return obj.process()

@csrf.exempt
@transactions.route('/enquireBalance', methods = ['GET', 'POST'])
def balance_enquiry():
    obj = enquiryBalance.Enquire()
    return obj.process()