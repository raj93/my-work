from flask import Blueprint
from flask.ext.login import login_required

transaction = Blueprint('transactionPortal', __name__, url_prefix='/transaction')

from src.portal import transactionPortal

@transaction.route('/viewtransactions')
@login_required
def viewTransactions():
	return transactionPortal.viewTransactions()


@transaction.route('/transactions/')
@login_required
def show_transactions():
	return transactionPortal.show_transactions()
