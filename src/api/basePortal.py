from flask import Blueprint, render_template , make_response, request, session
from flask.ext.login import current_user
from flask.ext.login import login_required

common = Blueprint('basePortal', __name__, url_prefix='/base')

from src.portal import basePortal, authenticate, companyPortal, agentPortal, merchantPortal, posPortal, employeePortal, posUserPortal

###### testing new thing

@common.route('/deleteMerchant/<merchant>', methods = ['GET'])
@login_required
def deleteMerchant(merchant):
	return merchantPortal.deleteMerchant(merchant)

@common.route('/allotMid/<merchant>', methods = ['POST'])
@login_required
def allotMid(merchant):
	return merchantPortal.allotMid(merchant)

@common.route('/allotTid/<terminal>', methods = ['POST'])
@login_required
def allotTid(terminal):
	return posPortal.allotTid(terminal)

@common.route('/moveToSettled', methods = ['GET'])
@login_required
def moveToSettled():
	return basePortal.moveToSettled()
	
@common.route('/addSupport', methods = ['GET', 'POST'])
@login_required
def addSupport():
	return basePortal.addSupport()

@common.route('/paymentVerification', methods = ['GET'])
@login_required
def paymentVerification():
	return basePortal.paymentVerification()

@common.route('/getPayments', methods = ['POST'])
@login_required
def getPayments():
	return basePortal.getPayments()

@common.route('/verifyPayment', methods = ['GET','POST'])
@login_required
def verifyPayment():
	return basePortal.verifyPayment()
	
# add/view/edit company
@common.route('/addCompany', methods = ['GET','POST'])
@login_required
def addCompany():
	""" Add a company Under Aasaanapay"""
	return companyPortal.addCompany()
	
@common.route('/editCompany/<company>', methods = ['GET', 'POST'])
@login_required
def editCompany(company):
	return companyPortal.editCompany(company)

@common.route('/viewCompany/<name>')
@login_required
def viewCompany(name):
	return companyPortal.viewCompany(name)


# add/edit/view agent
@common.route('/addAgent', methods = ['GET', 'POST'])
@login_required
def addAgent():
	return agentPortal.addAgent()

@common.route('/editAgent/<agent>',methods=['GET','POST'])
@login_required
def editAgent(agent):
	return agentPortal.editAgent(agent)

@common.route('/viewAgent/<name>')
@login_required
def viewAgent(name):
	return agentPortal.viewAgent(name)


@common.route('/verifyMerchant',methods=['POST'])
@login_required
def verifyMerchant():
	return merchantPortal.verifyMerchant()


@common.route('/merchantVerification',methods=['GET','POST'])
@login_required
def merchantVerification():
	return merchantPortal.merchantVerification()

# add/edit/view merchant
@common.route('/addMerchant',methods=['GET','POST'])
@login_required
def addMerchant():
	return merchantPortal.addMerchant()

@common.route('/editMerchant/<merchant>',methods=['GET','POST'])
@login_required
def editMerchant(merchant):
	return merchantPortal.editMerchant(merchant)

@common.route('/viewMerchant/<name>')
@login_required
def viewMerchant(name):
	return merchantPortal.viewMerchant(name)

@common.route('/changeMerchantState/<merchant>')
@login_required
def changeMerchantState(merchant):
	
	return merchantPortal.changeMerchantState(merchant)

# add merchant with pos
@common.route('/addMerchantWithPos',methods=['GET','POST'])
@login_required
def addMerchantWithPos():
	return merchantPortal.addMerchantWithPos()


# add/edit/view employee
@common.route('/addEmployee',methods=['GET','POST'])
@login_required
def addEmployee():
	return employeePortal.addEmployee()

@common.route('/editEmployee/<employee>',methods=['GET','POST'])
@login_required
def editEmployee(employee):
	return employeePortal.editEmployee(employee)
	
@common.route('/viewEmployee/<name>',methods=['GET','POST'])
@login_required
def viewEmployee(name):
	return employeePortal.viewEmployee(name)

# experimental
# This is how a user can add terminal to his children 
@common.route('/addEmp',methods=['GET','POST'])
@login_required
def addEmployeeByHierarchy():
	return employeePortal.addEmpByHierarchy()


@common.route('/POSVerification',methods=['GET','POST'])
@login_required
def POSVerification():
	return posPortal.POSVerification()

@common.route('/verifyPOS',methods=['GET','POST'])
@login_required
def verifyPOS():
	return posPortal.verifyPOS()


# add/edit/view pos

@common.route('/addPOS',methods=['GET','POST'])
@login_required
def addPOS():
	return posPortal.addPOS()


@common.route('/viewPOS/<clientId>',methods=['GET','POST'])
@login_required
def viewPOS(clientId):
	return posPortal.viewPOS(clientId)

@common.route('/changeTerminalState/<merchant>/<clientId>',methods=['GET','POST'])
@login_required
def changeTerminalState(merchant,clientId):
	return posPortal.changeTerminalState(merchant,clientId)


@common.route('/changeTerminalBitState/<clientId>/<bitName>',methods=['GET','POST'])
@login_required
def changeTerminalBitState(clientId,bitName):
	return posPortal.changeTerminalBitState(clientId,bitName)

# add/delete/view pos User

@common.route('/addPosUser',methods =['GET','POST'])
@login_required
def addPosUser():
	return posUserPortal.addPosUser()

@common.route('/deletePosUser',methods =['GET'])
@login_required
def deletePosUser():
	return posUserPortal.deletePosUser()

@common.route('/viewPosUsers')
@login_required
def viewPosUsers():
	return posUserPortal.viewPosUsers()


@common.route('/changePosUserPassword',methods=['GET','POST'])
@login_required
def changePosUserPassword():
	return posUserPortal.changePosUserPassword()

@common.route('/lockPosUser',methods=['GET'])
@login_required
def lockPosUser():
	return posUserPortal.lockPosUser()

@common.route('/unlockPosUser',methods=['GET'])
@login_required
def unlockPosUser():
	return posUserPortal.unlockPosUser()


@common.route('/hierarchyDemo',methods=['GET','POST'])
@login_required
def hierarchyDemo():
	return posPortal.hierarchyDemo()


# view users
@common.route('/viewCompanies')
@login_required
def viewCompanies():
	return companyPortal.viewCompanies()

@common.route('/viewAgents')
@login_required
def viewAgents():
	return agentPortal.viewAgents()

@common.route('/viewMerchants')
@login_required
@login_required
def viewMerchants():
	return merchantPortal.viewMerchants()

@common.route('/viewEmployees')
@login_required
def viewEmployees():
	return employeePortal.viewEmployees()


@common.route('/viewTerminals')
@login_required
def viewTerminals():
	return posPortal.viewTerminals()


# common functionalities for all users
@common.route('/login', methods = ['GET', 'POST'])
def login(): 
	return authenticate.login()
	
@common.route('/logout', methods = ['GET', 'POST'])
@login_required
def logout():
	return authenticate.logout()

@common.route('/changePassword', methods = ['GET', 'POST'])
@login_required
def changePassword():
	return authenticate.changePassword()

@common.route('/viewProfile', methods = ['GET'])
@login_required
def viewProfile():
	return authenticate.viewProfile()

@common.route('/edit-profile', methods = ['GET', 'POST'])
@login_required
def editProfile():
	return authenticate.editProfile()


@common.route('/shouldChangePassword', methods = ['GET', 'POST'])
def shouldChangePassword():
	return authenticate.shouldChangePassword()

@common.route('/forgotPassword', methods = ['GET', 'POST'])
def forgotPassword():
	return authenticate.forgotPassword()

@common.route('/reset',methods = ['GET', 'POST'])
def resetPassword():
	return authenticate.resetPassword()


# helper methods and ajax calls

# given a company it returns a list of agents directly under him.
@common.route('/getAgents', methods = ['GET', 'POST'])
@login_required
def getAgents():
	return agentPortal.getAgents()

# given an agent it returns list of merchants directly under him
@common.route('/getAgentMerchants', methods = ['GET', 'POST'])
@login_required
def getAgentMerchants():
	return merchantPortal.getAgentMerchants()

# given a company it returns list of merchants directly under him
@common.route('/getCompanyMerchants', methods = ['GET', 'POST'])
@login_required
def getCompanyMerchants():
	return merchantPortal.getCompanyMerchants()

# given a pair of (company,agent) it returns the merchants under that pair.
@common.route('/getMerchants', methods = ['GET', 'POST'])
@login_required
def getMerchants():
	return merchantPortal.getMerchants()
 

@common.route('/searchMerchants', methods = ['GET', 'POST'])
@login_required
def searchMerchants():
	return merchantPortal.searchMerchants()

@common.route('/searchTerminals', methods = ['GET', 'POST'])
@login_required
def searchTerminals():
	return posPortal.searchTerminals()

