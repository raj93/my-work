from flask import Blueprint, redirect, url_for
from flask import current_app
from flask.helpers import send_from_directory
import os
from ext import loginManager
from src.models import portalLogin


base = Blueprint('base', __name__, url_prefix='/')


@loginManager.user_loader
def load_user(id):
	return portalLogin.query.get(id) 


@base.route('/')
def root():
	return redirect(url_for('basePortal.login'))


@base.route('/favicon.ico')
def favicon():
	return send_from_directory(os.path.join(current_app.config['basedir'], 'static'),
							   'favicon.ico', mimetype='image/vnd.microsoft.icon')
