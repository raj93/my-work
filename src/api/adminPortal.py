from flask import Blueprint
from src.portal import adminPortal
from flask.ext.login import login_required
admin = Blueprint('adminPortal', __name__, url_prefix='/admin')

@admin.route('/addClientId',methods=['GET','POST'])
@login_required
def addClientId():
	return adminPortal.addClientId()	

@admin.route('/addDongle',methods=['GET','POST'])
@login_required
def addDongle():
	return adminPortal.addDongle()


@admin.route('/unallotDongle',methods=['POST'])
def unallotDongle():
	return adminPortal.unallotDongle()


# returns the dongle allocation page.
@admin.route('/allotDongle/<merchant>',methods=['POST'])
def allotDongle(merchant):
	return adminPortal.allotDongle(merchant)
