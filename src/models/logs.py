from ext import db
from src.models.base import CRUDMixin
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, TIMESTAMP
from sqlalchemy.orm import relationship


class posLogs(CRUDMixin,db.Model):
	__tablename__ = "posLogs"

	merchant = Column(String(16), ForeignKey('merchant.username', ondelete='cascade'))
	agent = Column(String(16), ForeignKey('agent.username', ondelete='cascade'), nullable = True)
	company = Column(String(16), ForeignKey('company.username', ondelete='cascade'), nullable = False)

	username = Column(String(16),nullable = False)
	clientId = Column(Integer, ForeignKey('posDetails.clientId', ondelete='cascade'),nullable = False)

	time = Column(TIMESTAMP, nullable = False)
	event = Column(String(100),default = "")

	def __init__(self,username,clientId,time,event,merchant,agent,company):
		self.username = username
		self.clientId = clientId
		self.time = time
		self.event = event
		self.merchant = merchant
		self.agent = agent
		self.company = company

	def __repr__(self):
		return "user %s under pos %d has performed action %s at time %s" \
		% (self.username, self.clientId, self.event, self.time)

class portalUserLogs(CRUDMixin,db.Model):
	__tablename__ = "portalUserLogs"

	username = Column(String(16), ForeignKey('portalLogin.username',ondelete='cascade'))
	time = Column(TIMESTAMP, nullable = False)
	event = Column(String(100),default = "")
	onActionUser = Column(String(16), ForeignKey('portalLogin.username',ondelete='cascade'))

	role = Column(Integer)
	subRole = Column(Integer)

	portalLogin = relationship("portalLogin", backref="userLogs", foreign_keys=[username])

	def __init__(self,username,role,subRole,onActionUser,event,time):
		self.username = username
		self.role = role
		self.subRole = subRole

		self.onActionUser = onActionUser

		self.event = event
		self.time = time
		
	def __repr__(self):
		return "portal user %s has performed action %s at time %s on user %s" \
		% (self.username, self.event, self.time, self.onActionUser)
