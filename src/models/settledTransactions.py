from ext import db1
from sqlalchemy import Column, Integer, String, SmallInteger, Boolean, ForeignKey, and_
from sqlalchemy.types import TIMESTAMP
from sqlalchemy.orm import relationship
from src.models import CRUDMixin

class SettledTransaction(CRUDMixin, db1.Model):
	__tablename__ = 'settledTransaction'
	__bind_key__ = 'settled'

	INPUT_TYPE = {
		'MANUAL' : 0,
		'SWIPE' : 1,
		'CHIP' : 2,
		}

	TTYPE = {
		'INVALID': 0,
		'PURCHASE': 1,
		'REFUND': 2,
		'PREAUTH': 3,
		'ENQUIRY': 4,
		}

	STATUS = {
		'ENABLED' : 1 << 0,
		'APPROVED' : 1 << 1,
		'ONLINE' : 1 << 2,#Signifies if transaction is online or offline
		#Transaction type bits
		'CASHBACK' : 1 << 8,
		'VOID' : 1 << 9,
		'TIP' : 1 << 10,
		'SETTLED' : 1 << 11,
		'CAPTURE': 1 << 12,

		}

	VALID_TXN = STATUS['ENABLED'] + STATUS['APPROVED'] + STATUS['SETTLED']
	VOIDED = VALID_TXN  + STATUS['VOID']
	TIPPED = VALID_TXN  + STATUS['TIP']
	VOIDED_TIP = VALID_TXN + STATUS['VOID'] + STATUS['TIP']

	APPROVED_ONLINE = STATUS['ENABLED'] + STATUS['APPROVED'] + STATUS['ONLINE'] + STATUS['SETTLED']
	APPROVED_OFFLINE = STATUS['ENABLED'] + STATUS['APPROVED'] + (0 * STATUS['ONLINE']) + STATUS['SETTLED']
	DECLINED_ONLINE = STATUS['ENABLED'] + (0 * STATUS['APPROVED']) + STATUS['ONLINE'] + STATUS['SETTLED']
	DECLINED_OFFLINE = STATUS['ENABLED'] + (0 * STATUS['APPROVED']) + (0 * STATUS['ONLINE']) + STATUS['SETTLED']

	@classmethod
	def conditions(cls, condition):
		'''
			Helper module
			#TODO Explore adding an additional argument ttype since there would 4 extra conditions for each ttype
			@param condition(string) -> One of the conditions defined in `CONDITIONS`
			@returns sqlalchemy filter condition
		'''

		if condition is 'APPROVED':#TODO replaced this with 'VALID' as it is more appropriate. remove if unused.
			return cls.status.op('&')(cls.STATUS['APPROVED']) == cls.STATUS['APPROVED']

		if condition is 'VALID':
			return cls.status.op('&')(cls.VALID_TXN) == cls.VALID_TXN

		if condition is 'SIMPLE_PURCHASE':
			return and_(cls.ttype == cls.TTYPE['PURCHASE'], \
							cls.status.op('&')(~(cls.STATUS['ONLINE'] | cls.STATUS['CASHBACK'])) == cls.VALID_TXN)

		if condition is 'VOIDED_PURCHASE':
			return and_(cls.ttype == cls.TTYPE['PURCHASE'], \
							cls.status.op('&')(~(cls.STATUS['ONLINE'] | cls.STATUS['CASHBACK'])) == cls.VOIDED)

		if condition is 'TIPPED_PURCHASE':
			return and_(cls.ttype == cls.TTYPE['PURCHASE'], \
							cls.status.op('&')(~(cls.STATUS['ONLINE'] | cls.STATUS['CASHBACK'])) == cls.TIPPED)

		if condition is 'VOIDED_TIP':#Tip is only for Purchases ==> VOIDED_TIP_PURCHASE
			return and_(cls.ttype == cls.TTYPE['PURCHASE'], \
							cls.status.op('&')(~(cls.STATUS['ONLINE'] | cls.STATUS['CASHBACK'])) == cls.VOIDED_TIP)

		raise keyError('No condition with given name exists')

	def getTransactionType(self):
		if self.ttype == self.TTYPE['PURCHASE']:
			if self.isVoidedTipTxn():
				return 'VOID(SALE WITH TIP)'

			if self.isTipped():
				return 'SALE WITH TIP'

			if self.isVoided():
				return 'VOID(SALE)'

			return 'SALE'

		elif self.ttype == self.TTYPE['REFUND']:
			if self.isVoided():
				return 'VOID(REFUND)'

			return 'REFUND'

		elif self.ttype == self.TTYPE['PREAUTH']:
			if self.isCaptured():
				return 'CAPTURED(PREAUTH)'

			return 'PREAUTH'

		elif self.ttype == self.TTYPE['ENQUIRY']:
			return 'ENQUIRY'

		return 'INVALID'

	def isEnabled(self):
		'''
			Helper function
			@returns If this entry is active
		'''
		return self.status & self.STATUS['ENABLED']

	def setEnabled(self, flag):
		'''
			Enables/Disables the transaction entry
			@Params:
				flag(bool) : True/False
		'''
		if flag is False:
			self.status &= ~self.STATUS['ENABLED']
		else:
			self.status |= self.STATUS['ENABLED']

	def isApproved(self):
		'''
			Helper function
			@returns If this entry is approved or declined
			@Note Since all transactions stored in this table have purchase bit set by default,
			we're not checking with purchase bit here. It might be necessary when Refund comes into picture
		'''
		return self.status & self.STATUS['APPROVED']

	def setApproved(self, flag):
		'''
			Approves/Declines this transaction
			@Params:
				flag(bool) : True/False
		'''
		if flag is False:
			self.status &= ~self.STATUS['APPROVED']
		else:
			self.status |= self.STATUS['APPROVED']

	def isPurchase(self):
		'''
			Helper function
			@returns If this entry is a purchase
		'''
		return self.ttype == self.TTYPE['PURCHASE']

	def isVoided(self):
		'''
			Helper function
			@returns If this purchase is voided
		'''
		return self.status & self.STATUS['VOID']

	def setVoid(self, flag=True):
		'''
			Helper function. Adds VOID state to this entry
		'''
		if flag is False:
			self.status &= ~self.STATUS['VOID']
		else:
			self.status |= self.STATUS['VOID']

	def hasCashBack(self):
		'''
			Helper function
			@return If this purchase has a cashback
		'''
		return self.status & self.STATUS['CASHBACK']

	def isTipped(self):
		'''
			Helper function
			@returns If this purchase has a tip transaction
		'''
		return self.status & self.STATUS['TIP']

	def setTip(self, flag=True):
		'''
			Helper function. Adds Tip state to this entry
		'''
		if flag is False:
			self.status &= ~self.STATUS['TIP']
		else:
			self.status |= self.STATUS['TIP']

	def setSettled(self, flag=True):
		'''
			Helper function. Adds Settled state to this entry
		'''
		if flag is False:
			self.status &= ~self.STATUS['SETTLED']
		else:
			self.status |= self.STATUS['SETTLED']


	def isCaptured(self):
		'''
			Helper function meant for PreAuth transaction
			@returns If this transaction is captured
		'''
		return self.status & self.STATUS['CAPTURE']

	def setCaptured(self, flag=True):
		'''
			Helper function meant for PreAuth transaction.
			Adds Capture state to this entry
		'''
		if flag is False:
			self.status &= ~self.STATUS['CAPTURE']
		else:
			self.status |= self.STATUS['CAPTURE']

	def isSimpleTxn(self):
		'''
			Helper function
			Checks if only these bits are set ENABLED + APPROVED in addition to ONLINE bit
			@returns if this entry is a simple transaction i.e., without states such as void and tip
		'''
		return (self.status & ~(self.STATUS['ONLINE'] | self.STATUS['CASHBACK'])) == self.VALID_TXN

	def isTipTxn(self):
		'''
			@returns if this entry is only tipped. No other states like Void are present. Cashback is ignore for the timebeing
		'''
		return (self.status  & ~(self.STATUS['ONLINE'] | self.STATUS['CASHBACK'])) == self.TIPPED

	def isVoidTxn(self):
		'''	Helper function
			Checks with ENABLED + APPROVED + VOID in addition to ONLINE bit
			@returns if this entry is a voided purchase and has no tip
			Can also be used for other transaction like refund since cashback doesn't interfere with it
		'''
		return (self.status & ~(self.STATUS['ONLINE'] | self.STATUS['CASHBACK'])) == self.VOIDED

	def isVoidedTipTxn(self):
		'''
			Returns if this purchase is tipped and voided
		'''
		return (self.status & ~(self.STATUS['ONLINE'] | self.STATUS['CASHBACK'])) == self.VOIDED_TIP


	# it lets us move void and tip separately with out a relationship
	unSettledTransactionId = Column(Integer, index = True)
	inputType = Column(Integer, default = INPUT_TYPE['MANUAL'], nullable=False)

	merchant = Column(String(16), nullable = False)
	agent = Column(String(16), nullable = True)
	company = Column(String(16), nullable = False)

	#sequenceCode can be used for monitoring the customer activity and connection status. To know the ratio of successful transactions. all such requests are logged in this table itself!!
	username = Column(String(16), index = True)
	clientId = Column(Integer, index = True) # should reference the main database

	sequenceCode = Column(Integer, index = True) #for synch with mobile
	plutusSequenceCode = Column(Integer, index = True) #for synch with plutus
	transId = Column(String(40))
	transTime = Column(TIMESTAMP)#if ksn is of ukpt it remains constant. Now for any two requests with same sequence ID, transID can't be a unique identifier. ==> keeping transTime in the loop

	amount = Column(Integer, default=0, nullable=False)
	amount2 = Column(Integer, default=0, nullable=False)#can be used as cashBack amount or pre-auth amount
	tipAmount = Column(Integer, default=0, nullable=False)

	#TODO Store the card details in different table and keep a relationship with Purchase:TransactionID
	chname = Column(String(26))
	maskedPan = Column(String(19))#TODO storage format to be worked on
	expiryMonth = Column(SmallInteger)
	expiryYear = Column(SmallInteger)

	tid = Column(String(15))
	mid = Column(String(15))
	cardNumber = Column(String(512))

	invoice = Column(String(12))
	batch = Column(String(12))
	acquirer = Column(String(20))
	#	issuer = Column(String(20), index = True)

	apprCode = Column(String(12))
	rrn = Column(String(12))

	receiptId = Column(String(40), nullable = True)
	receiptMobile = Column(String(13), nullable = True)
	receiptEmail = Column(String(30), nullable = True)

	ttype = Column(Integer, default=0, nullable=False)

	'''
		Least significant byte holds the approval status of the transaction.
			bit 0: enabled(1)/disabled(0)(sequence code)
			bit 1: approval status(txn approved(1)/declined(0))
			bit 2: approved online/offline
			bit 3-8: RFU
		Second least significant byte holds the transaction type
			bit 0: cashBack
			bit 1: Void
			bit 2: Tip
			bit 3: Settlement
			bit 4: Refund(Not implemented)
			bit 5-8: RFU
		Other two bytes are RFU.
	'''
	status = Column(Integer, default=0, nullable=False)
	cryptogram = Column(String(256), nullable = True)#Can store TC(approved) or AAC(declined)

	batch = Column(Integer)

	#consider the case of doing purchases with same sequence code.

	#for void, use a different table and a 1-1 relationship should suffice to check the history of transactions
	def __init__(self, unstld_txn, batch):

		self.unSettledTransactionId = unstld_txn.id

		self.username = unstld_txn.username
		self.clientId = unstld_txn.clientId
		self.sequenceCode = unstld_txn.sequenceCode
		self.plutusSequenceCode = unstld_txn.plutusSequenceCode
		self.transId = unstld_txn.transId
		self.transTime = unstld_txn.transTime

		self.amount = unstld_txn.amount
		self.amoutn2 = unstld_txn.amount2

		self.chname = unstld_txn.chname
		self.maskedPan = unstld_txn.maskedPan
		self.expiryMonth = unstld_txn.expiryMonth
		self.expiryYear = unstld_txn.expiryYear
		self.inputType = unstld_txn.inputType

		self.invoice = unstld_txn.invoice
		self.batch = unstld_txn.batch
		self.acquirer = unstld_txn.acquirer
		self.apprCode = unstld_txn.apprCode
		self.rrn = unstld_txn.rrn

		self.tid = unstld_txn.tid
		self.mid = unstld_txn.mid
		self.cardNumber = unstld_txn.cardNumber

		self.ttype = unstld_txn.ttype
		self.status = unstld_txn.status

		self.merchant = unstld_txn.merchant
		self.agent = unstld_txn.agent
		self.company = unstld_txn.company
		self.batch = batch

		self.receiptId = unstld_txn.receiptId
		self.receiptMobile = unstld_txn.receiptMobile
		self.receiptEmail = unstld_txn.receiptEmail

	def __repr__(self):
		return ('Id: %d | Unsettled Transaction Id: %d | User: %s | ClientId: %d | SeqCode: %d | PSeqCode: %d | TransID: %s | '
			'TransTime: %s | Amount: %s | Amount2: %s | Chname: %s | PAN: %s | MM: %d | YY: %d | Invoice: %s | Batch: %s | '
			'Acquirer: %s | ApprCode: %s | RRN: %s | TID: %s | MID: %s | Card : %s | Type: %d | Status: %d | Company: %s | Agent: %s | '
			'Merchant: %s | Batch : %d| InputType: %d') % \
		(self.id, self.unSettledTransactionId, self.username, self.clientId, self.sequenceCode, self.plutusSequenceCode, self.transId, \
			self.transTime, self.amount, self.amount2, self.chname, self.maskedPan, self.expiryMonth, self.expiryYear, \
			self.invoice, self.batch, self.acquirer, self.apprCode, self.rrn, self.tid, self.mid, self.cardNumber, self.ttype, \
			self.status, self.company, self.agent, self.merchant, self.batch, self.inputType)

class SettledTip(CRUDMixin, db1.Model):

	__tablename__ = 'settledTip'
	__bind_key__ = 'settled'

	STATUS = {
		'ENABLED' : 1<<0,
		'APPROVED' : 1<<1
	}

	clientId = Column(Integer)

	plutusSequenceCode = Column(Integer) #for synch with plutus

	ppurchaseId = Column(Integer, ForeignKey('settledTransaction.unSettledTransactionId'))
	purchaseDetails = relationship("SettledTransaction", backref="tip_details")

	transTime = Column(TIMESTAMP)
	amount = Column(String(10), index = True)
	'''
		Least significant byte holds the approval status of the transaction.
			bit 0: enabled(1)/disabled(0)(sequence code) (emulates)
			bit 1: approval status(txn approved(1)/declined(0))
	'''
	status = Column(Integer, default=0)

	def __init__(self, stld_tip):
		self.clientId = stld_tip.clientId
		self.ppurchaseId = stld_tip.purchaseId
		self.transTime = stld_tip.transTime
		self.plutusSequenceCode = stld_tip.plutusSequenceCode
		self.amount = stld_tip.amount
		self.status = stld_tip.status

	def __repr__(self):
		return 'Id: %d | Client Id: %d | Purchase Id: %d | TransTime: %s | PSeqCode: %s | Amount : %s | status : %d' % \
				(self.id, self.clientId, self.ppurchaseId, self.transTime, self.plutusSequenceCode, self.amount, self.status)


class SettledVoid(CRUDMixin, db1.Model):
	__tablename__ = 'settledVoid'
	__bind_key__ = 'settled'

	STATUS = {
		'ENABLED' : 1<<0,
		'APPROVED' : 1<<1
	}

	username = Column(String(16))
	clientId = Column(Integer)

	sequenceCode = Column(Integer) #for sync with mobile
	plutusSequenceCode = Column(Integer) #for sync with plutus

	ppurchaseId = Column(Integer,ForeignKey('settledTransaction.unSettledTransactionId'))
	purchaseDetails = relationship("SettledTransaction", backref="void_details")

	transId = Column(String(40))
	transTime = Column(TIMESTAMP)

	invoice = Column(String(12))
	apprCode = Column(String(12))
	rrn = Column(String(12))

	receiptId = Column(String(20), nullable = True)#TODO Do we need them?
	receiptMobile = Column(String(13), nullable = True)
	receiptEmail = Column(String(30), nullable = True)

	'''
		Least significant byte holds the approval status of the transaction.
			bit 0: enabled(1)/disabled(0)(sequence code) (emulates)
			bit 1: approval status(txn approved(1)/declined(0))
	'''
	status = Column(Integer, default=0)

	def __init__(self, stld_void):
		self.username = stld_void.username
		self.clientId = stld_void.clientId
		self.sequenceCode = stld_void.sequenceCode
		self.plutusSequenceCode = stld_void.plutusSequenceCode

		self.ppurchaseId = stld_void.purchaseId
		self.transId = stld_void.transId
		self.transTime = stld_void.transTime

		self.invoice = stld_void.invoice
		self.apprCode = stld_void.apprCode
		self.rrn = stld_void.rrn

		self.receiptId = stld_void.receiptId
		self.receiptMobile = stld_void.receiptMobile
		self.receiptEmail = stld_void.receiptEmail

		self.status = stld_void.status

	def __repr__(self):
		return ('<User: %s | ClientId : %s | SeqCode : %s | PSeqCode : %s | PurchaseId : %s | TransId : %s |'
			' TransTime : %s | Invoice : %s | ApprCode : %s | RRN : %s | status : %s> ') % \
		(self.username, self.clientId, self.sequenceCode, self.plutusSequenceCode, self.purchaseId, self.transId, \
			self.transTime, self.invoice, self.apprCode, self.rrn, self.status)


class SettledCapture(CRUDMixin, db1.Model):
	__tablename__ = 'capture'
	__bind_key__ = 'settled'

	STATUS = {
		'ENABLED' : 1<<0,
		'APPROVED' : 1<<1
	}

	username = Column(String(16))
	clientId = Column(Integer)

	sequenceCode = Column(Integer) #for sync with mobile
	plutusSequenceCode = Column(Integer) #for sync with plutus

	authId = Column(Integer, ForeignKey('settledTransaction.unSettledTransactionId'))
	authDetails = relationship("SettledTransaction", backref="capture_details")

	transId = Column(String(40))
	transTime = Column(TIMESTAMP)

	invoice = Column(String(12))
	apprCode = Column(String(12))
	rrn = Column(String(12))

	receiptId = Column(String(20), nullable = True)#TODO Do we need them?
	receiptMobile = Column(String(13), nullable = True)
	receiptEmail = Column(String(30), nullable = True)

	'''
		Least significant byte holds the approval status of the transaction.
			bit 0: enabled(1)/disabled(0)(sequence code) (emulates)
			bit 1: approval status(txn approved(1)/declined(0))
	'''
	status = Column(Integer, default=0)

	def __init__(self, capture):
		self.username = capture.username
		self.clientId = capture.clientId
		self.sequenceCode = capture.sequenceCode
		self.plutusSequenceCode = capture.plutusSequenceCode

		self.authId = capture.authId
		self.transId = capture.transId
		self.transTime = capture.transTime

		self.invoice = capture.invoice
		self.apprCode = capture.apprCode
		self.rrn = capture.rrn

		self.receiptId = capture.receiptId
		self.receiptMobile = capture.receiptMobile
		self.receiptEmail = capture.receiptEmail

		self.status = capture.status

	def __repr__(self):
		return ('<User: %s | ClientId : %s | SeqCode : %s | PSeqCode : %s | authId : %s | TransId : %s |'
			' TransTime : %s | Invoice : %s | ApprCode : %s | RRN : %s | status : %s> ') % \
		(self.username, self.clientId, self.sequenceCode, self.plutusSequenceCode, self.authId, self.transId, \
			self.transTime, self.invoice, self.apprCode, self.rrn, self.status)
