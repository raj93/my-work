from ext import db
from src.models.base import CRUDMixin
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, TIMESTAMP


class vanillaHierarchy(CRUDMixin,db.Model):
	__tablename__ = "hierarchy"

	company = Column(String(16), ForeignKey('company.username'), nullable = False)
	agent = Column(String(16), ForeignKey('agent.username'), nullable = True)
	merchant = Column(String(16), ForeignKey('merchant.username'), nullable = False)

	def __init__(self,company,agent,merchant):
		self.company = company
		self.agent = agent
		self.merchant = merchant
	
class merchantProofs(CRUDMixin,db.Model):
	__tablename__ = "merchantProofs"

	merchant = Column(String(16), ForeignKey('merchant.username'), nullable = False)
	uploadedName = Column(String(40))
	objectId = Column(String(50),unique=True)

	def __init__(self,merchant,uploadedName,objectId):
		self.merchant = merchant
		self.uploadedName = uploadedName
		self.objectId = objectId

	def __repr__(self):
		return ("Merchant %s has proof with object id %s"%(self.merchant,self.objectId))

class support(CRUDMixin, db.Model):
	FEED_BACK_TYPES = {
		'0' : 'Query',
		'1' : 'Complaint',
		'2' : 'Suggestion'
		}

	FEED_BACK_VALUES = FEED_BACK_TYPES.values()
	feedbackChoices = []
	
	for item in FEED_BACK_TYPES:
		feedbackChoices.append((item,FEED_BACK_TYPES[item]))

	__tablename__ = "support"
	username = Column(String(16), index = True)
	time = Column(TIMESTAMP)
	contactNumber = Column(String(10), index = True)
	email = Column(String(30))
	message = Column(String(150))
	ticketId = Column(String(20),default='0')


	# 0-Query 1-Complaint 2-Suggestion
	feedbackType = Column(Integer, default = 0)

	merchant = Column(String(16), ForeignKey('merchant.username'), nullable = True)#merchant username
	agent = Column(String(16), ForeignKey('agent.username'), nullable = True)
	company = Column(String(16), ForeignKey('company.username'), nullable = True)

	isResolved = Column(Boolean, default = False)

	def __init__(self, username, time, contactNumber, message,email,feedbackType,ticketId,merchant,agent,company):
		self.username = username
		self.time = time
		self.contactNumber = contactNumber
		self.email = email
		self.message = message
		self.feedbackType = feedbackType
		self.ticketId = ticketId
		self.merchant = merchant
		self.agent = agent
		self.company = company


	def __repr__(self):
		return '<User %s requested at %s to contact him at %s. Mesg: %s>' % (self.username, self.time, self.contactNumber, self.message)


class dongleInventory(CRUDMixin,db.Model):
	__tablename__ = "dongleInventory"
	
	DEVICE_TYPE = {
	"UNKNOWN" : 0,
	"MIURA" : 1,
	"GDSEEDS" : 2
	}

	dongleSerial = Column(String(30), unique = True)#2 different device models might have same serial number
	deviceType = Column(Integer, nullable = False)
	
	isAlloted = Column(Boolean, default = False)
	merchant = Column(String(16), ForeignKey('merchant.username'),nullable=True)#merchant username
	agent = Column(String(16), ForeignKey('agent.username'), nullable = True)
	company = Column(String(16), ForeignKey('company.username'), nullable = True)

	def __init__(self, dongleSerial, deviceType = DEVICE_TYPE['MIURA']):
		self.deviceType = deviceType
		self.dongleSerial = dongleSerial
		

	def __repr__(self):
		return "DeviceType %d of serial %s is under merchant %s who i under company %s"%(self.deviceType, self.dongleSerial, self.merchant, self.company)
