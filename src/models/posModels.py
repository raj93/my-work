from ext import db
from src.models.base import CRUDMixin
from sqlalchemy import Column, Integer, String, SmallInteger, Boolean, ForeignKey, BigInteger, Float, TIMESTAMP, UniqueConstraint, text
from sqlalchemy.orm import relationship, backref


class posDetails(CRUDMixin, db.Model):
	__tablename__ = "posDetails"

	"""
		Constants for columns
	"""
	PROCESSORS = {
		"DEMO" : 0,
		"PLUTUS" : 1,
		"STRIPE" : 2,
		"ATOS" : 3
	}

	VALIDATE_BITMAP_TYPE = {
		"PRE_ACTIVATION" : 0,
		"PRE_LOGIN" : 1,
		"POST_LOGIN" : 2
	}

	def isActivated(self):
		return bool(self.bitmap & self.BITMAP['POS_ACTIVATION'])

	def setActivated(self, state = True):
		self.bitmap |= state * self.BITMAP['POS_ACTIVATION']

	def getTransactionStatus(self):
		#First 2 bits
		return self.transactionStatus & 3

	def setTransactionStatus(self, state):#TODO decide on this behavior
		#int(bool) = 1 for True and 0 for False
		self.transactionStatus = (self.transactionStatus & 4) | int(state)

	def hasSettlementConflict(self):
		return self.transactionStatus & 4

	def setConflict(self, state):
		self.transactionStatus = (self.transactionStatus & 3) | (state * 4)

	PRE_ACTIVATION_BITMAP = ['MERCHANT_VERIFICATION', 'POS_VERIFICATION', 'PAYMENT_VERIFICATION', \
		'MID_ALLOTED', 'TID_ALLOTED']
	PRE_LOGIN_BITMAP = PRE_ACTIVATION_BITMAP + ['POS_ACTIVATION']

	POSITIVE_BITMAP = PRE_LOGIN_BITMAP + ['LOGIN_STATUS']
	NEUTRAL_BITMAP = ['SKIP_OTP_VERIFICATION', 'FRAUD']
	NEGATIVE_BITMAP = ['MERCHANT_LOCK', 'MERCHANT_DISABLE', 'POS_LOCK', 'POS_DISABLE']

	TOTAL_BITMAP = POSITIVE_BITMAP + NEUTRAL_BITMAP + NEGATIVE_BITMAP

	BITMAP = {}
	for i in range(len(TOTAL_BITMAP)):
		BITMAP[TOTAL_BITMAP[i]] = (1 << i)

	''' Bitmap value to be present for a POS whose user is logged in'''
	# positive bitmap value
	POSITIVE_BITMAP_VALUE = 0
	for bitName in POSITIVE_BITMAP:
		POSITIVE_BITMAP_VALUE |= BITMAP[bitName]

	# neutral bitmap value
	NEUTRAL_BITMAP_VALUE = 0
	for bitName in NEUTRAL_BITMAP:
		NEUTRAL_BITMAP_VALUE |= BITMAP[bitName]

	# negative bitmap value
	NEGATIVE_BITMAP_VALUE = 0
	for bitName in NEGATIVE_BITMAP:
		NEGATIVE_BITMAP_VALUE |= BITMAP[bitName]

	# positive preactivation bitmap value
	VALID_PRE_ACTIVATION_VALUE = 0
	for bitName in PRE_ACTIVATION_BITMAP:
		VALID_PRE_ACTIVATION_VALUE |= BITMAP[bitName]

	# positive pre login bitmap value
	VALID_PRE_LOGIN_VALUE = 0
	for bitName in PRE_LOGIN_BITMAP:
		VALID_PRE_LOGIN_VALUE |= BITMAP[bitName]


	merchant = Column(String(16), ForeignKey('merchant.username', onupdate='cascade'))
	agent = Column(String(16), ForeignKey('agent.username'), nullable = True)
	company = Column(String(16), ForeignKey('company.username'), nullable = False)
	paymentId = Column(Integer,ForeignKey('merchantPayments.id')) 

	clientId = Column(Integer, unique = True, nullable = True)#Nullable = True is used to prevent Duplicate entry(ERROR 1062) before assigning self.clientId = self.id
	tid = Column(String(20), nullable = True)
	processor = Column(Integer, nullable = False)
	acquirer = Column(String(30), nullable = False, default = "acquirer")

	mobileNumber = Column(String(13), nullable=False, unique = True)
	mobileModel = Column(String(25))
	deviceId = Column(String(24))

	dongleSerial = Column(String(30))

	sequenceCode = Column(BigInteger, default = 1)
	minSequenceCode =  Column(BigInteger, default = 1)
	bitmap = Column(Integer, default=0)

	plutusClientId = Column(Integer, index = True)#, unique = True)
	plutusSequenceCode = Column(BigInteger, default = 1)	
	plutusSecretKey = Column(String(40))#securityToken in case of pinelabs

	currency = Column(String(5), nullable=False, default="INR")
	batch = Column(Integer,default = 1) # used for settlement batch purposes

	loggedInUser = Column(String(16))#stores the user who is logged in. Null if no one is logged in. If there is no logout request, this field might not be NULL even though the session is expired. Hence, the usage of last_activity.. using this field to know who is logged in at the moment
	lastActivity = Column(TIMESTAMP)#False ==> auto updates the timestamps. But keeping True as RDS uses UTC time which is causing issues. We can't modify RDS timezone as of 5 Jul '13
	loginTime = Column(TIMESTAMP)
	
	'''
		Least 2 bits of transactionStatus signify:
		* 00 : RELAXED,
		* 01 : SERVER_BUSY,
		* 02 : TIP_BUSY,#TODO remove it if not required
		* 03 : RESERVED FOR FUTURE USE
		3rd least significant bit signifies settlement conflict of the client Id.
	'''
	transactionStatus = Column(Integer, default = 0) # TODO from constants
	transactionType = Column(Integer, default = 4) # TODO ERROR_IN_TRANSACTION

	paymentDetails = relationship("payment", uselist = False, backref = backref("terminals"))	
	merchantDetails = relationship("Merchant", backref = backref("devices"))

	def __init__(self, merchant,agent,company,paymentId,mobileNumber,mobileModel,currency,processor = PROCESSORS['DEMO']):
		self.merchant = merchant
		self.agent = agent
		self.company = company

		self.paymentId = paymentId

		self.mobileNumber = mobileNumber
		self.mobileModel = mobileModel
		self.currency = currency

		self.processor = processor

	def __repr__(self):
		return ("Bitmap: %d | Merchant: %s | deviceId: %s | Dongle: %s | Client: %s | PClient: %s |"
			" PsecretKey : %s | SeqCode: %s | PSeqCode: %s | MinSeqCode: %s | Mobile: %s |"
			" Model : %s | Currency: %s | Processor : %d | Company: %s | Agent: %s | Batch : %d") \
		% 	(self.bitmap, self.merchant, self.deviceId, self.dongleSerial, self.clientId, self.plutusClientId, \
			self.plutusSecretKey, self.sequenceCode, self.plutusSequenceCode, self.minSequenceCode, self.mobileNumber, self.mobileModel, self.currency, self.processor, self.company, self.agent, \
			self.batch)

	def printCheckedBits(self):#helper function
		for ele in self.BITMAP:
			if self.bitmap & self.BITMAP[ele]:
				print ele + " \/"


class posUsers(CRUDMixin, db.Model):
	__tablename__ = "posUsers"

	username = Column(String(16))
	passwd = Column(String(128), nullable = False)
	salt = Column(String(64), nullable = False)
	# TODO locking
	clientId = Column(Integer,ForeignKey('posDetails.clientId', onupdate='cascade'))
	details = relationship("posDetails", backref = "users")

	shouldChangePassword = Column(Boolean, default = False)
	rn = Column(String(32),default='1234')

	# to put limit on incorrect password attempts
	invalidAttempts = Column(Integer, default=0)
	lastHoldedTime = Column(TIMESTAMP, nullable=True)

	bitmap = Column(Integer, default=0, nullable=False)

	TOTAL_BITMAP = ['POS_USER_LOCK']

	BITMAP = {}
	for i in range(len(TOTAL_BITMAP)):
		BITMAP[TOTAL_BITMAP[i]] = (1 << i)

	__table_args__ = (UniqueConstraint('username', 'clientId', name='user'),)

	def lock(self):
		self.bitmap |= self.BITMAP['POS_USER_LOCK']

	def unlock(self):
		if self.bitmap & self.BITMAP['POS_USER_LOCK'] != 0:
			self.bitmap ^= self.BITMAP['POS_USER_LOCK']

	def isLocked(self):
		return self.bitmap & self.BITMAP['POS_USER_LOCK'] != 0

	def __init__(self, username, passwd, salt, clientId, shouldChangePassword = False):
		self.username = username
		self.passwd = passwd
		self.salt=salt
		self.clientId = clientId
		self.shouldChangePassword = shouldChangePassword

	def __repr__(self):
		return "User: %s | clientId: %s | passwd : %s | salt : %s | shouldChangePassword: %s | rn : %s" %(self.username, self.clientId, self.passwd, self.salt, self.shouldChangePassword, self.rn)

class clientIdInventory(CRUDMixin, db.Model):
	"""
		List of free ClientIDs of Pinelabs
	"""

	__tablename__ = "clientIDs"
	clientID = Column(Integer, unique = True)
	securityToken = Column(String(50), nullable = False,unique=True)
	isAlloted = Column(Boolean, default = False)

	localClientID = Column(Integer,ForeignKey('posDetails.clientId'),nullable=True)

	def __init__(self, clientID, securityToken):
		self.clientID = clientID
		self.securityToken = securityToken

	def __repr__(self):
		return "<%s>:<%s>:<%s>" % (self.clientID, self.securityToken, repr(self.isAlloted))

class smsOTP(CRUDMixin, db.Model):
	__tablename__ = "smsOTP"

	mobileNumber = Column(String(13), unique = True)
	email = Column(String(30), nullable = True)
	deviceId = Column(String(30), nullable = False)
	otp = Column(String(8), nullable = False)
	lastSMSTime = Column(TIMESTAMP, nullable = False)
	numTries = Column(Integer, default = 1)
	numActivationRequests = Column(Integer, default = 0)
	isSuccess = Column(Boolean, default = False)	

	def __init__(self, mobileNumber,email,deviceId,otp,lastSMSTime):
		self.mobileNumber = mobileNumber
		self.email = email
		self.deviceId = deviceId
		self.otp = otp
		self.lastSMSTime = lastSMSTime


	def __repr__(self):
		return "OTP of %s is %s; Last SMS sent at %s. Requested DeviceId = %s" % (self.mobileNumber, self.otp,self.lastSMSTime, self.deviceId)


class deviceIdCount(CRUDMixin, db.Model):
	__tablename__ = "deviceIdCount"
	deviceId = Column(String(24), unique = True,nullable = False)
	numTries = Column(Integer,default = 0)

	def __init__(self,deviceId):
		self.deviceId = deviceId

	def __repr__(self):
		return "device with this id %s had tried %d times" % (self.deviceId,self.numTries)

class mSupport(CRUDMixin, db.Model):
	
	__tablename__ = "mSupport"
	username = Column(String(16), index = True)

	clientId = Column(Integer, ForeignKey('posDetails.clientId', onupdate='cascade'))
	details = relationship("posDetails", backref = backref("support"))
	
	time = Column(TIMESTAMP)
	mobile = Column(String(10), index = True)
	message = Column(String(1000))

	ticketId = Column(String(20),default='0')

	def __init__(self, clientId, time, mobile, message,ticketId):
		self.clientId = clientId
		self.time = time
		self.mobile = mobile
		self.message = message
		self.ticketId = ticketId

	def __repr__(self):
		return 'ClientId: %s | Time: %s | Contact: %s | Message: %s | ticketId: %s' % (self.clientId, self.time, self.mobile, self.message, self.ticketId)


class payment(CRUDMixin, db.Model):
	"""
		Payments submitted by merchant during registration
	"""
	
	__tablename__ = "merchantPayments"
	
	#TODO attach a scan copy of cheque/dd
	
	#cheque : micr + date + account no + cheque serial
	#dd : micr + date + dd serial
	#micr = Magnetic ink character recognition code
	# in micr either dd number or cheque number is stored

	username = Column(String(16), ForeignKey('merchant.username', onupdate='cascade', ondelete='cascade'))
	paymentMode = Column(Integer, default = 0) #cheque or DD 0-cheque 1-dd
	micr = Column(String(9), nullable = False)
	date = Column(TIMESTAMP)
	acNumber = Column(String(15),nullable=True)
	amount = Column(Float, nullable = False)

	numPos = Column(Integer) # money for that many number of pos
	isVerified = Column(Boolean,default=False)

	serial = Column(String(6), nullable = False)#DD serial or Cheque serial

	merchantDetails = relationship("Merchant", uselist = False, backref = backref("payments"))
	

	def __init__(self, username, paymentMode, micr, date, acNumber, serial, amount,numPos):
		self.username = username
		self.paymentMode = paymentMode
		self.micr = micr
		self.date = date
		self.acNumber = acNumber
		self.serial = serial
		self.amount = amount
		self.numPos = numPos
		
	def __repr__(self):
		return "merchant %s has given us payment %s using %d having serial number %s" \
			% (self.username, self.amount, self.paymentMode, self.serial)
