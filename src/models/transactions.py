from ext import db
from sqlalchemy import Column, Integer, String, SmallInteger, Boolean, ForeignKey, text, and_
from sqlalchemy.types import TIMESTAMP
from sqlalchemy.orm import relationship, backref
from src.models import CRUDMixin
#history of transactions to merge over purchase and void

class Transaction(CRUDMixin, db.Model):
	__tablename__ = "transaction"

	#Note the ordering INPUT_TYPE_KEY[inputType] is used in places
	INPUT_TYPE = {
		'MANUAL' : 0,
		'SWIPE' : 1,
		'CHIP' : 2,
		}

	INPUT_TYPE_KEY = {
		0: 'MANUAL',
		1: 'SWIPE',
		2: 'CHIP',
		}

	TTYPE = {
		'INVALID': 0,
		'PURCHASE': 1,
		'REFUND': 2,
		'PREAUTH': 3,
		'ENQUIRY': 4,
		}

	STATUS = {
		'ENABLED' : 1 << 0,
		'APPROVED' : 1 << 1,
		'ONLINE' : 1 << 2,#Signifies if transaction is online or offline
		#Transaction type bits
		'CASHBACK' : 1 << 8,
		'VOID' : 1 << 9,
		'TIP' : 1 << 10,
		'SETTLED' : 1 << 11,
		'CAPTURE': 1 << 12,
		}

	VALID_TXN = STATUS['ENABLED'] | STATUS['APPROVED']
	VOIDED = VALID_TXN  | STATUS['VOID']
	TIPPED = VALID_TXN  | STATUS['TIP']
	VOIDED_TIP = VALID_TXN | STATUS['VOID'] | STATUS['TIP']
	CAPTURED = VALID_TXN | STATUS['CAPTURE']

	#Do we need following ?
	APPROVED_ONLINE = VALID_TXN | STATUS['ONLINE']
	APPROVED_OFFLINE = VALID_TXN | (0 * STATUS['ONLINE'])
	DECLINED_ONLINE = STATUS['ENABLED'] | (0 * STATUS['APPROVED']) | STATUS['ONLINE']
	DECLINED_OFFLINE = STATUS['ENABLED'] | (0 * STATUS['APPROVED']) | (0 * STATUS['ONLINE'])

	@classmethod
	def conditions(cls, condition):
		'''
			Helper module
			#TODO Explore adding an additional argument ttype since there would 4 extra conditions for each ttype
			@param condition(string) -> One of the conditions defined in `CONDITIONS`
			@returns sqlalchemy filter condition
		'''

		if condition is 'APPROVED':#TODO replaced this with 'VALID' as it is more appropriate. remove if unused.
			return cls.status.op('&')(cls.STATUS['APPROVED']) == cls.STATUS['APPROVED']

		if condition is 'VALID':
			return cls.status.op('&')(cls.VALID_TXN) == cls.VALID_TXN

		if condition is 'SIMPLE_PURCHASE':
			return and_(cls.ttype == cls.TTYPE['PURCHASE'], \
							cls.status.op('&')(~(cls.STATUS['ONLINE'] | cls.STATUS['CASHBACK'])) == cls.VALID_TXN)

		if condition is 'VOIDED_PURCHASE':
			return and_(cls.ttype == cls.TTYPE['PURCHASE'], \
							cls.status.op('&')(~(cls.STATUS['ONLINE'] | cls.STATUS['CASHBACK'])) == cls.VOIDED)

		if condition is 'TIPPED_PURCHASE':
			return and_(cls.ttype == cls.TTYPE['PURCHASE'], \
							cls.status.op('&')(~(cls.STATUS['ONLINE'] | cls.STATUS['CASHBACK'])) == cls.TIPPED)

		if condition is 'VOIDED_TIP':#Tip is only for Purchases ==> VOIDED_TIP_PURCHASE
			return and_(cls.ttype == cls.TTYPE['PURCHASE'], \
							cls.status.op('&')(~(cls.STATUS['ONLINE'] | cls.STATUS['CASHBACK'])) == cls.VOIDED_TIP)

		raise keyError('No condition with given name exists')

	def getTransactionType(self):
		if self.ttype == self.TTYPE['PURCHASE']:
			if self.isVoidedTipTxn():
				return 'VOID(SALE WITH TIP)'

			if self.isTipped():
				return 'SALE WITH TIP'

			if self.isVoided():
				return 'VOID(SALE)'

			return 'SALE'

		elif self.ttype == self.TTYPE['REFUND']:
			if self.isVoided():
				return 'VOID(REFUND)'

			return 'REFUND'

		elif self.ttype == self.TTYPE['PREAUTH']:
			if self.isCaptured():
				return 'CAPTURED(PREAUTH)'

			return 'PREAUTH'

		elif self.ttype == self.TTYPE['ENQUIRY']:
			return 'ENQUIRY'

		return 'INVALID'

	def isEnabled(self):
		'''
			Helper function
			@returns If this entry is active
		'''
		return self.status & self.STATUS['ENABLED']

	def setEnabled(self, flag):
		'''
			Enables/Disables the transaction entry
			@Params:
				flag(bool) : True/False
		'''
		if flag is False:
			self.status &= ~self.STATUS['ENABLED']
		else:
			self.status |= self.STATUS['ENABLED']

	def isApproved(self):
		'''
			Helper function
			@returns If this entry is approved or declined
			@Note Since all transactions stored in this table have purchase bit set by default,
			we're not checking with purchase bit here. It might be necessary when Refund comes into picture
		'''
		return self.status & self.STATUS['APPROVED']

	def setApproved(self, flag):
		'''
			Approves/Declines this transaction
			@Params:
				flag(bool) : True/False
		'''
		if flag is False:
			self.status &= ~self.STATUS['APPROVED']
		else:
			self.status |= self.STATUS['APPROVED']

	def isVoided(self):
		'''
			Helper function
			@returns If this purchase is voided
		'''
		return self.status & self.STATUS['VOID']

	def setVoid(self, flag=True):
		'''
			Helper function. Adds VOID state to this entry
		'''
		if flag is False:
			self.status &= ~self.STATUS['VOID']
		else:
			self.status |= self.STATUS['VOID']

	def hasCashBack(self):
		'''
			Helper function
			@return If this purchase has a cashback
		'''
		return self.status & self.STATUS['CASHBACK']

	def isTipped(self):
		'''
			Helper function
			@returns If this purchase has a tip transaction
		'''
		return self.status & self.STATUS['TIP']

	def setTip(self, flag=True):
		'''
			Helper function. Adds Tip state to this entry
		'''
		if flag is False:
			self.status &= ~self.STATUS['TIP']
		else:
			self.status |= self.STATUS['TIP']

	def setSettled(self, flag=True):
		'''
			Helper function. Adds Settled state to this entry
		'''
		if flag is False:
			self.status &= ~self.STATUS['SETTLED']
		else:
			self.status |= self.STATUS['SETTLED']

	def isCaptured(self):
		'''
			Helper function meant for PreAuth transaction
			@returns If this transaction is captured
		'''
		return self.status & self.STATUS['CAPTURE']

	def setCaptured(self, flag=True):
		'''
			Helper function meant for PreAuth transaction.
			Adds Capture state to this entry
		'''
		if flag is False:
			self.status &= ~self.STATUS['CAPTURE']
		else:
			self.status |= self.STATUS['CAPTURE']

	def isSimpleTxn(self):
		'''
			Helper function
			Checks if only these bits are set ENABLED + APPROVED in addition to ONLINE bit
			@returns if this entry is a simple transaction i.e., without states such as void and tip
		'''
		return (self.status & ~(self.STATUS['ONLINE'] | self.STATUS['CASHBACK'])) == self.VALID_TXN

	def isTipTxn(self):
		'''
			@returns if this entry is only tipped. No other states like Void are present. Cashback is ignore for the timebeing
		'''
		return (self.status  & ~(self.STATUS['ONLINE'] | self.STATUS['CASHBACK'])) == self.TIPPED

	def isVoidTxn(self):
		'''	Helper function
			Checks with ENABLED + APPROVED + VOID in addition to ONLINE bit
			@returns if this entry is a voided purchase and has no tip
			Can also be used for other transaction like refund since cashback doesn't interfere with it
		'''
		return (self.status & ~(self.STATUS['ONLINE'] | self.STATUS['CASHBACK'])) == self.VOIDED

	def isVoidedTipTxn(self):
		'''
			Returns if this purchase is tipped and voided
		'''
		return (self.status & ~(self.STATUS['ONLINE'] | self.STATUS['CASHBACK'])) == self.VOIDED_TIP

	def isCapturedTxn(self):
		'''
			Returns if a pre-Auth is captured
		'''
		return self.status & ~self.STATUS['ONLINE'] == self.CAPTURED

	inputType = Column(Integer, default = INPUT_TYPE['MANUAL'], nullable=False)

	merchant = Column(String(16), ForeignKey('merchant.username'), nullable = False)
	merchantDetails = relationship("Merchant", uselist = False, backref = backref("transactions", uselist = True))

	agent = Column(String(16), ForeignKey('agent.username'), nullable = True)
	company = Column(String(16), ForeignKey('company.username'), nullable = False)

	#DeviceID, secretKey are not stored in this.. They're verified through user access details. If they're invalid, store the log in fraud related logs..
	#sequenceCode can be used for monitoring the customer activity and connection status. To know the ratio of successful transactions. all such requests are logged in this table itself!!
	username = Column(String(16), index = True)
	clientId = Column(Integer, ForeignKey('posDetails.clientId'), index = True)

	sequenceCode = Column(Integer, index = True) #for synch with mobile
	plutusSequenceCode = Column(Integer, index = True) #for synch with plutus
	transId = Column(String(40))
	transTime = Column(TIMESTAMP)#if ksn is of ukpt it remains constant. Now for any two requests with same sequence ID, transID can't be a unique identifier. ==> keeping transTime in the loop

	amount = Column(Integer, default=0, nullable=False)
	amount2 = Column(Integer, default=0, nullable=False)#can be used as cashBack amount or pre-auth amount
	tipAmount = Column(Integer, default=0, nullable=False)

	#TODO Store the card details in different table and keep a relationship with Purchase:TransactionID
	chname = Column(String(26))
	maskedPan = Column(String(19))#TODO storage format to be worked on
	expiryMonth = Column(SmallInteger)
	expiryYear = Column(SmallInteger)

	tid = Column(String(15))
	mid = Column(String(15))
	cardNumber = Column(String(512))#Change it to card token

	invoice = Column(String(12))
	batch = Column(String(12))
	acquirer = Column(String(20))
	#	issuer = Column(String(20), index = True)

	apprCode = Column(String(12))
	rrn = Column(String(12))

	receiptId = Column(String(20), nullable = True)
	receiptMobile = Column(String(13), nullable = True)
	receiptEmail = Column(String(30), nullable = True)

	ttype = Column(Integer, default=0, nullable=False)
	'''
		Least significant byte holds the approval status of the transaction.
			bit 0: enabled(1)/disabled(0)(sequence code)
			bit 1: approval status(txn approved(1)/declined(0))
			bit 2: approved online/offline
			bit 3-8: RFU
		Second least significant byte holds the transaction type
			bit 0: cashBack
			bit 1: Void
			bit 2: Tip
			bit 3: Settlement
			bit 4: Refund(Not implemented)
			bit 5-8: RFU
		Other two bytes are RFU.
		Such bitmap can be used for other models as well. Currently restricting this bitmap to only purchase
	'''
	status = Column(Integer, default=0, nullable=False)
	cryptogram = Column(String(256), nullable = True)#Can store TC(approved) or AAC(declined)

	terminalDetails = relationship("posDetails", uselist=False, backref="transactions")


	#for void, use a different table and a 1-1 relationship should suffice to check the history of transactions
	def __init__(self, username, clientId, sequenceCode, plutusSequenceCode, transId, transTime, \
		amount, amount2, chname, maskedPan, expiryMonth, expiryYear,inputType, invoice, batch, \
		acquirer, apprCode, rrn, tid, mid, cardNumber, ttype, status, company, agent, merchant):
		self.username = username
		self.clientId = clientId
		self.sequenceCode = sequenceCode
		self.plutusSequenceCode = plutusSequenceCode
		self.transId = transId
		self.transTime = transTime

		self.amount = amount
		self.amount2 = amount2

		self.chname = chname
		self.maskedPan = maskedPan
		self.expiryMonth = expiryMonth
		self.expiryYear = expiryYear
		self.inputType = inputType

		self.invoice = invoice
		self.batch = batch
		self.acquirer = acquirer
		self.apprCode = apprCode
		self.rrn = rrn

		self.tid = tid
		self.mid = mid
		self.cardNumber = cardNumber

		self.ttype = ttype
		self.status = status

		self.company = company
		self.agent = agent
		self.merchant = merchant


	def __repr__(self):
		return ('Id: %d | User: %s | ClientId: %s | SeqCode: %s | PSeqCode: %s | TransID: %s | TransTime: %s | Amount: %d | '
			'Amount2: %d | Tip Amount: %d | Chname: %s | PAN: %s | MM: %s | YY: %s | Invoice: %s | Batch: %s | Acquirer: %s | '
			'ApprCode: %s | RRN: %s | TID: %s | MID: %s | Card : %s | Type: %d | Status: %d | Company: %s | Agent: %s | Merchant: %s |'
			'InputType: %d |') % \
		(self.id, self.username, self.clientId, self.sequenceCode, self.plutusSequenceCode, self.transId, self.transTime, \
			self.amount, self.amount2, self.tipAmount, self.chname, self.maskedPan, self.expiryMonth, self.expiryYear, \
			self.invoice, self.batch, self.acquirer, self.apprCode, self.rrn, self.tid, self.mid, self.cardNumber, \
			self.ttype, self.status, self.company, self.agent, self.merchant, self.inputType)


class pendingTip(CRUDMixin, db.Model):

	__tablename__ = "pendingTips"

	clientId = Column(Integer, ForeignKey('posDetails.clientId'))
	#TODO relation
	transId = Column(String(40), unique = True)
	transTime = Column(TIMESTAMP, server_default = text("CURRENT_TIMESTAMP"), nullable = False)#if ksn is of ukpt it remains constant. Now for any two requests with same sequence ID, transID can't be a unique identifier. ==> keeping transTime in the loop	invoice = Column(String(12))
	tipAmount = Column(Integer)

	def __init__(self, clientId, transId, transTime,tipAmount):
		self.clientId = clientId
		self.transId = transId
		self.transTime = transTime
		self.tipAmount = tipAmount
	def __repr__(self):
		return "ClientId: %d | TransId: %s | TransTime: %s | TipAmount: %d " % \
				(self.clientId, self.transId, self.transTime, self.tipAmount)


class Tip(CRUDMixin, db.Model):
	__tablename__ = "tip"

	STATUS = {
		'ENABLED' : 1<<0,
		'APPROVED' : 1<<1
	}

	APPROVED = STATUS['ENABLED'] + STATUS['APPROVED']

	@classmethod
	def conditions(cls, condition):
		'''
			Helper module.
			@param condition(string) -> One of the conditions defined in `CONDITIONS`
			@returns sqlalchemy filter condition
		'''
		CONDITIONS = {
			'VALID' : cls.status.op('&')(cls.APPROVED) == cls.APPROVED
		}
		if condition in CONDITIONS.keys():
			return CONDITIONS[condition]
		raise keyError('No condition with given name exists')

	def isEnabled(self):
		'''
			Helper function
			@returns If this entry is active
		'''
		return self.status & self.STATUS['ENABLED']

	def setEnabled(self, flag):
		'''
			Enables/Disables the transaction entry
			@Params:
				flag(bool) : True/False
		'''
		if flag is False:
			self.status &= ~self.STATUS['ENABLED']
		else:
			self.status |= self.STATUS['ENABLED']

	def isApproved(self):
		'''
			Helper function
			@returns If this entry is approved or declined
		'''
		return self.status & self.STATUS['APPROVED']

	def setApproved(self, flag):
		'''
			Approves/Declines this transaction
			@Params:
				flag(bool) : True/False
		'''
		if flag is False:
			self.status &= ~self.STATUS['APPROVED']
		else:
			self.status |= self.STATUS['APPROVED']

	clientId = Column(Integer, ForeignKey('posDetails.clientId'), index=True)

	plutusSequenceCode = Column(Integer)

	purchaseId = Column(Integer, ForeignKey('transaction.id'))
	purchaseDetails = relationship("Transaction", primaryjoin=Transaction.id == purchaseId,
			uselist=False)

	transTime = Column(TIMESTAMP)
	amount = Column(Integer, default=0)
	'''
		Least significant byte holds the approval status of the transaction.
			bit 0: enabled(1)/disabled(0)(sequence code) (emulates)
			bit 1: approval status(txn approved(1)/declined(0))
	'''
	status = Column(Integer, default=0)

	def __init__(self, clientId, purchaseId, transTime, plutusSequenceCode, amount, status=0):
		self.clientId = clientId
		self.purchaseId = purchaseId
		self.transTime = transTime
		self.plutusSequenceCode = plutusSequenceCode
		self.amount = amount
		self.status = status

	def __repr__(self):
		return "ClientId: %d | PSeqCode : %d | PurchaseId: %d | TransTime: %s | TipAmount: %d | status : %s" % \
				(self.clientId, self.plutusSequenceCode, self.purchaseId, self.transTime, self.amount, self.status)


class Void(CRUDMixin, db.Model):
	__tablename__ = "void"

	STATUS = {
		'ENABLED' : 1<<0,
		'APPROVED' : 1<<1
	}

	APPROVED = STATUS['ENABLED'] + STATUS['APPROVED']
	@classmethod
	def conditions(cls, condition):
		'''
			Helper module.
			@param condition(string) -> One of the conditions defined in `CONDITIONS`
			@returns sqlalchemy filter condition
		'''
		CONDITIONS = {
			'VALID' : cls.status.op('&')(cls.APPROVED) == cls.APPROVED
		}
		if condition in CONDITIONS.keys():
			return CONDITIONS[condition]
		raise keyError('No condition with given name exists')

	def isEnabled(self):
		'''
			Helper function
			@returns If this entry is active
		'''
		return self.status & self.STATUS['ENABLED']

	def setEnabled(self, flag):
		'''
			Enables/Disables the transaction entry
			@Params:
				flag(bool) : True/False
		'''
		if flag is False:
			self.status &= ~self.STATUS['ENABLED']
		else:
			self.status |= self.STATUS['ENABLED']

	def isApproved(self):
		'''
			Helper function
			@returns If this entry is approved or declined
		'''
		return self.status & self.STATUS['APPROVED']

	def setApproved(self, flag):
		'''
			Approves/Declines this transaction
			@Params:
				flag(bool) : True/False
		'''
		if flag is False:
			self.status &= ~self.STATUS['APPROVED']
		else:
			self.status |= self.STATUS['APPROVED']

	username = Column(String(16))
	clientId = Column(Integer, ForeignKey('posDetails.clientId'), index = True) #Foreign key... authentication tables and transaction tables are stored in different databases.. finalise on it.

	sequenceCode = Column(Integer) #for synch with mobile
	plutusSequenceCode = Column(Integer) #for synch with plutus

	purchaseId = Column(Integer, ForeignKey('transaction.id'))
	purchaseDetails = relationship("Transaction", primaryjoin=Transaction.id == purchaseId,
						uselist=False)

	transId = Column(String(40))
	transTime = Column(TIMESTAMP)

	invoice = Column(String(12))
	apprCode = Column(String(12))
	rrn = Column(String(12))
	#Note that we're skipping other values such as Batch, Acquirer, Issuer as they will hold same values as that in purchase model

	receiptId = Column(String(20), nullable = True)#TODO Do we need them?
	receiptMobile = Column(String(13), nullable = True)
	receiptEmail = Column(String(30), nullable = True)

	'''
		Least significant byte holds the approval status of the transaction.
			bit 0: enabled(1)/disabled(0)(sequence code) (emulates)
			bit 1: approval status(txn approved(1)/declined(0))
	'''
	status = Column(Integer, default=0)

	terminalDetails = relationship("posDetails", uselist = False, backref = "voids")

	def __init__(self, username, clientId, sequenceCode, plutusSequenceCode, purchaseId, transId, transTime, invoice, apprCode, rrn, status=0):
		self.username = username
		self.clientId = clientId
		self.sequenceCode = sequenceCode
		self.plutusSequenceCode = plutusSequenceCode

		self.purchaseId = purchaseId
		self.transId = transId
		self.transTime = transTime

		self.invoice = invoice
		self.apprCode = apprCode
		self.rrn = rrn
		self.status = status

	def __repr__(self):
		return ('<User: %s | ClientId : %s | SeqCode : %s | PSeqCode : %s | PurchaseId : %s | TransId : %s |'
			' TransTime : %s | Invoice : %s | ApprCode : %s | RRN : %s | status : %s> ') % \
		(self.username, self.clientId, self.sequenceCode, self.plutusSequenceCode, self.purchaseId, self.transId, \
			self.transTime, self.invoice, self.apprCode, self.rrn, self.status)


class Capture(CRUDMixin, db.Model):
	__tablename__ = "capture"

	STATUS = {
		'ENABLED' : 1<<0,
		'APPROVED' : 1<<1
	}

	VALID_TXN = STATUS['ENABLED'] + STATUS['APPROVED']
	@classmethod
	def conditions(cls, condition):
		'''
			Helper module.
			@param condition(string) -> One of the conditions defined in `CONDITIONS`
			@returns sqlalchemy filter condition
		'''
		CONDITIONS = {
			'VALID' : cls.status.op('&')(cls.VALID_TXN) == cls.VALID_TXN
		}
		if condition in CONDITIONS.keys():
			return CONDITIONS[condition]
		raise keyError('No condition with given name exists')

	def isEnabled(self):
		'''
			Helper function
			@returns If this entry is active
		'''
		return self.status & self.STATUS['ENABLED']

	def setEnabled(self, flag):
		'''
			Enables/Disables the transaction entry
			@Params:
				flag(bool) : True/False
		'''
		if flag is False:
			self.status &= ~self.STATUS['ENABLED']
		else:
			self.status |= self.STATUS['ENABLED']

	def isApproved(self):
		'''
			Helper function
			@returns If this entry is approved or declined
		'''
		return self.status & self.STATUS['APPROVED']

	def setApproved(self, flag):
		'''
			Approves/Declines this transaction
			@Params:
				flag(bool) : True/False
		'''
		if flag is False:
			self.status &= ~self.STATUS['APPROVED']
		else:
			self.status |= self.STATUS['APPROVED']

	username = Column(String(16))
	clientId = Column(Integer, ForeignKey('posDetails.clientId')) #Foreign key... authentication tables and transaction tables are stored in different databases.. finalise on it.

	sequenceCode = Column(Integer) #for synch with mobile
	plutusSequenceCode = Column(Integer) #for synch with plutus

	authId = Column(Integer, ForeignKey('transaction.id'))#TODO authId?? change the name damnit
	authDetails = relationship("Transaction", primaryjoin=Transaction.id == authId,
						uselist=False)

	transId = Column(String(40))
	transTime = Column(TIMESTAMP)

	invoice = Column(String(12))
	apprCode = Column(String(12))
	rrn = Column(String(12))
	#Note that we're skipping other values such as Batch, Acquirer, Issuer as they will hold same values as that in purchase model

	receiptId = Column(String(20), nullable = True)
	receiptMobile = Column(String(13), nullable = True)
	receiptEmail = Column(String(30), nullable = True)

	'''
		Least significant byte holds the approval status of the transaction.
			bit 0: enabled(1)/disabled(0)(sequence code) (emulates)
			bit 1: approval status(txn approved(1)/declined(0))
	'''
	status = Column(Integer, default=0)

	terminalDetails = relationship("posDetails", uselist = False, backref = "captures")

	def __init__(self, username, clientId, sequenceCode, plutusSequenceCode, authId, transId, transTime, invoice, apprCode, rrn, status=0):
		self.username = username
		self.clientId = clientId
		self.sequenceCode = sequenceCode
		self.plutusSequenceCode = plutusSequenceCode

		self.authId = authId
		self.transId = transId
		self.transTime = transTime

		self.invoice = invoice
		self.apprCode = apprCode
		self.rrn = rrn
		self.status = status

	def __repr__(self):
		return ('<User: %s | ClientId : %s | SeqCode : %s | PSeqCode : %s | authId : %s | TransId : %s |'
			' TransTime : %s | Invoice : %s | ApprCode : %s | RRN : %s | status : %s> ') % \
		(self.username, self.clientId, self.sequenceCode, self.plutusSequenceCode, self.authId, self.transId, \
			self.transTime, self.invoice, self.apprCode, self.rrn, self.status)

class Settlement(CRUDMixin, db.Model):
	"""
	Only after the settlement is success we store it in this table
	"""
	__tablename__ = "settle"

	username = Column(String(16), index = True)
	clientId = Column(Integer, ForeignKey('posDetails.clientId'), index = True)#,unique = True)
	sequenceCode = Column(Integer, index = True) #for synch with mobile
	plutusSequenceCode = Column(Integer, index = True) #for synch with plutus

	transTime = Column(TIMESTAMP)

	bankName = Column(String(20))

	# TODO unique for all fields is not working the way we expect
	batchNumber = Column(Integer)#,unique = True)
	tid = Column(String(15))#,unique = True)
	mid = Column(String(15))#,unique = True)

	numSales = Column(Integer)
	numVoids = Column(Integer)
	numTips = Column(Integer)
	numRefunds = Column(Integer, default=0)
	numPreAuths = Column(Integer, default=0)

	saleAmount = Column(Integer)
	voidAmount = Column(Integer)
	tipAmount = Column(Integer)
	refundAmount = Column(Integer, default=0)
	preAuthAmount = Column(Integer, default=0)

	totalAmount = Column(Integer)
	batch = Column(Integer)#Local batch Number

	def __init__(self, username, clientId, sequenceCode, plutusSequenceCode, transTime, bankName, batchNumber, \
		tid, mid, numSales, numVoids, numTips, numRefunds, numPreAuths, saleAmount, voidAmount, tipAmount, \
		refundAmount, preAuthAmount, totalAmount, batch):

		self.username = username
		self.clientId = clientId
		self.sequenceCode = sequenceCode
		self.plutusSequenceCode = plutusSequenceCode
		self.transTime = transTime
		self.bankName = bankName
		self.batchNumber = batchNumber
		self.tid = tid
		self.mid = mid
		self.numSales = numSales
		self.numVoids = numVoids
		self.numTips = numTips
		self.numRefunds = numRefunds
		self.numPreAuths = numPreAuths

		self.saleAmount = saleAmount
		self.voidAmount = voidAmount
		self.tipAmount = tipAmount
		self.refundAmount = refundAmount
		self.preAuthAmount = preAuthAmount
		self.totalAmount = totalAmount

		self.batch = batch

	def __repr__(self):
		return '<%s did a settlemnt of batch: %s.>' % (self.username, self.batchNumber)

#we store the last transaction that happened with plutus even if it is an error(not exception) we store it
class LastTransaction(CRUDMixin, db.Model):
	__tablename__ = "lastTransaction"

	clientId = Column(Integer,ForeignKey('posDetails.clientId'), index = True, unique = True)
	transactionType = Column(Integer) # P,V,T,S,E
	#transactionTime = Column(TIMESTAMP, unique = True)
	sequenceCode = Column(Integer)
	rowId = Column(Integer) #row number of the transaction

	def __init__(self, clientId, transactionType, sequenceCode, rowId):
		self.clientId = clientId
		self.transactionType = transactionType
		self.sequenceCode = sequenceCode
		self.rowId = rowId

	def __repr__(self):
		return 'ClientId: %d | Type: %d | Sequence Code: %d | Row Id: %d' % (self.clientId, self.transactionType, self.sequenceCode, self.rowId)

'''
	Note that we are using two relationships for:
	i. tipDetails in Purchase
	ii. purchaseDetails in Tip
	instead of one relationship with a backref. Reason is that there might be a case where there
	is one disabled tip1 and one enabled tip2 both referring to same purchase entry. Ideally both tips shold be able
	to refer to purchase whether its disabled or enabled. Hence, check for 'ENABLED' can't be used from tip. It is used
	here as purchase.tip should return a valid value.

	Note 2: Same is the case with Void relationship
'''
Transaction.tipDetails = relationship("Tip", primaryjoin="and_(Transaction.id == Tip.purchaseId, "
		"Tip.status.op('&')(" + str(Tip.STATUS['ENABLED']) + ") ==" +
		str(Tip.STATUS['ENABLED']) + ")", uselist=False)

Transaction.voidDetails = relationship("Void", primaryjoin="and_(Transaction.id == Void.purchaseId, "
		"Void.status.op('&')(" + str(Void.STATUS['ENABLED']) + ") == " +
		str(Void.STATUS['ENABLED']) + ")", uselist=False)