from ext import db
from sqlalchemy import Column, String, ForeignKey, Float, TIMESTAMP
from sqlalchemy.ext.declarative import declared_attr

#http://pydoc.net/Python/SQLAlchemy/0.7.4/sqlalchemy.ext.declarative/
class CRUDMixin(object):
    __table_args__ = {'extend_existing': True}

    id = db.Column(db.Integer, primary_key=True)
    #When creating tables, SQLAlchemy will automatically set AUTO_INCREMENT on an integer primary key column:

    @classmethod
    def get_by_id(cls, id):
        if any(
            (isinstance(id, basestring) and id.isdigit(),
             isinstance(id, (int, float))),
        ):
            return cls.query.get(int(id))
        return None

    @classmethod
    def create(cls, **kwargs):
        instance = cls(**kwargs)
        return instance.save()

    def update(self, commit=True, **kwargs):
        for attr, value in kwargs.iteritems():
            setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=True):
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit=True):
        db.session.delete(self)
        return commit and db.session.commit()

class User(object):
 
    @declared_attr   
    def username(cls):
        return Column(String(16), ForeignKey('portalLogin.username'), unique = True)
    
    fullname = Column(String(50), nullable = False)
    registrationTime = Column(TIMESTAMP, nullable = True)
    mobileNumber = Column(String(15), nullable = False)
    email = Column(String(30), nullable = False)
    
    def __init__(self, username, fullname, registrationTime, mobileNumber, email):

        self.username = username
        self.fullname = fullname
        self.registrationTime = registrationTime
        self.mobileNumber = mobileNumber
        self.email = email
    

class Employee(User):

    def __init__(self, username, fullname, registrationTime, mobileNumber, email):
        User.__init__(self, username, fullname, registrationTime, mobileNumber, email)

class Bank(object):
    
    bankName = Column(String(50), nullable = False)
    bankBranch = Column(String(50), nullable = False)
    ifsc = Column(String(12), nullable = False)#TODO swift code for international accounts
    acNumber = Column(String(34), nullable = False)
    
    MDR_Debit=Column(Float)
    MDR_Credit=Column(Float)
    MDR_CreditGold=Column(Float)

    def __init__(self, bankName, bankBranch, ifsc, acNumber, MDR_Debit, MDR_Credit, MDR_CreditGold):
        self.bankName = bankName
        self.bankBranch = bankBranch
        self.ifsc = ifsc
        self.acNumber = acNumber
        
        self.MDR_Debit = MDR_Debit
        self.MDR_Credit = MDR_Credit
        self.MDR_CreditGold = MDR_CreditGold

class Address(object):
    
    address1 = Column(String(50), nullable = False)#Part 1
    address2 = Column(String(50), nullable = True)#Part 2
    city = Column(String(20), nullable = False)
    district = Column(String(20), nullable = False)
    state = Column(String(20), nullable = False)
    pincode = Column(String(6), nullable = False)
    country = Column(String(20), nullable = False)
   
    def __init__(self, address1, address2, city, district, state, pincode, country):
        self.address1 = address1
        self.address2 = address2
        self.city = city
        self.district = district
        self.state = state
        self.pincode = pincode
        self.country = country
 
class BaseRole(User, Bank, Address):
    def __init__(self, username, fullname, registrationTime, mobileNumber, email, \
        bankName, bankBranch, ifsc, acNumber, MDR_Debit, MDR_Credit, MDR_CreditGold, \
        address1, address2, city, district, state, pincode, country):

        User.__init__(self, username, fullname, registrationTime, mobileNumber, email)
        Bank.__init__(self, bankName, bankBranch, ifsc, acNumber, MDR_Debit, MDR_Credit, MDR_CreditGold)
        Address.__init__(self, address1, address2, city, district, state, pincode, country) 
