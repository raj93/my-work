from ext import db
from flask.ext.login import UserMixin
from src.models.base import CRUDMixin, User, BaseRole, Employee
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, TIMESTAMP, text
from sqlalchemy.orm import relationship,backref


class portalLogin(UserMixin, CRUDMixin, db.Model):
	"""	
	This table stores portal access details for all users.
	List of users:
		i. Admin
		ii. Company
		iii. Agent
		iv. Merchant
		
		And employees of each of above
	"""
	__tablename__ = "portalLogin"

	username = Column(String(16), unique = True)
	password = Column(String(128), nullable = False)#TODO can make storage efficient by storing it in binary instead of hex
	salt = Column(String(64), nullable = False)
	role = Column(Integer, nullable = False, default = 255)
	subRole = Column(Integer, nullable = False, default = 255)
	
	isActivated = Column(Boolean, default = False)

	shouldChangePassword = Column(Boolean,default=False)
	rn = Column(String(32),default='1234')

	# why both are because we are using time stamp signer.
	resetSignedToken = Column(String(40),default='void')
	resetToken = Column(String(30),default='void')

	# to put limit on incorrect password attempts
	invalidAttempts = Column(Integer, default = 0)
	lastHoldedTime = Column(TIMESTAMP, nullable = True) 

	# to allow the most recent login session
	loginTime = Column(TIMESTAMP,nullable=True)

	def __init__(self, username, password, salt, role, subRole, isActivated,shouldChangePassword=False):
		self.username = username
		self.password = password
		self.salt = salt
		self.role = role
		self.subRole = subRole
		self.isActivated = isActivated
		self.shouldChangePassword = shouldChangePassword

	def __repr__(self):
		return 'Username: %s | Password: %s | Role: %s | Subrole: %s| Activated: %s' % (self.username, self.password,str(self.role), str(self.subRole), self.isActivated)

	"""
	The is_authenticated method denotes whether user is authenticated using his credentials or not

	The is_active method should return True for users unless they are inactive, for example because they have been banned.

	The is_anonymous method should return whether the user is anonymous or not.

	Finally, the get_id method should return a unique identifier for the user, in unicode format. We use the unique id generated by the database layer for this.
	"""

	def is_active(self):
		return self.isActivated
	
	def get_id(self):
		return unicode(self.id)

	def is_anonymous(self):
		return False

	def is_authenticated(self):

		# if 'TESTING' flag is there in config this flag does not mean anything.
		from flask import session
		from datetime import datetime

		if 'loginTime' not in session:
			return False

		if self.loginTime and self.loginTime.strftime("%Y-%m-%d %X") != str(session['loginTime']):
			return False

		return True


class Admin(CRUDMixin, User, db.Model):
	"""
		Admins of Aasaanpay
		Use Employees for various roles for a particular bank.
	"""

	__tablename__ = "admin"

	loginDetails = relationship("portalLogin", uselist=False, backref = backref("admin", uselist=False))

	def __init__(self, username, fullname, registrationTime, mobileNumber, email):
		User.__init__(self, username, fullname, registrationTime, mobileNumber, email)

	def __repr__(self):
		return "user: %s | fullname: %s | mobile: %s | email : %s " \
				% (self.username, self.fullname, self.mobileNumber, self.email)

class AdminEmployee(CRUDMixin, Employee, db.Model):
	"""
	Employees of the admin
	"""
	__tablename__ = "adminEmployee"
	admin = Column(String(16), ForeignKey('admin.username'), nullable = False)
	loginDetails = relationship("portalLogin", uselist=False, backref = backref("adminEmployee", uselist=False))

	parentDetails = relationship("Admin", backref = "employees")

	def __init__(self, username, fullname, registrationTime, mobileNumber, email,admin):
		Employee.__init__(self, username, fullname, registrationTime, mobileNumber, email)
		self.admin = admin
	
	def __repr__(self):
		return "user: %s | fullname: %s | mobile: %s | email : %s | role: %s" \
				% (self.username, self.fullname, self.mobileNumber, self.email, str(self.role))

class Company(CRUDMixin, BaseRole, db.Model):
	"""
		Agent personal/contact details are present in this table
		Agents = Sellers who sells POS os a particular company to merchants
	"""

	__tablename__ = "company"

	nameOnReceipt = Column(String(30), nullable = False)#Usecase: Tags like "Powered by" on Receipt
	loginDetails = relationship("portalLogin", uselist=False, backref = backref("company", uselist=False))

	def __init__(self, username, fullname, registrationTime, mobileNumber, email, \
		bankName, bankBranch, ifsc, acNumber, MDR_Debit, MDR_Credit, MDR_CreditGold, \
		address1, address2, city, district, state, pincode, country, \
		nameOnReceipt = None):

		BaseRole.__init__(self, username, fullname, registrationTime, mobileNumber, email, \
			bankName, bankBranch, ifsc, acNumber, MDR_Debit, MDR_Credit, MDR_CreditGold, \
			address1, address2, city, district, state, pincode, country)

		if nameOnReceipt is None:
			self.nameOnReceipt = fullname
		else:
			self.nameOnReceipt = nameOnReceipt

	def __repr__(self):
		return "user: %s | Company Name: %s | registrationTime: %s | mobile: %s | email : %s | " \
			"bankName: %s | ifsc: %s | acNumber: %s | MDR_Debit: %s | MDR_Credit: %s | MDR_CreditGold : %s | " \
			"address1: %s | address2: %s | city: %s | district: %s | state: %s | pincode: %s" \
				% (self.username, self.fullname, self.registrationTime, self.mobileNumber, self.email, \
					self.bankName, self.ifsc, self.acNumber, str(self.MDR_Debit), str(self.MDR_Credit), str(self.MDR_CreditGold), \
					self.address1, self.address2, self.city, self.district, self.state, self.pincode)

class CompanyEmployee(CRUDMixin, Employee, db.Model):
	__tablename__ = "companyEmployee"

	company = Column(String(16), ForeignKey('company.username'),nullable=False) #to which company employee belongs
	loginDetails = relationship("portalLogin", uselist=False, backref = backref("companyEmployee", uselist=False))

	parentDetails = relationship("Company", backref = "employees")

	def __init__(self, username, fullname, registrationTime, mobileNumber, email,company):
		Employee.__init__(self, username, fullname, registrationTime, mobileNumber, email)
		self.company = company

	def __repr__(self):
		return "user: %s | fullname: %s | mobile: %s | email : %s | role: %s" \
				% (self.username, self.fullname, self.mobileNumber, self.email, str(self.role))

class Agent(CRUDMixin, BaseRole, db.Model):
	"""
		Agent personal/contact details are present in this table
		Agents = Sellers who sells POS os a particular company to merchants
	"""

	__tablename__ = "agent"
	
	nameOnReceipt = Column(String(30), nullable = False)#Usecase: Tags like "Powered by" on Receipt
	company = Column(String(16), ForeignKey('company.username'), nullable = False)
	loginDetails = relationship("portalLogin", uselist = False, backref = backref("agent", uselist = False))
	
	companyDetails = relationship("Company", backref = "agents")

	def __init__(self, username, fullname, registrationTime, mobileNumber, email, \
		bankName, bankBranch, ifsc, acNumber, MDR_Debit, MDR_Credit, MDR_CreditGold, \
		address1, address2, city, district, state, pincode, country, \
		company, nameOnReceipt = None):

		BaseRole.__init__(self, username, fullname, registrationTime, mobileNumber, email, \
			bankName, bankBranch, ifsc, acNumber, MDR_Debit, MDR_Credit, MDR_CreditGold, \
			address1, address2, city, district, state, pincode, country)

		self.company = company
		if nameOnReceipt is None:
			self.nameOnReceipt = company
		else:
			self.nameOnReceipt = nameOnReceipt

	def __repr__(self):
		return "company: %s | user: %s | fullname: %s | registrationTime: %s | mobile: %s | email : %s | " \
			"bankName: %s | ifsc: %s | acNumber: %s | MDR_Debit: %s | MDR_Credit: %s | MDR_CreditGold : %s" \
			"address1: %s | address2: %s | city: %s | district: %s | state: %s | pincode: %s" \
				% (self.company, self.username, self.fullname, self.registrationTime, self.mobileNumber, self.email, \
					self.bankName, self.ifsc, self.acNumber, str(self.MDR_Debit), str(self.MDR_Credit), str(self.MDR_CreditGold), \
					self.address1, self.address2, self.city, self.district, self.state, self.pincode)

class AgentEmployee(CRUDMixin, Employee, db.Model):
	__tablename__ = "agentEmployee"

	loginDetails = relationship("portalLogin", uselist=False, backref = backref("agentEmployee", uselist=False))
	agent = Column(String(16), ForeignKey('agent.username'), nullable = False)

	parentDetails = relationship("Agent", backref = "employees")

	def __init__(self, username, fullname, registrationTime, mobileNumber, email,agent):
		Employee.__init__(self, username, fullname, registrationTime, mobileNumber, email)
		self.agent = agent

	def __repr__(self):
		return "user: %s | fullname: %s | mobile: %s | email : %s " \
				% (self.username, self.fullname, self.mobileNumber, self.email, str(self.role))

class Merchant(CRUDMixin, BaseRole, db.Model):
	__tablename__ = "merchant"

	BITMAP = {
		'MERCHANT_VERIFICATION' : 1,
		'MERCHANT_MID_ALLOTED' : 2 ,
		'MERCHANT_LOCK' : 4,
		'MERCHANT_DISABLE' : 8
	}

	agent = Column(String(16), ForeignKey('agent.username'), nullable = True)
	company = Column(String(16), ForeignKey('company.username'), nullable = False)

	mid = Column(String(20))

	bitmap = Column(Integer,default=0)

	dob =Column(String(20), nullable = False)
	idProof = Column(String(50))
	email = Column(String(30), nullable = False)#Overriding User `email` as a single merchant can have multiple businesses registered
	nameOnReceipt = Column(String(30), nullable = False)
	
	posDetails = relationship("posDetails", backref = backref("merch", uselist=False))   
	companyDetails = relationship("Company", backref = "merchants")
	loginDetails = relationship("portalLogin", uselist=False, backref = backref("merchant", uselist=False))
	dongles = relationship("dongleInventory",backref = backref("owner", uselist=False) )
	
	currency = Column(String(5), nullable=False, default="INR")

	def __init__(self, username, fullname, registrationTime, mobileNumber, email, \
		bankName, bankBranch, ifsc, acNumber, MDR_Debit, MDR_Credit, MDR_CreditGold, \
		dob, idProof,address1, address2, city, district, state, pincode, country,currency, company, \
		agent = None, nameOnReceipt = None):

		BaseRole.__init__(self, username, fullname, registrationTime, mobileNumber, email, \
			bankName, bankBranch, ifsc, acNumber, MDR_Debit, MDR_Credit, MDR_CreditGold, \
			address1, address2, city, district, state, pincode, country)	    

		self.dob = dob
		self.idProof = idProof
		self.company = company
		self.currency = currency
		
		if agent is not None:
			self.agent = agent

		if nameOnReceipt is None:
			self.nameOnReceipt = fullname
		else:
			self.nameOnReceipt = nameOnReceipt


	def __repr__(self):
		return "company: %s | agent: %s | user: %s | fullname: %s | dob : %s | idproof: %s | mobile: %s | email : %s | "\
			"bankName: %s | ifsc: %s | acNumber: %s | MDR_Debit: %s | MDR_Credit: %s | MDR_CreditGold : %s | " \
			"address1: %s | address2: %s | city: %s | district: %s | state: %s | pincode: %s" \
				% (self.company, self.agent, self.username, self.fullname, self.dob, self.idProof, self.mobileNumber, self.email, \
					 self.bankName, self.ifsc, self.acNumber, str(self.MDR_Debit), str(self.MDR_Credit), str(self.MDR_CreditGold), \
					self.address1, self.address2, self.city, self.district, self.state, self.pincode)


class MerchantEmployee(CRUDMixin, Employee, db.Model):
	__tablename__ = "merchantEmployee"

	loginDetails = relationship("portalLogin", uselist=False, backref = backref("merchantEmployee", uselist=False))
	merchant = Column(String(16), ForeignKey('merchant.username'), nullable = False)

	parentDetails = relationship("Merchant", backref = "employees")


	def __init__(self, username, fullname, registrationTime, mobileNumber, email,merchant):
		Employee.__init__(self, username, fullname, registrationTime, mobileNumber, email)
		self.merchant = merchant

	def __repr__(self):
		return "user: %s | fullname: %s | mobile: %s | email : %s  | role: %s" \
				% (self.username, self.fullname, self.mobileNumber, self.email, str(self.subRole))
