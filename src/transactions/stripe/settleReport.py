from src.mAuthenticate import common
from ext import db
from src.models import Transaction
from sqlalchemy import and_


class SettleReport:

	def __init__(self, details, bundle):
		self.details = details
		self.bundle = bundle

	def process(self):
		report = {}
		report['HDFC'] = {}

		mynumSales = 0
		mysaleAmount = 0
		mynumRefunds = 0
		myRefundAmount = 0
		mynumTips = 0
		mytipAmount = 0
		mynumVoids = 0
		myVoidAmount =	 0
		mynumPreAuths = 0
		myPreAuthAmount = 0
		mynumUncapturedPreAuths = 0

		txns = Transaction.query.filter(Transaction.clientId == self.bundle['clientId'],
			Transaction.conditions('VALID')).all()

		for txn in txns:
			if txn.ttype == txn.TTYPE['PURCHASE']:
				if txn.isSimpleTxn():
					mynumSales += 1
					mysaleAmount += txn.amount + txn.amount2
				elif txn.isVoidTxn():
					mynumSales += 1
					mynumVoids += 1
					myVoidAmount += txn.amount + txn.amount2
					mysaleAmount += txn.amount + txn.amount2
				elif txn.isTipTxn():
					mynumSales += 1
					mynumTips += 1
					mysaleAmount += txn.amount + txn.amount2
					mytipAmount += txn.tipAmount
				elif txn.isVoidedTipTxn():
					#incase of VOID(TIP_ADJUST) entry in settlement there is no seperate tip entry in plutus response.
					mynumSales += 1
					mynumVoids += 1
					mysaleAmount += txn.amount + txn.amount2 + txn.tipAmount
					myVoidAmount += txn.amount + txn.amount2 + txn.tipAmount

			elif txn.ttype == txn.TTYPE['REFUND']:
				if txn.isSimpleTxn():
					mynumRefunds += 1
					myRefundAmount += txn.amount
				elif txn.isVoidTxn():
					#TODO not including it in either refund amount or void amount. One way is to add one more row for Void(Refund)
					pass

			elif txn.ttype == txn.TTYPE['PREAUTH']:
				if txn.isCapturedTxn():
					mynumPreAuths += 1
					myPreAuthAmount += txn.amount
				elif not txn.isCaptured():
					mynumUncapturedPreAuths += 1

		sale = {'count' : mynumSales,
				'total' : 'Rs ' + str(mysaleAmount/100.0)}

		preAuth = {'count' : mynumPreAuths,
					'total' : 'Rs ' + str(myPreAuthAmount/100.0)}

		void = {'count' : mynumVoids,
				'total' : '-Rs ' + str(myVoidAmount/100.0)}

		refund = {'count' : mynumRefunds,
				'total' : '-Rs ' + str(myRefundAmount/100.0)}


		#TODO if refund is unavailable for this tid, skip it
		total = mysaleAmount + myPreAuthAmount - myVoidAmount
		netTotal = total - myRefundAmount

		summary = {'title' : 'Current Batch',
					'mid' : ' ',
					'ttime' : self.bundle['timeStamp'],
					'batch' : int(self.details.batch),
					'sale' : sale,
					'preAuth' : preAuth,
					'void' : void,
					'total' : str(total/100.0),
					'refund' : refund,
					'netTotal' : str(netTotal/100.0),
					'type' : common.BATCH_TOTALS_REPORT}#for debugging purposes

		report['HDFC'][str(self.bundle['clientId'])] = summary
		report['isSettled'] = (netTotal == 0)

		report = {'report' : report}#settleReport response format or errorMessage

		if mynumUncapturedPreAuths != 0:#TODO make it more intuitive after a switch integration
			report['iError'] = 'Pending PreAuths present in settlement batch'

		#we got a result so we are incrementing sequence codes.
		self.details.minSequenceCode = self.details.sequenceCode
		self.details.sequenceCode += 1
		#details.plutusSequenceCode = plutusRespSequenceCode

		common.disableLastTransaction(self.bundle['clientId'], common.SETTLEMENT_REPORT, self.details.sequenceCode - 1, 0)
		#db.session.commit() #ERRORPRONE
		return report
