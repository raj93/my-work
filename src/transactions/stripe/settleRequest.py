from src.mAuthenticate import common
from ext import db
from src.models import Transaction, Void, Tip, Capture, Settlement, SettledCapture
from sqlalchemy import and_

class SettleRequest:

	def __init__(self, details, bundle):
		self.details = details
		self.bundle = bundle

	def process(self):
		output = {}

		settlement_entries = []

		mynumSales = 0
		mysaleAmount = 0
		mynumRefunds = 0
		myRefundAmount = 0
		myPreAuthAmount = 0

		mynumTips = 0
		mytipAmount = 0
		mynumVoids = 0
		myVoidAmount =	 0
		mynumPreAuths = 0

		txns = Transaction.query.filter(Transaction.clientId==self.bundle['clientId'],
			Transaction.conditions('VALID')).all()

		for txn in txns:
			if txn.ttype == txn.TTYPE['PURCHASE']:
				if txn.isSimpleTxn():
					mynumSales += 1
					mysaleAmount += txn.amount + txn.amount2
				elif txn.isVoidTxn():
					mynumSales += 1
					mynumVoids += 1
					myVoidAmount += txn.amount + txn.amount2
					mysaleAmount += txn.amount + txn.amount2
				elif txn.isTipTxn():
					mynumSales += 1
					mynumTips += 1
					mysaleAmount += txn.amount + txn.amount2
					mytipAmount += txn.tipAmount
				elif txn.isVoidedTipTxn():
					#incase of VOID(TIP_ADJUST) entry in settlement there is no seperate tip entry in plutus response.
					mynumSales += 1
					mynumVoids += 1
					mysaleAmount += txn.amount + txn.amount2 + txn.tipAmount
					myVoidAmount += txn.amount + txn.amount2 + txn.tipAmount

			elif txn.ttype == txn.TTYPE['REFUND']:
				if txn.isSimpleTxn():
					mynumRefunds += 1
					myRefundAmount += txn.amount
				elif txn.isVoidTxn():
					#TODO not including it in either refund amount or void amount. One way is to add one more row for Void(Refund)
					pass

			elif txn.ttype == txn.TTYPE['PREAUTH']:
				if txn.isCapturedTxn():
					mynumPreAuths += 1
					myPreAuthAmount += txn.amount

		total = mysaleAmount + myPreAuthAmount - myVoidAmount
		netTotal = total - myRefundAmount

		if netTotal == 0:
			output = {
						'isSettled': False,
						'ierror': 'No transactions in this batch',
						}
			return output

		settlement_entries.append(Settlement(self.bundle['username'], self.bundle['clientId'], \
			self.details.sequenceCode, self.details.plutusSequenceCode, self.bundle['timeStamp'], 'HDFC', \
			self.details.batch, self.details.clientId, '', mynumSales, mynumVoids, mynumTips, mynumRefunds, \
			mynumPreAuths, mysaleAmount, myVoidAmount, mytipAmount, myRefundAmount, myPreAuthAmount, \
			netTotal, self.details.batch))

		void_entries = Void.query.filter(Void.clientId==self.bundle['clientId'],
							Void.conditions('VALID')).all()
		tip_entries = Tip.query.filter(Tip.clientId==self.bundle['clientId'],
							Tip.conditions('VALID')).all()
		capture_entries = Capture.query.filter(Capture.clientId==self.bundle['clientId'],
							Capture.conditions('VALID')).all()
		#Add settlements of different acquirers or TIDs
		for settlement_row in settlement_entries:
			db.session.add(settlement_row)

		#Change status of Purchase entries to Settled
		for txn in txns:
			txn.setSettled()

		#Add settled entries to settled database
		from src.models import SettledTransaction, SettledVoid, SettledTip

		settledTxns = []

		for txn in txns:
			settledTxn = SettledTransaction(txn, self.details.batch)
			settledTxns.append(settledTxn)
			db.session.add(settledTxn)

		for void in void_entries:#ERRORPRONE foreign key restraint..
			settledVoid = SettledVoid(void)
			db.session.add(settledVoid)

		for tip in tip_entries:
			settledTip = SettledTip(tip)
			db.session.add(settledTip)

		for capture in capture_entries:
			settledCapture = SettledCapture(capture)
			db.session.add(settledCapture)

		#TODO can remove this
		db.session.commit()


		#Cleaning up settled entries in unsettled database
		for tip_row in tip_entries:
			db.session.delete(tip_row)

		for void_row in void_entries:
			db.session.delete(void_row)

		for capture_row in capture_entries:
			db.session.delete(capture_row)

		for txn in txns:
			db.session.delete(txn)

		db.session.commit()

		# Update batch count in posDetails. This count is common in the series of settlements made by this settlement request
		self.details.batch += 1
		output = {'isSettled' : True}

		if len(txns) != 0:
			common.notifyMerchant(self.details, settlement_entries, settledTxns)

		#We got a result so we are incrementing sequence codes
		self.details.minSequenceCode = self.details.sequenceCode
		self.details.sequenceCode += 1
#		details.plutusSequenceCode = plutusRespSequenceCode

		common.disableLastTransaction(self.bundle['clientId'], common.SETTLEMENT, self.details.sequenceCode - 1, 0)
		return output
