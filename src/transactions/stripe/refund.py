import stripe
from src.mAuthenticate import common
from ext import db
from src.models import Transaction
from flask import current_app


class Refund:

	#What happens when two requests with same TransId i.e., same ksn & sequenceCode comes at same timestamp. And response to first request after the response of second request?
	#Planning to store all the success requests and show them by grouping based on TransID
	#transID different for any two legitimate requests because of difference in KSN!! -> so grouping based on transID would be bullshit.. grouping based on sequenceCode gives good results..

	def __init__(self, details, bundle):
		self.details = details
		self.bundle = bundle

	def create_token(self):
		token = stripe.Token.create(card={
			"number": self.cardNumber,
			"exp_month": self.MM,
			"exp_year": self.YY
			},)
		return token

	def process(self):
		output = {}

		# populating variables necessary for stripe
		self.cardNumber = self.bundle['pan']
		self.chName = self.bundle['chName']
		self.MM = self.bundle['expiry'][2:4]
		self.YY = self.bundle['expiry'][:2]

		if current_app.config["STRIPE_LIVE"]:
			stripe.api_key = current_app.config['STRIPE_LIVE_KEY']
		else:
			stripe.api_key = current_app.config['STRIPE_DEMO_KEY']

		try:
			#Create a new receipient
			recipient = stripe.Recipient.create(
							name = "John Doe",
							type = "individual",
							card = self.create_token().id,
							)

			transfer = stripe.Transfer.create(amount=int(self.bundle['baseAmount']/current_app.config['STRIPE_EXCHANGE_RATE']),
				currency = 'usd',#self.bundle['currencyCode'], Only USD are currently supported
				recipient = recipient,
				)

			# setting the transid
			self.transId =  transfer.id
		except stripe.error.InvalidRequestError as err:
			current_app.logger.error(err.message, exc_info=True)
			output.update({'errorCode': err.message})
			return output

		except Exception as err:
			current_app.logger.error(err, exc_info=True)
			error = str(err)
			if error.find("stripe") != -1:
				output.update({'errorCode': 'Your card is declined'})
			else:
				output.update({'errorCode':error})

			return output

		cn = self.cardNumber
		maskedPan = cn[0: 6] + '*'*(len(cn) - 10) + cn[len(cn) - 4: len(cn)]

		card = {'cardNumber' : '*'*6 + maskedPan[6:],
				'MM' : self.MM,
				'YY' : self.YY
				}

		card['chName'] = self.chName

		merch = self.details.merchantDetails

		merchant = {'id' : str(merch.id),
					'name' : merch.nameOnReceipt,
					'address' : merch.address1 + merch.address2,
					'city' : merch.city}

		transactionInfo = {'baseAmount' : self.bundle['baseAmount'],
							'acquirer' : 'HDFC',
							'invoice' : str(self.details.sequenceCode),
							'batchNumber' : str(self.details.batch),
							'transactionTime' : self.bundle['timestamp'],
							'apprCode' : '000000',
							'rrn' : '000000000020',
							'transId' : self.transId,
							'terminalId' : self.bundle['clientId']
				}

		entry = Transaction(self.bundle['username'], self.bundle['clientId'], self.details.sequenceCode, \
			self.details.plutusSequenceCode, self.transId, self.bundle['timestamp'], self.bundle['baseAmount'], \
			0, card['chName'], card['cardNumber'], card['MM'], card['YY'], \
			self.bundle['inputType'], transactionInfo['invoice'], transactionInfo['batchNumber'], \
			transactionInfo['acquirer'], transactionInfo['apprCode'], transactionInfo['rrn'], \
			self.bundle['clientId'], merchant['id'], self.bundle['token'], Transaction.TTYPE['REFUND'], \
			Transaction.APPROVED_ONLINE, self.details.company, self.details.agent, self.details.merchant)

		db.session.add(entry)
		db.session.commit()

		rowId = entry.id
		tranType = common.REFUND
		output.update({'merchant' : merchant, 'card' : card, 'transactionInfo': transactionInfo})


		#we got a result so we are incrementing sequence codes.
		self.details.minSequenceCode = self.details.sequenceCode
		self.details.sequenceCode += 1
		#details.plutusSequenceCode = plutusRespSequenceCode

		common.disableLastTransaction(self.bundle['clientId'], tranType, self.details.sequenceCode - 1, rowId)
		return output
