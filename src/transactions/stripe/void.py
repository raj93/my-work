from flask import current_app
from src.mAuthenticate import common
from ext import db
from src.models import Void as void_table
import stripe

class Void:

	def __init__(self, details, txn, bundle):
		self.details = details
		self.txn = txn
		self.bundle = bundle

	def process(self):
		if current_app.config["STRIPE_LIVE"]:
			stripe.api_key = "rXn8L8t7AQOv30Vw72RUnF0bN0PKFvIA"
		else:
			stripe.api_key = "j590LNM0G7iun5At6QHOv6PrYiLbAmPM"
		
		output = {}

		try:
			if self.txn.ttype == self.txn.TTYPE['PURCHASE']:
				ch = stripe.Charge.retrieve(self.txn.transId)
				ch.refund()
			elif self.txn.ttype == self.txn.TTYPE['REFUND']:
				tr = stripe.Transfer.retrieve(self.txn.transId)
				if tr['status'] == 'paid':
					output.update({'errorCode': 'You may not cancel transfers that have already been paid out, or automatic Stripe transfers'})
					return output
		except Exception as err:
			error = str(err)
			print err
			if 'stripe' in error:
				output.update({'errorCode':'void not allowed for this transaction'})
				return output
			else:
				output.update({'errorCode':error})
				return output
		
		card = {'cardNumber' : self.txn.maskedPan,
				'MM' : self.txn.expiryMonth,
				'YY' : self.txn.expiryYear
				}
		
		card['chName'] = self.txn.chname
		
		merch = self.txn.merchantDetails

		merchant = {'id' : str(merch.id),
					'name' : merch.nameOnReceipt,
					'address' : merch.address1+merch.address2 ,
					'city' : merch.city}		

		transactionInfo = {'baseAmount' : self.bundle['amount'],
							'acquirer' : 'HDFC',
							'invoice' : str(self.details.sequenceCode),
							'batchNumber' : str(self.details.batch),
							'transactionTime' : self.bundle['timestamp'],
							'apprCode' : '000000',
							'rrn' : '000000000020',
							'transId' : self.txn.transId,
							'terminalId' : self.bundle['clientId']
				}			
		
		if not self.txn.isVoided():
			self.txn.setVoid()
			if self.txn.isTipped():
				self.txn.tipDetails.setEnabled(False)
		else:
			#voiding a void 
			current_app.logger.warning('Voiding a void is not possible. ID:' + self.bundle['transId'] + ' ClientID:' + str(self.bundle['clientId']))

		#Adding entry to the void table with reference to txn entry(id)
		entry = void_table(self.bundle['username'], self.bundle['clientId'], self.details.sequenceCode, self.details.plutusSequenceCode, \
			self.txn.id, self.bundle['transId'], self.bundle['timestamp'], transactionInfo['invoice'], transactionInfo['apprCode'], \
			transactionInfo['rrn'], void_table.APPROVED)
		db.session.add(entry)
		db.session.commit()

		rowId = entry.id
		tranType = common.VOID
		output.update({'merchant' : merchant, 'card' : card, 'transactionInfo': transactionInfo})
	
		#we got a result so we are incrementing sequence codes.
		self.details.minSequenceCode = self.details.sequenceCode
		self.details.sequenceCode += 1
		
		common.disableLastTransaction(self.bundle['clientId'], tranType, self.details.sequenceCode - 1, rowId)
		return output
