from flask import current_app
from src.mAuthenticate import common
from ext import db
from src.models import Capture
import stripe

class CaptureAuth:

	def __init__(self, details, txn, bundle):
		self.details = details
		self.txn = txn
		self.bundle = bundle

	def process(self):
		if current_app.config["STRIPE_LIVE"]:
			stripe.api_key = "rXn8L8t7AQOv30Vw72RUnF0bN0PKFvIA"
		else:
			stripe.api_key = "j590LNM0G7iun5At6QHOv6PrYiLbAmPM"

		output = {}

		try:
			ch = stripe.Charge.retrieve(self.txn.transId)
			ch.capture(amount = self.bundle['captureAmount'])
		except Exception as err:
			error = str(err)
			print err
			if 'stripe' in error:
				output.update({'errorCode':'capture not allowed for this transaction'})
				return output
			else:
				output.update({'errorCode':error})
				return output

		card = {'cardNumber' : self.txn.maskedPan,
				'MM' : self.txn.expiryMonth,
				'YY' : self.txn.expiryYear
				}

		card['chName'] = self.txn.chname

		merch = self.txn.merchantDetails

		merchant = {'id' : str(merch.id),
					'name' : merch.nameOnReceipt,
					'address' : merch.address1+merch.address2 ,
					'city' : merch.city}

		transactionInfo = {'baseAmount' : self.bundle['captureAmount'],
							'acquirer' : 'HDFC',
							'invoice' : str(self.details.sequenceCode),
							'batchNumber' : str(self.details.batch),
							'transactionTime' : self.bundle['timestamp'],
							'apprCode' : '000000',
							'rrn' : '000000000020',
							'transId' : self.txn.transId,
							'terminalId' : self.bundle['clientId']
				}


		#Adding entry to the void table with reference to txn entry(id)
		entry = Capture(self.bundle['username'], self.bundle['clientId'], self.details.sequenceCode, \
			self.details.plutusSequenceCode, self.txn.id, self.bundle['transId'], self.bundle['timestamp'], \
			transactionInfo['invoice'], transactionInfo['apprCode'], transactionInfo['rrn'], Capture.VALID_TXN)

		self.txn.amount2 = self.txn.amount
		self.txn.amount = self.bundle['captureAmount']
		self.txn.setCaptured()

		db.session.add(entry)
		db.session.commit()

		rowId = entry.id
		tranType = common.PREAUTH
		output.update({'merchant' : merchant, 'card' : card, 'transactionInfo': transactionInfo})

		#we got a result so we are incrementing sequence codes.
		self.details.minSequenceCode = self.details.sequenceCode
		self.details.sequenceCode += 1

		common.disableLastTransaction(self.bundle['clientId'], tranType, self.details.sequenceCode - 1, rowId)
		return output
