from flask import request, current_app
from datetime import datetime
import simplejson as json

from src.mAuthenticate import common
from ext import db
from sqlalchemy import and_
from src.models import Transaction, posDetails
import src.transactions.demo.captureAuth
import src.transactions.stripe.captureAuth
from src.utils.validate import validateEntry

class CaptureAuth:

	CAPTURE_AUTH_TAGS = {
		'terminalInfo': dict,
		'authentication': dict,
		'payload': dict,
		}

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp': unicode,
		#'appVersion': unicode,
		}

	AUTHENTICATION_TAGS = {
		'sequenceCode': int,
		'clientId': int,
		'sessionToken': 'SESSION',
		}

	#this transid is of the txn for which this preauth is requested
	PAYLOAD_TAGS = {
		'transId': 'TRANSID',
		'amount': 'AMOUNT',
		}

	def __init__(self):
		self.json_data = request.get_json()
		self.bundle = {}

	def isValidRequest(self):
		if self.json_data == None:
			return False

		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.CAPTURE_AUTH_TAGS.items()):
			return False

		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['authentication'] and validateEntry(self.json_data['authentication'][k], v) \
							for k, v in self.AUTHENTICATION_TAGS.items()):
			return False

		if not all(k in self.json_data['payload'] and validateEntry(self.json_data['payload'][k], v) \
							for k, v in self.PAYLOAD_TAGS.items()):
			return False

		return True

	def populateEntries(self):
		self.bundle['deviceId'] = self.deviceId = self.json_data['terminalInfo']['deviceId']
		self.bundle['timestamp'] = self.timestamp = common.getCurrentTimeStamp()

		#self.bundle['appVersion'] = self.appVersion = self.json_data['terminalInfo']['appVersion']

		self.bundle['reqSequenceCode'] = self.reqSequenceCode = self.json_data['authentication']['sequenceCode']
		self.sessionToken = self.json_data['authentication']['sessionToken']
		self.bundle['clientId'] = self.clientId = self.json_data['authentication']['clientId']

		self.bundle['txnId'] = self.txnId = self.json_data['payload']['transId']
		self.bundle['captureAmount'] = self.json_data['payload']['amount']

	def poplulateTransactionEntries(self,txn, details):
		#TODO shift the currency code to txn ?
		self.bundle['currencyCode'] = self.CurrencyCode =details.currency
		self.bundle['acquirer'] = self.acquirer = txn.acquirer
		self.bundle['invoice'] = self.invoice = txn.invoice
		self.bundle['ksn'] = self.ksn = self.txnId.split('_')[0]
		self.bundle['transId'] = self.transId = str(self.ksn) + '_' + str(details.plutusSequenceCode)

	def captureAuth(self, details, txn):
		if details.processor == posDetails.PROCESSORS['DEMO']:
			fn = src.transactions.demo.captureAuth
		elif details.processor == posDetails.PROCESSORS['STRIPE']:
			fn = src.transactions.stripe.captureAuth
		elif details.processor == posDetails.PROCESSORS['ATOS']:
			pass
		elif details.processor == posDetails.PROCESSORS['PLUTUS']:
			pass
			#fn = src.transactions.plutus.captureAuth

		a = fn.CaptureAuth(details, txn, self.bundle)
		return a.process()

	def process(self):
		if not self.isValidRequest(): return common.errorMessage('INVALID_REQUEST')

		self.populateEntries()
		isAuthorised, self.username = common.isAuthorisedRequest(self.clientId, self.sessionToken, self.deviceId)
		if isAuthorised == False: return common.errorMessage(self.username)#Second return value is errorCode

		canI, details = common.canIdoTransaction(self.clientId, common.PREAUTH, self.username)
		if not canI: return common.errorMessage('SERVER_BUSY')

		isValid, details = common.validateSequenceCode(self.reqSequenceCode, details)
		if not isValid: return details # Second return value is output to be returned

		#txn entry to be captured
		txn = Transaction.query.filter(and_(Transaction.ttype == Transaction.TTYPE['PREAUTH'],
											Transaction.transId == self.txnId,
											Transaction.conditions('VALID'))).first()

		if txn is None:
			if current_app.config['DEBUG']:
				output = {'ierror':'No valid txn found'}
			else:
				output = {}
			output.update(common.errorMessageD('INVALID_TRANSACTION'))
		elif txn.isCaptured():
			output = {'errorCode': 'PreAuth already captured'}
		else:
			#populating entries which require its values from details
			self.poplulateTransactionEntries(txn, details)

			if details.sequenceCode == self.reqSequenceCode + 1:
				details.sequenceCode -= 1
				details.plutusSequenceCode -= 1
			self.bundle['username'] = self.username
			output = self.captureAuth(details, txn)#json dumps
			output['sequenceCode'] = details.sequenceCode

		details.setTransactionStatus(False)
		db.session.commit()
		return json.dumps(output)