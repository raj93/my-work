from flask import request
import simplejson as json
from src.mAuthenticate import common
from ext import db
from src.models import Transaction
from sqlalchemy import and_
from src.utils.validate import validateEntry
	
class TransactionHistory:	
	'''
		Retrieves Transaction History in case client(mobile application) is out of sync with the server
		in terms of transaction history. It can also be used to clear the settlement conflict state on the client
	'''

	HISTORY_TAGS = {
		'terminalInfo': dict,
		'authentication': dict,
		'conflict': bool,
		}

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp': unicode,
		#'appVersion': unicode,
		}

	AUTHENTICATION_TAGS = {
		'sessionToken': 'SESSION',
		'sequenceCode': int,
		'clientId': int,
		}

	def  __init__(self):	
		self.json_data = request.get_json()
		
	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.HISTORY_TAGS.items()):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['authentication'] and validateEntry(self.json_data['authentication'][k], v) \
							for k, v in self.AUTHENTICATION_TAGS.items()):
			return False

		return True

	def populateEntries(self):

		self.deviceId = self.json_data['terminalInfo']['deviceId']
		self.timeStamp = common.getCurrentTimeStamp()

		#self.timeStamp = self.json_data['terminalInfo']['timeStamp']
		#self.appVersion = self.json_data['terminalInfo']['appVersion']

		self.sessionToken = self.json_data['authentication']['sessionToken'] 
		self.reqSequenceCode = self.json_data['authentication']['sequenceCode']
		self.clientId = self.json_data['authentication']['clientId']

		self.conflict  = self.json_data['conflict']		

	def getTransactions(self):
		transactions = db.session.query(Transaction).filter(and_(Transaction.clientId == self.clientId, \
			Transaction.conditions('VALID'))).all()
        #String trid, amount, invoice, pan, chName, expiry, acquirer, trTime, rrn;
		#cardholder name , date , time,mid,tid,batch num,invoice,cardno,apprcode,rrn,base amount,tip amount,total amount
		data = []
		for t in transactions:
			d = {'transId' : t.transId, 'amount' : (t.amount + t.tipAmount), 'invoice' : t.invoice, 'maskedPan' : t.maskedPan, \
			'chName' : t.chname, 'expiry' : str(t.expiryMonth) + '/' + str(t.expiryYear), 'acquirer' : t.acquirer, \
			'transTime' : str(t.transTime), 'rrn' : t.rrn, 'apprCode' : t.apprCode}
			data.append(d)

		return {'transactions' : data}

	def process(self):
		if not self.isValidRequest(): return common.errorMessage('INVALID_REQUEST')
		self.populateEntries()
		
		isAuthorised, self.username = common.isAuthorisedRequest(self.clientId, self.sessionToken, self.deviceId)
		if isAuthorised == False: return common.errorMessage(self.username)#Second return value is errorCode

		canI, details = common.canIdoTransaction(self.clientId, common.VOID_SEARCH, self.username)
		if not canI: return common.errorMessage('SERVER_BUSY')

		output = common.resolveConflict(details, self.username, self.conflict, self.reqSequenceCode)
		if not (output or 'errorCode' in output):
			output = self.getTransactions()
		
		details.setTransactionStatus(False)
		db.session.commit()
		return json.dumps(output)
