from flask import request
from datetime import datetime
from src.transactions import client
import xmltodict
try:
	import xml.etree.cElementTree as ET
except ImportError:
	import xml.etree.ElementTree as ET

import simplejson as json

from src.mAuthenticate import common
from ext import db
from src.models import Transaction, Tip as tip_table
from src.models import posDetails
from sqlalchemy import and_
from src.utils.validate import validateEntry

class Tip:

	TIP_REQUEST_TYPE = 11#Move to plutus package

	TIP_TAGS = {
		'terminalInfo': dict,
		'authentication': dict,
		'payload': dict,
		}

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp': unicode,
		#'appVersion': unicode,
		}	

	AUTHENTICATION_TAGS = {
		'clientId': int,
		'sessionToken': 'SESSION',
		}

	#this transid is of the purchase for which this tip is requested
	PAYLOAD_TAGS = {
		'tipAmount': 'AMOUNT',
		'transId': 'TRANSID',
		}

	def __init__(self):
		self.json_data = request.get_json()

	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.TIP_TAGS.items()):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['authentication'] and validateEntry(self.json_data['authentication'][k], v) \
							for k, v in self.AUTHENTICATION_TAGS.items()):
			return False

		if not all(k in self.json_data['payload'] and validateEntry(self.json_data['payload'][k], v) \
							for k, v in self.PAYLOAD_TAGS.items()):
			return False

		return True

	def populateEntries(self):
		self.deviceId = self.json_data['terminalInfo']['deviceId']
		#self.timestamp = self.json_data['terminalInfo']['timeStamp']
		self.timestamp = common.getCurrentTimeStamp()
		#self.appVersion = self.json_data['terminalInfo']['appVersion']

		self.clientId = self.json_data['authentication']['clientId']
		self.sessionToken = self.json_data['authentication']['sessionToken'] 

		self.tipAmount = self.json_data['payload']['tipAmount']
		self.purchaseId = self.json_data['payload']['transId']

	def poplulateTransactionEntries(self,purchase_entry,details_entry):
		#TODO shift the currency code to purchase ?
		self.CurrencyCode =details_entry.currency
		self.acquirer = purchase_entry.acquirer
		self.invoice = purchase_entry.invoice
		self.ksn = self.purchaseId.split('_')[0]#TODO ERROR PRONE
		self.transId = str(self.ksn) + '_' + str(details_entry.plutusSequenceCode)#TODO ERROR PRONE
		

	def generateTipXmlRequest(self):
		amount = ET.Element('Amount')
		BaseAmount = ET.SubElement(amount, 'BaseAmount')
		BaseAmount.text = str(self.tipAmount)
		CurrencyCode = ET.SubElement(amount, 'CurrencyCode')	
		CurrencyCode.text = self.CurrencyCode
		
		AdditionalInformation = ET.Element('AdditionalInformation')

		AcquirerAddlInfoField = ET.SubElement(AdditionalInformation, 'AddlInfoField')
		
		AcquirerAddlInfoFieldID = ET.SubElement(AcquirerAddlInfoField, 'ID')
		AcquirerAddlInfoFieldID.text = '1011'
		
		AcquirerAddlInfoFieldValue = ET.SubElement(AcquirerAddlInfoField, 'Value')
		AcquirerAddlInfoFieldValue.text = self.acquirer
		
		AcquirerAddlInfoFieldLength = ET.SubElement(AcquirerAddlInfoField, 'Length')
		AcquirerAddlInfoFieldLength.text = str(len(self.acquirer))


		InvoiceAddlInfoField = ET.SubElement(AdditionalInformation, 'AddlInfoField')
		
		InvoiceAddlInfoFieldID = ET.SubElement(InvoiceAddlInfoField, 'ID')
		InvoiceAddlInfoFieldID.text = '1000'
		
		InvoiceAddlInfoFieldValue = ET.SubElement(InvoiceAddlInfoField, 'Value')
		InvoiceAddlInfoFieldValue.text = self.invoice

		InvoiceAddlInfoFieldLength = ET.SubElement(InvoiceAddlInfoField, 'Length')
		InvoiceAddlInfoFieldLength.text = str(len(self.invoice))

		#TODO id = deviceID_timestamp ==> unique as a single deviceId can't generate two legitimate requests at the same timestamp
		#TODO TrackingNumber
		TransactionInput = ET.Element('TransactionInput')
		TransactionInput.set('ID', self.transId)
		TransactionInput.extend((amount,AdditionalInformation))

		self.TipRequest = ET.Element('purchase-request')
		self.TipRequest.insert(0, TransactionInput)

	def plutus_request(self, CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE):
			#TODO #Critical Check this(client) if any concurrent request error arise! Also check whether the object client has any expiry time
		return client.service.PLHub_Exchange(CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE)

	def authorise(self,purchase_entry,details_entry):
		output = {}
		self.generateTipXmlRequest()
		result = self.plutus_request(details_entry.plutusClientId, details_entry.plutusSecretKey, self.TIP_REQUEST_TYPE, ET.tostring(self.TipRequest),details_entry.plutusSequenceCode)

		root = ET.fromstring(result.RESPONSE_XML)
	
		plutusRespSequenceCode = result.SEQUENCE_CODE
		if plutusRespSequenceCode != details_entry.plutusSequenceCode + 1:
			from flask import current_app
			current_app.logger.critical('Implementation error. Check clientID:' + self.clientId)
			#output = {'errorCode':'IMPLEMENTATION_ERROR','plutusSequenceCode':plutusRespSequenceCode}
			return common.errorMessage('SERVER_ERROR')
	
		#TODO it might be  better to remove indexed reference for XML -> find out the overhead of find and use it.
		transactionOutput = root.find('TransactionOutput')
		# it is unnecessary as plutus won't change the format. perhaps decide after talking to them. Unnecessary overhead for validation check
		if transactionOutput is None:
			return common.errorMessage('SYSTEM_ERROR')

		state = transactionOutput.find('State')
		if state is None:
			return common.errorMessage('SYSTEM_ERROR')
		#TODO send high ALERT to the developer #INTEGRATION #PINELABS
		if state.find('StatusCode').text == '2025':
			
			#Transaction Approved
			transactionOutput.remove(transactionOutput.find('ChargeslipData')) # Removing ChargeslipData
			doc = xmltodict.parse(ET.tostring(root))
			#return str(doc)
			Card = doc['hub-response']['TransactionOutput']['Card']
			card = {'cardNumber' : Card['CardNumber'], 
					'MM' : Card['ExpirationDate']['MM'],
					'YY' : Card['ExpirationDate']['YY']
					}
			if Card['CardholderName'] != None:
				card['chName'] = Card['CardholderName']

			Merchant = doc['hub-response']['TransactionOutput']['State']['Merchant']
			merchant = {'id' : Merchant['ID'],
						'name' : Merchant['Name'],
							'address' : Merchant['Address'],
						'city' : Merchant['City']}		

			state = doc['hub-response']['TransactionOutput']['State']
			transactionInfo = {'baseAmount' : self.tipAmount,
								'acquirer' : state['AcquirerName'],
								'invoice' : state['InvoiceNumber'],
								'batchNumber' : state['BatchNumber'],
								'transactionTime' : state['TransactionTime'],
								'apprCode' : state['HostResponse']['ApprovalCode'],
								'rrn' : state['HostResponse']['RetrievalReferenceNumber'],
								'transId' : self.transId,
								'terminalId' : state['TerminalID']
					}			
			
			transactionInfo['tipAmount'] = self.tipAmount

			
			#adding tip entry
			tip_entry = tip_table(self.clientId, self.purchaseId, self.transTime, self.plutusSequenceCode,
				self.tipAmount, tip_entry, tip_table.STATUS['ENABLED'] + tip_table.STATUS['APPROVED'])
			db.session.add(tip_entry)

			#modifying purchase entry with tip
			purchase_entry.tip = self.tipAmount

			#as it is successul increase plutus sequence code
			details_entry.plutusSequenceCode += 1

			output.update({'merchant' : merchant, 'card' : card, 'transactionInfo': transactionInfo})
			return output
		else:
			output.update({'errorCode': state.find('StatusMessage').text})
			return output

	def  process(self):
		if not self.isValidRequest():
			return common.errorMessage('INVALID_REQUEST')
		self.populateEntries() 

		isAuthorised, statusCode = common.isAuthorisedRequest(self.clientId, self.sessionToken, self.deviceId)

		if isAuthorised == False:
			return common.errorMessage(statusCode)
		else:
			self.username = statusCode

		purchase = Transaction.query.filter(and_(Transaction.transId==self.purchaseId, \
									Transaction.conditions('VALID'))).first()

		if purchase == None:
			output = {'errorCode': common.ERROR_CODES['INVALID_TRANSACTION']}

		pos = posDetails.query.filter(posDetails.clientId == self.clientId).first()
		self.poplulateTransactionEntries(purchase, pos)
		output = self.authorise(purchase, pos)

		output.update({'sequenceCode': pos.sequenceCode})
		db.session.commit()
		
		return json.dumps(output)

		










