from flask import request, current_app
from datetime import datetime
import simplejson as json
from sqlalchemy import and_
from src.models import pendingTip as pendingtip_table
from src.models import posDetails
from src.models import Transaction
from ext import db
from src.mAuthenticate import common
from src.transactions.tipWorker import TipWorker
from multiprocessing import Process#TODO Creating a process is too costly. optimise it
from src.utils.validate import validateEntry

class PendingTip:

	PENDING_TIP_TAGS = {
		'terminalInfo': dict,
		'authentication': dict,
		'payload': dict
		}

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp': unicode,
		#'appVersion': unicode,
		}

	AUTHENTICATION_TAGS = {
		'clientId': int,
		'sessionToken': 'SESSION',
		}

	#this transid is of the purchase for which this tip is requested
	PAYLOAD_TAGS = {
		'tipAmount': 'AMOUNT',
		'transId': 'TRANSID',
		'sequenceCode': int,
		}

	def __init__(self):
		self.json_data = request.get_json() 

	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.PENDING_TIP_TAGS.items()):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['authentication'] and validateEntry(self.json_data['authentication'][k], v) \
							for k, v in self.AUTHENTICATION_TAGS.items()):
			return False
	
		if not all(k in self.json_data['payload'] and validateEntry(self.json_data['payload'][k], v) \
							for k, v in self.PAYLOAD_TAGS.items()):
			return False

		return True

	def populateEntries(self):
		self.deviceId = self.json_data['terminalInfo']['deviceId']
		#self.timestamp = self.json_data['terminalInfo']['timeStamp']
		self.timestamp = common.getCurrentTimeStamp()
		#self.appVersion = self.json_data['terminalInfo']['appVersion']

		self.clientId = self.json_data['authentication']['clientId']
		self.sessionToken = self.json_data['authentication']['sessionToken'] 

		self.tipAmount = self.json_data['payload']['tipAmount']
		self.transId = self.json_data['payload']['transId']
		self.sequenceCode = self.json_data['payload']['sequenceCode']
		
	def process(self):
		if not self.isValidRequest(): return common.errorMessage('INVALID_REQUEST')

		self.populateEntries()
		
		isAuthorised, self.username = common.isAuthorisedRequest(self.clientId, self.sessionToken, self.deviceId)
		if isAuthorised == False: return common.errorMessage(self.username)#Second return value is errorCode
		
		purchase = db.session.query(Transaction).filter(and_(Transaction.clientId == self.clientId, Transaction.transId == self.transId)).first()
		if purchase == None:
			return common.errorMessage('INVALID_TRANSACTION')

		entry = pendingtip_table(self.clientId, self.transId, self.timestamp, self.tipAmount)
		db.session.add(entry)
		db.session.commit()#TODO catch a duplicate entry for transId error IntegrityError -> raise critical alert. CRITICAL
		#POTENTIAL BUG
		
		details = db.session.query(posDetails).filter(posDetails.clientId == self.clientId).first()
		
		if self.sequenceCode == details.sequenceCode:
			'''
				Note that we also need to check the condition posDetails.transactionStatus == False before we start the thread
				to take care of the following case
				i. Purchase request with seqCode = 60
				ii. PendingTip request with seqCode = 60(started in background thread in parallel with purchase request. Ex: PendingTipUploader @Android app)
				Suppose that PendingTIP request reaches server when Purchase request is in progress. 
				Condition seqCode == posDetails.sequenceCode is satisfied and tipWorker is started. Ideally, it shouldn't 
				because a new Tip request will be sent with same PlutusSequenceCode as the purchase request that was sent earlier.
				As a result, purchase request is cancelled and the fun is spoiled.

				But, note that this condition is satisfied using canIdoTransaction() in tipWorker.run(). So, we're skipping this part.
				
				Note 3: An additional check for sequenceCode has to be put after canIdoTransaction() in tipWorker as in the above case, 
				if Purchase request is completed by the time canIdoTransaction() is executed -> details.sequenceCode becomes 61. Whereas 
				pendingTip's seqCode is 60. As a result, purchase will be cancelled if pendingTips are processed.
				The reason we've put this additional check of seqCode here is a tradeoff in creating a subprocess
			'''
			def do_some_work():
				db.session.close()#To prevent the sub process from using this db session values
				t1 = TipWorker(self.clientId, self.sequenceCode)
				t1.run()

			p = Process(target = do_some_work)
 			p.start()#TODO note that this sub process would remain as a defunct process as its parent process doesn't get notified about its termination. Verify this behavior in large scale
			if current_app.config['TESTING']:
				#p.join()
				return json.dumps({'mesg': 'Success', 'pid' : p.pid})

		return json.dumps({'mesg': 'Success'})
