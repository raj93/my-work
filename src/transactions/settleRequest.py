from flask import request, current_app
from datetime import datetime
import simplejson as json

from src.mAuthenticate import common
from ext import db
from src.models import posDetails
import src.transactions.demo.settleRequest
import src.transactions.stripe.settleRequest
import src.transactions.plutus.settleRequest
from src.utils.validate import validateEntry

'''Settlement request gives a detailed report of the batch. It shows list of all transactions that took place in the current batch and settles them.
Ex: Sale 23.23
    Void 23.23
    Sale 10.00
    
    Settlement Summary:
    Sale Amount (2): 33.23
    Void Amount (1): 23.23
    Total Amount   : 10.00
    Refund Amount  : 10.00
    Net Amount     : 0.00

If the request is given for the second time, it just shows the previous settlement summary.
Ex: Last Settlement Summary:
    Sale Amount (2): 33.23
    Void Amount (1): 23.23
    Total Amount   : 10.00
    Refund Amount  : 10.00
    Net Amount     : 0.00

'''
class SettleRequest:
	SETTLE_REQUEST_TYPE = 5
	
	SETTLE_REQUEST_TAGS = {
		'terminalInfo': dict,
		'authentication': dict,
		}

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp': unicode,
		#'appVersion': unicode,
		}

	AUTHENTICATION_TAGS = {
		'sessionToken': 'SESSION',
		'sequenceCode': int,
		'clientId': int,
		}

	def __init__(self):
		self.json_data = request.get_json() 
		self.bundle = {}

	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.SETTLE_REQUEST_TAGS.items()):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['authentication'] and validateEntry(self.json_data['authentication'][k], v) \
							for k, v in self.AUTHENTICATION_TAGS.items()):
			return False
			
		return True

	def populateEntries(self):
		self.bundle['deviceid'] = self.deviceId = self.json_data['terminalInfo']['deviceId']
		self.bundle['timeStamp'] = common.getCurrentTimeStamp()
		#self.bundle['timeStamp'] = self.timeStamp = datetime.now().strftime('%Y-%m-%d %X')
		#self.bundle['appVersion'] = self.appVersion = self.json_data['terminalInfo']['appVersion']

		self.bundle['sessionToken'] = self.sessionToken = self.json_data['authentication']['sessionToken'] 
		self.bundle['reqSequenceCode'] = self.reqSequenceCode = self.json_data['authentication']['sequenceCode']
		self.bundle['clientId'] = self.clientId = self.json_data['authentication']['clientId']
	
	def settlement(self, details):
		if details.processor == posDetails.PROCESSORS['DEMO']:
			txn = src.transactions.demo.settleRequest
		elif details.processor == posDetails.PROCESSORS['STRIPE']:
			txn = src.transactions.stripe.settleRequest
		elif details.processor == posDetails.PROCESSORS['ATOS']:
			pass
		elif details.processor == posDetails.PROCESSORS['PLUTUS']:
			txn = src.transactions.plutus.settleRequest

		a = txn.SettleRequest(details, self.bundle)
		return a.process()
		
	def process(self):
		if not self.isValidRequest(): return common.errorMessage('INVALID_REQUEST')

		self.populateEntries()
		
		isAuthorised, self.username = common.isAuthorisedRequest(self.clientId, self.sessionToken, self.deviceId)
		if isAuthorised == False: return common.errorMessage(self.username)#Second return value is errorCode
		self.bundle['username'] = self.username
		
		canI, details = common.canIdoTransaction(self.clientId, common.SETTLEMENT, self.username)
		if not canI: return common.errorMessage('SERVER_BUSY')

		isValid, details = common.validateSequenceCode(self.reqSequenceCode, details)
		if not isValid: return details # Second return value is output to be returned
		
		output = {}	
		
		if details.hasSettlementConflict():
			#settle report must have been called before settle request which would resolve the conflict.
			current_app.logger.warn('Settle Request when server in conflict @ ClientId:' + str(self.clientId))
			output['errorCode'] = common.ERROR_CODES['INVALID_TRANSACTION']
		else:
			if details.sequenceCode == self.reqSequenceCode + 1:
				#this cannot happen becuase we do settle only after mobile 
				#acknowledges the response of settlement report
				output['errorCode'] = common.ERROR_CODES['INVALID_SEQUENCE_CODE']
				current_app.logger.warn('sequence code found to be unsynchronized during settle @clientId:' + str(self.clientId))
			else:#details.sequenceCode = self.reqSequenceCode
				output = self.settlement(details)
		
		output['sequenceCode'] = details.sequenceCode
		details.setTransactionStatus(False)#transactionStatus in posDetails are committed, un-freeing the clientId to do any Xns
		db.session.commit()		

		return json.dumps(output)
