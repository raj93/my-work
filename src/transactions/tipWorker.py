from flask import current_app
from src.transactions import client
try:
	import xml.etree.cElementTree as ET
except ImportError:
	import xml.etree.ElementTree as ET

from src.models import pendingTip, Tip as tip_table, Transaction as purchase_table,posDetails
from src.mAuthenticate import common
from ext import db
from sqlalchemy import and_
#import threading

#Assuming that there is no transaciton conflict
class TipWorker():
	'''
		Pre-requisite to start the thread: Sequence code has to be in sync with the client
		self.sequenceCode == details.sequenceCode in any function which uses this class.
		This is run through two classes:
			i. pendingTip
			ii. SettleReport
	'''
	def __init__(self, clientId, seqCode):
		#threading.Thread.__init__(self) Using threads gives "Application not registered on db instance and no applicationi bound to current context"
		self.clientId = clientId
		self.TIP_REQUEST_TYPE = 11
		self.sequenceCode = seqCode

	def populateEntries(self,pending_tip_row,purchase_entry,details_entry):
		self.timestamp = common.getCurrentTimeStamp()

		self.tipAmount = pending_tip_row.tipAmount
		self.purchaseId = pending_tip_row.transId

		self.CurrencyCode =details_entry.currency
		self.acquirer = purchase_entry.acquirer
		self.invoice = purchase_entry.invoice
		self.ksn = self.purchaseId.split('_')[0]
		self.transId = str(self.ksn) + '_' + str(details_entry.plutusSequenceCode)

	def generateTipXmlRequest(self):
		amount = ET.Element('Amount')
		BaseAmount = ET.SubElement(amount, 'BaseAmount')
		BaseAmount.text = str(self.tipAmount)
		CurrencyCode = ET.SubElement(amount, 'CurrencyCode')	
		CurrencyCode.text = self.CurrencyCode
		
		AdditionalInformation = ET.Element('AdditionalInformation')

		AcquirerAddlInfoField = ET.SubElement(AdditionalInformation, 'AddlInfoField')
		
		AcquirerAddlInfoFieldID = ET.SubElement(AcquirerAddlInfoField, 'ID')
		AcquirerAddlInfoFieldID.text = '1011'
		
		AcquirerAddlInfoFieldValue = ET.SubElement(AcquirerAddlInfoField, 'Value')
		AcquirerAddlInfoFieldValue.text = self.acquirer
		
		AcquirerAddlInfoFieldLength = ET.SubElement(AcquirerAddlInfoField, 'Length')
		AcquirerAddlInfoFieldLength.text = str(len(self.acquirer))


		InvoiceAddlInfoField = ET.SubElement(AdditionalInformation, 'AddlInfoField')
		
		InvoiceAddlInfoFieldID = ET.SubElement(InvoiceAddlInfoField, 'ID')
		InvoiceAddlInfoFieldID.text = '1000'
		
		InvoiceAddlInfoFieldValue = ET.SubElement(InvoiceAddlInfoField, 'Value')
		InvoiceAddlInfoFieldValue.text = self.invoice

		InvoiceAddlInfoFieldLength = ET.SubElement(InvoiceAddlInfoField, 'Length')
		InvoiceAddlInfoFieldLength.text = str(len(self.invoice))

		#TODO id = deviceID_timestamp ==> unique as a single deviceId can't generate two legitimate requests at the same timestamp
		#TODO TrackingNumber
		TransactionInput = ET.Element('TransactionInput')
		TransactionInput.set('ID', self.transId)
		TransactionInput.extend((amount,AdditionalInformation))

		self.TipRequest = ET.Element('purchase-request')
		self.TipRequest.insert(0, TransactionInput)

	def plutus_request(self, CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE):
		return client.service.PLHub_Exchange(CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE)

	def authorise(self,pending_tip_row,purchase_entry,details_entry):
		output = {}
		self.generateTipXmlRequest()
		try:
			result = self.plutus_request(details_entry.plutusClientId, details_entry.plutusSecretKey, self.TIP_REQUEST_TYPE, ET.tostring(self.TipRequest),details_entry.plutusSequenceCode)
		except Exception as e:
			return common.errorMessage('SERVER_ERROR')

		root = ET.fromstring(result.RESPONSE_XML)
	
		if result.SEQUENCE_CODE != details_entry.plutusSequenceCode + 1:
			current_app.logger.critical('IMPLEMENTATION_ERROR: found @ clientId:' + str(self.clientId))
			return common.errorMessage('SERVER_ERROR')
	
		transactionOutput = root.find('TransactionOutput')
		state = transactionOutput.find('State')
		
		if state.find('StatusCode').text == '2025':	
			
			#adding tip entry
			tip_entry = tip_table(self.clientId, purchase_entry.id, common.getCurrentTimeStamp(), \
				details_entry.plutusSequenceCode, self.tipAmount)
			db.session.add(tip_entry)

			#deleting entry from pending tip
			db.session.delete(pending_tip_row)

			#modifying purchase entry with tip amount
			purchase_entry.tipAmount = self.tipAmount

			#modifying the tip bit because it is a success
			if not purchase_entry.isTipped():
				purchase_entry.setTip()
			else:
				current_app.logger.critical('Implementation error encountered @ clientId: ' + str(self.clientId))

			# Increment ONLY plutus sequence code
			details_entry.plutusSequenceCode += 1

			db.session.commit()
			output.update({'mesg' : 'Success'})
		else:
			output.update({'errorCode': state.find('StatusMessage').text})

		return output		

	def run(self):
		canI, details_entry = common.canIdoTransaction(self.clientId, common.TIP, 'tipWorker')
		if not canI: return 
		
		'''
			Note that we also need to check the condition posDetails.transactionStatus == False before we start the thread
			to take care of the following case
			i. Purchase request with seqCode = 60
			ii. PendingTip request with seqCode = 60(started in background thread in parallel with purchase request. Ex: PendingTipUploader @Android app)
			Suppose that PendingTIP request reaches server when Purchase request is in progress. 
			Condition seqCode == posDetails.sequenceCode is satisfied and tipWorker is started. Ideally, it shouldn't 
			because a new Tip request will be sent with same PlutusSequenceCode as the purchase request that was sent earlier.
			As a result, purchase request is cancelled and the fun is spoiled.

			But, note that this condition is satisfied using canIdoTransaction() in tipWorker.run(). So, we're skipping this part.
			
			An additional check for sequenceCode has to be put after canIdoTransaction() in tipWorker as in the above case, 
			if Purchase request is completed by the time canIdoTransaction() is executed -> details.sequenceCode becomes 61. 
			Whereas pendingTip's seqCode is 60. As a result, purchase will be cancelled if pendingTips are processed.
			The reason we've put this additional check of seqCode here is a tradeoff in creating a subprocess
		'''
		if self.sequenceCode != details_entry.sequenceCode: return
		pending_tips = db.session.query(pendingTip).filter(pendingTip.clientId == self.clientId).all()
		
		for pending_tip_row in pending_tips:
			purchase_entry = db.session.query(purchase_table).filter(and_(purchase_table.transId == pending_tip_row.transId, purchase_table.conditions('SIMPLE_PURCHASE'))).first()
			#details_entry = db.session.query(posDetails).filter(posDetails.clientId == self.clientId).first()
		
			if purchase_entry != None and details_entry != None: 
				self.populateEntries(pending_tip_row, purchase_entry, details_entry)
				#if success it does the required changes else leave it the same
				self.authorise(pending_tip_row, purchase_entry, details_entry)
			else:
				current_app.logger.warn('Pending Tip removed as purchase wasnt found\n' + str(pending_tip_row))
				db.session.delete(pending_tip_row)

			#checking whether any transaction has come.
			if details_entry.transactionType != common.TIP:
				details_entry.setTransactionStatus(False)
				db.session.commit()
				return

		#transactionStatus in posDetails are committed,un-freeing the clientId to do any Xns
		details_entry.setTransactionStatus(False)
		db.session.commit()
		