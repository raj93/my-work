from flask import request, current_app

import simplejson as json

from src.mAuthenticate import common
from ext import db
from multiprocessing import Process#TODO Creating a process is too costly. optimise it
from src.models import pendingTip,posDetails
from src.transactions.tipWorker import TipWorker
import src.transactions.demo.settleReport
import src.transactions.stripe.settleReport
import src.transactions.plutus.settleReport
from src.utils.validate import validateEntry


'''Settlement report gives a brief report of the settlement
Ex: Sale Amount (5): 53.23
    Void Amount (2): 23.23
    Total Amount   : 30.00
    Refund Amout (1)  : 10.00
    Net Amount     : 20.00
'''

class SettleReport:
	SETTLE_REPORT_REQUEST_TYPE = 16
	SETTLE_REQUEST_TYPE = 5

	SETTLE_REPORT_TAGS = {
		'terminalInfo': dict,
		'authentication': dict,
		'conflict': bool,
		}

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp': unicode,
		#'appVersion': unicode,
		}

	AUTHENTICATION_TAGS = {
		'sessionToken': 'SESSION',
		'sequenceCode': int,
		'clientId': int,
		}

	def __init__(self):
		self.json_data = request.get_json() 
		self.bundle = {}

	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.SETTLE_REPORT_TAGS.items()):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['authentication'] and validateEntry(self.json_data['authentication'][k], v) \
							for k, v in self.AUTHENTICATION_TAGS.items()):
			return False
			
		return True

	def populateEntries(self):
		self.bundle['deviceId'] = self.deviceId = self.json_data['terminalInfo']['deviceId']
		self.bundle['timeStamp'] = common.getCurrentTimeStamp()
		#self.timeStamp = self.json_data['terminalInfo']['timeStamp']
		#self.bundle['appVersion'] = self.appVersion = self.json_data['terminalInfo']['appVersion']

		self.bundle['sessionToken'] = self.sessionToken = self.json_data['authentication']['sessionToken'] 
		self.bundle['reqSequenceCode'] = self.reqSequenceCode = self.json_data['authentication']['sequenceCode']
		self.bundle['clientId'] = self.clientId = self.json_data['authentication']['clientId']
		self.bundle['confict'] = self.conflict = self.json_data['conflict']

	def settlementReport(self, details):
		if details.processor == posDetails.PROCESSORS['DEMO']:
			txn = src.transactions.demo.settleReport
		elif details.processor == posDetails.PROCESSORS['STRIPE']:
			txn = src.transactions.stripe.settleReport
		elif details.processor == posDetails.PROCESSORS['ATOS']:
			pass
		elif details.processor == posDetails.PROCESSORS['PLUTUS']:
			txn = src.transactions.plutus.settleReport
			
		a = txn.SettleReport(details, self.bundle)
		return a.process()


	def process(self):
		if not self.isValidRequest(): return common.errorMessage('INVALID_REQUEST')

		self.populateEntries()
		
		isAuthorised, self.username = common.isAuthorisedRequest(self.clientId, self.sessionToken, self.deviceId)
		if isAuthorised == False: return common.errorMessage(self.username)#Second return value is errorCode
		self.bundle['username'] = self.username
		
		canI, details = common.canIdoTransaction(self.clientId, common.SETTLEMENT_REPORT, self.username)
		if not canI: return common.errorMessage('SERVER_BUSY')
		isValid, details = common.validateSequenceCode(self.reqSequenceCode, details)
		if not isValid: return details # Second return value is output to be returned
		
		output = {}
		if details.hasSettlementConflict():
			
			'''
				If there is a conflict at server side, it has to resolved regardless of the client's state.
				Note: 
				i. we send the settleCode tag even if conflict = False is sent in the request. 
				ii. Code enters this block only if last transaction is SettleRequest #TODO move it into a function
			'''
			################################ Invalid input parameters #################################
			
			if not self.conflict:
				current_app.logger.critical('Fraud attempt or wrong implementation at client side.\nConflict at server, but request contains conflict = False. Request: \n' + str(self.json_data))
				output = {'errorCode': common.ERROR_CODES['INVALID_TRANSACTION']}#TODO

			else:	
				if details.sequenceCode == self.reqSequenceCode + 1:
					details.sequenceCode -= 1#Reducing both sequenceCodes to prevent cascading effect. Refer to docs for details
					details.plutusSequenceCode -= 1
				
				response = self.settlementReport(details)

				if response.has_key('report'):
					if response['report']['isSettled']:
						output = common.updateToNewBatch(details, response['report'], self.username, self.clientId, \
						details.sequenceCode - 1, details.plutusSequenceCode - 1)

						if not len(output):#No error found during updateToNewBatch. 
							details.setConflict(False)#Note that settle conflict is not removed if there is any error found

						output['settleCode'] = 'l'
					else:
						output['settleCode'] = 'c'

				output.update(response)
		elif self.conflict:
			if details.sequenceCode == self.reqSequenceCode + 1:
				details.sequenceCode -= 1
				details.plutusSequenceCode -= 1

				output.update(self.settlementReport(details))
				if 'report' in output and 'isSettled' in output['report'] and output['report']['isSettled']:
					output['settleCode'] = 'l'
				else:
					current_app.logger.critical(str(output) + ' ClientId: ' + str(self.clientId))
					output = {'errorCode': common.ERROR_CODES['SERVER_ERROR']}
			else:
				current_app.logger.critical('Conflict and sequenceCode imply conflicting states. Request:\n' + str(self.json_data) + \
				'\n SeqCode: ' + str(details.sequenceCode))
				output = {'errorCode': common.ERROR_CODES['INVALID_SEQUENCE_CODE']}
		else:						
			if details.sequenceCode == self.reqSequenceCode + 1:
				details.sequenceCode -= 1
				details.plutusSequenceCode -= 1
			else:
				#Check if there are any pending Tips on this clientId. If so, start the tip worker and send SERVER_ERROR
				if db.session.query(pendingTip).filter(pendingTip.clientId == self.clientId).count():
					def do_some_work():
						db.session.close()#To prevent the sub process from using this db session values
						t1 = TipWorker(self.clientId, self.reqSequenceCode)
						t1.run()

					p = Process(target = do_some_work)
 					p.start()#TODO note that this sub process would remain as a defunct process as its parent process doesn't get notified about its termination. Verify this behavior in large scale
					output = {'errorCode': common.ERROR_CODES['SERVER_BUSY']}
					if current_app.config['TESTING']:
						output.update({'pid' : p.pid})
				else:
					output = self.settlementReport(details)
				
		output['sequenceCode'] = details.sequenceCode#Note that in case of Tipworker being started, details.sequenceCode doesn't change
		details.setTransactionStatus(False)

		db.session.commit()
		return json.dumps(output)
