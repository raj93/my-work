from suds.client import Client
from suds.transport.http import HttpTransport as SudsHttpTransport

class WellBehavedHttpTransport(SudsHttpTransport):
	"""HttpTransport which properly obeys the ``*_proxy`` environment variables."""

	def u2handlers(self):
		"""
		Return a list of specific handlers to add.

		The urllib2 logic regarding ``build_opener(*handlers)`` is:
		- It has a list of default handlers to use
		- If a subclass or an instance of one of those default handlers is given
		in ``*handlers``, it overrides the default one.

		Suds uses a custom {'protocol': 'proxy'} mapping in self.proxy, and adds
		a ProxyHandler(self.proxy) to that list of handlers.
		This overrides the default behaviour of urllib2, which would otherwise
		use the system configuration (environment variables on Linux, System
		Configuration on Mac OS, ...) to determine which proxies to use for
		the current protocol, and when not to use a proxy (no_proxy).

		Thus, passing an empty list will use the default ProxyHandler which
		behaves correctly.

		"""

		return []

#https://fedorahosted.org/suds/wiki/Documentation
#https://gist.github.com/rbarrois/3721801

URL = 'https://122.160.150.51:90/PlutusHub/bin/PlutusHub.dll?Handler=GenPlutusHubWSDL'
#proxy = dict(http='http://proxy.iiit.ac.in:8080', https='https://proxy.iiit.ac.in:8080')

#TODO #CRITICAL Check for concurrency issues here -> we use an instance variable Service from this. mightn't be a problem.. verify
#client = Client(URL, transport=WellBehavedHttpTransport())#, proxy=proxy)
client = None
#cache = client.options.cache
#cache.setduration(days=10)
