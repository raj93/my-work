from flask import request
from src.transactions import client#concurrency
from src.mAuthenticate import common
from src.utils.validate import validateEntry

try:
	import xml.etree.cElementTree as ET
except ImportError:
	import xml.etree.ElementTree as ET

import simplejson as json

class Batch:
	'''	
		Not in use currently. 
	'''
	BATCH_REQUEST_TYPE = 14

	BATCH_REPORT_TAGS = { 
		'terminalInfo': dict,
		'authentication': dict,
		}

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp': unicode,
		#'appVersion': unicode,
		}

	AUTHENTICATION_TAGS = {
		'sessionToken': 'SESSION',
		'sequenceCode': int,
		'clientId': int,
		}

	def __init__(self):
		self.json_data = request.get_json() 

	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.BATCH_REPORT_TAGS.items()):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['authentication'] and validateEntry(self.json_data['authentication'][k], v) \
							for k, v in self.AUTHENTICATION_TAGS.items()):
			return False

		return True

	def populateEntries(self):
		self.deviceId = self.json_data['terminalInfo']['deviceId']
		self.timeStamp = common.getCurrentTimeStamp()#self.json_data['terminalInfo']['timeStamp']
		#self.appVersion = self.json_data['terminalInfo']['appVersion']

		self.sessionToken = self.json_data['authentication']['sessionToken']
		self.sequenceCode = self.json_data['authentication']['sequenceCode']
		self.clientId = self.json_data['authentication']['clientId']

	def generateXmlRequest(self):
		self.batchRequest = ET.tostring(ET.Element('purchase-request'))
		#store in self.batchRequest

	def plutus_request(self, CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE):
		#TODO #Critical Check this(client) if any concurrent request error arise! Also check whether the object client has any expiry time
		return client.service.PLHub_Exchange(CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE)


	def authorise(self):
		result = self.plutus_request(self.clientId, self.sessionToken, self.BATCH_REQUEST_TYPE, self.batchRequest, self.sequenceCode)
		#return str(result.RESPONSE_XML)
		root = ET.fromstring(result.RESPONSE_XML)
		self.sequenceCode = result.SEQUENCE_CODE
		#TODO it might be  better to remove indexed reference for XML -> find out the overhead of find and use it.
		
		transactionOutput = root.find('TransactionOutput')
		# it is unnecessary as plutus won't change the format. perhaps decide after talking to them. Unnecessary overhead for validation check
		output = {'errorCode': 'Server Error'}
		if transactionOutput is None:
			return json.dumps(output)

		state = transactionOutput.find('State')
		if state is None:
			return json.dumps(output)
		#TODO send high ALERT to the developer #INTEGRATION #PINELABS
		
		if state.find('StatusCode').text == '2025':
			acquirer = transactionOutput.find('Acquirer')
			for bank in acquirer:
				pass
                            #TODO based on reply from plutus, modify the content 
				#result[bank] = {}
			return 'success'
		else:
			output = {'errorCode': state.find('StatusMessage').text,
						'sequenceCode' : self.sequenceCode}
			return json.dumps(output)

	def process(self):		
		if not self.isValidRequest():
			return common.errorMessage('INVALID_REQUEST')
		self.populateEntries() #There would be no problem in loading Json as its already validated
		#self.isValidSession() -> validate the session data using timestampsigner of itsdangerous!!
		self.generateXmlRequest()
		#TODO store request in db? 
		return self.authorise()
