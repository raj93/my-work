from pyATOS.ISO8583 import AtosISO


atos = AtosISO()

atos.setMTI('0800')
atos.setBit(2,2)
atos.setBit(4,4)


print atos.getRawIso()
print atos.getNetworkISO()
print len(atos.getRawIso())

