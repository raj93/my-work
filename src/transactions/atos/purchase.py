from src.mAuthenticate import common
from ext import db
from src.models import Transaction as purchase_table
from flask import current_app

from src.utils.parseData import ParseTrackData

import stripe

from common import createSocketConnection
from pyATOS.ISO8583 import AtosISO
from pyATOS.ISOErrors import *


class Purchase:

	#What happens when two requests with same TransId i.e., same ksn & sequenceCode comes at same timestamp. And response to first request after the response of second request? 
	#Planning to store all the success requests and show them by grouping based on TransID
	#transID different for any two legitimate requests because of difference in KSN!! -> so grouping based on transID would be bullshit.. grouping based on sequenceCode gives good results.. 
	MAX_DATA_RESPONSE_SIZE = 2048

	def __init__(self, details, bundle):
		self.details = details
		self.bundle = bundle


	def process(self):

		output = {}

		status,socket =createSocketConnection():
		if not status:
			return  output.update({'errorCode': socket})

		# we should get cardNumber , month and year from track1
		track1 = self.bundle["track1"]
		parser = ParseTrackData()
		parser.parse(track1)

		# populating variables necessary for stripe
		self.cardNumber = parser.cardNumber
		self.chName = parser.chName
		self.MM = parser.MM
		self.YY = parser.YY

		"""
		# pre trans
		self.bundle['currencyCode']
		self.bundle['baseAmount']

		# post trans
		self.transId = ""
		"""
		iso = AtosISO()
		iso.setMTI('0200')
		# iso.setBit(3,'300000')
		# set the bits and values

		
		try:
			message = iso.getNetworkISO() 
			s.send(message)
			print ('Sending ... %s' % message)
			ans = s.recv(MAX_DATA_RESPONSE_SIZE) 
			print ("\nInput ASCII |%s|" % ans)
			isoAns = AtosISO()
			isoAns.setNetworkISO(ans)

			if isoAns.getMTI() == '0210':
				# successful purchase response
				# set trans id and send response to mobile.
			else:
				output.update({'errorCode': 'error in processing data please try again'})
				return output

			v1 = isoAns.getBitsAndValues()
			for v in v1:
				print ('Bit %s of type %s with value = %s' % (v['bit'],v['type'],v['value']))
				
		except InvalidIso8583, ii:
			output.update({'errorCode': 'error in processing data please try again'})
			return output
	
		cn = self.cardNumber
		maskedPan = cn[0:4] + '******' + cn[len(cn)-4:len(cn)]

		card = {'cardNumber' : maskedPan,
				'MM' : self.MM,
				'YY' : self.YY
				}
		
		card['chName'] = self.chName

		merch = self.details.merchantDetails

		merchant = {'id' : merch.mid,
					'name' : merch.nameOnReceipt,
					'address' : merch.address1+merch.address2 ,
					'city' : merch.city}		

		transactionInfo = {'baseAmount' : self.bundle['baseAmount'],
							'acquirer' : 'HDFC',
							'invoice' : str(self.details.sequenceCode),
							'batchNumber' : str(self.details.batch),
							'transactionTime' : self.bundle['timestamp'],
							'apprCode' : '000000',
							'rrn' : '000000000020',
							'transId' : self.transId,
							'terminalId' : self.bundle['clientId']
				}			
		
		entry = purchase_table(self.bundle['username'], self.bundle['clientId'], self.details.sequenceCode, self.details.plutusSequenceCode, \
			self.transId, self.bundle['timestamp'], self.bundle['baseAmount'], card['chName'], card['cardNumber'], card['MM'], \
			card['YY'],self.bundle['inputType'], transactionInfo['invoice'], transactionInfo['batchNumber'], transactionInfo['acquirer'], \
			transactionInfo['apprCode'], transactionInfo['rrn'], self.bundle['clientId'], merchant['id'], card['cardNumber'], \
			common.PURCHASE_BIT_VALUE, self.details.company, self.details.agent, self.details.merchant)
		
		db.session.add(entry)
		db.session.commit()
		
		rowId = entry.id
		tranType = common.PURCHASE
		output.update({'merchant' : merchant, 'card' : card, 'transactionInfo': transactionInfo})

			
		#we got a result so we are incrementing sequence codes.
		self.details.minSequenceCode = self.details.sequenceCode
		self.details.sequenceCode += 1
		#details.plutusSequenceCode = plutusRespSequenceCode
		
		common.disableLastTransaction(self.bundle['clientId'], tranType, self.details.sequenceCode - 1, rowId)
		return output