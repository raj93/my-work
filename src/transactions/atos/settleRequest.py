from src.mAuthenticate import common
from ext import db
from src.models import Transaction as purchase_table, Void as void_table, Tip as tip_table, Settlement as settlement_table
from sqlalchemy import and_

class SettleRequest:

	def __init__(self, details, bundle):
		self.details = details
		self.bundle = bundle

	def process(self):
		output = {}
	
		settlement_entries = []

		mynumSales = 0
		mysaleAmount = 0
		mynumTips = 0
		mytipAmount = 0
		mynumVoids = 0
		myVoidAmount =	 0

		purchase_entries = db.session.query(purchase_table).filter(and_(purchase_table.clientId == self.bundle['clientId'], purchase_table.status > 0)).all()
		for purchase_row in purchase_entries:
			s = purchase_row.status
			if s == common.PURCHASE_BIT_VALUE:
				mynumSales += 1
				mysaleAmount += int(purchase_row.amount)
			elif s == common.PURCHASE_BIT_VALUE | common.VOID_BIT_VALUE:
				mynumSales += 1
				mynumVoids += 1
				myVoidAmount += int(purchase_row.amount)
				mysaleAmount += int(purchase_row.amount)
			elif s == common.PURCHASE_BIT_VALUE | common.TIP_BIT_VALUE:
				mynumSales += 1
				mynumTips += 1
				mysaleAmount += int(purchase_row.amount)
				mytipAmount += int(purchase_row.tipAmount)
			elif s == common.PURCHASE_BIT_VALUE | common.TIP_BIT_VALUE | common.VOID_BIT_VALUE:
				#incase of VOID(TIP_ADJUST) entry in settlement there is no seperate tip entry in plutus response.
				mynumSales += 1
				mynumVoids += 1
				mysaleAmount += int(purchase_row.amount) + int(purchase_row.tipAmount)
				myVoidAmount += int(purchase_row.amount) + int(purchase_row.tipAmount)


		total = mysaleAmount - myVoidAmount
		netTotal = total# - refund['total']

		settlement_entries.append(settlement_table(self.bundle['username'], self.bundle['clientId'], self.details.sequenceCode, self.details.plutusSequenceCode, \
							self.bundle['timeStamp'], 'HDFC', self.details.batch, self.details.clientId, '', mynumSales, mynumVoids, mynumTips, \
							mysaleAmount, myVoidAmount, mytipAmount, netTotal, self.details.batch))

		void_entries = db.session.query(void_table).filter(and_(void_table.clientId == self.bundle['clientId'], \
			void_table.isDisabled == False)).all()
		tip_entries = db.session.query(tip_table).filter(and_(tip_table.clientId == self.bundle['clientId'], \
			tip_table.isDisabled == False)).all()

		#Add settlements of different acquirers or TIDs
		for settlement_row in settlement_entries:
			db.session.add(settlement_row)
			
		#Change status of Purchase entries to Settled	
		for purchase_row in purchase_entries:
			purchase_row.status += common.SETTLEMENT_BIT_VALUE

		#Add settled entries to settled database
		from src.models import SettledTransaction, SettledVoid, SettledTip

		settled_purchase_entries = []

		for purchase in purchase_entries:
			settledPurchase = SettledTransaction(purchase, self.details.batch)
			settled_purchase_entries.append(settledPurchase)
			db.session.add(settledPurchase)

		for void_row in void_entries:#ERRORPRONE foreign key restraint..
			settlementdVoid = SettledVoid(void_row)
			db.session.add(settlementdVoid)

		for tip_row in tip_entries:
			settlementdTip = SettledTip(tip_row)
			db.session.add(settlementdTip)

		db.session.commit() 

			
		#Cleaning up settled entries in unsettled database
		for tip_row in tip_entries:
			db.session.delete(tip_row)

		for void_row in void_entries:
			db.session.delete(void_row)

		for purchase_row in purchase_entries:
			db.session.delete(purchase_row)

		db.session.commit()

		# Update batch count in posDetails. This count is common in the series of settlements made by this settlement request
		self.details.batch += 1
		output = {'isSettled' : True}

		if len(purchase_entries) != 0:
			common.notifyMerchant(self.details, settlement_entries, settled_purchase_entries)

		#We got a result so we are incrementing sequence codes
		self.details.minSequenceCode = self.details.sequenceCode
		self.details.sequenceCode += 1
#		details.plutusSequenceCode = plutusRespSequenceCode
		
		common.disableLastTransaction(self.bundle['clientId'], common.SETTLEMENT, self.details.sequenceCode - 1, 0)
		return output
