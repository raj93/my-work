from src.mAuthenticate import common
from ext import db
from src.models import Transaction as purchase_table
from sqlalchemy import and_


class SettleReport:
	
	def __init__(self, details, bundle):
		self.details = details
		self.bundle = bundle
	
	def process(self):
		report = {}
		report['HDFC'] = {}

		mynumSales = 0
		mysaleAmount = 0
		mynumTips = 0
		mytipAmount = 0
		mynumVoids = 0
		myVoidAmount =	 0

		purchase_entries = db.session.query(purchase_table).filter(and_(purchase_table.clientId == self.bundle['clientId'], purchase_table.status > 0)).all()
		for purchase_row in purchase_entries:
			s = purchase_row.status
			if s == common.PURCHASE_BIT_VALUE:
				mynumSales += 1
				mysaleAmount += int(purchase_row.amount)
			elif s == common.PURCHASE_BIT_VALUE | common.VOID_BIT_VALUE:
				mynumSales += 1
				mynumVoids += 1
				myVoidAmount += int(purchase_row.amount)
				mysaleAmount += int(purchase_row.amount)
			elif s == common.PURCHASE_BIT_VALUE | common.TIP_BIT_VALUE:
				mynumSales += 1
				mynumTips += 1
				mysaleAmount += int(purchase_row.amount)
				mytipAmount += int(purchase_row.tipAmount)
			elif s == common.PURCHASE_BIT_VALUE | common.TIP_BIT_VALUE | common.VOID_BIT_VALUE:
				#incase of VOID(TIP_ADJUST) entry in settlement there is no seperate tip entry in plutus response.
				mynumSales += 1
				mynumVoids += 1
				mysaleAmount += int(purchase_row.amount) + int(purchase_row.tipAmount)
				myVoidAmount += int(purchase_row.amount) + int(purchase_row.tipAmount)


		sale = {'count' : mynumSales,
				'total' : 'Rs ' + str(mysaleAmount/100.0)}

		void = {'count' : mynumVoids,
				'total' : '-Rs ' + str(myVoidAmount/100.0)}

		refund = {'count' : 0,
				'total' : '0'}

		#TODO if refund is unavailable for this tid, skip it
		total = mysaleAmount - myVoidAmount
		netTotal = total# - refund['total']

		summary = {'title' : 'Current Batch',
					'mid' : ' ',
					'ttime' : self.bundle['timeStamp'],
					'batch' : int(self.details.batch),
					'sale' : sale,
					'void' : void,
					'total' : str(total/100.0),
					'refund' : refund,
					'netTotal' : str(netTotal/100.0),
					'type' : common.BATCH_TOTALS_REPORT}#for debugging purposes
		
		report['HDFC'][str(self.bundle['clientId'])] = summary
		report['isSettled'] = (len(purchase_entries) == 0)

		report = {'report' : report}#settleReport response format or errorMessage

		#we got a result so we are incrementing sequence codes.
		self.details.minSequenceCode = self.details.sequenceCode
		self.details.sequenceCode += 1
		#details.plutusSequenceCode = plutusRespSequenceCode
		
		common.disableLastTransaction(self.bundle['clientId'], common.SETTLEMENT_REPORT, self.details.sequenceCode - 1, 0)
				
		#db.session.commit() #ERRORPRONE
		return report
