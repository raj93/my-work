from flask import current_app
from src.mAuthenticate import common
from ext import db
from src.models import Void as void_table
import stripe

class Void:

	def __init__(self, details, purchase_entry, bundle):
		self.details = details
		self.purchase_entry = purchase_entry
		self.bundle = bundle

	def process(self):

		output = {}

		"""
		# pre request ATOS
		purchase_entry.transId
		"""

		# TODO ISO 8583 code
		
		card = {'cardNumber' : self.purchase_entry.cardNumber, 
				'MM' : self.purchase_entry.expiryMonth,
				'YY' : self.purchase_entry.expiryYear
				}
		
		card['chName'] = self.purchase_entry.chname
		
		merch = self.purchase_entry.merchantDetails

		merchant = {'id' : merch.mid,
					'name' : merch.nameOnReceipt,
					'address' : merch.address1+merch.address2 ,
					'city' : merch.city}		

		transactionInfo = {'baseAmount' : self.bundle['amount'],
							'acquirer' : 'HDFC',
							'invoice' : str(self.details.sequenceCode),
							'batchNumber' : str(self.details.batch),
							'transactionTime' : self.bundle['timestamp'],
							'apprCode' : '000000',
							'rrn' : '000000000020',
							'transId' : self.purchase_entry.transId,
							'terminalId' : self.bundle['clientId']
				}			
		
		#modifying the void bit because it is a success
		if self.purchase_entry.status & common.VOID_BIT_VALUE == 0:
			self.purchase_entry.status += common.VOID_BIT_VALUE
			if self.purchase_entry.status & common.TIP_BIT_VALUE != 0:
				self.purchase_entry.tip_details.isDisabled = True
		else:
			#voiding a void 
			current_app.logger.warning('Voiding a void is not possible. ID:' + self.bundle['transId'] + ' ClientID:' + str(self.bundle['clientId']))

		#Adding entry to the void table with reference to Purchase entry(id)
		entry = void_table(self.bundle['username'], self.bundle['clientId'], self.details.sequenceCode, self.details.plutusSequenceCode, \
			self.purchase_entry.id, self.bundle['transId'], self.bundle['timestamp'], transactionInfo['invoice'], transactionInfo['apprCode'], \
			transactionInfo['rrn'])
		db.session.add(entry)
		db.session.commit()

		rowId = entry.id
		tranType = common.VOID
		output.update({'merchant' : merchant, 'card' : card, 'transactionInfo': transactionInfo})
	
		#we got a result so we are incrementing sequence codes.
		self.details.minSequenceCode = self.details.sequenceCode
		self.details.sequenceCode += 1
		#details.plutusSequenceCode = plutusRespSequenceCode
		
		common.disableLastTransaction(self.bundle['clientId'], tranType, self.details.sequenceCode - 1, rowId)
		return output
