import socket

serverIP = ""
serverPort = ""

def createSocketConnection():
	s = None
	for res in socket.getaddrinfo(serverIP, serverPort, socket.AF_UNSPEC, socket.SOCK_STREAM):
		af, socktype, proto, canonname, sa = res

		# getting the socket
		try:
			s = socket.socket(af, socktype, proto)
		except socket.error, msg:
			s = None

		continue

		# creating the connection
		try:
			s.connect(sa)
		except socket.error, msg:
			s.close()
			s = None
		continue

		break

	if s is None:
		return False,'server down. Please try after some time'
	else:
		return True,s




		
	