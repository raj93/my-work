#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login
from src.transactions.tests.test_settleReport import settleReport
from src.transactions.tests.test_purchase import purchase
from src.transactions.tests.test_tip import tip
#from src.transactions.tests.test_settleRequest import settleRequest, settleRequest1
from src.models import LastTransaction, Transaction, Settlement, pendingTip, Tip, Void
import simplejson as json
from datetime import datetime
from src.mAuthenticate.common import errorMessageD, ERROR_CODES
import os


class testSettleReport(KitTestCase):
	testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)

		self.details.save()
		self.details.clientId = self.details.id
		self.details.update()

		self.payment.clientId = self.details.clientId
		self.payment.save()

		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP, deviceIdCount
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Tip.query.delete()
		Void.query.delete()
		pendingTip.query.delete()
		Settlement.query.delete()
		Transaction.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()


	def test4(self):
		'''
			Simple settleReport request. 
			Expected Output:  {'report' : report}
		'''
		
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])#ShouldChangePassword set to True

		self.user.update(shouldChangePassword = False)#Setting ShouldChangePassword to False

		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		
		response_data = settleReport(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, \
			self.details.sequenceCode, conflict)

		assert 'report' in response_data
	
"""	def test5(self):
		'''
			Tests the case where conflict is at mobile but not server. Sends valid sequence code corresponding to conflict case.
			i. details.insettleConflict = False
			ii. self.conflict = True
			2 cases:
			i. sequeneCode = reqSequenceCode + 1 ==> settleCode = 'l'
			ii. sequenceCode = reqSequenceCode ==> INVALID_SEQUENCE_CODE
			Expected Output: {'settleCode' : 'l'}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])#ShouldChangePassword set to True

		self.user.update(shouldChangePassword = False)#Setting ShouldChangePassword to False
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		
		sessionToken = response_data['sessionKey']
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = True
		amount = '100'
	
		response_data = settleRequest(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode)
		
		response_data = settleReport(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, \
			self.details.sequenceCode - 1, conflict)
		assert response_data['settleCode'] == 'l'

		response_data = settleReport(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, \
			self.details.sequenceCode, conflict)
		assert response_data['errorCode'] == ERROR_CODES['INVALID_SEQUENCE_CODE']

	def test7(self):
		'''
			Testing conflict cases with settleReport requests. 

			i. Conflict at mobile i.e., self.conflict = True
			ii. Conflict at server side ==> settle request was failure at server i.e., details.sequenceCode == self.reqSequenceCode. We emulate this simply by setting detials.settleConflict = True
			
			a. Dummy purchase to align the sequence code. 
			b. This scenario occurs when a settle request is sent and it fails at server level. We emulate conflict at server level by:
				i. Setting self.conflict = True;
				ii. To get settleCode = 'l', we settle the batch by using settleRequest1 function src/plutus/tests/settle_request.py. It doesn't change the state of transactions
					To get settleCode = 'c', we don't do any request
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		settleRequest1(self.details.plutusClientId, self.details.plutusSecretKey, self.details.plutusSequenceCode)#Clearing any transactions from earlier tests
		#Fill the database with a sample Purchase
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)

		#Emulating settleCode = 'l'
		settleRequest1(self.details.plutusClientId, self.details.plutusSecretKey, self.details.plutusSequenceCode)
		self.details.plutusSequenceCode += 1 #To prevent invalid sequence code for settleStatus in resolveConflict
		self.details.sequenceCode += 1#To prevent below purchase disabling previous purchase. If this is not done... lastTransaction.sequenceCode == seqCode will match in disableLastTransaction and screws it
		self.details.inSettleConflict = True
		self.details.update()

		
		#reqSequenceCode = self.details.sequenceCode - 1
		response_data = settleReport(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, \
			self.details.sequenceCode - 1, True)
		assert 'errorCode' not in response_data and response_data['settleCode'] == 'l'

		#Start a new settlement batch. Fill the database with a sample Purchase
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)

		#Emulating settleCode = 'c'
		self.details.plutusSequenceCode += 1#To prevent settleStatus in resolveConflict cancelling the above purchase
		self.details.sequenceCode += 1#To prevent below purchase disabling previous purchase. If this is not done... lastTransaction.sequenceCode == seqCode will match in disableLastTransaction and screws it
		self.details.inSettleConflict = True
		self.details.update()

		#reqSequenceCode = self.details.sequenceCode - 1
		response_data = settleReport(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, \
			self.details.sequenceCode - 1, True)
		assert 'errorCode' not in response_data and response_data['settleCode'] == 'c'

	def test8(self):
		'''
			settleReport request with pendingTips to be cleared
			Expected Output1:  {'errorCode' : SERVER_BUSY, 'sequenceCode' : sequenceCode, 'pid': pid}#PID returned only in test environments
		'''
		
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, False, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, False, testCard, amount)#Purchase 1
		transId = response_data['transactionInfo']['transId']
		response_data = tip(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode - 1, transId, amount)#PendingTip with invalid sequence code. To prevent the tipWorker from being started
		assert response_data == {'mesg' : 'Success'}

		assert pendingTip.query.count() == 1
		assert Tip.query.count() == 0
		
		response_data = settleReport(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, \
			self.details.sequenceCode, conflict)

		assert response_data['errorCode'] == ERROR_CODES['SERVER_BUSY'] and 'sequenceCode' in response_data and \
			'pid' in response_data
		os.waitpid(response_data['pid'], 0)
		assert Tip.query.count() == 1
		assert pendingTip.query.count() == 0
		
		response_data = settleReport(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, \
			self.details.sequenceCode, conflict)
		assert 'errorCode' not in response_data
"""