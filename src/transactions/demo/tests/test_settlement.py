#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login
from src.models import Transaction
import simplejson as json

from datetime import datetime
from src.mAuthenticate import common
from src.mAuthenticate.common import errorMessageD, ERROR_CODES

def settleRequest(client, deviceId, appVersion, clientId, sessionToken, sequenceCode):

	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")

	terminalInfo = {'deviceId' : deviceId, 'timeStamp' : timeStamp, 'appVersion' : appVersion}
	authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
	settlementRequest_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication}
	
	return client.post(url_for('transactions.settlement_request'),
					data = json.dumps(settlementRequest_data),
					content_type = 'application/json').json

class testSettleRequest(KitTestCase):
	testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)
		
		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP, deviceIdCount
		from src.models import LastTransaction, Settlement
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Settlement.query.delete()
		Transaction.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()

	def test6(self):
		'''
			Settle Request with valid sequenceCode.
			Note: details.inSettleConflict = False
			Expected Output: {'HDFC' : {}, 'sequenceCode' : sequenceCode}
		'''
		from src.transactions.tests.test_purchase import purchase#Keeping it here to avoid circular imports
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		
		self.user.update(shouldChangePassword = False)
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']

		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)
		response_data = settleRequest(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode)

		assert 'errorCode' not in response_data and 'isSettled' in response_data
		
