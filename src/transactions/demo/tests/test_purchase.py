#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login

import simplejson as json
from datetime import datetime
from src.mAuthenticate import common
from src.mAuthenticate.common import errorMessageD
from src.models import Transaction, LastTransaction, Settlement
from sqlalchemy import and_

def purchase(client, deviceId, appVersion, clientId, sessionToken, sequenceCode, deviceType, conflict, card, amount):

	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
	
	terminalInfo = {'deviceId' : deviceId, 'timeStamp' : timeStamp, 'appVersion' : appVersion}
	authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
	
	payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

	purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
	
	response= client.post(url_for('transactions.Purchase'),
					data = json.dumps(purchase_data),
					content_type = 'application/json').json
	return response


class testPurchase(KitTestCase):
	testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)
		
		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP, deviceIdCount
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Transaction.query.delete()
		Settlement.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()

	def testDemo(self):
		# TODO testing pruchase from muira
		# mostly same as testParsedTrack1andTrack2 except ksn and encryptedData adds up. 
		pass


	def testParsedTrack1andTrack2(self):
		'''
			All the deviceTypes finally generate track1 and track2 data. That's what we are checking with deviceType = 0
			Expected Output:  {u'merchant': {u'city': u'NOIDA - INDIA', u'id': None, u'name': u'PINE LABS PVT.LTD.', u'address': u'Testing Server'}, u'sequenceCode': 2, u'transactionInfo': {u'rrn': u'000020', u'baseAmount': u'100', u'terminalId': u'34646946', u'transactionTime': u'2014-01-07T16:35:00.00', u'acquirer': u'HDFC', u'invoice': u'000401', u'batchNumber': u'000222', u'transId': u'2014-01-07 16:34:51.505129_385', u'apprCode': u'00'}, u'card': {u'MM': u'09', u'YY': u'10', u'cardNumber': u'517652******7557', u'chName': u''}}
		'''
		
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		
		self.user.update(shouldChangePassword = False)
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		deviceType = 0
		conflict = False
		amount = '100'
		
		seqCode = self.details.sequenceCode
		
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)
		assert 'errorCode' not in response_data
		p = Transaction.query.filter(and_(Transaction.sequenceCode == seqCode, Transaction.conditions('VALID'))).first()
		lastTransaction = LastTransaction.query.filter(LastTransaction.rowId == p.id).first()
		assert lastTransaction.sequenceCode == seqCode
