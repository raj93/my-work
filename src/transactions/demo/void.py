from flask import current_app
from src.mAuthenticate import common
from ext import db
from src.models import Void as void_table

class Void:

	def __init__(self, details, txn, bundle):
		self.details = details
		self.txn = txn
		self.bundle = bundle

	def process(self):
		output = {}
		card = {'cardNumber' : self.txn.maskedPan,
				'MM' : self.txn.expiryMonth,
				'YY' : self.txn.expiryYear
				}
		
		card['chName'] = self.txn.chname
		
		merchant = {'id' : '',
					'name' : 'AasaanPay Test merchant',
						'address' : 'Gachibowli, IIIT',
					'city' : 'Hyderabad'}		

		transactionInfo = {'baseAmount' : self.bundle['amount'],
							'acquirer' : 'HDFC',
							'invoice' : str(self.details.sequenceCode),
							'batchNumber' : str(self.details.batch),
							'transactionTime' : self.bundle['timestamp'],
							'apprCode' : '000000',
							'rrn' : '000000000020',
							'transId' : self.bundle['transId'],
							'terminalId' : self.bundle['clientId']
				}			
		
		#modifying the void bit because it is a success
		if not self.txn.isVoided():
			self.txn.setVoid()
			if self.txn.isTipped():
				self.txn.tipDetails.setEnabled(False)
		else:
			#voiding a void #TODO
			current_app.logger.warning('Voiding a void is not possible. ID:' + self.bundle['transId'] + ' ClientID:' + str(self.bundle['clientId']))

		#Adding entry to the void table with reference to txn entry(id)
		entry = void_table(self.bundle['username'], self.bundle['clientId'], self.details.sequenceCode, self.details.plutusSequenceCode, \
			self.txn.id, self.bundle['transId'], self.bundle['timestamp'], transactionInfo['invoice'], transactionInfo['apprCode'], \
			transactionInfo['rrn'], void_table.APPROVED)
		db.session.add(entry)
		db.session.commit()

		rowId = entry.id
		tranType = common.VOID
		output.update({'merchant' : merchant, 'card' : card, 'transactionInfo': transactionInfo})
	
		#we got a result so we are incrementing sequence codes.
		self.details.minSequenceCode = self.details.sequenceCode
		self.details.sequenceCode += 1
		#details.plutusSequenceCode = plutusRespSequenceCode
		
		common.disableLastTransaction(self.bundle['clientId'], tranType, self.details.sequenceCode - 1, rowId)
		return output
