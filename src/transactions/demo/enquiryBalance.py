from src.mAuthenticate import common
from ext import db
from src.models import Transaction
from src.utils.parseData import ParseTrackData
import random, requests, simplejson as json
from flask import current_app
from src.utils import TLV

class Enquire:

	#What happens when two requests with same TransId i.e., same ksn & sequenceCode comes at same timestamp. And response to first request after the response of second request?
	#Planning to store all the success requests and show them by grouping based on TransID
	#transID different for any two legitimate requests because of difference in KSN!! -> so grouping based on transID would be bullshit.. grouping based on sequenceCode gives good results..

	def __init__(self, details, bundle):
		self.details = details
		self.bundle = bundle

	def isPinRequired(self):
		#return random.randint(1, 3) == 2 #Mandating pin for 1 out of every 3 transactions
		return False

	def translatePIN(self):
		anb = self.pan[-13:-1]#Right most 12 digits excluding check digit#If pan is not defined, error - bundle['pan'] safer but inefficient
		from src.utils.crypto import otp_generator
		kpe = otp_generator(32, chars = '0123456789abcdef')#TODO will be replaced with pin key sent by processor.

		payload = {
			'pinBlock' : self.bundle['pinBlock'].encode('hex'),
			'ksn' : self.bundle['pinKSN'].encode('hex'),
			'anb' : anb,
			'kpe' : kpe
			}

		headers = {'content-type': 'application/json'}
		try:
			response = requests.post(current_app.config['KEY_TIER_URL'] + 'translatePIN',
					data = json.dumps(payload), headers = headers, verify = False).json()
		except Exception as e:
			current_app.logger.error(e, exc_info=True)
			return False, common.errorMessageD('SERVER_ERROR')

		if response is None:
			return False, common.errorMessageD('SERVER_ERROR')

		if 'errorCode' in response:
			current_app.logger.debug(response['errorCode'])
			return False, common.errorMessageD('INVALID_REQUEST')

		return True, response['mesg']

	def process(self):
		output = {}
		parser = ParseTrackData()

		if not all(key in self.bundle for key in ['pan', 'chName', 'expiry']):
			return common.errorMessageD('INVALID_REQUEST')
		self.pan = self.bundle['pan']

		if 'ksn' in self.bundle:#if ksn is not present, it means that input data is not encrypted ==> not eligible for PIN transactions
			if not all(key in self.bundle for key in ['pinBlock', 'pinKSN']):
				if self.bundle['inputType'] == Transaction.INPUT_TYPE['SWIPE'] and self.isPinRequired():
					return common.errorMessageD('REQUIRE_PIN')
			else:
				status, response = self.translatePIN()
				if not status: return response
				pinBlock = response#Not used in dummy processor

		maskedPan = self.pan[0: 6] + '*'*(len(self.pan) - 10) + self.pan[len(self.pan) - 4: len(self.pan)]

		card = {
			'cardNumber': '*'*6 + maskedPan[6:],
			'MM' : self.bundle['expiry'][2:4],
			'YY' : self.bundle['expiry'][:2],
			'chName': self.bundle['chName'],
			}

		merch = self.details.merchantDetails

		merchant = {'id' : str(merch.id),
					'name' : merch.nameOnReceipt,
					'address' : merch.address1 + merch.address2,
					'city' : merch.city}

		balance = random.randint(1, 10000000)#Random value between 1 paise and 1 lakh
		transactionInfo = {'balance' : balance,
							'acquirer' : 'HDFC',
							'invoice' : str(self.details.sequenceCode),
							'batchNumber' : str(self.details.batch),
							'transactionTime' : self.bundle['timestamp'],
							'apprCode' : '000000',
							'rrn' : '000000000020',
							'transId' : self.bundle['transId'],
							'terminalId' : self.bundle['clientId']
				}

		#TODO Can we store balance ??
		entry = Transaction(self.bundle['username'], self.bundle['clientId'], self.details.sequenceCode, \
			self.details.plutusSequenceCode, self.bundle['transId'], self.bundle['timestamp'], \
			0, 0, card['chName'], card['cardNumber'], card['MM'], \
			card['YY'], self.bundle['inputType'], transactionInfo['invoice'], transactionInfo['batchNumber'], \
			transactionInfo['acquirer'], transactionInfo['apprCode'], transactionInfo['rrn'], \
			self.bundle['clientId'], merchant['id'], self.bundle['token'], Transaction.TTYPE['ENQUIRY'], \
			Transaction.APPROVED_ONLINE, self.details.company, self.details.agent, self.details.merchant)

		db.session.add(entry)
		db.session.commit()
		rowId = entry.id
		tranType = common.BALANCE_ENQUIRY
		output.update({'merchant' : merchant, 'card' : card, 'transactionInfo': transactionInfo})
		# A sample EMV return object for tlv -> Approve transaction Don't remove the following
		'''
		decision = 1
		tlv_DOL = TLV.TLV()
		#tlv_DOL.append(0x01, chr(decision))
		tlv_DOL.append(0x8A, "00")
		tlv_Template = TLV.TLV(0xE1, tlv_DOL)
		output['tlv'] = tlv_Template.serialise().encode('hex')
		print output['tlv']
		'''
		#we got a result so we are incrementing sequence codes.
		self.details.minSequenceCode = self.details.sequenceCode
		self.details.sequenceCode += 1
		common.disableLastTransaction(self.bundle['clientId'], tranType, self.details.sequenceCode - 1, rowId)
		return output
