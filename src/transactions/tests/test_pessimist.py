#TODO
#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login
from src.transactions.tests.test_purchase import purchase
from src.transactions.tests.test_tip import tip
from src.models import Transaction, LastTransaction, Settlement, Void, Tip, pendingTip

import os

class testPessimist(KitTestCase):
	testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)
		
		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP, deviceIdCount
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()
		posLogs.query.delete()
		Tip.query.delete()
		pendingTip.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Void.query.delete()
		Transaction.query.delete()
		Settlement.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()

	def rest1(self):
		'''
			Checks if tip thread clears all the pending tips if there is no interruption with new transaction requests.
			Process:
				i. Send 3 Purchase requests
				ii. Send 2 Tip requests with invalid sequence codes(so that they don't start the tip worker)
				iii. Send a tip request with valid sequence code
				v. Wait for the tip worker process to end. 
				vi. Get count of pendingTip entries in db.
				vii. Verify if second count is zero. 
		'''			
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])#should change password set to true during activation process

		self.user.update(shouldChangePassword = False)#Setting ShouldChangePassword to False
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, False, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, False, testCard, amount)#Purchase 1
		transId = response_data['transactionInfo']['transId']
		response_data = tip(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode - 1, transId, amount)#PendingTip with invalid sequence code. To prevent the tipWorker from being started
		assert response_data == {'mesg' : 'Success'}

		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, False, testCard, amount)#Purchase 2
		transId = response_data['transactionInfo']['transId']
		response_data = tip(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode - 1, transId, amount)#PendingTip 2 with invalid sequence code. To prevent the tipWorker from being started
		assert response_data == {'mesg' : 'Success'}
		
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, False, testCard, amount)#Purchase 3
		transId = response_data['transactionInfo']['transId']
		response_data = tip(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, transId, amount)#PendingTip 3 with valid sequence code. Starts the tipworker
		assert response_data['mesg'] == 'Success' and response_data['pid'] != None
		
		os.waitpid(response_data['pid'], 0)
		
		assert Tip.query.count() == 3
		assert pendingTip.query.count() == 0

	def rest2(self):#TODO 
		'''
			Checks if tip thread stops when a purchase request is sent.
			Process:
				i. Send 3 Purchase requests
				ii. Send 2 Tip requests with invalid sequence codes(so that they don't start the tip worker)
				iii. Send a tip request with valid sequence code
				iv. Send a new purchase request. 
				v. Wait for the tip worker process to end. 
				vi. Get count of pendingTip entries in db.
				vii. Verify if second count is not zero. 
		'''
		
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		self.user.update(shouldChangePassword = False)#Setting ShouldChangePassword to False
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, False, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, False, testCard, amount)#Purchase 1
		transId = response_data['transactionInfo']['transId']
		response_data = tip(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode - 1, transId, amount)#PendingTip with invalid sequence code. To prevent the tipWorker from being started
		assert response_data == {'mesg' : 'Success'}

		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, False, testCard, amount)#Purchase 2
		transId = response_data['transactionInfo']['transId']
		response_data = tip(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode - 1, transId, amount)#PendingTip 2 with invalid sequence code. To prevent the tipWorker from being started
		assert response_data == {'mesg' : 'Success'}
		
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, False, testCard, amount)#Purchase 3
		transId = response_data['transactionInfo']['transId']
		
		response_data = tip(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, transId, amount)#PendingTip 3 with valid sequence code. Starts the tipworker
		assert response_data['mesg'] == 'Success' and response_data['pid'] != None
		pid = response_data['pid']

		# db.engine.dispose()
		db.session.commit()
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, False, testCard, amount)#Purchase 3
		
		os.waitpid(pid, 0)
		db.session.commit()
		assert Tip.query.count() != 3
		assert pendingTip.query.count() != 0
