#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login
from src.models import Transaction, posLogs
import simplejson as json

from datetime import datetime
from src.mAuthenticate import common
from src.mAuthenticate.common import errorMessageD, ERROR_CODES

def settleRequest(client, deviceId, appVersion, clientId, sessionToken, sequenceCode):

	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")

	terminalInfo = {'deviceId' : deviceId, 'timeStamp' : timeStamp, 'appVersion' : appVersion}
	authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
	settlementRequest_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication}
	
	return client.post(url_for('transactions.settlement_request'),
					data = json.dumps(settlementRequest_data),
					content_type = 'application/json').json

class testSettleRequest(KitTestCase):
	testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)

		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP
		from src.models import LastTransaction, Settlement, deviceIdCount
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()
		posLogs.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Settlement.query.delete()
		Transaction.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()
		
	def test1(self):
		'''
			Invalid request. Each request misses one of the elements
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''

		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		sessionToken = current_app.signer.sign('sessionToken')
		clientId = self.details.clientId
		sequenceCode = 1
		
		
		#Missing deviceId
		terminalInfo = {'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		request = {'terminalInfo' : terminalInfo, 'authentication' : authentication}

		response_data =  self.client.post(url_for('transactions.settlement_request'),
							data = json.dumps(request),
							content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing sequenceCode
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : clientId, 'sessionToken' : sessionToken}
		request = {'terminalInfo' : terminalInfo, 'authentication' : authentication}

		response_data =  self.client.post(url_for('transactions.settlement_request'),
							data = json.dumps(request),
							content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing clientId		
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'sessionToken' : sessionToken}
		request = {'terminalInfo' : terminalInfo, 'authentication' : authentication}

		response_data =  self.client.post(url_for('transactions.settlement_request'),
							data = json.dumps(request),
							content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing sessionToken
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId}
		request = {'terminalInfo' : terminalInfo, 'authentication' : authentication}

		response_data =  self.client.post(url_for('transactions.settlement_request'),
							data = json.dumps(request),
							content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing terminalInfo
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		request = {'authentication' : authentication}

		response_data =  self.client.post(url_for('transactions.settlement_request'),
							data = json.dumps(request),
							content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing authentication
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		request = {'terminalInfo' : terminalInfo}

		response_data =  self.client.post(url_for('transactions.settlement_request'),
							data = json.dumps(request),
							content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Valid Entries
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		request = {'terminalInfo' : terminalInfo, 'authentication' : authentication}

		response_data =  self.client.post(url_for('transactions.settlement_request'),
							data = json.dumps(request),
							content_type = 'application/json').json
		
		self.assertNotEquals(response_data, errorMessageD('INVALID_REQUEST'))

	def test2(self):
		'''
			Invalid values in the request. Each request has one invalid type
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
			#TODO validate checks 
		'''

		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		sessionToken = current_app.signer.sign('sessionToken')
		clientId = self.details.clientId
		
		#Invalid sequenceCode value
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : 'sequenceCode', 'clientId' : clientId, 'sessionToken' : sessionToken}
		request = {'terminalInfo' : terminalInfo, 'authentication' : authentication}

		response_data =  self.client.post(url_for('transactions.settlement_request'),
							data = json.dumps(request),
							content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))		#Invalid sequenceCode
		
		#Invalid ClientId value
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : 'sequenceCode', 'clientId' : 'clientId', 'sessionToken' : sessionToken}
		request = {'terminalInfo' : terminalInfo, 'authentication' : authentication}

		response_data =  self.client.post(url_for('transactions.settlement_request'),
							data = json.dumps(request),
							content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))		#Invalid sequenceCode

		#Invalid Conflict value
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : 'sequenceCode', 'clientId' : clientId, 'sessionToken' : sessionToken}
		request = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'conflict' : 'conflict'}

		response_data =  self.client.post(url_for('transactions.settlement_request'),
							data = json.dumps(request),
							content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))		#Invalid sequenceCode

	def test3(self):
		'''
			Settle Request when server is in conflict i.e., details.inSettleConflict = True
			Expected Output: {'errorCode' : 'INVALID_TRANSACTION'}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])#ShouldChangePassword set to True during activation

		self.user.update(shouldChangePassword = False)#Setting ShouldChangePassword to False
		
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']
		
		self.details.setConflict(True)#Settlement conflict
		self.details.update()

		response_data = settleRequest(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, \
			self.details.sequenceCode)

		assert response_data['errorCode'] == ERROR_CODES['INVALID_TRANSACTION']

	def test4(self):
		'''
			Settle Request with invalid sequenceCode. As settle request is not allowed without sync in the sequence code, it is a invalid request
			Note: details.inSettleConflict = False
			Expected Output: {'errorCode' : 'INVALID_SEQUENCE_CODE}
		'''

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])#ShouldChangePassword set to True

		self.user.update(shouldChangePassword = False)#Setting ShouldChangePassword to False
		
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']
		
		response_data = settleRequest(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, \
			self.details.sequenceCode - 1)

		assert response_data['errorCode'] == ERROR_CODES['INVALID_SEQUENCE_CODE']
