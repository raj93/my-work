#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login
from src.transactions.tests.test_purchase import purchase
#from src.transactions.tests.test_settleRequest import settleRequest1 #TODO
import random
import simplejson as json
from src.mAuthenticate import common
from src.mAuthenticate.common import errorMessageD, canIdoTransaction, validateSequenceCode, disableLastTransaction, resolveConflict
from src.models import Transaction, LastTransaction, Settlement, posLogs

class test_canIdoTransaction(KitTestCase):
	ttypes = [common.PURCHASE, common.VOID, common.TIP, common.SETTLEMENT, common.ERROR_IN_TRANSACTION, common.SETTLEMENT_REPORT, common.VOID_SEARCH]	
	'''
	Note that calls to this function are made only after isAuthorise function. 
	So no requests with invalid credentials are sent to the function
	'''

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)
		
		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP, deviceIdCount
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()
		posLogs.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Transaction.query.delete()
		Settlement.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()
	
	def canI(self, ttype):
		response = canIdoTransaction(self.details.clientId, ttype, self.user.username)
		assert response[0] and response[1] == self.details

	def canI2(self, ttype):
		response = canIdoTransaction(self.details.clientId, ttype, self.user.username)
		assert response[0] == False and response[1] == None

	def test1(self):
		'''
			Invalid request. Doesn't happen in live scenario.
			Giving a clientId that doesn't exist in db
			Expected Output: False, None
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		self.user.update(shouldChangePassword = False)

		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)

		response = canIdoTransaction(self.details.clientId + 1, random.randint(1, 6), self.user.username)
		assert response[0] == False and response[1] == None

	def test2(self):
		'''
			Valid request. 
			* details.transactionStatus = True
			* details.transactionType = TIP
			Emulating the scenario through db requests.
			Test repeated for all transaction Types: Purchase, Tip, Void, Settlement, Settle Report
			Also checks if transaction Type is changed to common.PURCHASE when canIdoTransaction request is 
			send with ttype = Purchase
			Expected Output: False, None
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		self.user.update(shouldChangePassword = False)

		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)

		self.details.setTransactionStatus(True)
		self.details.transactionType = common.TIP
		self.details.update()
		
		self.canI2(common.VOID)
		assert self.details.transactionType == common.VOID

		self.details.setTransactionStatus(True)
		self.details.transactionType = common.TIP
		self.details.update()
		
		self.canI2(common.TIP)
		assert self.details.transactionType == common.TIP
		self.canI2(common.SETTLEMENT)
		assert self.details.transactionType == common.TIP
		self.canI2(common.ERROR_IN_TRANSACTION)
		assert self.details.transactionType == common.TIP
		self.canI2(common.SETTLEMENT_REPORT)
		assert self.details.transactionType == common.TIP
		self.canI2(common.VOID_SEARCH)
		assert self.details.transactionType == common.TIP
		#Keeping purchase in the end as it would modify self.details.transactionType to common.PURCHASE
		self.canI2(common.PURCHASE)
		assert self.details.transactionType == common.PURCHASE
	
	def test3(self):
		'''
			Valid request. Similar to test2 except for self.transactionType 
			* details.transactionStatus = True
			* details.transactionType != TIP (setting to PURCHASE)
			Emulating the scenario through db requests.
			Test repeated for all transaction Types: Purchase, Tip, Void, Settlement, Settle Report
			Expected Output: False, None
		'''	

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		self.user.update(shouldChangePassword = False)
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)

		self.details.setTransactionStatus(True)
		self.details.transactionType = common.PURCHASE
		self.details.update()

		for ttype in self.ttypes:
			self.canI2(ttype)
		
	def test4(self):
		'''
			Valid request. 
			* details.transactionStatus = False(Default value after entry of new credentials is False)
			Test repeated for all transaction Types: Purchase, Tip, Void, Settlement, Settle Report
			Checks if 
				i. details.transactionStatus = True
				ii. details.transactionType = ttype
		'''
		
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		self.user.update(shouldChangePassword = False)
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)

		for ttype in self.ttypes:
			self.canI(ttype)
			assert self.details.getTransactionStatus() and self.details.transactionType == ttype
			self.details.setTransactionStatus(False)
			db.session.commit()

class test_validateSequenceCode(KitTestCase):
	'''
	Note that calls to this function are made only after isAuthorise function. 
	So no requests with invalid credentials are sent to the function
	'''

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)
		
		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP, deviceIdCount
		from src.models import LastTransaction, Transaction
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()
		posLogs.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == details.clientId
		Transaction.query.delete()
		Settlement.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()
	
	def validate1(self, seqCode):
		response = validateSequenceCode(seqCode, self.details)
		assert response[0] == False and response[1] == json.dumps({'errorCode' : common.ERROR_CODES['INVALID_SEQUENCE_CODE'], 'sequenceCode' : self.details.sequenceCode})
		assert self.details.getTransactionStatus() == False

	def test1(self):
		'''
			Invalid sequenceCode. 
			i. Sequence code less than minSequenceCode
			ii. Sequence code more than minSequenceCode
			Expected Output: 
				(False, INVALID_SEQUENCE_CODE)
			Expected config in db:
				details.transactionStatus = False
		'''
		self.validate1(self.details.minSequenceCode - 1)
		self.validate1(self.details.sequenceCode + 1)
	
	def validate2(self, seqCode):
		response = validateSequenceCode(seqCode, self.details)
		assert response[0] == True and response[1] == self.details
	
	def test2(self):
		'''
			Valid sequenceCode. 
			i. Sequence code = details.minSequenceCode
			ii. Sequence code = details.sequenceCode
			Expected Output: 
				(True, self.details)
			Expected config in db:
				status.transactionStatus = True(unchanged)

		'''	
		self.validate2(self.details.minSequenceCode)
		self.validate2(self.details.sequenceCode)	

class test_disableLastTransaction(KitTestCase):#TODO move to demo package??
	'''
	Note that calls to this function are made only after isAuthorise function. 
	So no requests with invalid credentials are sent to the function
	'''

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)

		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP, deviceIdCount
		from src.models import LastTransaction, Transaction
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()
		posLogs.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Transaction.query.delete()
		Settlement.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()

	def test1(self):
		'''
			Tests the behavior when no lastTransaction entry is found. 
			Note: This case is not practical in live scenarios
			Process of execution: Manually deletes the LastTransaction entry
			Expected Behavior: A new LastTransaction is to be created
		'''	
		LastTransaction.query.delete()
		db.session.commit()
		disableLastTransaction(self.details.clientId, common.PURCHASE, 1, 0)#Random values for seqCode and rowId arguments
		db.session.commit()
		assert LastTransaction.query.count() == 1

	def test2(self):
		'''
			Sequence Code of current transaction = Previous transaction's sequence code
			Process: Do a purchase and disableLastTransaction with same sequence code
			Expected Behavior: Purchase will be disabled and lastTransaction entry will point to the current transaction
			#TODO extend the tests to VOID and TIP after they're done
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])

		self.user.update(shouldChangePassword = False)
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, 
			self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
	 		'track2' : ';5176521005587557=10091010017720?'}
	 	deviceType = 0
	 	conflict = False
	 	amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)#Purchase entry added to db
		disableLastTransaction(self.details.clientId, common.PURCHASE, self.details.sequenceCode - 1, random.randint(1, 100))
		db.session.commit()

		purchaseEntry = Transaction.query.first()
		assert not purchaseEntry.isEnabled()
		lastTransaction = LastTransaction.query.first()
		assert lastTransaction.transactionType == common.PURCHASE
		assert lastTransaction.sequenceCode == self.details.sequenceCode - 1		

	def test3(self):
		'''
			Sequence Code of current transaction != Previous transaction's sequence code
			Process: Do a purchase and disableLastTransaction with next sequence code
			Expected Behavior: LastTransaction entry will point to the current transaction
			#TODO extend the tests to VOID and TIP after they're done
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])

		self.user.update(shouldChangePassword = False)
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
	 		'track2' : ';5176521005587557=10091010017720?'}
	 	deviceType = 0
	 	conflict = False
	 	amount = '100'
		# response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)#Alignment of PLUTUS sequenceCode	0
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)#Purchase entry added to d0
		disableLastTransaction(self.details.clientId, common.PURCHASE, self.details.sequenceCode + 1, random.randint(1, 100))
		db.session.commit()

		purchaseEntry = Transaction.query.first()
		assert purchaseEntry.isEnabled()
		lastTransaction = LastTransaction.query.first()
		assert lastTransaction.transactionType == common.PURCHASE
		assert lastTransaction.sequenceCode == self.details.sequenceCode + 1


class test_resolveConflict(KitTestCase):

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)
		
		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP, deviceIdCount
		from src.models import LastTransaction, Transaction
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()
		posLogs.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		self.details.delete()
		self.payment.delete()
		Transaction.query.delete()
		Settlement.query.delete()
		self.details.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()


	def rest1(self):#TODO
		'''
			i. Conflict at mobile i.e., self.conflict = True
			ii. Conflict at server side ==> settle request was failure at server i.e., details.sequenceCode == self.reqSequenceCode. 
			
			a. Dummy purchase to align the sequence code. 
			b. This scenario occurs when a settle request is sent and it fails at server level. We emulate conflict at server level by:
				i. Setting self.conflict = True;
				ii. To get settleCode = 'l', we settle the batch by using settleRequest1 function src/plutus/tests/settle_request.py. It doesn't change the state of transactions
					To get settleCode = 'c', we don't do any request
			c. Sending a new purchase with (b) steps' sequence code
			Note: Might require POS_SESSION_TIME > 60 seconds at times.
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		settleRequest1(self.details.plutusClientId, self.details.plutusSecretKey, self.details.plutusSequenceCode)#Clearing any transactions from earlier tests
		
		#Fill the database with sample transactions
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)
		

		#Emulating settleCode = 'l'
		settleRequest1(self.details.plutusClientId, self.details.plutusSecretKey, self.details.plutusSequenceCode)
		
		#Emulating conflict at server
		self.details.plutusSequenceCode += 1 #To prevent invalid sequence code for settleStatus in resolveConflict
		self.details.inSettleConflict = True
		self.details.update()

		output = resolveConflict(self.details, self.user.username, True, self.details.sequenceCode - 1)#reqSequenceCode = self.details.sequenceCode - 1
		assert output['settleCode'] == 'l'

		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, False, testCard, amount)
		#Emulating settleCode = 'c'
		self.details.plutusSequenceCode += 1#To prevent settleStatus in resolveConflict cancelling the above purchase
		#Note that .sequenceCode has to be changed if purchase is invoked directly below instead of resolveConflict to take care of DisableLastTransaction
		self.details.inSettleConflict = True
		self.details.update()
		
		output = resolveConflict(self.details, self.user.username, True, self.details.sequenceCode - 1)#reqSequenceCode = self.details.sequenceCode - 1
		assert output['settleCode'] == 'c'
	
	def rest2(self):#Plutus. Doesn't work with dummy
		'''
			Tests the case where:
			i. self.conflict = False
			ii. details.inSettleConflict = True
			Expected Output: {'errorCode' : 'INVALID_TRANSACTION'}	
		'''	
		self.details.setConflict(True)
		self.details.update()

		output = resolveConflict(self.details, self.user.username, False, self.details.sequenceCode - 1)
		self.assertEquals(output, errorMessageD('INVALID_TRANSACTION'))

	def test3(self):
		'''
			Tests the case where conflict is at mobile but not server. Sends valid sequence code corresponding to conflict case.
			i. details.insettleConflict = False
			ii. self.conflict = True
			iii. sequeneCode = reqSequenceCode + 1
			Expected Output: {'settleCode' : 'l'}
		'''
		#Note: details.insettleconflict is False by default
		output = resolveConflict(self.details, self.user.username, True, self.details.sequenceCode - 1)
		self.assertEquals(output, {'settleCode' : 'l'})

	def test4(self):
		'''
			Tests the case where conflict is at mobile but not server. Sends invalid sequence code corresponding to conflict case.
			i. details.insettleConflict = False
			ii. self.conflict = True
			iii. sequeneCode != reqSequenceCode + 1 (sending = reqSequenceCode)
			Expected Output: {'errorCode' : 'INVALID_SEQUENCE_CODE'}
		'''
		#Note: details.insettleconflict is False by default
		output = resolveConflict(self.details, self.user.username, True, self.details.sequenceCode )
		self.assertEquals(output, errorMessageD('INVALID_SEQUENCE_CODE'))

	