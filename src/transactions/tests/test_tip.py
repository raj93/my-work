#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login
from src.transactions.tests.test_purchase import purchase
from src.models import smsOTP
from src.models import LastTransaction, Transaction, pendingTip, Tip, posLogs
import simplejson as json
from datetime import datetime
from src.mAuthenticate.common import errorMessageD
import os

def tip(client, deviceId, appVersion, clientId, sessionToken, sequenceCode, transId, amount):

	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
	
	terminalInfo = {'deviceId' : deviceId, 'timeStamp' : timeStamp, 'appVersion' : appVersion}
	authentication = {'clientId' : clientId, 'sessionToken' : sessionToken}
	payload = {'transId' : transId, 'tipAmount' : amount, 'sequenceCode' : sequenceCode}
	pendingTip_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}

	return client.post(url_for('transactions.pending_tip'),
					data = json.dumps(pendingTip_data),
					content_type = 'application/json').json

class testTip(KitTestCase):
	'''
		This concerns pendingTip. Tip is not used as of now
	'''
	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)
		
		self.deviceId = getDeviceId()
		self.transId = getTransID()

		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from ext import db
		from src.models import deviceIdCount
		
		deviceIdCount.query.delete()
		self.user.delete()
		posLogs.query.delete()
		Tip.query.delete()
		pendingTip.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Transaction.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()
		
	def test1(self):
		'''
			Invalid request. Each request misses one of the elements
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''

		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		sessionToken = current_app.signer.sign('sessionToken')
		clientId = self.details.clientId
		sequenceCode = 1
		amount = '100'

		#Valid request
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'tipAmount' : amount, 'transId' : self.transId, 'sequenceCode' : sequenceCode}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.pending_tip'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json

		self.assertNotEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing deviceId
		terminalInfo = {'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'tipAmount' : amount, 'transId' : self.transId, 'sequenceCode' : sequenceCode}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.pending_tip'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing clientId
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sessionToken' : sessionToken}
		
		payload = {'tipAmount' : amount, 'transId' : self.transId, 'sequenceCode' : sequenceCode}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.pending_tip'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing sessionToken
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : clientId}
		
		payload = {'tipAmount' : amount, 'transId' : self.transId, 'sequenceCode' : sequenceCode}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.pending_tip'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing tipAmount
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'transId' : self.transId, 'sequenceCode' : sequenceCode}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.pending_tip'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing transId
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'tipAmount' : amount, 'sequenceCode' : sequenceCode}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.pending_tip'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing sequenceCode
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'tipAmount' : amount, 'transId' : 'transId'}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.pending_tip'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing terminalInfo
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'tipAmount' : amount, 'transId' : self.transId, 'sequenceCode' : sequenceCode}

		purchase_data = {'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.pending_tip'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing authentication
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'tipAmount' : amount, 'transId' : self.transId, 'sequenceCode' : sequenceCode}

		purchase_data = {'terminalInfo' : terminalInfo, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.pending_tip'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing payload tag
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'tipAmount' : amount, 'transId' : self.transId, 'sequenceCode' : sequenceCode}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication}
		
		response_data = self.client.post(url_for('transactions.pending_tip'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

	def test2(self):
		'''
			Invalid values in the request. Each request has one invalid value
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''

		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		sessionToken = current_app.signer.sign('sessionToken')
		clientId = self.details.clientId
		amount = '100'

		#Invalid sequenceCode
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'tipAmount' : amount, 'transId' : self.transId, 'sequenceCode' : 'sequenceCode'}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.pending_tip'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Invalid clientId
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : 'clientId', 'sessionToken' : sessionToken}
		
		payload = {'tipAmount' : amount, 'transId' : self.transId, 'sequenceCode' : 'sequenceCode'}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.pending_tip'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

	def test3(self):
		'''
			Pending Tip request with invalid TransID
			Expected Output: {'errorCode' : 'INVALID_TRANSACTION'}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])#ShouldChangePassword set to True during activation

		self.user.update(shouldChangePassword = False)#Setting ShouldChangePassword to False

		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		amount = '100'
		response_data = tip(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, self.transId, amount)
		self.assertEquals(response_data, errorMessageD('INVALID_TRANSACTION'))


	def is_running(self, pid):
	    """Check whether pid exists in the current process table.  UNIX only. Supported in windows 
	    """
	    if pid < 0:
	        return False

	    if pid == 0:
	        # According to "man 2 kill" PID 0 refers to every process
	        # in the process group of the calling process.
	        # On certain systems 0 is a valid PID but we have no way
	        # to know that in a portable fashion.
	        raise ValueError('invalid PID 0')
	    try:
	    	os.kill(pid, 0)
	    except OSError as err:
	        if err.errno == errno.ESRCH: # ESRCH == No such process
	            return False
	        elif err.errno == errno.EPERM:
	            # EPERM clearly means there's a process to deny access to
	            return True
	        else:
	            # According to "man 2 kill" possible error values are
	            # (EINVAL, EPERM, ESRCH)
	            raise
	    else:
	        return True
