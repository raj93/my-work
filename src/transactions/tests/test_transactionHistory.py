#TODO work on tests 3-5. 
#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login
from src.transactions.tests.test_purchase import purchase
from src.models import Transaction, LastTransaction, Settlement, Void, Tip, pendingTip, posLogs
import simplejson as json
from datetime import datetime
from src.mAuthenticate.common import errorMessageD

def transactionHistory(client, deviceId, appVersion, clientId, sessionToken, sequenceCode, conflict):

	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")

	terminalInfo = {'deviceId' : deviceId, 'timeStamp' : timeStamp, 'appVersion' : appVersion}
	authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
	history_data = {'terminalInfo': terminalInfo, 'authentication' : authentication, 'conflict' : conflict}
		
	return client.post(url_for('transactions.transaction_history'),
					data = json.dumps(history_data),
					content_type = 'application/json').json

class testTransactionHistory(KitTestCase):
	'''
		These tests are not related to any payment processor. All tests related to this functionality will exist in this class
	'''
	testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)
		
		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP, deviceIdCount
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()		
		posLogs.query.delete()
		Tip.query.delete()
		pendingTip.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Void.query.delete()
		Transaction.query.delete()
		Settlement.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()
		
	def test1(self):
		'''
			Invalid request. Each request misses one of the elements (except one)
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''
		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		sessionToken = current_app.signer.sign('sessionToken')

		clientId = self.details.clientId
		sequenceCode = 1
		conflict = True

		# Valid request
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		history_data = {'terminalInfo': terminalInfo, 'authentication' : authentication, 'conflict' : conflict}
		
		response_data = self.client.post(url_for('transactions.transaction_history'),
					data = json.dumps(history_data),
					content_type = 'application/json').json
		
		self.assertNotEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing deviceId
		terminalInfo = {'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		history_data = {'terminalInfo': terminalInfo, 'authentication' : authentication, 'conflict' : conflict}
		
		response_data = self.client.post(url_for('transactions.transaction_history'),
					data = json.dumps(history_data),
					content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing sequenceCode 
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : clientId, 'sessionToken' : sessionToken}
		history_data = {'terminalInfo': terminalInfo, 'authentication' : authentication, 'conflict' : conflict}
		
		response_data = self.client.post(url_for('transactions.transaction_history'),
					data = json.dumps(history_data),
					content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing clientId
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'sessionToken' : sessionToken}
		history_data = {'terminalInfo': terminalInfo, 'authentication' : authentication, 'conflict' : conflict}
		
		response_data = self.client.post(url_for('transactions.transaction_history'),
					data = json.dumps(history_data),
					content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing sessionToken
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId}
		history_data = {'terminalInfo': terminalInfo, 'authentication' : authentication, 'conflict' : conflict}
		
		response_data = self.client.post(url_for('transactions.transaction_history'),
					data = json.dumps(history_data),
					content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing terminalInfo
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		history_data = {'authentication' : authentication, 'conflict' : conflict}
		
		response_data = self.client.post(url_for('transactions.transaction_history'),
					data = json.dumps(history_data),
					content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing authentication
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		history_data = {'terminalInfo': terminalInfo, 'conflict' : conflict}
		
		response_data = self.client.post(url_for('transactions.transaction_history'),
					data = json.dumps(history_data),
					content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing conflict
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		history_data = {'terminalInfo': terminalInfo, 'authentication' : authentication}
		
		response_data = self.client.post(url_for('transactions.transaction_history'),
					data = json.dumps(history_data),
					content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

	def test2(self):
		'''
			Invalid values in the request. Each request has one invalid type
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''
		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		sessionToken = current_app.signer.sign('sessionToken')

		clientId = self.details.clientId
		sequenceCode = 1
		conflict = False
		#Invalid sequenceCode
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : 'sequenceCode', 'clientId' : clientId, 'sessionToken' : sessionToken}
		history_data = {'terminalInfo': terminalInfo, 'authentication' : authentication, 'conflict' : conflict}
		
		response_data = self.client.post(url_for('transactions.transaction_history'),
					data = json.dumps(history_data),
					content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Invalid ClientId
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : 'clientId', 'sessionToken' : sessionToken}
		history_data = {'terminalInfo': terminalInfo, 'authentication' : authentication, 'conflict' : conflict}
		
		response_data = self.client.post(url_for('transactions.transaction_history'),
					data = json.dumps(history_data),
					content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Invalid conflict value
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		history_data = {'terminalInfo': terminalInfo, 'authentication' : authentication, 'conflict' : 'conflict'}
		
		response_data = self.client.post(url_for('transactions.transaction_history'),
					data = json.dumps(history_data),
					content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

	def test3(self):
		'''
			Conflict at client
			Expected Output: {'settleCode' : }
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])#ShouldChangePassword set to True during activation

		self.user.update(shouldChangePassword = False)#Setting ShouldChangePassword to False

		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		deviceType = 0
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)
		assert 'errorCode' not in response_data

		response_data = transactionHistory(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode - 1, True)
		assert 'settleCode' in response_data and 'errorCode' not in response_data

	def test4(self):
		'''
			Conflict at server and client
			Expected Output: {'settleCode' : }
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])#ShouldChangePassword set to True during activation

		self.user.update(shouldChangePassword = False)#Setting ShouldChangePassword to False
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		deviceType = 0
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)
		assert 'errorCode' not in response_data

		self.details.setConflict(True)#Settlement conflict
		self.details.update()

		response_data = transactionHistory(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode - 1, True)
		assert 'settleCode' in response_data and 'errorCode' not in response_data

	def test5(self):
		'''
			No conflict at client or server
			Expected Output: {'transactions' : }
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])#ShouldChangePassword set to True during activation

		self.user.update(shouldChangePassword = False)#Setting ShouldChangePassword to False
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		deviceType = 0
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)
		assert 'errorCode' not in response_data

		response_data = transactionHistory(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, False)
		assert 'transactions' in response_data and 'errorCode' not in response_data

