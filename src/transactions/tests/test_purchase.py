#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.from src.transactions.tests.test_settleRequest import settleRequest1
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login

import simplejson as json
from datetime import datetime
from src.mAuthenticate import common
from src.mAuthenticate.common import errorMessageD
from src.models import Transaction, LastTransaction, Settlement, posLogs
from sqlalchemy import and_

def purchase(client, deviceId, appVersion, clientId, sessionToken, sequenceCode, deviceType, conflict, card, amount):

	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
	
	terminalInfo = {'deviceId' : deviceId, 'timeStamp' : timeStamp, 'appVersion' : appVersion}
	authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
	
	payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

	purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
	
	response= client.post(url_for('transactions.Purchase'),
					data = json.dumps(purchase_data),
					content_type = 'application/json').json
	return response


class testPurchase(KitTestCase):
	'''
		This class covers test cases which cover only missing attributes or invalid attributes. Valid cases and complex ones will be covered in src/transactions/[xxxx]/tests/test_purchase where xxxx is Atos/Demo/Plutus
	'''
	testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)
		
		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP, deviceIdCount
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()
		posLogs.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Transaction.query.delete()
		Settlement.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()
		
	def test1(self):
		'''
			Invalid request. Each request misses one of the elements
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''
		
		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		sessionToken = current_app.signer.sign('sessionToken')
		clientId = self.details.clientId
		sequenceCode = 1
		conflict = True
		amount = '100'

		# Going with deviceType = 1 & card = PLAIN for verifying missing tags..
		deviceType = 0
		card = self.testCard


		#Missing deviceIdCount
		terminalInfo = {'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : self.testCard, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Missing terminalInfo
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : self.testCard, 'amount' : amount}

		purchase_data = {'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Missing sequenceCode
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : self.testCard, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing clientId
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'sessionToken' : sessionToken}
		
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : self.testCard, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing sessionToken
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId}
		
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : self.testCard, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing authentication
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : self.testCard, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		

		#Missing deviceType
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'conflict' : conflict, 'card' : self.testCard, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))


		#Missing conflict tag
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'deviceType' : deviceType, 'card' : self.testCard, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))


		#Missing card tag
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))


		#Missing amount tag
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : self.testCard}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))


		#Missing payload tag
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : self.testCard, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# We don't support this now. 
		# #Verifying response is not equal to INVALID_REQUEST; Sending deviceType = MANUAL_TAG & tags corresponding to manual entry

		# terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		# authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		# deviceType = 0
		# card = {'cardNumber' : '5176521005587557', 'MM' : '01', 'YY' : '01', 'chName' : 'test'}

		# payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		# purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		# response_data = self.client.post(url_for('transactions.Purchase'),
		# 				data = json.dumps(purchase_data),
		# 				content_type = 'application/json').json
		# self.assertNotEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing cardNumber for deviceType = MANUAL_TAG

		# terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		# authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		# deviceType = 0
		# card = {'MM' : '01', 'YY' : '01', 'chName' : 'test'}

		# payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		# purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		# response_data = self.client.post(url_for('transactions.Purchase'),
		# 				data = json.dumps(purchase_data),
		# 				content_type = 'application/json').json
		# self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))


		# #Missing MM
		# terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		# authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		# deviceType = 0
		# card = {'cardNumber' : '5176521005587557', 'YY' : '01', 'chName' : 'test'}

		# payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		# purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		# response_data = self.client.post(url_for('transactions.Purchase'),
		# 				data = json.dumps(purchase_data),
		# 				content_type = 'application/json').json
		# self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))


		# #Missing YY
		# terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		# authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		# deviceType = 0
		# card = {'cardNumber' : '5176521005587557', 'MM' : '01', 'chName' : 'test'}

		# payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		# purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		# response_data = self.client.post(url_for('transactions.Purchase'),
		# 				data = json.dumps(purchase_data),
		# 				content_type = 'application/json').json
		# self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# #Missing chName
		# terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		# authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		# deviceType = 0
		# card = {'cardNumber' : '5176521005587557', 'MM' : '01', 'YY' : '14'}

		# payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		# purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		# response_data = self.client.post(url_for('transactions.Purchase'),
		# 				data = json.dumps(purchase_data),
		# 				content_type = 'application/json').json
		# self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))


		#Verifying response is not equal to INVALID_REQUEST; Sending deviceType = TEST & tags corresponding to Plain card data

		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		deviceType = 0
		card = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
	
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		self.assertNotEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing track1
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		deviceType = 1
		card = {'track2' : ';5176521005587557=10091010017720?'}

		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing track2
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		deviceType = 1
		card = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?'}

		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Verifying response is not equal to INVALID_REQUEST; Sending deviceType = UKPT & tags corresponding to UKPT
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		# deviceType = 2
		# card = {'encryptedTrack' : '%B5176521005587557^sainath         /^10091010010000000772000000?'.encode('hex'), 'track1Length' : 23, 'track2Length' : 35}
	
		# payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		# purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		# response_data = self.client.post(url_for('transactions.Purchase'),
		# 				data = json.dumps(purchase_data),
		# 				content_type = 'application/json').json
		# self.assertNotEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# #Missing encryptedTrack
		# terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		# authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		# deviceType = 2
		# card = {'track1Length' : 23, 'track2Length' : 35}
	
		# payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		# purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		# response_data = self.client.post(url_for('transactions.Purchase'),
		# 				data = json.dumps(purchase_data),
		# 				content_type = 'application/json').json
		# self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))


		# #Missing track1Length
		# terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		# authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		# deviceType = 2
		# card = {'encryptedTrack' : '%B5176521005587557^sainath         /^10091010010000000772000000?'.encode('hex'), 'track2Length' : 35}
	
		# payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		# purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		# response_data = self.client.post(url_for('transactions.Purchase'),
		# 				data = json.dumps(purchase_data),
		# 				content_type = 'application/json').json
		# self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))


		# #Missing track2Length
		# terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		# authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		# deviceType = 2
		# card = {'encryptedTrack' : '%B5176521005587557^sainath         /^10091010010000000772000000?'.encode('hex'), 'track1Length' : 23}
	
		# payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		# purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		# response_data = self.client.post(url_for('transactions.Purchase'),
		# 				data = json.dumps(purchase_data),
		# 				content_type = 'application/json').json
		# self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# #Verifying response is not equal to INVALID_REQUEST; Sending deviceType = DUKPT & tags corresponding to DUKPT
		# terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		# authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}

		''' Device = GDSEEDS DUKPT'''		
		from src.utils.crypto import GDSeeds
		gd = GDSeeds()
		encryptedtrack = gd.encryptData('%B5176521005587557^sainath         /^10091010010000000772000000?'.encode('hex'))
		

		deviceType = 2
		ksn = '0'*20
		card = {'encryptedTrack' : encryptedtrack, 'eType' : 1, 'ksn' : ksn}
	
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		self.assertNotEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing encryptedTrack
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		deviceType = 2
		card = {'eType': 1, 'ksn' : ksn }
	
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing deviceSerial
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		deviceType = 2
		card = {'encryptedTrack' : encryptedtrack, 'eType': 1}
	
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		

		''' Device = GDSEEDS UKPT'''		
		deviceType = 2
		card = {'encryptedTrack' : encryptedtrack, 'eType' : 0, 'deviceSerial' : '0'*10}
	
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		self.assertNotEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing encryptedTrack
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		deviceType = 2
		card = {'eType': 0, 'deviceSerial' : '0'*12 }
	
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing deviceSerial
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		deviceType = 2
		card = {'encryptedTrack' : encryptedtrack, 'eType': 0}
	
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))


		#DeviceType = Miura(1) but TLV is invalid
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		deviceType = 1
		#minimum length of TLV is 2 bytes
		card = {'tlv' : 't'.encode('hex'), 'inputType': 2}
	
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing tlv tag
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		deviceType = 1
		card = {'inputType': 2}
	
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Missing inputType tag
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		deviceType = 1
		card = {'tlv' : 'E181B448020007DFAE0278682DC744CF3078F7720A1531A8741228B866CDC313FBA92E356F0599E054A06E85AEBDC19528058B50CFBB34D67CDC6F394AF89927B115F51BDD06444B2AF105F56960B7806ADBA38321B95B3CB7E03C3D480CE048C8F95B382D7EC819C81437040D9C05A5D653A0F71E058ACECE46AFA77C92944D66C895DFAE030A0000090171808C200071DFAE22223B3534353232362A2A2A2A2A2A323235383D31353130323031303031363339303F3F'}
	
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

	
	def test2(self):
		'''
			Invalid values in the request. Each request has one invalid type
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''

		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		sessionToken = current_app.signer.sign('sessionToken')

		clientId = self.details.clientId
		sequenceCode = 1
		conflict = True
		amount = '100'

		# Going with deviceType = 0 & card = PLAIN for verifying missing tags..
		deviceType = 0
		card = self.testCard


		#Invalid sequenceCode
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : 'sequenceCode', 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : self.testCard, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Invalid clientId
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : 'clientId', 'sessionToken' : sessionToken}
		
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : self.testCard, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Invalid conflict
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : 'sequenceCode', 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		payload = {'deviceType' : deviceType, 'conflict' : 'conflict', 'card' : self.testCard, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
				
		#Invalid inputType value - Doesn't cover all the cases properly; since all devices except GDSeeds use similar code - this should do
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		authentication = {'sequenceCode' : sequenceCode, 'clientId' : clientId, 'sessionToken' : sessionToken}
		

		deviceType = 3
		card = {'tlv' : 'tlv'.encode('hex'), 'inputType' : max(Transaction.INPUT_TYPE.values()) + 1}
	
		payload = {'deviceType' : deviceType, 'conflict' : conflict, 'card' : card, 'amount' : amount}

		purchase_data = {'terminalInfo' : terminalInfo, 'authentication' : authentication, 'payload' : payload}
		
		response_data = self.client.post(url_for('transactions.Purchase'),
						data = json.dumps(purchase_data),
						content_type = 'application/json').json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
