from flask import request, current_app
from datetime import datetime
import simplejson as json
import requests, random
from src.mAuthenticate import common
from ext import db
from src.utils.TLV import unserialise
from src.utils.validate import validateEntry
from src.utils.miuraDataValidators import MSRData, MSRData_SRED, EMVData, EMVData_SRED
from src.models import Transaction, posDetails
import src.transactions.demo.refund
import src.transactions.stripe.refund
from src.utils.parseData import ParseTrackData

class Refund:

	#TODO
	#What happens when two requests with same TransId i.e., same ksn & sequenceCode comes at same timestamp. And response to first request after the response of second request?
	#Planning to store all the success requests and show them by grouping based on TransID
	#transID different for any two legitimate requests because of difference in KSN!! -> so grouping based on transID would be bullshit.. grouping based on sequenceCode gives good results..

	DEVICE_TYPE = {
		'TEST': 0,
		'MIURA': 1,
		'POSMATE': 2,
		'GDSEEDS': 3,
		}

	INPUT_TYPE = Transaction.INPUT_TYPE
	# INPUT_TYPE = {
	# 	'MANUAL' : 0,
	# 	'SWIPE' : 1,
	# 	'CHIP' : 2
		# }
	#Input tags used for validation
	REFUND_TAGS = {
		'terminalInfo': dict,
		'authentication': dict,
		'payload': dict,
		}

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp': unicode,
		#'appVersion': unicode,
		}

	AUTHENTICATION_TAGS = {
		'sequenceCode': int,
		'clientId': int,
		'sessionToken': 'SESSION',
		}

	PAYLOAD_TAGS = {
		'deviceType': int,#Device Type
		'card': dict,
		'amount': 'AMOUNT',
		'conflict': bool,
		}#TODO Tip

	MIURA_CARD_TAGS = {#eType is always DUKPT here
		'inputType': int,
		'tlv': 'TLV',
		}

	MIURA_CARD_TAGS_OPTIONAL = {
		'pin': dict,
		}

	MIURA_PIN_TAGS = {
		'block': 'PINBLOCK',
		'ksn': 'KSN',
		}

	GDSEEDS_CARD_TAGS = {	#inputType is always SWIPE here
		'encryptedTrack': 'ENC_TRACK',
		'eType': int,#Encryption type = UKPT, DUKPT
		}

	GDSEEDS_CARD_TAGS_OPTIONAL = {
		'ksn': 'KSN',
		'deviceSerial': 'DEVICE_ID',
		}

	GDSEEDS_ETYPE = {
		'UKPT': 0,
		'DUKPT': 1,
		}

	TEST_CARD_TAGS = {
		'track1': 'TRACK1',
		'track2': 'TRACK2',
		}

	# #Possible json values of card
	# MANUAL_ENTRY_TAGS = {
	# 	'cardNumber': 'CARD',
	# 	'MM': 'MM',
	# 	'YY': 'YY',
	# 	'chName': 'CHNAME',
	# 	}

	def __init__(self):
		self.json_data = request.get_json()
		self.bundle = {}

	def isValidRequest(self):
		if self.json_data == None:
			return False

		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.REFUND_TAGS.items()):
			return False

		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['authentication'] and validateEntry(self.json_data['authentication'][k], v) \
							for k, v in self.AUTHENTICATION_TAGS.items()):
			return False

		if not all(k in self.json_data['payload'] and validateEntry(self.json_data['payload'][k], v) \
							for k, v in self.PAYLOAD_TAGS.items()):
			return False

		self.deviceType = self.json_data['payload']['deviceType']


		if self.deviceType == self.DEVICE_TYPE['MIURA']:
			if not all(k in self.json_data['payload']['card'] and validateEntry(self.json_data['payload']['card'][k], v) \
							for k, v in self.MIURA_CARD_TAGS.items()):
				return False

			for k, v in self.MIURA_CARD_TAGS_OPTIONAL.items():
				if k in self.json_data['payload']['card'] and not validateEntry(self.json_data['payload']['card'][k], v):
					return False

			if 'pin' in self.json_data['payload']['card']:#If pin key exists, validate it and its child elements
				if not all(k in self.json_data['payload']['card']['pin'] and validateEntry(self.json_data['payload']['card']['pin'][k], v) \
							for k, v in self.MIURA_PIN_TAGS.items()):
					return False

		elif self.deviceType == self.DEVICE_TYPE['GDSEEDS']:
			if not all(k in self.json_data['payload']['card'] and validateEntry(self.json_data['payload']['card'][k], v) \
							for k, v in self.GDSEEDS_CARD_TAGS.items()):
				return False

			if self.json_data['payload']['card']['eType'] == self.GDSEEDS_ETYPE['UKPT']:
				if not ('deviceSerial' in self.json_data['payload']['card'] and \
						validateEntry(self.json_data['payload']['card']['deviceSerial'], self.GDSEEDS_CARD_TAGS_OPTIONAL['deviceSerial'])):
					return False

			elif self.json_data['payload']['card']['eType'] == self.GDSEEDS_ETYPE['DUKPT']:
				if not ('ksn' in self.json_data['payload']['card'] and \
						validateEntry(self.json_data['payload']['card']['ksn'], self.GDSEEDS_CARD_TAGS_OPTIONAL['ksn'])):
					return False

		elif self.deviceType == self.DEVICE_TYPE['TEST']:
			if not all(k in self.json_data['payload']['card'] and validateEntry(self.json_data['payload']['card'][k], v) \
							for k, v in self.TEST_CARD_TAGS.items()):
				return False

		return True

	def match(self, data, comparator):#To match TLV with comparitor
		return any(i.match(self.tlv) for i in comparator)

	def populateTrack1(self, track1):
		parser = ParseTrackData()
		parser.parse(track1)
		self.bundle['pan'] = parser.cardNumber
		self.bundle['chName'] = parser.chName
		self.bundle['expiry'] = parser.YY + parser.MM

	def decodeMiura(self, cardData):
		self.inputType = cardData['inputType']
		self.tlv = unserialise(self.json_data["payload"]["card"]["tlv"].decode("hex"))
		if self.inputType == self.INPUT_TYPE['CHIP']:
			if self.match(self.tlv, EMVData):
				current_app.logger.debug('Chip data without encryption')
				self.inputType = self.bundle['inputType'] = self.INPUT_TYPE['CHIP']
				self.bundle['pan'] = self.tlv.firstMatch([0xE4, 0x5A]).encode('hex')
				self.bundle['chName'] = self.tlv.firstMatch([0xE4, 0x5F20])
				self.bundle['expiry'] = self.tlv.firstMatch([0xE4, 0x5F24]).encode('hex')
				self.bundle['track2'] = ';' + self.bundle['pan'] + '=' + self.bundle['expiry'] + '10017720?'
				return None

			if self.match(self.tlv, EMVData_SRED):
				current_app.logger.debug('Chip data with encryption')
				sredData = self.tlv.firstMatch([0xE4, 0xDFAE02])#encrypted sred data

				self.bundle['ksn'] = self.ksn = self.tlv.firstMatch([0xE4, 0xDFAE03])
				self.bundle['chName'] = self.tlv.firstMatch([0xE4, 0x5F20])
				self.bundle['expiry'] = self.tlv.firstMatch([0xE4, 0x5F24]).encode('hex')

				payload = {'ksn' : self.ksn.encode('hex'), 'cardData' : sredData.encode('hex')}
				headers = {'content-type': 'application/json'}
				try:
					response = requests.post(current_app.config['KEY_TIER_URL'] + 'decryptTrack', data = json.dumps(payload), headers = headers, verify = False)
					response = response.json()
				except Exception as e:
					current_app.logger.error(e)
					return 'SERVER_ERROR'

				if response is None: return 'SERVER_ERROR'
				if 'errorCode' in response:
					current_app.logger.debug(response['errorCode'])
					return response['errorCode']#TODO suitable error

				self.inputType = self.bundle['inputType'] = Transaction.INPUT_TYPE['CHIP']
				decryptedData = response['mesg']
				dfae02 = unserialise(decryptedData.decode('hex'))


				# 5a and 57 are present during unencrypted TLVs . but how was it working earlier? :-/
				try:
					self.bundle['pan'] = (dfae02.firstMatch([0x5a])).encode('hex')
					self.bundle['track2'] = (dfae02.firstMatch([0x57])).encode('hex')
				except AttributeError as e:
					current_app.logger.error(e, exc_info=True)
					return 'INVALID_REQUEST'

				# if pan == '':
				# 	current_app.logger.info('No PAN tag #5a found in #dfae02')
				# 	return False

				# self.bundle['track1'] = self.track1 = dfae02.firstMatch([0x5f21]).encode('hex')
				# self.bundle['track2'] = self.track2 = dfae02.firstMatch([0x5f22]).encode('hex')
				pinBlock = self.tlv.firstMatch([0xE4, 0xDFAE04])
				pinKsn = self.tlv.firstMatch([0xE4, 0xDFAE05])
				if pinBlock is not None and pinKsn is not None:
					self.bundle['pinBlock'] = pinBlock
					self.bundle['pinKSN'] = pinKsn

				return None

		elif self.inputType == self.INPUT_TYPE['SWIPE']:
			if self.match(self.tlv, MSRData):
				self.bundle['track2'] = self.track2 = self.tlv.firstMatch([0xE1, 0x5f22])
				self.track1 = self.tlv.firstMatch([0xE1, 0x5f21])
				self.populateTrack1(self.track1)

				self.inputType = self.bundle['inputType'] = Transaction.INPUT_TYPE['SWIPE']
				current_app.logger.info('MSR data without encryption')
				return None

			if self.match(self.tlv, MSRData_SRED):
				current_app.logger.info('MSR data with encryption')
				sredData = self.tlv.firstMatch([0xE1, 0xDFAE02])#encrypted sred data
				self.bundle['ksn'] = self.ksn = self.tlv.firstMatch([0xE1, 0xDFAE03])

				payload = {'ksn' : self.ksn.encode('hex'), 'cardData' : sredData.encode('hex')}
				headers = {'content-type': 'application/json'}

				response = {}
				try:
					a = current_app.config['KEY_TIER_URL'] + 'decryptTrack'
					response = requests.post(a, data = json.dumps(payload), headers = headers, verify = False, proxies = None).json()
				except Exception as e:
					current_app.logger.error(e, exc_info=True)
					return 'SERVER_ERROR'

				if 'errorCode' in response:
					current_app.logger.debug(response['errorCode'])
					return 'INVALID_REQUEST'

				self.inputType = self.bundle['inputType'] = Transaction.INPUT_TYPE['SWIPE']
				decryptedData = response['mesg']
				dfae02 = unserialise(decryptedData.decode('hex'))
				self.bundle['track2'] = self.track2 = dfae02.firstMatch([0x5f22])
				self.track1 = dfae02.firstMatch([0x5f21])
				self.populateTrack1(self.track1)

				pinBlock = self.tlv.firstMatch([0xE1, 0xDFAE04])
				pinKsn = self.tlv.firstMatch([0xE1, 0xDFAE05])
				if pinBlock is not None and pinKsn is not None:
					self.bundle['pinBlock'] = pinBlock
					self.bundle['pinKSN'] = pinKsn


		else:
			current_app.logger.debug('Only swipe and chip are valid input types')
			return 'INVALID_REQUEST'

		return None

	def decodeGD(self, cardData):
		'''
			This will not be used in production. Keeping it for legacy/backup purpose.
			Revalidate this module when is moved to production.
			Since cardData may be encrypted using either UKPT or DUKPT coming from this device,
			we identify it using 'ksn'
			@returns errorCode if any else None
		'''
		self.inputType = self.bundle['inputType'] = Transaction.INPUT_TYPE['SWIPE']
		self.encryptedTrack = cardData['encryptedTrack']
		self.eType = cardData['eType']
		if self.eType == self.GDSEEDS_ETYPE['UKPT']:
			self.deviceSerial = cardData['deviceSerial']
			from src.utils.crypto import GDSeeds
			gd = GDSeeds()
			try:
				gd.decryptData(self.encryptedTrack)# encrypted hex data

				self.track1 = gd.track1
				self.populateTrack1(self.track1)

				self.bundle['track2'] = self.track2 = gd.track2
			except Exception as e:
				return 'INVALID_REQUEST'

		elif self.eType == self.GDSEEDS_ETYPE['DUKPT']:
			self.ksn = cardData['ksn']
			#TODO for testing purposes
			self.track1 = '%B4242424242424242^gautama buddha    /^14091010010000000772000000?'
			self.populateTrack1(self.track1)

			self.bundle['track2'] = self.track2 = ';5176521005587557=10091010017720?'


		return None

	def decodeTestData(self, cardData):
		'''
			Plain track data from swipe
			@returns errorCode if any else None
		'''
		self.inputType = self.bundle['inputType'] = Transaction.INPUT_TYPE['SWIPE']
		self.track1 = cardData['track1']
		self.populateTrack1(self.track1)

		self.bundle['track2'] = self.track2 = cardData['track2']

		return None

	def decodeManualData(self, cardData):
		'''
		Not in use
		if self.eType == self.MANUAL_ENTRY:
			#TODO keep a isManualEntryAllowed column for each POS as well
			self.inputType = self.bundle['inputType'] = Transaction.INPUT_TYPE['MANUAL']
			self.cardNumber = self.bundle['cardNumber'] = self.json_data['payload']['card']['cardNumber']
			self.chName = self.json_data['payload']['card']['chName']
			self.bundle['MM'] = self.MM = self.json_data['payload']['card']['MM']
			self.bundle['YY'] = self.YY = self.json_data['payload']['card']['YY']

			self.bundle['track1'] = self.track1 = self.cardNumber + '^' + self.chName + '^' + self.YY + self.MM
			self.bundle['track2'] = self.track2 = ';' + self.cardNumber + '=' + self.YY + self.MM + '10017720?' # creating track2 from pieces

			return True
		'''
		pass

	def generateToken(self, pan):
		payload = {'pan' : pan}
		url = current_app.config['KEY_TIER_URL'] + 'generateToken'

		try:
			response = requests.post(url, \
							data = payload, \
							verify = False).json()
		except Exception as e:
			current_app.logger.error(e, exc_info=True)
			return False, 'SERVER_ERROR'

		if 'errorCode' in response:
			current_app.logger.debug(response['errorCode'])
			return False, 'SERVER_ERROR'
		return True, response['mesg']

	def populateEntries(self):
		self.deviceId = self.bundle['deviceId'] = self.json_data['terminalInfo']['deviceId']
		self.timestamp = self.bundle['timestamp'] = common.getCurrentTimeStamp()
		#self.appVersion = self.bundle['appVersion'] = self.json_data['terminalInfo']['appVersion']

		self.reqSequenceCode = self.bundle['reqSequenceCode'] = self.json_data['authentication']['sequenceCode']
		self.clientId = self.bundle['clientId'] = self.json_data['authentication']['clientId']
		self.sessionToken = self.bundle['sessionToken'] = self.json_data['authentication']['sessionToken']

		#eType populated in isValidRequest() itself
		self.conflict = self.bundle['conflict'] = self.json_data['payload']['conflict']
		self.baseAmount = self.bundle['baseAmount'] = self.json_data['payload']['amount']

	def populateRefundEntries(self, details):
		#if transaction conflict should store it as previous
		# if self.eType == 3: #DUKPT
		# 	if details.sequenceCode == self.reqSequenceCode + 1:
		# 		self.transId = str(self.ksn) + '_' + str(details.plutusSequenceCode - 1)
		# 	else:
		# 		self.transId = str(self.ksn) + '_' + str(details.plutusSequenceCode)
		# else:
		#REVALIDATE TRANSID GENERATION
		decoder = None
		if self.deviceType == self.DEVICE_TYPE['MIURA']:
			decoder = self.decodeMiura
		elif self.deviceType == self.DEVICE_TYPE['GDSEEDS']:
			decoder = self.decodeGD
		elif self.deviceType == self.DEVICE_TYPE['TEST']:
			decoder = self.decodeTestData
		else:
			return 'INVALID_REQUEST'
		#These decoders return errorCode if any found.
		errorCode = decoder(self.json_data['payload']['card'])

		if errorCode is not None: return errorCode

		isValid, self.bundle['token'] = self.generateToken(self.bundle['pan'])#TODO Efficient if token is generated after refund is authorised.
		if not isValid: return self.bundle['token']

		#not populating tansId because it requires sequence code
		#self.transId = str(self.ksn) + '_' + str(self.sequenceCode)

		if details.sequenceCode == self.reqSequenceCode + 1:
			self.transId = str(datetime.now()) + '_' + str(details.sequenceCode - 1)
		else:
			self.transId = str(datetime.now()) + '_' + str(details.sequenceCode)

		self.bundle['transId'] = self.transId
		self.bundle['currencyCode'] = details.currency

		return None

	def refund(self, details):
		if details.processor == posDetails.PROCESSORS['DEMO']:
			txn = src.transactions.demo.refund
		elif details.processor == posDetails.PROCESSORS['STRIPE']:
			txn = src.transactions.stripe.refund
		elif details.processor == posDetails.PROCESSORS['ATOS']:
			pass
		elif details.processor == posDetails.PROCESSORS['PLUTUS']:
			txn = src.transactions.plutus.refund

		a = txn.Refund(details, self.bundle)
		return a.process()

	def process(self):
		if not self.isValidRequest(): return common.errorMessage('INVALID_REQUEST')
		self.populateEntries()

		isAuthorised, self.username = common.isAuthorisedRequest(self.clientId, self.sessionToken, self.deviceId)
		if isAuthorised == False: return common.errorMessage(self.username)#Second return value is errorCode

		canI, details = common.canIdoTransaction(self.clientId, common.REFUND, self.username)
		if not canI: return common.errorMessage('SERVER_BUSY')

		isValid, details = common.validateSequenceCode(self.reqSequenceCode, details)
		if not isValid: return details # Second return value is output to be returned
		#populating entries which require its values from details

		errorCode = self.populateRefundEntries(details)
		if errorCode is not None: return common.errorMessage(errorCode)

		output = common.resolveConflict(details, self.username, self.conflict, self.reqSequenceCode)
		if 'errorCode' not in output:
			if details.sequenceCode == self.reqSequenceCode + 1:
				details.sequenceCode -= 1
				details.plutusSequenceCode -= 1
			self.bundle['username'] = self.username
			output.update(self.refund(details))

		output['sequenceCode'] = details.sequenceCode
		details.setTransactionStatus(False)
		db.session.commit()
		return json.dumps(output)
