"""
from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from src.transactions.plutus.tests.test_settleRequest import settleRequest1
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login

import simplejson as json
from datetime import datetime
from src.mAuthenticate import common
from src.mAuthenticate.common import errorMessageD
from src.transactions.tests.test_purchase import purchase

from src.models import Purchase, LastTransaction, Settlement
from sqlalchemy import and_


class testPurchase(KitTestCase):
	'''
		These tests cover plutus transaction system. They might not be functional as Plutus is no more being used in AasaanPay. 
		All the tests in this class are valid purchase requests. If any errorCode is returned, it is due to mis-coordination between Aasaanpay servers and Plutus		
	'''
	testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.status = getNewPOS(self.merchant)
		","

		self.details.save()
		self.details.clientId = self.details.id
		self.details.update()

		self.status.clientId = self.details.clientId
		self.status.save()

		self.deviceId = otp_generator(16)
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP
		from ext import db
		
		self.user.delete()
		self.status.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Purchase.query.delete()
		Settlement.query.delete()
		self.details.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()
		
	def test3(self):
		'''
			Simple Purchase request. 
			Expected Output:  {u'merchant': {u'city': u'NOIDA - INDIA', u'id': None, u'name': u'PINE LABS PVT.LTD.', u'address': u'Testing Server'}, u'sequenceCode': 2, u'transactionInfo': {u'rrn': u'000020', u'baseAmount': u'100', u'terminalId': u'34646946', u'transactionTime': u'2014-01-07T16:35:00.00', u'acquirer': u'HDFC', u'invoice': u'000401', u'batchNumber': u'000222', u'transId': u'2014-01-07 16:34:51.505129_385', u'apprCode': u'00'}, u'card': {u'MM': u'09', u'YY': u'10', u'cardNumber': u'517652******7557', u'chName': u''}}
		'''
		
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		seqCode = self.details.sequenceCode
		
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)
		assert 'errorCode' not in response_data
		p = Purchase.query.filter(and_(Purchase.sequenceCode == seqCode, Purchase.status == 1)).first()
		lastTransaction = LastTransaction.query.filter(LastTransaction.rowId == p.id).first()
		assert lastTransaction.sequenceCode == seqCode

	
	def test4(self):
		'''
			Tests if two purchase requests with same sequence Code works as intended i.e., disables the first purchase
			Process: 
			i. Purchase() -> to align the seqCode with Plutus
			ii. Purchase(seq) -> Purchase1
			iii. Purchase(seq) -> Purchase2 

			Expected Behavior: 
			i. Purchase1 should be disabled because of Purchase2
			ii. lastTransaction entry will point to Purchase2
			#TODO extend the tests to VOID and TIP after they're done
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
	 		'track2' : ';5176521005587557=10091010017720?'}
	 	eType = 1
	 	conflict = False
	 	amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)#Alignment of plutus sequenceCode		

		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)#Purchase entry added to db
		assert 'errorCode' not in response_data
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode - 1, eType, conflict, testCard, amount)#Purchase entry added to db -> should disable the above transaction
		assert 'errorCode' not in response_data
		
		purchaseEntries = Purchase.query.all()
		print purchaseEntries
		assert purchaseEntries[0].status == 0 and purchaseEntries[1].status == 1
		lastTransaction = LastTransaction.query.first()
		assert lastTransaction.transactionType == common.PURCHASE
		assert lastTransaction.sequenceCode == self.details.sequenceCode - 1
		assert lastTransaction.rowId == purchaseEntries[1].id
		
	def test5(self):
		'''
			Testing conflict cases with purchase requests. resolveConflict function has separate test cases in test_common.py . This can be removed. 

			i. Conflict at mobile i.e., self.conflict = True
			ii. Conflict at server side ==> settle request was failure at server i.e., details.sequenceCode == self.reqSequenceCode. We emulate this simply by setting detials.settleConflict = True
			
			a. Dummy purchase to align the sequence code. 
			b. This scenario occurs when a settle request is sent and it fails at server level. We emulate conflict at server level by:
				i. Setting self.conflict = True;
				ii. To get settleCode = 'l', we settle the batch by using settleRequest1 function src/plutus/tests/settle_request.py. It doesn't change the state of transactions
					To get settleCode = 'c', we don't do any request
			c. Sending a new purchase with (b) steps' sequence code
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		settleRequest1(self.details.plutusClientId, self.details.plutusSecretKey, self.details.plutusSequenceCode)#Clearing any transactions from earlier tests
		#Fill the database with a sample Purchase
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)

		#Emulating settleCode = 'l'
		settleRequest1(self.details.plutusClientId, self.details.plutusSecretKey, self.details.plutusSequenceCode)
		
		#Emulating conflict at server
		self.details.plutusSequenceCode += 1 #To prevent invalid sequence code for settleStatus in resolveConflict
		self.details.sequenceCode += 1#To prevent below purchase disabling previous purchase. If this is not done... lastTransaction.sequenceCode == seqCode will match in disableLastTransaction and screws it
		self.details.inSettleConflict = True
		self.details.update()

		
		#reqSequenceCode = self.details.sequenceCode - 1
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode - 1, eType, True, testCard, amount)
		assert response_data['settleCode'] == 'l'

		#Emulating settleCode = 'c'
		self.details.plutusSequenceCode += 1#To prevent settleStatus in resolveConflict cancelling the above purchase
		self.details.sequenceCode += 1#To prevent below purchase disabling previous purchase. If this is not done... lastTransaction.sequenceCode == seqCode will match in disableLastTransaction and screws it
		self.details.inSettleConflict = True
		self.details.update()

		#reqSequenceCode = self.details.sequenceCode - 1		
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode - 1, eType, True, testCard, amount)
		assert response_data['settleCode'] == 'c'

	
"""