"""
from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login

from src.transactions.tests.test_settleRequest import settleRequest
from src.models import Purchase
import simplejson as json

from datetime import datetime
from src.mAuthenticate import common
from src.mAuthenticate.common import errorMessageD, ERROR_CODES


def settleRequest1(clientId, secretKey, sequenceCode):
	'''
		Used to emulate the conflict at server level. 
		Note this settles the batch directly with plutus. Doesn't change the state of transactions
	'''
	settle_request = '''<?xml version="1.0" encoding="utf-8" ?>
	<Settlement-Request xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />'''
	request_type = 5
	from src.transactions import client
	client.service.PLHub_Exchange(clientId, secretKey, request_type, settle_request, sequenceCode)

class testSettleRequest(KitTestCase):
	testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.status = getNewPOS(self.merchant)
		","

		self.details.save()
		self.details.clientId = self.details.id
		self.details.update()

		self.status.clientId = self.details.clientId
		self.status.save()

		self.deviceId = otp_generator(16)
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP
		from src.models import LastTransaction, Settlement
		from ext import db
		
		self.user.delete()
		self.status.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Settlement.query.delete()
		Purchase.query.delete()
		self.details.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()
		
	def test5(self):
		'''
			Settle Request with invalid Plutus sequenceCode. Emulating it by manually changing the db entry.(By default, plutus sequenceCode is not in sync on our db)
			Expected Output: {'errorCode' : 'INVALID_SEQUENCE_CODE1'}
		'''

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']

		seqCode = self.details.plutusSequenceCode
		response_data = settleRequest(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, \
			self.details.sequenceCode)
		seqCode = self.details.plutusSequenceCode
		assert response_data['errorCode'] == ERROR_CODES['INVALID_SEQUENCE_CODE1']
		assert (seqCode + 1) != self.details.plutusSequenceCode#In valid transactions, pSeqCode increases by 1.

	def test6(self):
		'''
			Settle Request with valid sequenceCode.
			Note: details.inSettleConflict = False
			Expected Output: {'HDFC' : {}, 'sequenceCode' : sequenceCode}
		'''
		from src.transactions.tests.test_purchase import purchase#Keeping it here to avoid circular imports
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']

		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		settleRequest1(self.details.plutusClientId, self.details.plutusSecretKey, self.details.plutusSequenceCode)#Clearing any transactions from earlier tests

		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#New purchase for current settlement batch
		assert 'errorCode' not in response_data
		
		response_data = settleRequest(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, \
			self.details.sequenceCode)

		assert 'errorCode' not in response_data and 'isSettled' in response_data
		
	def test7(self):
		'''
			Settle Request with valid sequenceCode. Simulating error in double accounting with Purchase entries. Removing a purchase
			Note: details.inSettleConflict = False
			Expected output: {'errorCode' : 'SERVER_ERROR'}
		'''
		from src.transactions.tests.test_purchase import purchase#Keeping it here to avoid circular imports
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']

		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		settleRequest1(self.details.plutusClientId, self.details.plutusSecretKey, self.details.plutusSequenceCode)#Clearing any transactions from earlier tests

		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#New purchase for current settlement batch
		Purchase.query.delete()#Removing purchase entry to mismatch the double accounting
		response_data = settleRequest(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, \
			self.details.sequenceCode)

		assert response_data['errorCode'] == ERROR_CODES['SERVER_ERROR']
		
	def test8(self):
		'''
			Settle Request with valid sequenceCode. Simulating error in double accounting with Purchase entries. Modifying amount in a purchase
			Note: details.inSettleConflict = False
			Expected output: {'errorCode' : 'SERVER_ERROR'}
		'''
		from src.transactions.tests.test_purchase import purchase#Keeping it here to avoid circular imports
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']

		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		settleRequest1(self.details.plutusClientId, self.details.plutusSecretKey, self.details.plutusSequenceCode)#Clearing any transactions from earlier tests

		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#New purchase for current settlement batch
		
		p = Purchase.query.first()#Removing purchase entry to mismatch the double accounting
		p.amount += 10
		p.update()
		
		response_data = settleRequest(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, \
			self.details.sequenceCode)
		assert response_data['errorCode'] == ERROR_CODES['SERVER_ERROR']
	
	def test9(self):
		'''
			Settle Request with valid sequenceCode. Simulating error in double accounting with Tip entries. Removing a Tip(Modifying purchase)
			Note: details.inSettleConflict = False
			Expected output: {'errorCode' : 'SERVER_ERROR'}
		'''
		from src.transactions.tests.test_purchase import purchase#Keeping it here to avoid circular imports
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']

		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		settleRequest1(self.details.plutusClientId, self.details.plutusSecretKey, self.details.plutusSequenceCode)#Clearing any transactions from earlier tests

		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#New purchase for current settlement batch
		
		p = Purchase.query.first()#Removing purchase entry to mismatch the double accounting
		p.amount += 10
		p.update()

		response_data = settleRequest(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, \
			self.details.sequenceCode)
		assert response_data['errorCode'] == ERROR_CODES['SERVER_ERROR']

	def test10(self):
		'''
			Settle Request with valid sequenceCode. Simulating error in double accounting with Tip entries. Modifying tipAmount
			Note: details.inSettleConflict = False
			Expected output: {'errorCode' : 'SERVER_ERROR'}
		'''
		from src.transactions.tests.test_purchase import purchase#Keeping it here to avoid circular imports
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']

		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		settleRequest1(self.details.plutusClientId, self.details.plutusSecretKey, self.details.plutusSequenceCode)#Clearing any transactions from earlier tests

		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#New purchase for current settlement batch
		p = Purchase.query.first()#Removing purchase entry to mismatch the double accounting
		p.tipAmount += 10
		p.status += common.TIP_BIT_VALUE
		p.update()
		response_data = settleRequest(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, \
			self.details.sequenceCode)
		assert response_data['errorCode'] == ERROR_CODES['SERVER_ERROR']
	
	def test11(self):
		'''
			Settle Request with valid sequenceCode. Simulating error in double accounting with Void entries. Removing a void(modifying purchase)
			Note: details.inSettleConflict = False
			Expected output: {'errorCode' : 'SERVER_ERROR'}
		'''

	def test12(self):
		'''
			Settle Request with valid sequenceCode. Simulating error in double accounting with Void entries. Modifying amount in a void
			Note: details.inSettleConflict = False
			Expected output: {'errorCode' : 'SERVER_ERROR'}
		'''

	def test13(self):
		'''
			Tests if settle request start tip worker if there are pending tips.
			Process:
				i. Purchase
				ii. Tip with invalid sequence code
				iii. Settle request (with valid sequence Code)
				iv. Wait for the tip worker process to complete
				v. Check if there any pending Tips and tips available
		'''
"""