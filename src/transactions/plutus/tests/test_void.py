"""
from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login
from src.transactions.tests.test_purchase import purchase
from src.transactions.tests.test_void import void
from src.transactions.tests.test_tip import tip
from src.models import Purchase, LastTransaction, Settlement, Void, Tip, pendingTip
import simplejson as json

from datetime import datetime
from src.mAuthenticate import common
from src.mAuthenticate.common import errorMessageD, ERROR_CODES
import os

class testVoid(KitTestCase):
	'''
		These 
	'''
	testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.status = getNewPOS(self.merchant)
		","

		self.details.save()
		self.details.clientId = self.details.id
		self.details.update()

		self.status.clientId = self.details.clientId
		self.status.save()

		self.deviceId = otp_generator(16)
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP
		from ext import db
		
		self.user.delete()
		self.status.delete()
		Tip.query.delete()
		pendingTip.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Void.query.delete()
		Purchase.query.delete()
		Settlement.query.delete()
		self.details.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()
		

	def test4(self):
		'''
			Sending void for a disabled purchase Id. 
			Expected Output: {'errorCode' : INVALID_TRANSACTION}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
	 		'track2' : ';5176521005587557=10091010017720?'}
	 	eType = 1
	 	conflict = False
	 	amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#Alignment of plutus sequenceCode		

		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#Purchase entry added to db
		assert 'errorCode' not in response_data
		transId = response_data['transactionInfo']['transId']

		#### Disabling the purchase #### Manually setting the db value for efficiency
		p = Purchase.query.first()
		p.status = 0
		p.update()
		
		response_data = void(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, transId)
		assert response_data['errorCode'] == ERROR_CODES['INVALID_TRANSACTION']

	def test5(self):
		'''
			Normal void of a purchase transaction.
			Expected Output: {'merchant' : merchant, 'card' : card, 'transactionInfo' : transactionInfo, 'sequenceCode' : sequenceCode}
		'''


		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
	 		'track2' : ';5176521005587557=10091010017720?'}
	 	eType = 1
	 	conflict = False
	 	amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#Alignment of plutus sequenceCode		

		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#Purchase entry added to db
		assert 'errorCode' not in response_data
		transId = response_data['transactionInfo']['transId']

		response_data = void(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, transId)
		assert 'errorCode' not in response_data and \
				'merchant' in response_data and \
				'card' in response_data and \
				'transactionInfo' in response_data and \
				'sequenceCode' in response_data

		p = Purchase.query.first()
		assert p.status == common.PURCHASE_BIT_VALUE + common.VOID_BIT_VALUE
		v = Void.query.first()
		lastTransaction = LastTransaction.query.first()
		assert lastTransaction.transactionType == common.VOID and lastTransaction.rowId == v.id

	def test6(self):
		'''
			Normal void of a purchase+tip transaction.
			Expected Output: {'merchant' : merchant, 'card' : card, 'transactionInfo' : transactionInfo}
			Expected conditions in db:
				i. Purchase status = PURCHASE_BIT_VALUE + TIP_BIT_VALUE + VOID_BIT_VALUE
				ii. Tip status = Disabled
				iii. New void entry in purchase
		'''

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionToken = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
	 		'track2' : ';5176521005587557=10091010017720?'}
	 	eType = 1
	 	conflict = False
	 	amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#Alignment of plutus sequenceCode		

		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, eType, conflict, testCard, amount)#Purchase entry added to db
		transId = response_data['transactionInfo']['transId']
		response_data = tip(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, transId, amount)
		assert response_data['mesg'] == 'Success' and response_data['pid'] != None
		os.waitpid(response_data['pid'], 0)#Waiting for the pending Tip process to complete
		
		response_data = void(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionToken, self.details.sequenceCode, transId)

		assert 'errorCode' not in response_data and \
				'merchant' in response_data and \
				'card' in response_data and \
				'transactionInfo' in response_data and \
				'sequenceCode' in response_data

		p = Purchase.query.first()
		assert p.status == common.PURCHASE_BIT_VALUE + common.VOID_BIT_VALUE + common.TIP_BIT_VALUE
		t = Tip.query.first()
		assert t.isDisabled
		v = Void.query.first()
		lastTransaction = LastTransaction.query.first()
		assert lastTransaction.transactionType == common.VOID and lastTransaction.rowId == v.id

"""