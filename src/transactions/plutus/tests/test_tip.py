"""
from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login
from src.transactions.tests.test_purchase import purchase
from src.transactions.tests.test_tip import tip
from src.models import smsOTP
from src.models import LastTransaction, Purchase, pendingTip, Tip
import simplejson as json
from datetime import datetime
from src.mAuthenticate.common import errorMessageD
import os

class testTip(KitTestCase):
	'''
		This concerns pendingTip. Tip is not used as of now
	'''
	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.status = getNewPOS(self.merchant)
		","

		self.details.save()
		self.details.clientId = self.details.id
		self.details.update()

		self.status.clientId = self.details.clientId
		self.status.save()

		self.deviceId = otp_generator(16)
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from ext import db
		
		self.user.delete()
		self.status.delete()
		Tip.query.delete()
		pendingTip.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		Purchase.query.delete()
		self.details.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()

	def is_running(self, pid):
	    '''Check whether pid exists in the current process table.  UNIX only. Supported in windows 
	    '''
	    if pid < 0:
	        return False

	    if pid == 0:
	        # According to "man 2 kill" PID 0 refers to every process
	        # in the process group of the calling process.
	        # On certain systems 0 is a valid PID but we have no way
	        # to know that in a portable fashion.
	        raise ValueError('invalid PID 0')
	    try:
	    	os.kill(pid, 0)
	    except OSError as err:
	        if err.errno == errno.ESRCH: # ESRCH == No such process
	            return False
	        elif err.errno == errno.EPERM:
	            # EPERM clearly means there's a process to deny access to
	            return True
	        else:
	            # According to "man 2 kill" possible error values are
	            # (EINVAL, EPERM, ESRCH)
	            raise
	    else:
	        return True

	def test4(self):
		'''
			Valid pending Tip request
			Expected Output: {'mesg' : 'Success', 'process' : True}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])#ShouldChangePassword set to True during activation

		self.user.update(shouldChangePassword = False)#Setting ShouldChangePassword to False
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)

		transId = response_data['transactionInfo']['transId']

		response_data = tip(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, transId, amount)
		assert response_data['mesg'] == 'Success' and response_data['pid'] != None
		
		assert pendingTip.query.count() == 1
		assert Tip.query.count() == 0
		
		os.waitpid(response_data['pid'], 0)
		
		assert Tip.query.count() == 1
		assert pendingTip.query.count() == 0
	
	def test5(self):
		'''
			Valid pending Tip request. Invalid sequence code(using self.details.sequenceCode - 1)
			Expected Output: {'mesg' : 'Success'}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		eType = 1
		conflict = False
		amount = '100'
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, eType, conflict, testCard, amount)

		transId = response_data['transactionInfo']['transId']
		response_data = tip(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode - 1, transId, amount)
		assert response_data == {'mesg' : 'Success'}
"""