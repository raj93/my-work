from flask import request, current_app
from datetime import datetime
from src.transactions import client
try:
	import xml.etree.cElementTree as ET
except ImportError:
	import xml.etree.ElementTree as ET

import simplejson as json

from src.mAuthenticate import common
from ext import db
from src.models import Transaction as purchase_table, Void as void_table, Tip as tip_table, Settlement as settlement_table
from sqlalchemy import and_
import re

class SettleRequest:
	SETTLE_REQUEST_TYPE = 5
	
	SETTLE_REQUEST_TAGS = ("terminalInfo", "authentication")	
	TERMINAL_TAGS = ("deviceId", "timeStamp", "appVersion")	
	AUTHENTICATION_TAGS = ("sessionToken", "sequenceCode", "clientId")

	def __init__(self):
		self.json_data = request.get_json() 

	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data for k in self.SETTLE_REQUEST_TAGS):
			return False 

		if not all(k in self.json_data["terminalInfo"] for k in self.TERMINAL_TAGS):
			return False
		
		if not all(k in self.json_data["authentication"] for k in self.AUTHENTICATION_TAGS):
			return False
		
		return True

	def populateEntries(self):
		self.deviceId = self.json_data["terminalInfo"]["deviceId"]
		self.timeStamp = datetime.now().strftime("%Y-%m-%d %X")
		self.appVersion = self.json_data["terminalInfo"]["appVersion"]

		self.sessionToken = self.json_data["authentication"]["sessionToken"] 
		self.reqSequenceCode = self.json_data["authentication"]["sequenceCode"]
		self.clientId = self.json_data["authentication"]["clientId"]

		if type(self.reqSequenceCode) != int or type(self.clientId) != int:
			return False

		return True

	def generateXmlRequest(self):
		self.settleRequest = ET.tostring(ET.Element('Settlement-Request'))

	def plutus_request(self, CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE):
		#TODO #Critical Check this(client) if any concurrent request error arise! Also check whether the object client has any expiry time
		return client.service.PLHub_Exchange(CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE)

	def getTotalTransactionsAndAmount(self, line):
		match = re.search(r'(\s)([\d]+)(\s)',line)
		if match:
			num_trans = match.group().strip()
		results = re.findall(r'(Rs.)([\d]+).([\d]+)',line)

		return int(num_trans),int(results[0][1]+results[0][2])

	
	
	def settlement_dummy(self, details):
		output = {}
	
		settlement_entries = []

		mynumSales = 0
		mysaleAmount = 0
		mynumTips = 0
		mytipAmount = 0
		mynumVoids = 0
		myVoidAmount =	 0

		purchase_entries = db.session.query(purchase_table).filter(and_(purchase_table.clientId == self.clientId, purchase_table.status > 0)).all()
		for purchase_row in purchase_entries:
			s = purchase_row.status
			if s == common.PURCHASE_BIT_VALUE:
				mynumSales += 1
				mysaleAmount += int(purchase_row.amount)
			elif s == common.PURCHASE_BIT_VALUE | common.VOID_BIT_VALUE:
				mynumSales += 1
				mynumVoids += 1
				myVoidAmount += int(purchase_row.amount)
				mysaleAmount += int(purchase_row.amount)
			elif s == common.PURCHASE_BIT_VALUE | common.TIP_BIT_VALUE:
				mynumSales += 1
				mynumTips += 1
				mysaleAmount += int(purchase_row.amount)
				mytipAmount += int(purchase_row.tipAmount)
			elif s == common.PURCHASE_BIT_VALUE | common.TIP_BIT_VALUE | common.VOID_BIT_VALUE:
				#incase of VOID(TIP_ADJUST) entry in settlement there is no seperate tip entry in plutus response.
				mynumSales += 1
				mynumVoids += 1
				mysaleAmount += int(purchase_row.amount) + int(purchase_row.tipAmount)
				myVoidAmount += int(purchase_row.amount) + int(purchase_row.tipAmount)


		# sale = {'count' : mynumSales,
		# 		'total' : 'Rs ' + str(mysaleAmount/100.0)}

		# void = {'count' : mynumVoids,
		# 		'total' : '-Rs ' + str(myVoidAmount/100.0)}

		# refund = {'count' : 0,
		# 		'total' : '0'}

		#TODO if refund is unavailable for this tid, skip it
		total = mysaleAmount - myVoidAmount
		netTotal = total# - refund['total']

		settlement_entries.append(settlement_table(self.username, self.clientId, details.sequenceCode, details.plutusSequenceCode, \
							self.timeStamp, 'HDFC', details.batch, details.clientId, '', mynumSales, mynumVoids, mynumTips, \
							mysaleAmount, myVoidAmount, mytipAmount, netTotal, details.batch))

		void_entries = db.session.query(void_table).filter(and_(void_table.clientId == self.clientId, \
			void_table.isDisabled == False)).all()
		tip_entries = db.session.query(tip_table).filter(and_(tip_table.clientId == self.clientId, \
			tip_table.isDisabled == False)).all()

		#Add settlements of different acquirers or TIDs
		for settlement_row in settlement_entries:
			db.session.add(settlement_row)
			
		#Change status of Purchase entries to Settled	
		for purchase_row in purchase_entries:
			purchase_row.status += common.SETTLEMENT_BIT_VALUE

		#Add settled entries to settled database
		from src.models import SettledTransaction as SettledPurchase, SettledVoid, SettledTip

		settled_purchase_entries = []

		for purchase in purchase_entries:
			settledPurchase = SettledPurchase(purchase, self.details.batch)
			settled_purchase_entries.append(settledPurchase)
			db.session.add(settledPurchase)

		for void_row in void_entries:#ERRORPRONE foreign key restraint..
			settlementdVoid = SettledVoid(void_row)
			db.session.add(settlementdVoid)

		for tip_row in tip_entries:
			settlementdTip = SettledTip(tip_row)
			db.session.add(settlementdTip)

		db.session.commit() 

			
		#Cleaning up settled entries in unsettled database
		for tip_row in tip_entries:
			db.session.delete(tip_row)

		for void_row in void_entries:
			db.session.delete(void_row)

		for purchase_row in purchase_entries:
			db.session.delete(purchase_row)

		db.session.commit()

		# Update batch count in posDetails. This count is common in the series of settlements made by this settlement request
		details.batch += 1
		output = {'isSettled' : True}

		common.notifyMerchant(details, settlement_entries, settled_purchase_entries)

		#We got a result so we are incrementing sequence codes
		details.minSequenceCode = details.sequenceCode
		details.sequenceCode += 1
#		details.plutusSequenceCode = plutusRespSequenceCode
		
		common.disableLastTransaction(self.clientId, common.SETTLEMENT, details.sequenceCode - 1, 0)
		return output
	
	def settlement(self, details):
		if not current_app.config['PLUTUS_STATUS']:
			return self.settlement_dummy(details)

		output = {}
		self.generateXmlRequest()
		try:
			result = self.plutus_request(details.plutusClientId, details.plutusSecretKey, self.SETTLE_REQUEST_TYPE, self.settleRequest, details.plutusSequenceCode)
		except BaseException as e:
			details.inSettleConflict = True # marking settlement conflict at server
			return common.errorMessageD('SETTLEMENT_CONFLICT')
		
		root = ET.fromstring(result.RESPONSE_XML)
		plutusRespSequenceCode = result.SEQUENCE_CODE
		
		if plutusRespSequenceCode != details.plutusSequenceCode + 1:
			#code should not come here except during initialisation
			if (current_app.config['DEBUG'] or current_app.config['TESTING']) and details.sequenceCode == 1:
				details.plutusSequenceCode = plutusRespSequenceCode
				return common.errorMessageD('INVALID_SEQUENCE_CODE1')
			else:	
				current_app.logger.critical('Implementation error. Sequence code mismatch found\n'+
					'ClientId: ' + str(self.clientId) + ' sequenceCode: ' + str(self.sequenceCode) + ' plutusSequenceCode: ' +
					str(result.SEQUENCE_CODE))
				details.plutusSequenceCode = plutusRespSequenceCode #Doing it to ensure service to merchant doesn't stop due to implementation error
				return common.errorMessageD('SERVER_ERROR')

		transactionOutput = root.find('TransactionOutput')
		if transactionOutput is None:
			current_app.logger.critical('TransactionOutput element not found in plutus response')
			return common.errorMessageD('SYSTEM_ERROR')

		state = transactionOutput.find('State')

		if state is None:
			current_app.logger.critical('TransactionOutput element state not found in plutus response')
			return common.errorMessageD('SYSTEM_ERROR')

		#change the settle status of all transactions..
		if state.find('StatusCode').text == '2025':
			#updates to the new batch and does double accounting

			ac = transactionOutput.find('Acquirer')
			banks = ac.findall('Bank')

			#variable initializations
			numSales, mynumSales = 0, 0
			saleAmount, mysaleAmount = 0, 0
			numTips, mynumTips = 0, 0
			tipAmount, mytipAmount = 0, 0
			numVoids, mynumVoids = 0, 0
			voidAmount, myVoidAmount = 0, 0
			totalAmount, mytotalAmount = 0, 0

			#list of entries to be added to database
			settlement_entries = []

			for bank in banks:
				terminals = bank.findall('Terminal')
				for terminal in terminals:
					state = terminal.find('TerminalState')
					statusCode = state.find('TerminalStatusCode')
					if statusCode.text == '2025':
						AuditReportData = terminal.find('AuditReportData')
						Receipt = AuditReportData.find('Receipt')
						lc = int(Receipt.get('lineCount'))

						#it means last settlement summary on this terminal
						if lc == 24:
							continue

						lines = Receipt.findall('PrintLine')
						if (lc - 31)%4 != 0:
							current_app.logger.critical('Implementation error')
						else:
							num_trans = (lc-31)>>4

						bankName = bank.get('Name')
						batchNumber = int(terminal.find('BatchNumber').text.strip())
						tid = terminal.find('TID').text.strip()
						mid = terminal.find('Merchant').find('MID').text.strip()

						s,sa = self.getTotalTransactionsAndAmount(lines[lc-12].text)
						t,ta = self.getTotalTransactionsAndAmount(lines[lc-11].text)
						v,va = self.getTotalTransactionsAndAmount(lines[lc-10].text)
						nta = common.getTotalAmount(lines[lc-8].text)
						
						#assuming no refunds
						#numRefunds,refundAmount = getTotalTransactionsAndAmount(lines[lc-6].text)
						#refundTotalAmount = getTotalAmount(lines[lc-4].text)
						#nta = getTotalAmount(lines[lc-2].text)

						numSales += s
						saleAmount += sa
						numTips += t
						tipAmount += ta
						numVoids += v
						voidAmount += va
						totalAmount += nta

						settlement_entries.append(settlement_table(self.username, self.clientId, details.sequenceCode, details.plutusSequenceCode, \
							self.timeStamp, bankName, batchNumber, tid, mid, s, v, t, sa, va, ta, nta, details.batch))
			
			#your amounts,i am anxious!
			if len(settlement_entries) != 0:
				purchase_entries = db.session.query(purchase_table).filter(and_(purchase_table.clientId == self.clientId, purchase_table.status > 0)).all()
				for purchase_row in purchase_entries:
					s = purchase_row.status
					if s == common.PURCHASE_BIT_VALUE:
						mynumSales += 1
						mysaleAmount += int(purchase_row.amount)
					elif s == common.PURCHASE_BIT_VALUE | common.VOID_BIT_VALUE:
						mynumSales += 1
						mynumVoids += 1
						myVoidAmount += int(purchase_row.amount)
						mysaleAmount += int(purchase_row.amount)
					elif s == common.PURCHASE_BIT_VALUE | common.TIP_BIT_VALUE:
						mynumSales += 1
						mynumTips += 1
						mysaleAmount += int(purchase_row.amount)
						mytipAmount += int(purchase_row.tipAmount)
					elif s == common.PURCHASE_BIT_VALUE | common.TIP_BIT_VALUE | common.VOID_BIT_VALUE:
						#incase of VOID(TIP_ADJUST) entry in settlement there is no seperate tip entry in plutus response.
						mynumSales += 1
						mynumVoids += 1
						mysaleAmount += int(purchase_row.amount) + int(purchase_row.tipAmount)
						myVoidAmount += int(purchase_row.amount) + int(purchase_row.tipAmount)
					else:
						current_app.logger.critical('Status of purchase entry is set to invalid value of ' + str(purchase_row.status) + \
							'\n. Client Id: ' + str(self.clientId) + ' | Row ID: ' + str(purchase_row.id))

				mytotalAmount = mysaleAmount - myVoidAmount #ERRORPRONE

				void_entries = db.session.query(void_table).filter(and_(void_table.clientId == self.clientId, \
					void_table.isDisabled == False)).all()
				tip_entries = db.session.query(tip_table).filter(and_(tip_table.clientId == self.clientId, \
					tip_table.isDisabled == False)).all()

				if len(void_entries) != mynumVoids:
					current_app.logger.critical('Number of void entries mismatch with Plutus settle req ' + str(self.clientId))	#TODO write settlement response to a file and link it here; can be sent as attachment to the email also
					return common.errorMessageD('SERVER_ERROR')

				if len(tip_entries) != mynumTips:
					current_app.logger.critical('Number of tip entries mismatch with Plutus settle req ' + str(self.clientId))
					return common.errorMessageD('SERVER_ERROR')					

				if numSales != mynumSales or saleAmount != mysaleAmount or \
					numTips != mynumTips or tipAmount!= mytipAmount or \
					numVoids != mynumVoids or voidAmount != myVoidAmount or \
					totalAmount != mytotalAmount:
					current_app.logger.critical('cross check with plutus not ok ' + str(self.clientId))
					return common.errorMessageD('SERVER_ERROR')
				
				#Add settlements of different acquirers or TIDs
				for settlement_row in settlement_entries:
					db.session.add(settlement_row)
					
				#Change status of Purchase entries to Settled	
				for purchase_row in purchase_entries:
					purchase_row.status += common.SETTLEMENT_BIT_VALUE

				#Add settled entries to settled database
				from src.models import SettledTransaction as SettledPurchase, SettledVoid, SettledTip

				for purchase_row in purchase_entries:
					settlementdPurchase = SettledPurchase(purchase_row, details.batch)
					db.session.add(settlementdPurchase)

				for void_row in void_entries:#ERRORPRONE foreign key restraint..
					settlementdVoid = SettledVoid(void_row)
					db.session.add(settlementdVoid)

				for tip_row in tip_entries:
					settlementdTip = SettledTip(tip_row)
					db.session.add(settlementdTip)

				db.session.commit() 

			
				#Cleaning up settled entries in unsettled database
				for tip_row in tip_entries:
					db.session.delete(tip_row)

				for void_row in void_entries:
					db.session.delete(void_row)

				for purchase_row in purchase_entries:
					db.session.delete(purchase_row)

				db.session.commit()

				# Update batch count in posDetails. This count is common in the series of settlements made by this settlement request
				details.batch += 1
				output = {'isSettled' : True}

				common.notifyMerchant(details, settlement_entries, purchase_entries)
			else:
				output.update({'info':'settlement showing last settlement summary'})
				output = {'isSettled' : False}

		else:
			output = {'errorCode': state.find('StatusMessage').text} 

		#We got a result so we are incrementing sequence codes
		details.minSequenceCode = details.sequenceCode
		details.sequenceCode += 1
		details.plutusSequenceCode = plutusRespSequenceCode
		
		common.disableLastTransaction(self.clientId, common.SETTLEMENT, details.sequenceCode - 1, 0)
		return output

	def process(self):
		if not self.isValidRequest() or not self.populateEntries():
			return common.errorMessage('INVALID_REQUEST')
		
		isAuthorised, self.username = common.isAuthorisedRequest(self.clientId, self.sessionToken, self.deviceId)
		if isAuthorised == False: return common.errorMessage(self.username)#Second return value is errorCode
		
		canI, status = common.canIdoTransaction(self.clientId, common.SETTLEMENT, self.username)
		if not canI: return common.errorMessage('SERVER_BUSY')

		isValid, details = common.validateSequenceCode(self.reqSequenceCode, status)
		if not isValid: return details # Second return value is output to be returned
		
		output = {}	
		
		if details.inSettleConflict == True:
			#settle report must have been called before settle request which would resolve the conflict.
			current_app.logger.warn('Settle Request when server in conflict @ ClientId:' + str(self.clientId))
			output['errorCode'] = common.ERROR_CODES['INVALID_TRANSACTION']
		else:
			if details.sequenceCode == self.reqSequenceCode + 1:
				#this cannot happen becuase we do settle only after mobile 
				#acknowledges the response of settlement report
				output['errorCode'] = common.ERROR_CODES['INVALID_SEQUENCE_CODE']
				current_app.logger.warn('sequence code found to be unsynchronized during settle @clientId:' + str(self.clientId))
			else:#details.sequenceCode = self.reqSequenceCode
				output = self.settlement(details)
		
		output['sequenceCode'] = details.sequenceCode
		status.transactionStatus = False		#transactionStatus in posDetails are committed, un-freeing the clientId to do any Xns
		db.session.commit()		

		return json.dumps(output)
