from flask import request, current_app
from datetime import datetime
from src.transactions import client
import xmltodict
try:
	import xml.etree.cElementTree as ET
except ImportError:
	import xml.etree.ElementTree as ET

import simplejson as json


from src.mAuthenticate import common
from ext import db
from src.models import Transaction as purchase_table, Void as void_table
from sqlalchemy import and_

class Void:

	VOID_REQUEST_TYPE = 1

	VOID_TAGS = ("terminalInfo", "authentication" , "payload")
	TERMINAL_TAGS = ("deviceId", "timeStamp", "appVersion")	
	AUTHENTICATION_TAGS = ("sequenceCode","clientId", "sessionToken")

	#this transid is of the purchase for which this tip is requested
	PAYLOAD_TAGS = ["transId"]

	def __init__(self):
		self.json_data = request.get_json()

	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data for k in self.VOID_TAGS):
			return False 
		
		if not all(k in self.json_data["terminalInfo"] for k in self.TERMINAL_TAGS):
			return False
		
		if not all(k in self.json_data["authentication"] for k in self.AUTHENTICATION_TAGS):
			return False
			
		if not all(k in self.json_data["payload"] for k in self.PAYLOAD_TAGS):
			return False

		return True

	def populateEntries(self):
		self.deviceId = self.json_data["terminalInfo"]["deviceId"]
		self.timestamp = datetime.now().strftime("%Y-%m-%d %X")
		self.appVersion = self.json_data["terminalInfo"]["appVersion"]
		
		self.reqSequenceCode = self.json_data["authentication"]["sequenceCode"]
		self.sessionToken = self.json_data["authentication"]["sessionToken"]
		self.clientId = self.json_data["authentication"]["clientId"]

		self.purchaseId = self.json_data["payload"]["transId"]

		if type(self.reqSequenceCode) != int or type(self.clientId) != int:
			return False

		return True

	def poplulateTransactionEntries(self,purchase_entry,details):
		#TODO shift the currency code to purchase ?
		self.CurrencyCode =details.currency
		self.acquirer = purchase_entry.acquirer
		self.invoice = purchase_entry.invoice
		self.amount = purchase_entry.amount
		self.ksn = self.purchaseId.split('_')[0]
		self.transId = str(self.ksn) + '_' + str(details.plutusSequenceCode)


	def generateXmlRequest(self):
		amount = ET.Element('Amount')
		BaseAmount = ET.SubElement(amount, 'BaseAmount')
		BaseAmount.text = str(self.amount)
		CurrencyCode = ET.SubElement(amount, 'CurrencyCode')	
		CurrencyCode.text = self.CurrencyCode
		
		AdditionalInformation = ET.Element('AdditionalInformation')

		AcquirerAddlInfoField = ET.SubElement(AdditionalInformation, 'AddlInfoField')
		
		AcquirerAddlInfoFieldID = ET.SubElement(AcquirerAddlInfoField, 'ID')
		AcquirerAddlInfoFieldID.text = '1011'
		
		AcquirerAddlInfoFieldValue = ET.SubElement(AcquirerAddlInfoField, 'Value')
		AcquirerAddlInfoFieldValue.text = self.acquirer
		
		AcquirerAddlInfoFieldLength = ET.SubElement(AcquirerAddlInfoField, 'Length')
		AcquirerAddlInfoFieldLength.text = str(len(self.acquirer))


		InvoiceAddlInfoField = ET.SubElement(AdditionalInformation, 'AddlInfoField')
		
		InvoiceAddlInfoFieldID = ET.SubElement(InvoiceAddlInfoField, 'ID')
		InvoiceAddlInfoFieldID.text = '1000'
		
		InvoiceAddlInfoFieldValue = ET.SubElement(InvoiceAddlInfoField, 'Value')
		InvoiceAddlInfoFieldValue.text = self.invoice

		InvoiceAddlInfoFieldLength = ET.SubElement(InvoiceAddlInfoField, 'Length')
		InvoiceAddlInfoFieldLength.text = str(len(self.invoice))

		#TODO id = deviceID_timestamp ==> unique as a single deviceId can't generate two legitimate requests at the same timestamp
		#TODO TrackingNumber
		TransactionInput = ET.Element('TransactionInput')
		TransactionInput.set('ID', self.transId)
		TransactionInput.extend((amount,AdditionalInformation))

		self.VoidRequest = ET.Element('purchase-request')
		self.VoidRequest.insert(0, TransactionInput)


	def plutus_request(self, CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE):
		#TODO #Critical Check this(client) if any concurrent request error arise! Also check whether the object client has any expiry time
		return client.service.PLHub_Exchange(CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE)

	def void(self, details, purchase_entry):
		if not current_app.config['PLUTUS_STATUS']:
			return self.void_dummy(details, purchase_entry)
		
		output = {}
		self.generateXmlRequest()

		#if any exception behaving as if nothing happened
		try:
			result = self.plutus_request(details.plutusClientId, details.plutusSecretKey, self.VOID_REQUEST_TYPE, ET.tostring(self.VoidRequest), details.plutusSequenceCode)
		except BaseException as e:
			#TODO rollback the session commits
			return common.errorMessageD('SERVER_BUSY')

		root = ET.fromstring(result.RESPONSE_XML)
	
		plutusRespSequenceCode = result.SEQUENCE_CODE
		if plutusRespSequenceCode != details.plutusSequenceCode + 1:
			#code should not come here 
			if current_app.config['DEBUG'] and details.sequenceCode == 0:	# set plutus sequenceCode during initial set up
				details.plutusSequenceCode = plutusRespSequenceCode  #it may screw testing
				return common.errorMessageD('SYSTEM_ERROR')
			else:	
				current_app.logger.critical('Implementation error. Sequence code mismatch found\n'+
					'ClientId: ' + str(self.clientId) + ' sequenceCode: ' + str(self.reqSequenceCode) + ' plutusSequenceCode: ' +
					str(result.SEQUENCE_CODE));
				details.plutusSequenceCode = plutusRespSequenceCode #Doing it to ensure service to merchant doesn't stop due to implementation error
				return common.errorMessageD('SERVER_ERROR')


		#TODO it might be  better to remove indexed reference for XML -> find out the overhead of find and use it.
		transactionOutput = root.find('TransactionOutput')
		# it is unnecessary as plutus won't change the format. perhaps decide after talking to them. Unnecessary overhead for validation check
		if transactionOutput is None:
			return common.errorMessageD('SYSTEM_ERROR')

		state = transactionOutput.find('State')
		if state is None:
			return common.errorMessageD('SYSTEM_ERROR')

		if state.find('StatusCode').text == '2025':		#Transaction Approved
			
			transactionOutput.remove(transactionOutput.find('ChargeslipData')) # Removing ChargeslipData
			doc = xmltodict.parse(ET.tostring(root))
			Card = doc['hub-response']['TransactionOutput']['Card']
			card = {'cardNumber' : Card['CardNumber'], 
					'MM' : Card['ExpirationDate']['MM'],
					'YY' : Card['ExpirationDate']['YY']
					}
			if Card['CardholderName'] != None:
				card['chName'] = Card['CardholderName']

			Merchant = doc['hub-response']['TransactionOutput']['State']['Merchant']
			merchant = {'id' : Merchant['ID'],
						'name' : Merchant['Name'],
							'address' : Merchant['Address'],
						'city' : Merchant['City']}		

			#TODO store additional details in DB like issuer and acquirere
			state = doc['hub-response']['TransactionOutput']['State']
			transactionInfo = {'baseAmount' : self.amount,
								'acquirer' : state['AcquirerName'],
								'invoice' : state['InvoiceNumber'],
								'batchNumber' : state['BatchNumber'],
								'transactionTime' : state['TransactionTime'],
								'apprCode' : state['HostResponse']['ApprovalCode'],
								'rrn' : state['HostResponse']['RetrievalReferenceNumber'],
								'transId' : self.transId,
								'terminalId' : state['TerminalID']
					}			
			
			#modifying the void bit because it is a success
			if purchase_entry.status & common.VOID_BIT_VALUE == 0:
				purchase_entry.status += common.VOID_BIT_VALUE
				if purchase_entry.status & common.TIP_BIT_VALUE != 0:
					purchase_entry.tip_details.isDisabled = True
			else:
				#voiding a void 
				current_app.logger.warning('Voiding a void is not possible. ID:' + self.transId + ' ClientID:' + str(self.clientId))

			#Adding entry to the void table with reference to Purchase entry(id)
			entry = void_table(self.username, self.clientId, details.sequenceCode, details.plutusSequenceCode, \
				purchase_entry.id, self.transId, self.timestamp, transactionInfo['invoice'], transactionInfo['apprCode'], \
				transactionInfo['rrn'])
			db.session.add(entry)
			db.session.commit()

			rowId = entry.id
			tranType = common.VOID
			output.update({'merchant' : merchant, 'card' : card, 'transactionInfo': transactionInfo})
		else:
			rowId = 0#Last Transaction doesn't refer to any row of a transaction(Purchase/Void/..) table
			tranType = common.ERROR_IN_TRANSACTION
			output.update({'errorCode': state.find('StatusMessage').text})

		#we got a result so we are incrementing sequence codes.
		details.minSequenceCode = details.sequenceCode
		details.sequenceCode += 1
		details.plutusSequenceCode = plutusRespSequenceCode
		
		common.disableLastTransaction(self.clientId, tranType, details.sequenceCode - 1, rowId)
		return output

	def void_dummy(self, details, purchase_entry):
		output = {}
		card = {'cardNumber' : '517652******7557', 
				'MM' : '09',
				'YY' : '10'
				}
		
		card['chName'] = 'Sainath Gupta K'
		
		merchant = {'id' : '',
					'name' : 'AasaanPay Test merchant',
						'address' : 'Gachibowli, IIIT',
					'city' : 'Hyderabad'}		

		transactionInfo = {'baseAmount' : self.amount,
							'acquirer' : 'HDFC',
							'invoice' : str(details.sequenceCode),
							'batchNumber' : str(details.batch),
							'transactionTime' : self.timestamp,
							'apprCode' : '000000',
							'rrn' : '000000000020',
							'transId' : self.transId,
							'terminalId' : self.clientId
				}			
		
		#modifying the void bit because it is a success
		if purchase_entry.status & common.VOID_BIT_VALUE == 0:
			purchase_entry.status += common.VOID_BIT_VALUE
			if purchase_entry.status & common.TIP_BIT_VALUE != 0:
				purchase_entry.tip_details.isDisabled = True
		else:
			#voiding a void 
			current_app.logger.warning('Voiding a void is not possible. ID:' + self.transId + ' ClientID:' + str(self.clientId))

		#Adding entry to the void table with reference to Purchase entry(id)
		entry = void_table(self.username, self.clientId, details.sequenceCode, details.plutusSequenceCode, \
			purchase_entry.id, self.transId, self.timestamp, transactionInfo['invoice'], transactionInfo['apprCode'], \
			transactionInfo['rrn'])
		db.session.add(entry)
		db.session.commit()

		rowId = entry.id
		tranType = common.VOID
		output.update({'merchant' : merchant, 'card' : card, 'transactionInfo': transactionInfo})
	
		#we got a result so we are incrementing sequence codes.
		details.minSequenceCode = details.sequenceCode
		details.sequenceCode += 1
		#details.plutusSequenceCode = plutusRespSequenceCode
		
		common.disableLastTransaction(self.clientId, tranType, details.sequenceCode - 1, rowId)
		return output

	def process(self):
		if not self.isValidRequest() or not self.populateEntries():
			return common.errorMessage('INVALID_REQUEST')
		
		isAuthorised, self.username = common.isAuthorisedRequest(self.clientId, self.sessionToken, self.deviceId)
		if isAuthorised == False: return common.errorMessage(self.username)#Second return value is errorCode
		
		canI, status = common.canIdoTransaction(self.clientId, common.VOID, self.username)
		if not canI: return common.errorMessage('SERVER_BUSY')

		isValid, details = common.validateSequenceCode(self.reqSequenceCode, status)
		if not isValid: return details # Second return value is output to be returned
		
		#purchase entry to be voided
		purchase_entry = db.session.query(purchase_table).filter(and_(purchase_table.transId == self.purchaseId, purchase_table.status % 2 == common.PURCHASE_BIT_VALUE)).first()
		
		if purchase_entry == None:
			if current_app.config['DEBUG']:
				output = {'ierror':'No valid purchase found'}
			else: 
				output = {}
			output.update(common.errorMessageD('INVALID_TRANSACTION'))
		
		else:	
			#populating entries which require its values from details
			self.poplulateTransactionEntries(purchase_entry, details)

			if details.sequenceCode == self.reqSequenceCode + 1:
				details.sequenceCode -= 1
				details.plutusSequenceCode -= 1
			output = self.void(details, purchase_entry)#json dumps
			
			output['sequenceCode'] = details.sequenceCode

		status.transactionStatus = False
		db.session.commit()
		return json.dumps(output)
