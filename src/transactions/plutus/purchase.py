from flask import request, current_app
from datetime import datetime
from src.transactions import client
import xmltodict
try:
	import xml.etree.cElementTree as ET
except ImportError:
	import xml.etree.ElementTree as ET

import simplejson as json

from src.mAuthenticate import common
from ext import db
from src.models import Transaction as purchase_table
from src.utils.TLV import unserialise
from src.utils.miuraDataValidators import MSRData, MSRData_SRED

class Purchase:

	#What happens when two requests with same TransId i.e., same ksn & sequenceCode comes at same timestamp. And response to first request after the response of second request? 
	#Planning to store all the success requests and show them by grouping based on TransID
	#transID different for any two legitimate requests because of difference in KSN!! -> so grouping based on transID would be bullshit.. grouping based on sequenceCode gives good results.. 
	PURCHASE_REQUEST_TYPE = 0

	TERMINAL_TAGS = ("deviceId", "timeStamp", "appVersion")
	AUTHENTICATION_TAGS = ("sequenceCode", "clientId", "sessionToken")
	
	PURCHASE_TAGS = ("terminalInfo", "authentication" , "payload")

	"""
		etype = encoding type 
		ksn will be present only for dukpt entries. So moving it to etype
		UKPT decryption key can be retrieved using POS client Id;
		TLV has ksn in it already
	"""
	PAYLOAD_TAGS = ("eType", "card", "amount", "conflict")#TODO Tip
	
	#Possible json values of card
	MANUAL_ENTRY_TAGS = ("cardNumber", "MM", "YY")	#MANUAL_ENTRY
	PLAIN_CARD_TAGS = ("track1", "track2")	#PLAIN
	UKPT_TAGS = ("encryptedTrack" , "track1Length", "track2Length")
	DUKPT_TAGS = ("encryptedTrack" , "track1Length", "track2Length", "ksn")
	TLV_TAGS = ('tlv', 'chip', 'deviceType') # deviceType = 0 ;MIURA
	
	#Card data type
	MANUAL_ENTRY, PLAIN, UKPT, DUKPT, TLV = range(5)
	#Device types
	MIURA, POSMATE = range(2)

	def __init__(self):
		self.json_data = request.get_json() 
	
	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data for k in self.PURCHASE_TAGS):
			return False 
		
		if not all(k in self.json_data["terminalInfo"] for k in self.TERMINAL_TAGS):
			return False
		
		if not all(k in self.json_data["authentication"] for k in self.AUTHENTICATION_TAGS):
			return False
		
		if not all(k in self.json_data["payload"] for k in self.PAYLOAD_TAGS):
			return False
	
		self.eType = self.json_data["payload"]["eType"]

		#TODO UKPT & DUKPT
		if self.eType == self.MANUAL_ENTRY:
			if not all(k in self.json_data["payload"]["card"] for k in self.MANUAL_ENTRY_TAGS):			
					return False
		elif self.eType == self.PLAIN:
			if not all(k in self.json_data["payload"]["card"] for k in self.PLAIN_CARD_TAGS):
				return False
		elif self.eType == self.UKPT:
			if not all(k in self.json_data["payload"]["card"] for k in self.UKPT_TAGS):
				return False
		elif self.eType == self.DUKPT:
			if not all(k in self.json_data["payload"]["card"] for k in self.DUKPT_TAGS):
				return False
		elif self.eType == self.TLV:
			if not all(k in self.json_data["payload"]["card"] for k in self.TLV_TAGS):
				return False
		else:
			return False
		
		return True


	def populateEntries(self):
		self.deviceId = self.json_data["terminalInfo"]["deviceId"]
		#self.timestamp = self.json_data["terminalInfo"]["timeStamp"]
		#we are not using the mobiles time stamp because it may be erroneous
		self.timestamp = datetime.now().strftime("%Y-%m-%d %X")
		self.appVersion = self.json_data["terminalInfo"]["appVersion"]

		self.reqSequenceCode = self.json_data["authentication"]["sequenceCode"]
		self.clientId = self.json_data["authentication"]["clientId"]
		self.sessionToken = self.json_data["authentication"]["sessionToken"] 

		#eType populated in isValidRequest() itself
		self.conflict = self.json_data["payload"]["conflict"]
		self.baseAmount = self.json_data["payload"]["amount"]

		#not populating tansId because it requires sequence code
		#self.transId = str(self.ksn) + '_' + str(self.sequenceCode)
		
		if type(self.reqSequenceCode) != int or type(self.clientId) != int or type(self.conflict) != bool:
			return False
			

		self.isManualEntry = False
		if self.eType == self.MANUAL_ENTRY:
			#TODO keep a isManualEntryAllowed column for each POS as well
			self.isManualEntry = True
			self.cardNumber = self.json_data["payload"]["card"]["cardNumber"]
			self.MM = self.json_data["payload"]["card"]["MM"]
			self.YY = self.json_data["payload"]["card"]["YY"]
		
		elif self.eType == self.PLAIN: 
			self.track1 = self.json_data["payload"]["card"]["track1"]
			self.track2 = self.json_data["payload"]["card"]["track2"]
		
		elif self.eType == self.UKPT:
			#TODO Validate the encrypted data based on length or checksum
			#Report any exception if any
			pass
		elif self.eType == self.DUKPT:
			pass
		elif self.eType == self.TLV:
			current_app.logger.debug(self.json_data["payload"]["card"]["tlv"])
			self.tlv = unserialise(self.json_data["payload"]["card"]["tlv"].decode("hex"))
			self.chip = self.json_data["payload"]["card"]["chip"]
			if type(self.chip) != bool:
				return False

			self.deviceType = self.json_data["payload"]["card"]["deviceType"]
			if self.deviceType == self.MIURA:
				if self.chip:
					#TODO
					self.track2 = ';5176521005587557=10091010017720?'
					print 'Chip data with encryption'
				else:
					result = False
					for comparitor in MSRData:
						result = result or comparitor.match(self.tlv)
					if result:
						self.track2 = self.tlv.firstMatch([0xE1, 0x5f22])
						print 'MSR data without encryption'
						return True
					
					for comparitor in MSRData_SRED:
						result = result or comparitor.match(self.tlv)
					if result:
						#TODO
						self.track2 = self.tlv.firstMatch([0xE1, 0xDFAE02, 0x5F22])#encrypted track2 #TODO decrypt it using BDK
						self.ksn = self.tlv.firstMatch([0xE1, 0xDFAE03])
						#Call decrypt ksn, track2 information
						self.track2 = ';5176521005587557=10091010017720?'
						print 'MSR data with encryption'
					else:
						print 'Invalid TLV input'
						return False
		
		return True

	def populatePurchaseEntries(self,details):
		#if transaction conflict should store it as previous
		if self.eType == 3: #DUKPT
			if details.sequenceCode == self.reqSequenceCode + 1:
				self.transId = str(self.ksn) + '_' + str(details.plutusSequenceCode - 1)
			else:
				self.transId = str(self.ksn) + '_' + str(details.plutusSequenceCode)
		else:
			if details.sequenceCode == self.reqSequenceCode + 1:
				self.transId = str(datetime.now()) + '_' + str(details.plutusSequenceCode-1)
			else:
				self.transId = str(datetime.now()) + '_' + str(details.plutusSequenceCode)
		
		self.CurrencyCode = details.currency

	def generateXmlRequest(self):
		
		card = ET.Element('Card')
		isManualEntry = ET.SubElement(card, 'IsManualEntry')
		if self.eType == self.MANUAL_ENTRY:
			isManualEntry.text = 'True'
			
			CardNumberElement = ET.SubElement(card, 'CardNumber')
			CardNumberElement.text = self.cardNumber
			
			ExpirationDateElement = ET.SubElement(card, 'ExpirationDate')
			MMElement = ET.SubElement(ExpirationDateElement, 'MM')
			MMElement.text = self.MM
			YYElement = ET.SubElement(ExpirationDateElement, 'YY')
			YYElement.text = self.YY

		else:
			'''
			By this time all the encrypted tracks will be converted to
			plain text tracks. We can directly use the track1 and track2
			variables.
			'''
			isManualEntry.text = 'False'

			# track1Element = ET.SubElement(card, 'Track1')
			# track1Element.text = self.track1
			track2Element = ET.SubElement(card, 'Track2')
			track2Element.text = self.track2

		amount = ET.Element('Amount')
		BaseAmount = ET.SubElement(amount, 'BaseAmount')
		BaseAmount.text = self.baseAmount
		CurrencyCode = ET.SubElement(amount, 'CurrencyCode')	
		CurrencyCode.text = self.CurrencyCode

		POS = ET.Element('POS')
		#TODO POSREFERENCENUMBER = DeviceID?
		POSReferenceNumber = ET.SubElement(POS, 'POSReferenceNumber')
		POSReferenceNumber.text = self.deviceId
		TransactionTime = ET.SubElement(POS, 'TransactionTime')
		TransactionTime.text = self.timestamp
		
		#TODO id = deviceID_timestamp ==> unique as a single deviceId can't generate two legitimate requests at the same timestamp
		#TODO TrackingNumber
		TransactionInput = ET.Element('TransactionInput')
		TransactionInput.set('ID', self.transId)
		TransactionInput.extend((card, amount, POS))

		self.purchaseRequest = ET.Element('purchase-request')
		self.purchaseRequest.insert(0, TransactionInput)

	def plutus_request(self, CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE):
		#TODO #Critical Check this(client) if any concurrent request error arise! Also check whether the object client has any expiry time
		return client.service.PLHub_Exchange(CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE)
	
	def purchase(self, details):
		if not current_app.config['PLUTUS_STATUS']:
			return self.purchase_dummy(details)

		output = {}
		self.generateXmlRequest()	

		#if any exception behaving as if nothing happened
		try:
			result = self.plutus_request(details.plutusClientId, details.plutusSecretKey, self.PURCHASE_REQUEST_TYPE, ET.tostring(self.purchaseRequest), details.plutusSequenceCode)
		except BaseException as e:
			#TODO rollback the session commits
			current_app.logger.debug(str(e))
			return common.errorMessageD('PROCESSOR_BUSY')

		root = ET.fromstring(result.RESPONSE_XML)

		plutusRespSequenceCode = result.SEQUENCE_CODE
		if plutusRespSequenceCode != details.plutusSequenceCode + 1:
			#code should not come here except during initialisation
			if (current_app.config['DEBUG'] or current_app.config['TESTING']) and details.sequenceCode == 1:
				details.plutusSequenceCode = plutusRespSequenceCode
				return common.errorMessageD('INVALID_SEQUENCE_CODE1')
			else:	
				current_app.logger.critical('Implementation error. Sequence code mismatch found\n'
+					'ClientId: ' + str(self.clientId) + ' sequenceCode: ' + str(self.reqSequenceCode) + ' plutusSequenceCode: ' +
					str(result.SEQUENCE_CODE) + ' details.plutusSequenceCode: ' + str(details.plutusSequenceCode))
				details.plutusSequenceCode = plutusRespSequenceCode #Doing it to ensure service to merchant doesn't stop due to implementation error
				return common.errorMessageD('SERVER_ERROR')

	
		transactionOutput = root.find('TransactionOutput')
		# it is unnecessary as plutus won't change the format. perhaps decide after talking to them. Unnecessary overhead for validation check
		if transactionOutput is None:
			current_app.logger.critical('TransactionOutput element not found in plutus response')
			return common.errorMessageD('SYSTEM_ERROR')

		state = transactionOutput.find('State')
		if state is None:
			current_app.logger.critical('TransactionOutput element State not found in plutus response')
			return common.errorMessageD('SYSTEM_ERROR')
		
		if state.find('StatusCode').text == '2025':	#Transaction Approved
		
			transactionOutput.remove(transactionOutput.find('ChargeslipData')) #Removing ChargeslipData
			doc = xmltodict.parse(ET.tostring(root))
			
			Card = doc['hub-response']['TransactionOutput']['Card']
			card = {'cardNumber' : Card['CardNumber'], 
					'MM' : Card['ExpirationDate']['MM'],
					'YY' : Card['ExpirationDate']['YY']
					}
			if Card['CardholderName'] != None:
				card['chName'] = Card['CardholderName']
			else:
				card['chName'] = ''

			Merchant = doc['hub-response']['TransactionOutput']['State']['Merchant']
			merchant = {'id' : Merchant['ID'],
						'name' : Merchant['Name'],
							'address' : Merchant['Address'],
						'city' : Merchant['City']}		

			state = doc['hub-response']['TransactionOutput']['State']
			transactionInfo = {'baseAmount' : self.baseAmount,
								'acquirer' : state['AcquirerName'],
								'invoice' : state['InvoiceNumber'],
								'batchNumber' : state['BatchNumber'],
								'transactionTime' : state['TransactionTime'],
								'apprCode' : state['HostResponse']['ApprovalCode'],
								'rrn' : state['HostResponse']['RetrievalReferenceNumber'],
								'transId' : self.transId,
								'terminalId' : state['TerminalID']
					}			
			
			entry = purchase_table(self.username, self.clientId, details.sequenceCode, details.plutusSequenceCode, \
				self.transId, self.timestamp, self.baseAmount, self.isManualEntry, card['chName'], card['cardNumber'], card['MM'], \
				card['YY'], transactionInfo['invoice'], transactionInfo['batchNumber'], transactionInfo['acquirer'], \
				transactionInfo['apprCode'], transactionInfo['rrn'], state['TerminalID'], Merchant['ID'], card['cardNumber'], \
				common.PURCHASE_BIT_VALUE, details.company, details.agent, details.merchant)
			db.session.add(entry)
			db.session.commit()
			
			rowId = entry.id
			tranType = common.PURCHASE
			output.update({'merchant' : merchant, 'card' : card, 'transactionInfo': transactionInfo})

		else:	
			rowId = 0#Last Transaction doesn't refer to any row of a transaction(Purchase/Void/..) table
			tranType = common.ERROR_IN_TRANSACTION
			output.update({'errorCode': state.find('StatusMessage').text})
			
		#we got a result so we are incrementing sequence codes.
		details.minSequenceCode = details.sequenceCode
		details.sequenceCode += 1
		details.plutusSequenceCode = plutusRespSequenceCode
		
		common.disableLastTransaction(self.clientId, tranType, details.sequenceCode - 1, rowId)
		return output

	def purchase_dummy(self, details):
		output = {}
		
		card = {'cardNumber' : '517652******7557', 
				'MM' : '09',
				'YY' : '10'
				}
		
		card['chName'] = 'Sainath Gupta K'
		
		merchant = {'id' : '',
					'name' : 'AasaanPay Test merchant',
						'address' : 'Gachibowli, IIIT',
					'city' : 'Hyderabad'}		

		transactionInfo = {'baseAmount' : self.baseAmount,
							'acquirer' : 'HDFC',
							'invoice' : str(details.sequenceCode),
							'batchNumber' : str(details.batch),
							'transactionTime' : self.timestamp,
							'apprCode' : '000000',
							'rrn' : '000000000020',
							'transId' : self.transId,
							'terminalId' : self.clientId
				}			
		
		entry = purchase_table(self.username, self.clientId, details.sequenceCode, details.plutusSequenceCode, \
			self.transId, self.timestamp, self.baseAmount, self.isManualEntry, card['chName'], card['cardNumber'], card['MM'], \
			card['YY'], transactionInfo['invoice'], transactionInfo['batchNumber'], transactionInfo['acquirer'], \
			transactionInfo['apprCode'], transactionInfo['rrn'], self.clientId, merchant['id'], card['cardNumber'], \
			common.PURCHASE_BIT_VALUE, details.company, details.agent, details.merchant)
		db.session.add(entry)
		db.session.commit()
		
		rowId = entry.id
		tranType = common.PURCHASE
		output.update({'merchant' : merchant, 'card' : card, 'transactionInfo': transactionInfo})

			
		#we got a result so we are incrementing sequence codes.
		details.minSequenceCode = details.sequenceCode
		details.sequenceCode += 1
		#details.plutusSequenceCode = plutusRespSequenceCode
		
		common.disableLastTransaction(self.clientId, tranType, details.sequenceCode - 1, rowId)
		return output

	def process(self):
		if not self.isValidRequest():
			return common.errorMessage('INVALID_REQUEST')
		
		#populating entries which require its values from details
		self.populateEntries()	#Merge populate entries and populatepurchaseentries into one function
		return self.purchase()
