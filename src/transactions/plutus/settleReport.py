from flask import request, current_app
from src.transactions import client
try:
	import xml.etree.cElementTree as ET
except ImportError:
	import xml.etree.ElementTree as ET

import simplejson as json

from src.mAuthenticate import common
from ext import db
from multiprocessing import Process#TODO Creating a process is too costly. optimise it
from src.models import pendingTip
from src.transactions.tipWorker import TipWorker
from src.models import Transaction as purchase_table
from sqlalchemy import and_


class SettleReport:
	SETTLE_REPORT_REQUEST_TYPE = 16
	SETTLE_REQUEST_TYPE = 5

	SETTLE_REPORT_TAGS = ('terminalInfo', 'authentication', 'conflict')
	TERMINAL_TAGS = ('deviceId', 'timeStamp', 'appVersion')	
	AUTHENTICATION_TAGS = ('sessionToken', 'sequenceCode', 'clientId')

	def __init__(self):
		self.json_data = request.get_json() 
		
	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data for k in self.SETTLE_REPORT_TAGS):
			return False

		if not all(k in self.json_data['terminalInfo'] for k in self.TERMINAL_TAGS):
			return False
		
		if not all(k in self.json_data['authentication'] for k in self.AUTHENTICATION_TAGS):
			return False
		
		return True

	def populateEntries(self):
		self.deviceId = self.json_data['terminalInfo']['deviceId']
		self.timeStamp = self.json_data['terminalInfo']['timeStamp']
		self.appVersion = self.json_data['terminalInfo']['appVersion']

		self.sessionToken = self.json_data['authentication']['sessionToken'] 
		self.reqSequenceCode = self.json_data['authentication']['sequenceCode']
		self.clientId = self.json_data['authentication']['clientId']
		self.conflict = self.json_data['conflict']

		if type(self.reqSequenceCode) != int or type(self.clientId) != int or type(self.conflict) != bool:
			return False

		return True

	def generateXmlRequest(self):
		self.settleReport = ET.tostring(ET.Element('purchase-request'))

	def plutus_request(self, CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE):
		#TODO #Critical Check this(client) if any concurrent request error arise! Also check whether the object client has any expiry time
		return client.service.PLHub_Exchange(CLIENT_ID, SECURITY_TOKEN, REQUEST_TYPE, REQUEST_XML, SEQUENCE_CODE)

	def parseOutput(self, root):
		transactionOutput = root.find('TransactionOutput')#checks for presence of transactionOutput are removed here as it is present in purchase.
		state = transactionOutput.find('State')
		
		if state.find('StatusCode').text == '2025':
			acquirer = transactionOutput.find('Acquirer')
			report = {}
			settleStatus = True
			for bank in acquirer:
				bankName = bank.attrib['Name']
				report[bankName] = {}

				for terminal in bank:
					
					terminalState = terminal.find('TerminalState')
					terminalStatus = terminalState.find('TerminalStatusCode')		
					if terminalStatus.text == '2025':
						
						AuditReportData = terminal.find('AuditReportData')
						Receipt = AuditReportData.find('Receipt')
						
						#lineCount = 21 ==> batch is settled and report is of previous batch.
						#lineCount = 22 ==> current batch summary
						if Receipt.attrib['lineCount'] == '22':
							#Current batch summary

							receiptType = Receipt[1].text
							TID = Receipt[2].text.split(':')[1]
							MID = Receipt[3].text
							batchNumber = Receipt[4].text.split(' ')[2][1:]
							saleSummary = Receipt[12].text
							voidSummary = Receipt[13].text
							total = Receipt[15].text.split(' ')[3]
							refundSummary = Receipt[17].text
							netTotal = Receipt[21].text.split(' ')[4]
							
							saleSplit = saleSummary.split(' ')
							sale = {'count' : saleSplit[2],
									'total' : saleSplit[5]}

							voidSplit = voidSummary.split(' ')		
							void = {'count' : voidSplit[2],
									'total' : voidSplit[4]}

							refundSplit = refundSummary.split(' ')
							refund = {'count' : refundSplit[2],
									'total' : refundSplit[4]}

							#total = {'total' : totalSummary.split(' ')[3]}
							#netTotal = {'netTotal' : netTotal.split(' ')[4]}
							
							#TODO if refund is unavailable for this tid, skip it

							summary = {'title' : receiptType,
										'mid' : MID.split(':')[1],
										'batch' : batchNumber,
										'sale' : sale,
										'void' : void,
										'total' : total,
										'refund' : refund,
										'netTotal' : netTotal,
										'type' : common.BATCH_TOTALS_REPORT}
							report[bankName][TID] = summary	
							settleStatus = False


						elif Receipt.attrib['lineCount'] == '21':
							#last settlement summary
							receiptType = Receipt[1].text
							TID = Receipt[2].text.split(':')[1]
							MID = Receipt[3].text
							batchNumber = Receipt[4].text.split(' ')[2][1:]
							time = Receipt[5].text
							saleSummary = Receipt[10].text
							voidSummary = Receipt[11].text
							total = Receipt[13].text.split(' ')[3]
							refundSummary = Receipt[15].text
							netTotal = Receipt[19].text.split(' ')[4]
							
							saleSplit = saleSummary.split(' ')
							sale = {'count' : saleSplit[2],
									'total' : saleSplit[5]}

							voidSplit = voidSummary.split(' ')		
							void = {'count' : voidSplit[2],
									'total' : voidSplit[4]}

							refundSplit = refundSummary.split(' ')
							refund = {'count' : refundSplit[2],
									'total' : refundSplit[4]}

							#TODO if refund is unavailable for this tid, skip it

							summary = {'title' : receiptType,
										'mid' : MID.split(':')[1],
										'ttime' : time,
										'batch' : batchNumber,
										'sale' : sale,
										'void' : void,
										'total' : total,
										'refund' : refund,
										'netTotal' : netTotal,
										'type':common.LAST_SETTLEMENT_SUMMARY}#for debugging purposes
							report[bankName][TID] = summary
						else:
							current_app.logger.critical('Implementation error')
							return False, common.errorMessageD('SYSTEM_ERROR')
					elif terminalStatus.text == '5053':
						#5053 ==> Batch Not open
						#TODO finalise how to show the settlement report on mobiles after PIN transactions.
						pass
					else:
						current_app.logger.critical('Implementation error')#Additional case overlooked
						return False, common.errorMessageD('SYSTEM_ERROR')

			report['isSettled'] = settleStatus
			return True, report
		else:
			return False, {'errorCode': state.find('StatusMessage').text}

	def settlementReport_dummy(self, details):
		report = {}
		report['HDFC'] = {}

		mynumSales = 0
		mysaleAmount = 0
		mynumTips = 0
		mytipAmount = 0
		mynumVoids = 0
		myVoidAmount =	 0

		purchase_entries = db.session.query(purchase_table).filter(and_(purchase_table.clientId == self.clientId, purchase_table.status > 0)).all()
		for purchase_row in purchase_entries:
			s = purchase_row.status
			if s == common.PURCHASE_BIT_VALUE:
				mynumSales += 1
				mysaleAmount += int(purchase_row.amount)
			elif s == common.PURCHASE_BIT_VALUE | common.VOID_BIT_VALUE:
				mynumSales += 1
				mynumVoids += 1
				myVoidAmount += int(purchase_row.amount)
				mysaleAmount += int(purchase_row.amount)
			elif s == common.PURCHASE_BIT_VALUE | common.TIP_BIT_VALUE:
				mynumSales += 1
				mynumTips += 1
				mysaleAmount += int(purchase_row.amount)
				mytipAmount += int(purchase_row.tipAmount)
			elif s == common.PURCHASE_BIT_VALUE | common.TIP_BIT_VALUE | common.VOID_BIT_VALUE:
				#incase of VOID(TIP_ADJUST) entry in settlement there is no seperate tip entry in plutus response.
				mynumSales += 1
				mynumVoids += 1
				mysaleAmount += int(purchase_row.amount) + int(purchase_row.tipAmount)
				myVoidAmount += int(purchase_row.amount) + int(purchase_row.tipAmount)


		sale = {'count' : mynumSales,
				'total' : 'Rs ' + str(mysaleAmount/100.0)}

		void = {'count' : mynumVoids,
				'total' : '-Rs ' + str(myVoidAmount/100.0)}

		refund = {'count' : 0,
				'total' : '0'}

		#TODO if refund is unavailable for this tid, skip it
		total = mysaleAmount - myVoidAmount
		netTotal = total# - refund['total']

		summary = {'title' : 'Current Batch',
					'mid' : ' ',
					'ttime' : self.timeStamp,
					'batch' : int(details.batch),
					'sale' : sale,
					'void' : void,
					'total' : str(total),
					'refund' : refund,
					'netTotal' : str(netTotal),
					'type' : common.BATCH_TOTALS_REPORT}#for debugging purposes
		
		report['HDFC'][str(self.clientId)] = summary
		report['isSettled'] = False

		report = {'report' : report}#settleReport response format or errorMessage

		#we got a result so we are incrementing sequence codes.
		details.minSequenceCode = details.sequenceCode
		details.sequenceCode += 1
		#details.plutusSequenceCode = plutusRespSequenceCode
		
		common.disableLastTransaction(self.clientId, common.SETTLEMENT_REPORT, details.sequenceCode - 1, 0)
				
		#db.session.commit() #ERRORPRONE
		return report

	def settlementReport(self, details):
		if not current_app.config['PLUTUS_STATUS']:
			return self.settlementReport_dummy(details)

		self.generateXmlRequest()
		try:
			result = self.plutus_request(details.plutusClientId,details.plutusSecretKey, self.SETTLE_REPORT_REQUEST_TYPE, self.settleReport, details.plutusSequenceCode)
		except BaseException as e:
			current_app.logger.warn(str(e))
			return common.errorMessageD('SERVER_ERROR')

		root = ET.fromstring(result.RESPONSE_XML)

		plutusRespSequenceCode = result.SEQUENCE_CODE
		if plutusRespSequenceCode != details.plutusSequenceCode + 1:
			#code should not come here 
			if current_app.config['DEBUG'] and details.sequenceCode == 0:	# set plutus sequenceCode during initial set up
				details.plutusSequenceCode = plutusRespSequenceCode  #it may screw testing
				return common.errorMessageD('SYSTEM_ERROR')
			else:	
				current_app.logger.critical('Implementation error. Sequence code mismatch found\n'+
					'ClientId: ' + str(self.clientId) + ' sequenceCode: ' + str(self.reqSequenceCode) + ' plutusSequenceCode: ' +
					str(result.SEQUENCE_CODE));
				details.plutusSequenceCode = plutusRespSequenceCode #Doing it to ensure service to merchant doesn't stop due to implementation error
				return common.errorMessageD('SERVER_ERROR')

		parseStatus, report = self.parseOutput(root)
		if parseStatus: report = {'report' : report}#settleReport response format or errorMessage

		#we got a result so we are incrementing sequence codes.
		details.minSequenceCode = details.sequenceCode
		details.sequenceCode += 1
		details.plutusSequenceCode = plutusRespSequenceCode
		
		common.disableLastTransaction(self.clientId, common.SETTLEMENT_REPORT, details.sequenceCode - 1, 0)
				
		#db.session.commit() #ERRORPRONE
		return report

	def getSettleStatus(self, details):
		'''
			Note:
			i. Since this is a request made using resolveConflict, it is embedded inside other transaction requests. Due to it, we send seqCode = details.pSeqCode - 1 to avoid jumping of seqCode by 2
			There won't be any problem here as the last request is settleRequest
			ii. Don't  modify sequence codes in this because this request is kind of test. Return value used to update batches in case settle was successful
			Example Outputs:
			i. Success: {'mesg' : True, ...}
			ii. Failure: {'errorCode' : ..}
		'''
		self.generateXmlRequest()
		try:
			result = self.plutus_request(details.plutusClientId, details.plutusSecretKey, self.SETTLE_REPORT_REQUEST_TYPE, self.settleReport, details.plutusSequenceCode - 1)
		except BaseException as e:
			return common.errorMessageD('SERVER_ERROR')

		root = ET.fromstring(result.RESPONSE_XML)
		parseStatus, report = self.parseOutput(root)
		
		#if parseStatus: report.update({'mesg' : report['type']})
		return report

	def process(self):
		if not self.isValidRequest() or not self.populateEntries():
			return common.errorMessage('INVALID_REQUEST')
		
		isAuthorised, self.username = common.isAuthorisedRequest(self.clientId, self.sessionToken, self.deviceId)
		if isAuthorised == False: return common.errorMessage(self.username)#Second return value is errorCode
		
		canI, status = common.canIdoTransaction(self.clientId, common.SETTLEMENT_REPORT, self.username)
		if not canI: return common.errorMessage('SERVER_BUSY')
		isValid, details = common.validateSequenceCode(self.reqSequenceCode, status)
		if not isValid: return details # Second return value is output to be returned
		
		output = {}
		if details.inSettleConflict:
			
			'''
				If there is a conflict at server side, it has to resolved regardless of the client's state.
				Note: 
				i. we send the settleCode tag even if conflict = False is sent in the request. 
				ii. Code enters this block only if last transaction is SettleRequest #TODO move it into a function
			'''
			################################ Invalid input parameters #################################
			
			if not self.conflict:
				current_app.logger.critical('Fraud attempt or wrong implementation at client side.\nConflict at server, but request contains conflict = False. Request: \n' + str(self.json_data))
				output = {'errorCode': common.ERROR_CODES['INVALID_TRANSACTION']}#TODO

			else:	
				if details.sequenceCode == self.reqSequenceCode + 1:
					details.sequenceCode -= 1#Reducing both sequenceCodes to prevent cascading effect. Refer to docs for details
					details.plutusSequenceCode -= 1
				
				response = self.settlementReport(details)

				if response.has_key('report'):
					if response['report']['isSettled']:
						output = common.updateToNewBatch(details, response['report'], self.username, self.clientId, \
						details.sequenceCode - 1, details.plutusSequenceCode - 1)

						if not len(output):#No error found during updateToNewBatch. 
							details.inSettleConflict = False#Note that settle conflict is not removed if there is any error found

						output['settleCode'] = 'l'
					else:
						output['settleCode'] = 'c'

				output.update(response)
		elif self.conflict:
			if details.sequenceCode == self.reqSequenceCode + 1:
				details.sequenceCode -= 1
				details.plutusSequenceCode -= 1

				output.update(self.settlementReport(details))
				if 'report' in output and 'isSettled' in output['report'] and output['report']['isSettled']:
					output['settleCode'] = 'l'
				else:
					current_app.logger.critical(str(output) + ' ClientId: ' + str(self.clientId))
					output = {'errorCode': common.ERROR_CODES['SERVER_ERROR']}
			else:
				current_app.logger.critical('Conflict and sequenceCode imply conflicting states. Request:\n' + str(self.json_data) + \
				'\n SeqCode: ' + str(details.sequenceCode))
				output = {'errorCode': common.ERROR_CODES['INVALID_SEQUENCE_CODE']}
		else:						
			if details.sequenceCode == self.reqSequenceCode + 1:
				details.sequenceCode -= 1
				details.plutusSequenceCode -= 1
			else:
				#Check if there are any pending Tips on this clientId. If so, start the tip worker and send SERVER_ERROR
				if db.session.query(pendingTip).filter(pendingTip.clientId == self.clientId).count():
					def do_some_work():
						db.session.close()#To prevent the sub process from using this db session values
						t1 = TipWorker(self.clientId, self.reqSequenceCode)
						t1.run()

					p = Process(target = do_some_work)
 					p.start()#TODO note that this sub process would remain as a defunct process as its parent process doesn't get notified about its termination. Verify this behavior in large scale
					output = {'errorCode': common.ERROR_CODES['SERVER_BUSY']}
					if current_app.config['TESTING']:
						output.update({'pid' : p.pid})
				else:
					output = self.settlementReport(details)
				
		output['sequenceCode'] = details.sequenceCode#Note that in case of Tipworker being started, details.sequenceCode doesn't change
		status.transactionStatus = False

		db.session.commit()
		return json.dumps(output)
