from flask import request, current_app
from datetime import datetime
import simplejson as json
from sqlalchemy import and_
from src.models import pendingTip as pendingtip_table
from src.models import posDetails
from src.models import Transaction as Purchase
from ext import db
from src.mAuthenticate import common
from src.transactions.tipWorker import TipWorker
from multiprocessing import Process#TODO Creating a process is too costly. optimise it
			
class PendingTip:

	PENDING_TIP_TAGS = ('terminalInfo', 'authentication', 'payload')
	TERMINAL_TAGS = ('deviceId', 'timeStamp', 'appVersion')	
	AUTHENTICATION_TAGS = ('clientId', 'sessionToken')

	#this transid is of the purchase for which this tip is requested
	PAYLOAD_TAGS = ('tipAmount', 'transId', 'sequenceCode')

	def __init__(self):
		self.json_data = request.get_json() 

	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data for k in self.PENDING_TIP_TAGS):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] for k in self.TERMINAL_TAGS):
			return False
		
		if not all(k in self.json_data['authentication'] for k in self.AUTHENTICATION_TAGS):
			return False
		
		if not all(k in self.json_data['payload'] for k in self.PAYLOAD_TAGS):
			return False

		return True

	def populateEntries(self):
		self.deviceId = self.json_data['terminalInfo']['deviceId']
		#self.timestamp = self.json_data['terminalInfo']['timeStamp']
		self.timestamp = datetime.now().strftime('%Y-%m-%d %X')
		self.appVersion = self.json_data['terminalInfo']['appVersion']

		self.clientId = self.json_data['authentication']['clientId']
		self.sessionToken = self.json_data['authentication']['sessionToken'] 

		self.tipAmount = self.json_data['payload']['tipAmount']
		self.transId = self.json_data['payload']['transId']
		self.sequenceCode = self.json_data['payload']['sequenceCode']
		

		if type(self.clientId) != int or type(self.sequenceCode) != int:
			return False

		return True

	def process(self):
		if not self.isValidRequest() or not self.populateEntries():
			return common.errorMessage('INVALID_REQUEST')
		isAuthorised, self.username = common.isAuthorisedRequest(self.clientId, self.sessionToken, self.deviceId)
		if isAuthorised == False: return common.errorMessage(self.username)#Second return value is errorCode
		
		purchase = db.session.query(Purchase).filter(and_(Purchase.clientId == self.clientId, Purchase.transId == self.transId)).first()
		if purchase == None:
			return common.errorMessage('INVALID_TRANSACTION')

		entry = pendingtip_table(self.clientId, self.transId, self.timestamp, self.tipAmount)
		db.session.add(entry)
		db.session.commit()#TODO catch a duplicate entry for transId error IntegrityError -> raise critical alert. CRITICAL
		#POTENTIAL BUG
		
		details = db.session.query(posDetails).filter(posDetails.clientId == self.clientId).first()
		
		if self.sequenceCode == details.sequenceCode:
			'''
				Note that we also need to check the condition posDetails.transactionStatus == False before we start the thread
				to take care of the following case
				i. Purchase request with seqCode = 60
				ii. PendingTip request with seqCode = 60(started in background thread in parallel with purchase request. Ex: PendingTipUploader @Android app)
				Suppose that PendingTIP request reaches server when Purchase request is in progress. 
				Condition seqCode == posDetails.sequenceCode is satisfied and tipWorker is started. Ideally, it shouldn't 
				because a new Tip request will be sent with same PlutusSequenceCode as the purchase request that was sent earlier.
				As a result, purchase request is cancelled and the fun is spoiled.

				But, note that this condition is satisfied using canIdoTransaction() in tipWorker.run(). So, we're skipping this part.
				
				Note 3: An additional check for sequenceCode has to be put after canIdoTransaction() in tipWorker as in the above case, 
				if Purchase request is completed by the time canIdoTransaction() is executed -> details.sequenceCode becomes 61. Whereas 
				pendingTip's seqCode is 60. As a result, purchase will be cancelled if pendingTips are processed.
				The reason we've put this additional check of seqCode here is a tradeoff in creating a subprocess
			'''
			def do_some_work():
				db.session.close()#To prevent the sub process from using this db session values
				t1 = TipWorker(self.clientId, self.sequenceCode)
				t1.run()

			p = Process(target = do_some_work)
 			p.start()#TODO note that this sub process would remain as a defunct process as its parent process doesn't get notified about its termination. Verify this behavior in large scale
			if current_app.config['TESTING']:
				#p.join()
				return json.dumps({'mesg': 'Success', 'pid' : p.pid})

		return json.dumps({'mesg': 'Success'})
