from cStringIO import StringIO
from flask import current_app
from flask.templating import render_template
from xhtml2pdf import pisa

# Define your data
sourceHtml = "<html><body><p>To PDF or not to PDF<p></body></html>"
outputFilename = "test.pdf"

def convertHtmlToPdf(sourceHtml, path):
	''' Utility function to convert html to pdf. Used to generate signatures '''

	# open output file for writing (truncated binary)
	outputFilename = path
	resultFile = open(outputFilename, "w+b")

	# convert HTML to PDF
	pisaStatus = pisa.CreatePDF(
			sourceHtml,                # the HTML to convert
			dest=resultFile)           # file handle to recieve result

	print 'created pdf'

	# close output file
	resultFile.close()                 # close output file

	# return True on success and False on errors
	return pisaStatus.err

def create_pdf(pdf_data):
	filename=current_app.config['UPLOAD_FOLDER']+"/file.pdf"
	f=file(filename,"wb")
	pisa.CreatePDF(StringIO(pdf_data.encode('utf-8')),f)

