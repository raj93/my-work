from PIL import Image,ImageDraw,ImageFont
from flask import current_app

def generateReceipt(purchase, merchantDetails, signaturePath, receiptPath):
	width = 320
	height = 600
	outerPadding = 5
	distBtnLines = 5

	textOffset = 10

	merchantName = merchantDetails.nameOnReceipt
	merchantLocation = merchantDetails.city
	dateTime = str(purchase.transTime)
	mid = purchase.mid
	tid = purchase.tid
	batchNum = purchase.batch
	invoiceNumber = purchase.invoice
	transactionType = "SALE"
	cardNumber = '*'*6 + purchase.maskedPan[6:]
	inputType = purchase.INPUT_TYPE_KEY[purchase.inputType]
	cardType = "VISA"

	approvalCode = purchase.apprCode
	rrnNumber = purchase.rrn

	baseAmount = str(purchase.amount/100.0)
	isTipped = True
	if purchase.tipAmount == 0:
		isTipped = False

	if isTipped:
		tipAmount = str(purchase.tipAmount/100.0)
		totalAmount = str(purchase.amount+purchase.tipAmount/100.0)

	currency = " "+merchantDetails.currency

	cardHolderName = purchase.chname

	bankDisclaimer = "I AGREE TO PAY BY THE RULES OF CARD ISSUER"

	resourcePath = current_app.config['RECEIPT_RESOURCES_FOLDER']
	regularFontPath = resourcePath+"regular.ttf"
	boldFontPath = resourcePath+"bold.ttf"
	logoPath = resourcePath+"logo.jpeg"
	aasaanpayLogoPath = resourcePath+"aasaanpay.png"
	signaturePath = signaturePath
	outputPath = receiptPath

	# DESCRBING FONTS
	font = ImageFont.truetype(regularFontPath, 12)
	boldFont = ImageFont.truetype(boldFontPath, 12)

	# creating an image
	im = Image.new("RGB", (width,height), "white")

	boldDraw = ImageDraw.Draw(im)
	boldDraw.setfont(boldFont)

	draw = ImageDraw.Draw(im)
	draw.setfont(font)

	# bank logo
	# logo should be of fixed dimensions
	logo = Image.open(logoPath)
	logo = logo.resize((150,logo.size[1]))

	bottomMark = 20

	im.paste(logo,((im.size[0]-logo.size[0])/2,bottomMark))


	bottomMark +=  logo.size[1]
	bottomMark += 3*distBtnLines

	# drawing merchant name
	merchantNameSize = draw.textsize(merchantName)
	startPosition = (im.size[0] - merchantNameSize[0])/2
	draw.text((startPosition,bottomMark),merchantName,"black")

	bottomMark += merchantNameSize[1]
	bottomMark += distBtnLines

	# drawing merchant location
	merchantLocationSize = draw.textsize(merchantLocation)
	startPosition = (im.size[0] - merchantLocationSize[0])/2
	draw.text((startPosition,bottomMark),merchantLocation,"black")

	bottomMark += merchantLocationSize[1]
	bottomMark += 2*distBtnLines

	# date time
	dateTimeHeading = "DATE/TIME:"
	dateTimeHeadingSize = boldDraw.textsize(dateTimeHeading)
	boldDraw.text((textOffset,bottomMark),dateTimeHeading,"black")

	# date time value
	startPosition = textOffset+dateTimeHeadingSize[0]+ distBtnLines
	draw.text((startPosition,bottomMark),dateTime,"black")

	bottomMark += dateTimeHeadingSize[1]
	bottomMark += distBtnLines


	# MID HEADING
	midHeading = "MID: "
	midHeadingSize = boldDraw.textsize(midHeading)
	boldDraw.text((textOffset,bottomMark),midHeading,"black")

	# MID value
	startPosition = textOffset + midHeadingSize[0]
	draw.text((startPosition,bottomMark),mid,"black")

	# TID HEADING
	tidSize = draw.textsize(tid)
	tidHeading = "TID: "
	tidHeadingSize = boldDraw.textsize(tidHeading)
	startPosition = im.size[0] - 2*textOffset - tidSize[0] - tidHeadingSize[0]
	boldDraw.text((startPosition,bottomMark),tidHeading,"black")

	# TID value
	startPosition = startPosition + tidHeadingSize[0] + textOffset
	draw.text((startPosition,bottomMark),tid,"black")

	bottomMark += dateTimeHeadingSize[1]
	bottomMark += distBtnLines


	# BATCH HEADING
	batchHeading = "BATCH: "
	batchHeadingSize = boldDraw.textsize(batchHeading)
	boldDraw.text((textOffset,bottomMark),batchHeading,"black")


	# BATCH Value
	startPosition = textOffset+batchHeadingSize[0]
	draw.text((startPosition,bottomMark),batchNum,"black")


	# INVOICE HEADING
	invoiceSize = draw.textsize(invoiceNumber)
	invoiceHeading = "INVOICE: "
	invoiceHeadingSize = boldDraw.textsize(invoiceHeading)
	startPosition = im.size[0] - 2*textOffset - invoiceHeadingSize[0] - invoiceSize[0]
	boldDraw.text((startPosition,bottomMark),invoiceHeading,"black")


	#INVOICE Value
	startPosition = startPosition + invoiceHeadingSize[0] + textOffset
	draw.text((startPosition,bottomMark),invoiceNumber,"black")

	bottomMark += invoiceHeadingSize[1]
	bottomMark += distBtnLines

	# TRANSACTION TYPE
	transactionTypeSize = draw.textsize(transactionType)
	startPosition = (im.size[0] - transactionTypeSize[0]) / 2
	draw.text((startPosition,bottomMark),transactionType,"black")

	bottomMark += transactionTypeSize[1]
	bottomMark += distBtnLines


	# CARD NUMBER
	cardNumberHeading = "CARD NUMBER: "
	cardNumberHeadingSize = boldDraw.textsize(cardNumberHeading)
	boldDraw.text((textOffset,bottomMark),cardNumberHeading,"black")

	startPosition = textOffset+cardNumberHeadingSize[0]
	draw.text((startPosition,bottomMark),cardNumber,"black")

	# INPUT TYPE
	inputTypeSize = draw.textsize(inputType)
	startPosition = im.size[0] - 2*textOffset - inputTypeSize[0]
	draw.text((startPosition,bottomMark),inputType,"black")

	bottomMark += cardNumberHeadingSize[1]
	bottomMark += distBtnLines


	# CARD TYPE
	cardTypeHeading = "CARD TYPE : "
	cardTypeHeadingSize = boldDraw.textsize(cardTypeHeading)
	boldDraw.text((textOffset,bottomMark),cardTypeHeading,"black")

	startPosition = textOffset + cardTypeHeadingSize[0]
	draw.text((startPosition,bottomMark),cardType,"black")


	bottomMark += cardTypeHeadingSize[1]
	bottomMark += distBtnLines



	# APPROVAL CODE HEADING
	approvalCodeHeading = "APPR CODE : "
	approvalCodeHeadingSize = boldDraw.textsize(approvalCodeHeading)
	boldDraw.text((textOffset,bottomMark),approvalCodeHeading,"black")


	# APPROVAL CODE VALUE
	startPosition = textOffset+approvalCodeHeadingSize[0]
	draw.text((startPosition,bottomMark),approvalCode,"black")


	# RRN HEADING
	rrnSize = draw.textsize(rrnNumber)
	rrnHeading = "RRN : "
	rrnHeadingSize = boldDraw.textsize(rrnHeading)
	startPosition = im.size[0] - 2*textOffset - rrnHeadingSize[0] - rrnSize[0]
	boldDraw.text((startPosition,bottomMark),rrnHeading,"black")


	# RRN Value
	startPosition = startPosition + rrnHeadingSize[0] + textOffset
	draw.text((startPosition,bottomMark),rrnNumber,"black")

	bottomMark += rrnHeadingSize[1]
	bottomMark += distBtnLines


	# BASE AMOUNT

	colonPosition = textOffset + boldDraw.textsize(10*'O')[0]

	baseAmountHeading = "AMOUNT "
	baseAmountHeadingSize = boldDraw.textsize(baseAmountHeading)
	boldDraw.text((textOffset,bottomMark),baseAmountHeading,"black")
	boldDraw.text((colonPosition,bottomMark),":","black")

	startPosition = 2*textOffset + colonPosition
	draw.text((startPosition,bottomMark),baseAmount,"black")

	bottomMark += baseAmountHeadingSize[1]
	bottomMark += distBtnLines

	if isTipped:
		tipAmountHeading = "TIP "
		tipAmountHeadingSize = boldDraw.textsize(tipAmountHeading)
		boldDraw.text((textOffset,bottomMark),tipAmountHeading,"black")
		boldDraw.text((colonPosition,bottomMark),":","black")

		#startPosition = 2*textOffset + colonPosition
		draw.text((startPosition,bottomMark),tipAmount,"black")

		bottomMark += tipAmountHeadingSize[1]
		bottomMark += distBtnLines

		divider = '-------------------------------'
		dividerSize = draw.textsize(divider)
		draw.text((textOffset,bottomMark),divider, "black")

		bottomMark += dividerSize[1]
		bottomMark += distBtnLines

		totalAmountHeading = "TOTAL "
		totalAmountHeadingSize = boldDraw.textsize(totalAmountHeading)
		boldDraw.text((textOffset,bottomMark),totalAmountHeading,"black")
		boldDraw.text((colonPosition,bottomMark),":","black")

		#startPosition = 2*textOffset + colonPosition
		draw.text((startPosition,bottomMark),totalAmount,"black")

		bottomMark += totalAmountHeadingSize[1]
		bottomMark += distBtnLines

	asterikSize = draw.textsize("*")
	divider = ((im.size[0] - 2*textOffset)/asterikSize[0])*"*"
	draw.text((textOffset,bottomMark),divider,"black")

	bottomMark += asterikSize[1]
	bottomMark += distBtnLines

	# SIGNATURE
	# TODO signature size should be fixed
	signature = Image.open(signaturePath)
	#signature = signature.resize((im.size[0]-2*textOffset,80))
	startPosition = (im.size[0] - signature.size[0])/2
	im.paste(signature,(startPosition,bottomMark))

	bottomMark += signature.size[1]
	bottomMark += distBtnLines

	# divider 

	asterikSize = draw.textsize("*")
	divider = ((im.size[0] - 2*textOffset)/asterikSize[0])*"*"
	draw.text((textOffset,bottomMark),divider,"black")

	bottomMark += asterikSize[1]
	bottomMark += distBtnLines

	# drawing merchant name
	cardHolderNameSize = draw.textsize(cardHolderName)
	startPosition = (im.size[0] - cardHolderNameSize[0])/2
	draw.text((startPosition,bottomMark),cardHolderName,"black")

	bottomMark += cardHolderNameSize[1]
	bottomMark += distBtnLines


	# divider 
	symbol = '-'
	asterikSize = draw.textsize(symbol)
	divider = ((im.size[0] - 2*textOffset)/asterikSize[0])*symbol
	draw.text((textOffset,bottomMark),divider,"black")

	bottomMark += asterikSize[1]
	bottomMark += distBtnLines

	# BANK DISCLAIMER
	bankDisclaimerSize = draw.textsize(bankDisclaimer)
	startPosition = (im.size[0] - bankDisclaimerSize[0])/2
	draw.text((startPosition,bottomMark),bankDisclaimer,"black")

	bottomMark += bankDisclaimerSize[1]
	bottomMark += distBtnLines

	# divider 

	symbol = '-'
	asterikSize = draw.textsize(symbol)
	divider = ((im.size[0] - 2*textOffset)/asterikSize[0])*symbol
	draw.text((textOffset,bottomMark),divider,"black")

	bottomMark += asterikSize[1]
	bottomMark += distBtnLines

	# aasaanpay logo
	apay = Image.open(aasaanpayLogoPath)
	#apay = apay.resize((150,50))
	xCord = (im.size[0] - apay.size[0]) /2

	im.paste(apay,(xCord,bottomMark))

	bottomMark += apay.size[1]
	bottomMark += distBtnLines


	# creating an image
	nim = Image.new("RGB", (width,bottomMark+textOffset), "white")
	ndraw = ImageDraw.Draw(nim)
	cropim = im.crop((0,0,width,bottomMark))
	nim.paste(cropim,(0,0))
	ndraw.rectangle((outerPadding,outerPadding,nim.size[0] - outerPadding, nim.size[1] - outerPadding),outline="black")

	nim.save(outputPath,optimize=True,quality=50)
