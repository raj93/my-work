from boto.ses.connection import SESConnection
from raven.handlers.logging import SentryHandler
from raven import Client
import logging
from logging import Formatter
from logging.handlers import SMTPHandler, RotatingFileHandler
#http://flask.pocoo.org/docs/errorhandling/#debugging-application-errors 

class SESHandler(SMTPHandler):
	""" 
		Send's an email using BOTO SES.
		Usage: For mailing severe errors in production environment
	"""
	def __init__(self, app, mailhost, fromaddr, toaddrs, subject):
		SMTPHandler.__init__(self, mailhost, fromaddr, toaddrs, subject)
		self.app = app

	def emit(self,record):
		conn = SESConnection()
		conn.send_email(self.fromaddr, self.subject, self.format(record), self.toaddrs) 

def mailHandler(app):
	if app.config['DEBUG'] or app.config['TESTING']:
		return None
	mail_handler = SESHandler(app, mailhost="", fromaddr='pruthvi@aasaanpay.com', toaddrs=app.config['DEVELOPERS'], subject='Production Error')
	mail_handler.setLevel(logging.CRITICAL)
	mail_handler.setFormatter(Formatter('''
	Message type:       %(levelname)s
	Location:           %(pathname)s:%(lineno)d
	Module:             %(module)s
	Function:           %(funcName)s
	Time:               %(asctime)s

	Message:

	%(message)s
	'''))
	return mail_handler


def fileHandler(app):
	file_handler = RotatingFileHandler(app.config['LOG_FILENAME'], mode = 'a', maxBytes = 1*1024*1024, backupCount=20)
	file_handler.setLevel(logging.WARNING)
	file_handler.setFormatter(Formatter(
	'%(asctime)s %(levelname)s: %(message)s  [in %(pathname)s:%(lineno)d]'
	))
	return file_handler

def sentryHandler(app):
	configValue = app.config['SENTRY_DSN']
	client = Client(configValue)
	handler = SentryHandler(client)
	handler.setLevel(logging.ERROR)
	if configValue: return handler

file_handler = lambda app: fileHandler(app)
mail_handler = lambda app: mailHandler(app)
sentry_handler = lambda app: sentryHandler(app)