import re
'''
Returns if new password satisfy the minimum strength as follows:
		1. Atleast one alpha character must exist
		2. Atleast one numeric character must exist
		3. Length must be 8-20 characters
'''
#Removing this as not used.
#timestamp
#appVersion

VALIDATORS = {
	'AMOUNT': re.compile('\d{,10}$'),#Revisit the range of this field
	'DEVICE_ID': re.compile('[a-fA-F0-9]{10,24}$'),
	'DEVICE_MODEL': re.compile('[a-zA-Z0-9 -]{3,25}$'),#- for HTC-One X
	'EMAIL': re.compile('[a-zA-Z0-9+_\-\.]+@[0-9a-zA-Z][.-0-9a-zA-Z]*\.[a-zA-Z]+$'),#take care of max length
	'ENC_TRACK': re.compile('[a-fA-F0-9]{32,256}$'),
	'KSN': re.compile('[a-fA-F0-9]{20}$'),
	'MOBILE': re.compile('\d{10}$'),#'^(\+91[\-\s]?)?[789]\d{9}$'
	'NEW_PASSWORD': re.compile('((?=.*\d)(?=.*[a-zA-Z]).{8,20})$'),
	'OTP': re.compile('[0-9]{8}$'),
	'PASSWORD': re.compile('[a-fA-F0-9]{64}$'),#SHA-256 hash in hex
	'PINBLOCK': re.compile('[a-fA-F0-9]{16}$'),
	'QUERY': re.compile('.{25,200}$'),
	'SESSION': re.compile('(?=[\w\-_]+\.[\w\-_]+\.[\w\-_]+$).{10,60}$'),#TODO error prone. Sign of 20 chars is 55
	'TLV': re.compile('[a-fA-F0-9]{4,1024}$'),#minimum 2 bytes(Tag, length); setting maximum length of 512 bytes of hex data
	'TRANSID': re.compile('[ ._\-:a-zA-Z0-9]{8,30}$'),#Stripe IDs sample transId: ch_47dkcru2qLDxjT
	'USER': re.compile('[a-z0-9][a-z0-9_]{5,19}$'),
	}

#clientId positive integer
#sessionToken isValid using unsign
#transType = integer max value of integer?
#amount in paise max value of transaction
#tipAmount subset of amount
#conflict -> false
#sequenceCode positive integer

def validateEntry(data, pattern):
	''' Returns if data matches the patern
	Params:
		data -- Input data
		pattern -- One of the patterns defined in VALIDATORS or data type
	'''
	#print data, pattern
	if pattern in (hex, 'TRACK2', 'TRACK1', 'MM', 'YY', 'CARD', 'CHNAME'): return True#TODO
	if pattern in (bool, dict): return type(data) is pattern
	elif pattern is int: return type(data) is pattern and data >= 0#= for transType
	else:
	#	print VALIDATORS[pattern].match(data) is not None
		return VALIDATORS[pattern].match(str(data)) is not None

#### Following functions are not used in input validation. Planning to use validateEntry as it is a generalised one and has almost same efficiency###
def isValidEmail(email):
	''' Returns if a email is valid '''
	return VALIDATORS['EMAIL'].match(email) is not None


def isValidDeviceId(deviceId):
	''' Returns if deviceId is valid '''
	return VALIDATORS['DEVICE_ID'].match(deviceId) is not None

def isValidDeviceModel(model):
	''' Returns if device Model is valid'''
	return VALIDATORS['DEVICE_MODEL'].match(model) is not None

def isValidOTP(otp):
	''' Returns if otp is valid'''
	return VALIDATORS['OTP'].match(otp) is not None

def isValidPasswd(passwd):
	''' Returns if password is a valid hash'''
	return VALIDATORS['PASSWORD'].match(passwd) is not None


def isValidNewPasswd(passwd):
	''' Returns if new password satisfy the minimum strength as follows:
		1. Atleast one alpha character must exist
		2. Atleast one numeric character must exist
		3. Length must be 8-20 characters
	'''
	return VALIDATORS['NEW_PASSWORD'].match(passwd) is not None

def isValidQuery(query):
	''' Returns if query is valid '''
	return VALIDATORS['QUERY'].match(query) is not None

def isValidUser(username):
	'''Returns if username is valid'''
	return VALIDATORS['USER'].match(username) is not None

def isValidMobile(mobile):
	''' Returns if mobile number is valid'''
	return VALIDATORS['MOBILE'].match(mobile) is not None

def isValidTransID(transID):
	''' Returns if TransID is valid'''
	return VALIDATORS['TRANSID'].match(transID) is not None
