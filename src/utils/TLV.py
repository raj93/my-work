"""
TLV (Tag/Length/Value) helpers.
"""


import string


class Tag(object):
    """
    Represents a Tag.  Tag objects cannot be modified.  The Tag class provides
    convenience functions for converting/comparing with string, int, and use
    with print.
    """

    def __init__(self, data):
        """
        Tag(data) -> Tag

        data is the tag to represent with the object being created.  It can
        be a string like "\\xDF\\x0D", integer like 0xDF0D, or a list of bytes
        like [0xDF, 0x0D].  It can also be another Tag object (which makes
        implicit conversion easier).
        """
        self._value = []
        if isinstance(data, int) or isinstance(data, long):
            while data:
                self._value = [data & 0xff] + self._value
                data = data >> 8
        elif isinstance(data, str):
            for byte in data:
                self._value.append(ord(byte))
        elif isinstance(data, list) or isinstance(data, tuple):
            self._value = data[:]
        elif isinstance(data, self.__class__):
            self._value = list(data._value)
        else:
            raise ValueError
        self._value = tuple(self._value)

    def constructed(self):
        """
        x.constructed() -> bool

        Returns True if tag coding indicates its data is "constructed",
        i.e. the associated data is more TLV items.
        """
        return (self._value[0] & 0x20) == 0x20

    def serialise(self):
        """
        x.serialise() -> str

        Converts the tag into a string such that Tag(x.serialise()) == x.
        """
        result = ""
        for byte in self._value:
            result += chr(byte)
        return result

    def description(self):
        """
        x.description() -> str

        Returns a description string associated with 'x'.  If no description
        is defined, "** No Description **" is returned.
        """
        return self._descriptions.get(self, "** No Description **")

    def __hash__(self):
        """
        x.__hash__() <==> hash(x)
        """
        return hash(self._value)

    def __cmp__(self, other):
        """
        x.__cmp__(other) <==> cmp(x, other)
        """
        return cmp(self._value, other)

    def __len__(self):
        """
        x.__len__() <==> len(x)

        Returns the number of bytes needed to represent the tag when
        serialised.
        """
        return len(self._value)

    def __int__(self):
        """
        x.__int__() <==> int(x)

        Returns an integer representation of the Tag, where Tag(int(x)) == x.
        """
        result = 0
        for byte in self._value:
            result = (result << 8) + byte
        return result

    def __str__(self):
        """
        x.__str__() <==> str(x)

        Returns a string showing the hex representation of the tag, without a
        leading '0x'.
        """
        return "".join("%02X" % byte for byte in self._value)

    def __repr__(self):
        """
        x.__repr__() <==> repr(x)
        """
        return "Tag(" + repr(self._value) + ")"


class TLV(object):
    """
    Represents a buildable/interrogatable form of tag/length/value data
    structure.  A TLV object maintains a list of Tag objects associated with
    values.  A value is either a string (for primitive tags), or another TLV
    object (for constructed tags).  A TLV object can be created from a data
    packet by using the unserialise() function, or received from a socket
    using recvTLV().
    """

    def __init__(self, tag=None, value=""):
        """
        TLV(tag, value) -> TLV

        Constructor.  Optional parameters tag and value are equivalent to calling
        append(tag, value) immediately after creating an empty TLV.
        """
        self._prefix = ""
        self._objects = []
        if tag:
            self.append(tag, value)

    def __len__(self):
        """
        x.__len__() <==> len(x)

        Uses built-in len() function to return the count of TLV items stored
        by the TLV instance.  Note: if any items are constructed, sub-items
        are not included in this count.
        """
        return len(self._objects)

    def __getitem__(self, index):
        """
        x.__getitem__(index) <==> x[index]
        x.__getitem__(index) -> str (if value at 'index' is primitive)
        x.__getitem__(index) -> TLV (if value at 'index' is constructed)

        Uses built-in subscript[] operator to get at TLV data items.
        """
        if isinstance(index, int) or isinstance(index, long):
            if len(self) > index:
                return self._objects[index][1]
        return None

    def copy(self):
        """
        x.copy() -> TLV

        Returns a TLV object which is a copy of the current state of 'x'.
        Changing 'x' after the call will not affect anything in returned copy.
        """
        return unserialise(self.serialise())

    def firstMatch(self, tagPath):
        """
        x.firstMatch(tagPath) -> str (if matching tag is primitive)
        x.firstMatch(tagPath) -> TLV (if matching tag is constructed)
        x.firstMatch(tagPath) -> None (if no matching tag)

        Returns the first value that matches the tagPath.  Returns None if
        there is no match.  Note that a value return of "" (empty string)
        means a match *is* found (albeit 0 length).
        """
        matches = self.search(tagPath)
        if matches:
            return matches[0]
        return None

    def search(self, tagPath):
        """
        x.search(tagPath) -> list

        Returns a list of values matching the specified tagPath.  tagPath is a
        list of Tag objects, or values which can be converted Tag objects via the
        Tag constructor.  The return list is empty [] if no matches are found.
        """
        values = []
        head = Tag(tagPath[0])
        tail = tagPath[1:]
        for tag, val in self._objects:
            if tag == head:
                if len(tail) > 0 and isinstance(val, TLV):
                    values += val.search(tail)
                elif len(tail) == 0:
                    values += [val]
        return values

    def empty(self):
        """
        x.empty() -> bool

        Returns True if no data is contained.
        """
        return len(self) == 0

    def firstTag(self, asInt=False):
        """
        x.firstTag(asInt) -> Tag
        x.firstTag(asInt) -> None

        Returns the first Tag in this TLV.  If asInt is True the value
        returned is an integer converted from the corresponding Tag
        object.  Returns None if there are no items.
        """
        if self._objects:
            tag, value = self._objects[0]
            return tag if not asInt else int(tag)
        else:
            return None

    def tags(self, asInts=False):
        """
        x.tags(asInts) -> list

        Returns a list of all the Tag items in this TLV.  Note: tags from
        inner constructed items are not included.  If asInts is True then
        the returned list contains integers converted from the Tag
        objects.
        """
        result = []
        for tag, value in self._objects:
            result += [tag]
        if asInts:
            result =  map(int, result)
        return result

    def append(self, tag, value):
        """
        x.append(tag, value) -> Updates 'x', no return value.

        Adds a new tag/value pair to 'x'.

        'tag' - any type supported by Tag.__init__().
        'value' - can be either a string for a primitive tag, or another TLV
                object for a constructed tag.

        Note: A TLV object passed as 'value' is used by reference.  Modifying
        the original object later also affects 'x'.
        """
        if not (isinstance(value, str) or isinstance(value, TLV)):
            raise ValueError
        self._objects.append((Tag(tag), value))

    def extend(self, tlv):
        """
        x.extend(tlv) -> Updates 'x', no return value.

        Individually appends all the items in 'tlv' to 'x'.  Note that any
        consructed items in 'tlv' that become modified after this call will
        also be updated in 'x'.
        """
        for tag, value in tlv._objects:
            self.append(tag, value)

    def __add__(self, other):
        """
        x.__add__(other) <==> x + other
        x.__add__(other) -> TLV

        Returns a TLV object that is the concatenation of 'x' and 'other'.
        Neither 'x' nor 'other' are modified or referenced by the result.
        """
        return unserialise(self.serialise() + other.serialise())

    def serialise(self):
        """
        x.serialise() -> str

        Returns formatted TLV data as a string.
        """
        data = ""
        for tag, value in self._objects:
            data += tag.serialise()
            tmpData = ""
            if isinstance(value, str):
                tmpData = value
            elif isinstance(value, TLV):
                tmpData = value.serialise()
            else:
                raise ValueError
            lenBytes = 0
            if len(tmpData) >= 128:
                lenBytes += 1
            if len(tmpData) >= 256:
                lenBytes += 1
            if lenBytes > 0:
                data += chr(0x80 + lenBytes)
                for index in reversed(range(lenBytes)):
                    data += chr((len(tmpData) >> (8 * index)) & 0xff)
            else:
                data += chr(len(tmpData))
            data += tmpData
        return data

    def __str__(self):
        """
        x.__str__() <==> str(x)

        Warning, returned string may be long and multiple lines!  Intended for
        use with built-in print function/debug logging.
        """
        if not self._objects:
            return ""
        data = ""
        maxTagWidth = max(map(lambda x: len(str(x[0])), self._objects))
        maxDescWidth = max(map(lambda x: len(x[0].description()), self._objects))
        maxLen = max(map(lambda x: len(x[1]) if isinstance(x[1], str) else 0, self._objects))
        maxLenWidth = len("%i" % maxLen)
        for tag, value in self._objects:
            preamble = self._prefix + ("%" + ("%i" % maxTagWidth) + "s ") % str(tag)
            preamble += ("%-" + ("%i" % (maxDescWidth + 2)) + "s ") % ("[" + tag.description() + "]")
            if isinstance(value, str):
                data += preamble + (("(%" + ("%i" % maxLenWidth) + "i) ") % len(value))
                if maxDescWidth > 32:
                    data += "\n"
                    data += " " * (len(self._prefix) + len(str(tag)) + 1)
                thisData = "".join("%02X" % ord(byte) for byte in value)
                data += thisData
                if not (False in map(lambda x: x in string.printable, value)):
                    thisLen = len(thisData)
                    remainingLen = (maxLen * 2) - thisLen
                    data += (" " * remainingLen) + " " + repr(value)
                data += "\n"
            elif isinstance(value, TLV):
                value._prefix = self._prefix + "    "
                data += preamble + (("(%" + ("%i" % maxLenWidth) + "i) ")  % len(value.serialise()))
                data += "\n" + str(value)
        return data

    def __repr__(self):
        """
        x.__repr__() <==> repr(x)
        """
        return "TLV(" + repr(self._objects) + ")"


class TagComparitor(object):
    """
    For comparison with a TLV object.  The comparison is only intended to care
    about corresponding existence of tags, values are ignored.
    """

    def __init__(self, *args):
        """
        TagComparitor(tagPath, [tagPath [, ...]]) -> TagComparitor
        TagComparitor(tlv) -> TagComparitor

        Accepts one or more tag path lists, or a single TLV object.
        tagPath - list of tags.  Each tag can be anything supported by
                  Tag.__init__().
        """
        self._tagPaths = {}
        
        if len(args) == 1 and isinstance(args[0], TLV):
            self._buildFromTLV(args[0])
        else:
            for tagPath in args:
                tp = tuple(map(lambda tag: Tag(tag), tagPath))
                try:
                    self._tagPaths[tp] += 1
                except KeyError:
                        self._tagPaths[tp] = 1

    def empty(self):
        """
        x.empty() -> bool

        Returns True if 'x' is an empty comparitor (i.e. contains no tags to
        compare).
        """
        return len(self._tagPaths) == 0

    def match(self, tlv):
        """
        x.match(tlv) -> bool

        Returns True if TLV object 'tlv' contains at least one of each tag
        matching the tag sets in 'x'.  Otherwise returns False.  Note: As a
        special case, if 'x' is empty, this will only match if 'tlv' is also
        empty.
        """
        result = True
        if isinstance(tlv, TLV):
            result = result and (self.empty() == tlv.empty())
            for tagPath, count in self._tagPaths.iteritems():
                result = result and (len(tlv.search(tagPath)) > 0)
        else:
            raise ValueError("tlv parameter is not a TLV object.")
        return result

    def matchExact(self, tlv):
        """
        x.matchExact(tlv) -> bool

        Returns True if TLV object 'tlv' contains only the tags in 'x', and in
        the same quantity.
        """
        return self.__class__(tlv) == self

    def __cmp__(self, other):
        """
        x.__cmp__() <==> cmp(x, other)
        """
        return cmp(self._tagPaths, other._tagPaths)

    def __str__(self):
        """
        x.__str__() <==> str(x)
        """
        return str(self._tagPaths)

    def _buildFromTLV(self, tlv, path=()):
        """
        Internal function, do not call this.
        """ 
        for tag, value in tlv._objects:
            if tag.constructed() and isinstance(value, TLV):
                self._buildFromTLV(value, path + (tag,))
            elif not tag.constructed() and isinstance(value, str):
                try:
                    self._tagPaths[path + (tag,)] += 1
                except KeyError:
                    self._tagPaths[path + (tag,)] = 1
            else:
                raise ValueError("(un)constructed tag/value mismatch.")


def _getTag(data):
    """
    _getTag(data) -> (Tag, str)

    Gets a TLV "tag" from data and returns it as a Tag object along with the
    data remaining after the tag element is removed.
    """
    if len(data) < 1:
        return None, ""
    tag = [ord(data[0])]
    if ord(data[0]) & 0x1f == 0x1f:
        for byte in data[1:]:
            tag += [ord(byte)]
            if ord(byte) & 0x80 != 0x80:
                break
    return Tag(tag), data[len(tag):]


def _getLength(data):
    """
    _getLength(data) -> (int, str)

    Gets a TLV "length" from data and returns it as an integer along with the
    data remaining after the length element is removed.
    """
    if len(data) < 1:
        return -1, ""
    length = 0
    lenBytes = 1
    if ord(data[0]) & 0x80 == 0x80:
        lenBytes += ord(data[0]) & 0x7f
        for byte in data[1:lenBytes]:
            length = (length << 8) + ord(byte)
    else:
        length = ord(data[0]) & 0x7f
    return length, data[lenBytes:]


def unserialise(data):
    """
    unserialise(data) -> TLV

    Take TLV encoded string 'data', and return a TLV object representation.
    """
    result = TLV()
    length = 0

    while length >= 0 and len(data):
        tag, data = _getTag(data)
        length, data = _getLength(data)
        if tag:
            if tag.constructed():
                result.append(tag, unserialise(data[:length]))
            else:
                result.append(tag, data[:length])
            data = data[length:]
    return result


def recvTLV(sock):
    """
    recvTLV(sock) -> TLV

    Use sock.recv() until a single TLV formatted data item has been received.
    The data item is returned as a TLV object.
    """
    tlv = TLV()
    tag = _readTag(sock)
    length = _readLen(sock)
    value = ""
    while len(value) < length:
        value += sock.recv(length)
    if tag.constructed():
        tlv.append(tag, unserialise(value))
    else:
        tlv.append(tag, value)
    return tlv


def _readTag(sock):
    """
    _readTag(sock) -> Tag

    Reads a TLV "tag" from sock, and returns a Tag object representing it.
    """
    data = sock.recv(1)
    if ord(data[0]) & 0x1f == 0x1f:
        data += sock.recv(1)
        while (ord(data[-1]) & 0x80 == 0x80) and (len(data) < 4):
            data += sock.recv(1)
    return Tag(data)


def _readLen(sock):
    """
    _readLen(sock) -> int

    Reads a TLV "length" from sock, and returns the integer value it
    represents.
    """
    data = sock.recv(1)
    length = 0
    if ord(data[0]) & 0x80 == 0x80:
        for index in range(ord(data[0]) & 0x7f):
            data += sock.recv(1)
            length = (length << 8) + ord(data[-1])
    else:
        length = ord(data[0]) & 0x7f
    return length


def tagLookup(description):
    """
    tagLookup(description) -> Tag

    Returns a Tag instance that matches 'description' string.
    """
    return Tag._lookup[description]


Tag._descriptions = {
    Tag(0x48): "Card Status",
    Tag(0x4f): "AID",
    Tag(0x50): "Application Label",
    Tag(0x57): "Track 2 Equivalent Data",
    Tag(0x5a): "Application Primary Account Number (PAN)",
    Tag(0x5f20): "Cardholder Name",
    Tag(0x5f2d): "Language Preference",
    Tag(0x5f24): "Application Expiration Date",
    Tag(0x5f25): "Application Effective Date",
    Tag(0x5f28): "Issuer Country Code",
    Tag(0x5f2a): "Transaction Currency Code",
    Tag(0x5f30): "Service Code",
    Tag(0x5f34): "Application Primary Account Number (PAN) Sequence Number",
    Tag(0x5f36): "Transaction Currency Exponent",
    Tag(0x61): "Application Template",
    Tag(0x6f): "FCI Template",
    Tag(0x70): "Read Record response",
    Tag(0x77): "Response Message Template Format 2",
    Tag(0x80): "Response Message Template Format 1",
    Tag(0x81): "Amount, Authorised (Binary",
    Tag(0x82): "Application Interchange Profile",
    Tag(0x84): "DF Name",
    Tag(0x86): "Issuer Script Command",
    Tag(0x87): "Application Priority Indicator",
    Tag(0x88): "SFI",
    Tag(0x8a): "Authorisation Response Code",
    Tag(0x8c): "Card Risk Management Data Object List 1 (CDOL1)",
    Tag(0x8d): "Card Risk Management Data Object List 2 (CDOL2)",
    Tag(0x8e): "Cardholder Verification Method (CVM) List",
    Tag(0x8f): "Certification Authority Public Key Index",
    Tag(0x90): "Issuer Public Key Certificate",
    Tag(0x92): "Issuer Public Key Remainder",
    Tag(0x93): "Signed Static Application Data",
    Tag(0x94): "Application File Locator (AFL)",
    Tag(0x95): "Terminal Verification Results",
    Tag(0x97): "Transaction Certificate Data Object List (TDOL)",
    Tag(0x9b): "Transaction Status Information",
    Tag(0x9c): "Transaction Type",
    Tag(0x9c00): "Transaction Information Status sale",
    Tag(0x9c01): "Transaction Information Status cash",
    Tag(0x9c09): "Transaction Information Status cashback",
    Tag(0x9f01): "Acquirer Identifier",
    Tag(0x9f02): "Amount, Authorised (Numeric)",
    Tag(0x9f03): "Amount, Other (Numeric)",
    Tag(0x9f04): "Amount, Other (Binary)",
    Tag(0x9f05): "Application Discretionary Data",
    Tag(0x9f06): "Application Identifier (AID) - terminal",
    Tag(0x9f07): "Application Usage Control",
    Tag(0x9f08): "ICC Application Version Number",
    Tag(0x9f09): "Term Application Version Number",
    Tag(0x9f0b): "Cardholder Name Extended",
    Tag(0x9f0d): "Issuer Action Code - Default",
    Tag(0x9f0e): "Issuer Action Code - Denial",
    Tag(0x9f0f): "Issuer Action Code - Online",
    Tag(0x9f10): "Issuer Application Data",
    Tag(0x9f11): "Issuer Code Table Index",
    Tag(0x9f12): "Application Preferred Name",
    Tag(0x9f13): "Last Online Application Transaction Counter (ATC) Register",
    Tag(0x9f14): "Lower Consecutive Offline Limit",
    Tag(0x9f17): "Personal Identification Number (PIN) Try Counter",
    Tag(0x9f1a): "Terminal Country Code",
    Tag(0x9f1b): "Terminal Floor Limit",
    Tag(0x9f1c): "Terminal ID",
    Tag(0x9f1e): "Interface Device Serial Number",
    Tag(0x9f1f): "Track 1 Discretionary Data",
    Tag(0x9f20): "Track 2 Discretionary Data",
    Tag(0x9f23): "Upper Consecutive Offline Limit",
    Tag(0x9f26): "Application Cryptogram",
    Tag(0x9f27): "Cryptogram Information Data",
    Tag(0x9f2d): "ICC PIN Encipherment Public Key Certificate",
    Tag(0x9f2e): "ICC PIN Encipherment Public Key Exponent",
    Tag(0x9f2f): "ICC PIN Encipherment Public Key Remainder",
    Tag(0x9f32): "Issuer Public Key Exponent",
    Tag(0x9f33): "Terminal Capabilities",
    Tag(0x9f34): "Cardholder Verification Method (CVM) Results",
    Tag(0x9f35): "Terminal Type",
    Tag(0x9f36): "Application Transaction Counter (ATC)",
    Tag(0x9f37): "Unpredictable Number",
    Tag(0x9f38): "Processing Options Data Object List (PDOL)",
    Tag(0x9f3b): "Application Reference Currency",
    Tag(0x9f40): "Terminal Capabilities Add'l",
    Tag(0x9f42): "Application Currency Code",
    Tag(0x9f43): "Application Reference Currency Exponent",
    Tag(0x9f44): "Application Currency Exponent",
    Tag(0x9f46): "ICC Public Key Certificate",
    Tag(0x9f47): "ICC Public Key Exponent",
    Tag(0x9f48): "ICC Public Key Remainder",
    Tag(0x9f49): "Dynamic Data Authentication Data Object List (DDOL)",
    Tag(0x9f4a): "Static Data Authentication Tag List",
    Tag(0x9f4b): "Signed Dynamic Application Data",
    Tag(0x9f4c): "ICC Dynamic Number",
    Tag(0x9f5b): "Issuer Script Results",
    Tag(0xa5): "FCI Proprietary Template",
    Tag(0xbf0c): "File Control Information (FCI) Issuer Discretionary Data",
    Tag(0xc0): "Decision",
    Tag(0xc2): "Acquirer Index",
    Tag(0xc3): "Status Code",
    Tag(0xc4): "Status Text",
    Tag(0xc5): "PIN Retry Counter",
    Tag(0xdf0d): "Identifier",
    Tag(0xdf28): "Cardholder Verification Status",
    Tag(0xdf7f): "Version",
    Tag(0xe0): "Command Data",
    Tag(0xe1): "Response Data",
    Tag(0xe2): "Decision Required",
    Tag(0xe3): "Transaction Approved",
    Tag(0xe4): "Online Authorisation Required",
    Tag(0xe5): "Transaction Declined",
    Tag(0xe6): "Terminal Status Changed",
    Tag(0xed): "Configuration Information",
    Tag(0xef): "Software Information",
    Tag(0xff0d): "Terminal Action Code DEFAULT",
    Tag(0xff0e): "Terminal Action Code OFFLINE",
    Tag(0xff0f): "Terminal Action Code ONLINE",
    Tag(0xdfa201): "PIN Digit Status",
    Tag(0xdfa202): "PIN Entry Status",
    Tag(0xdfa203): "Configure TRM Stage",
    Tag(0xdfa204): "Configure Application Selection",
    Tag(0xdfa205): "Keyboard Data",
    Tag(0xdfa206): "Secure Prompt",
    Tag(0xdfa207): "Number Format",
    Tag(0xdfa208): "Numeric Data",
    Tag(0xdfa209): "Battery Data",
    Tag(0xdfa301): "Stream Offset",
    Tag(0xdfa302): "Stream Size",
    Tag(0xdfa303): "Stream timeout",
    Tag(0xdfa304): "File md5sum",
    Tag(0xdfae01): "P2PE Status",
    Tag(0xdfae02): "SRED Data",
    Tag(0xdfae03): "SRED KSN",
    Tag(0xdfae04): "Online PIN Data",
    Tag(0xdfae05): "Online PIN KSN",
    Tag(0xdfae22): "Masked Track 2",
    Tag(0xdfae57): "ICC Masked Track 2",
    Tag(0xdfae5A): "Masked PAN",
    Tag(0xdfdf00): "Transaction Info Status bits",
    Tag(0xdfdf01): "Revoked certificates list",
    Tag(0xdfdf02): "Online DOL",
    Tag(0xdfdf03): "Referral DOL",
    Tag(0xdfdf04): "ARPC DOL",
    Tag(0xdfdf05): "Reversal DOL",
    Tag(0xdfdf06): "AuthResponse DO",
    Tag(0xdfdf09): "PSE Directory",
    Tag(0xdfdf10): "Threshold Value for Biased Random Selection",
    Tag(0xdfdf11): "Target Percentage for Biased Random Selection",
    Tag(0xdfdf12): "Maximum Target Percentage for Biased Random Selection",
    Tag(0xdfdf13): "Default CVM",
    Tag(0xdfdf16): "Issuer script size limit",
    Tag(0xdfdf17): "Log DOL",
    Tag(0xe001): "Partial AID Selection Allowed",
}


Tag._lookup = {}
for _tag, _name in Tag._descriptions.iteritems():
    if Tag._lookup.has_key(_name):
        raise ValueError("Duplicate tag description: %s" % _name)
    Tag._lookup[_name] = _tag


