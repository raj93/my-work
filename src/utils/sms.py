import requests
from flask import current_app

def send(payload):
	'''
	Sends SMS

	@type payload: dictionary
	@params payload: a dictionary containing phone number and sms body

	@return string "Success" or "Failure"
	'''
	try:
		url="https://mobilecozysolutions:fa2a343d09f9b4b30beb5c37c0337d3540f19a1e@twilix.exotel.in/v1/Accounts/mobilecozysolutions/Sms/send"
		#proxy = {'http' : 'http://proxy.iiit.ac.in:8080', 'https' : 'http://proxy.iiit.ac.in:8080'}#Note https://proxy doesn't work
		conn = requests.post(url, params = payload)#, proxies = proxy) #Environment variables to be used
		if conn.status_code == 200:
			return 'Success'
		else:
			current_app.logger.warn("Sending message with exotel failed "+"\n Response:" +conn.text)
			return 'Failure'
	except Exception as e:
		# log exception
		current_app.logger.error(str(e))
		return "Failure"

def sendTransactionSms(nameOnReceipt, transType, url, amount, cardNumber, rrn, invoice, phoneNumber):
	'''
		Sends SMS for a transaction
	'''
	#e-Receipt for your %s is available at %s Rs. %s Card: *%d RRN: %s Inv: %d. Hope you enjoyed the experience.
	amount = str(amount)
	amount = amount[:-2] + '.' + amount[-2:]
	body = 'e-Receipt for your %s is available at %s Rs. %s Card: *%s RRN: %s Inv: %s.' % \
		(transType, url, amount, cardNumber, rrn, invoice)
	payload = {'To': phoneNumber,'Body' : body}
	return send(payload)

def sendSettlementSms(amount, phoneNumber):
	'''
	Sends settlement SMS

	Params:
	amount -- Amount in paise in integer format
	phone -- Phone Number in string format
	'''
	amount = str(amount)
	amount = 'Rs. ' + amount[:-2] + '.' + amount[-2:]
	body = 'Your settlement request has been approved. Amount of %s will be credited to your account in the next working day.' % \
			(amount)
	payload = {'To' : phoneNumber, 'Body' : body}
	return send(payload)

def sendSupportSms(contact, phoneNumber):
	'''
		Sends a SMS to customer as soon as he logs a request.
		#TODO financial model ; 5 support sms free per month or possible exploit here
	'''

	body = 'Our support team will be in touch with you shortly on %s to resolve the issue. Have a nice day' % (contact)
	payload = {'To' : phoneNumber, 'Body' : body}
	return send(payload)

def sendPosCredentialsSms(reciepentNumber,username,password):
	body = "Congrats, your M-POS has been successfully activated. Please login with username: %s password: %s to start using AasaanPay services." % (username,password)
	payload = {'To' : reciepentNumber , 'Body' : body}
	return send(payload)