import os, errno

def ensure_dir(f):
	''' 
	Creates a directory hierarchy required for creating f
	'''
	if type(f) != str: raise Exception('String expected')
	try:
		os.makedirs(f.rsplit('/', 1)[0])
	except OSError as exception:
		if exception.errno != errno.EEXIST:
			raise
