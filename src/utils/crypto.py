
import hashlib, os, string, random

def otp_generator(size, chars=string.letters + string.digits):
	'''Generates a random string of length = size'''
	if type(size) != int or size < 0 or chars is None: return ''
	return ''.join(random.choice(chars) for x in range(size))

def otp_generator1(size, chars=string.letters + string.digits+"'~!@#$%^&*()_+``-=[]{}\|;:\"',<.>/?"):
	'''Generates a random string of length = size'''
	if type(size) != int or size < 0 or chars is None: return ''
	return ''.join(random.choice(chars) for x in range(size))

def sha256(data):
	if type(data) not in (str, unicode): raise Exception('Data must be string or unicode')
	return hashlib.sha256(data).hexdigest()

def hash_passwd(passwd, salt=None):
	'''
	@Params:
	passwd -- Hashed Password(sha256(passwd_plain)) in hex format; 32 bytes
	salt -- Salt in hex format; 32 bytes or None
	'''
	if type(passwd) not in (str, unicode): raise Exception('Input must be string or unicode')
	
	if salt is None: salt = os.urandom(32)
	else: 
		if type(salt) not in (str, unicode): raise Exception('Input must be string or unicode')
		salt = salt.decode('hex')

	hashed_passwd = hashlib.sha512(passwd.decode('hex') + salt).hexdigest()
	return (hashed_passwd, salt.encode('hex'))
 
def verify_passwd(passwd, hashed_passwd, salt):
	'''
		@param passwd :hex
		@param hashed_passwd :hex
		@param salt :hex
	'''
	re_hashed, salt = hash_passwd(passwd, salt)
 
	return re_hashed == hashed_passwd


def generateRandomPassword(size):
	'''Generates a random password of lengtg size
	This module is used when password is to be communicated to user'''
	if type(size) != int or size < 0: return ''
	chars = string.letters + string.digits# + "'~!@#$%^&*()_+``-=[]{}\|;:\"',<.>/?"
	# making it little easy to read as it is a temporary password
	s= random.choice(string.digits) + random.choice(string.letters) + ''.join(random.choice(chars) for x in range(size - 2))
	return ''.join(random.sample(s, len(s)))

class GDSeeds():
	'''
		Temporary utility to decrypt card data from GD seeds devices
		#TODO remove it immediately!
	'''
	track1 = ""
	track2 = ""

	# INPUT: encrypted data is in hex format
	# DELIVERABLE: fills track1 and track2
	# default key
	def decryptData(self,hexData):
		from Crypto.Cipher import DES3
		gdSeedsKey = "0123456789abcdeffedcba9876543210"
		eng = DES3.new(gdSeedsKey.decode("hex"))
		trackData =  eng.decrypt(hexData.decode("hex")).encode("hex")

		track2Hex = trackData[0:40]
		track1Hex = trackData[40:160]

		# decrypting track2
		for c in track2Hex:
			self.track2 += ('3'+c).decode("hex")

		# decrypting track1
		for i in range(len(track1Hex)):
			if i % 3 != 2:
				if i % 3 == 0:
					#take 2 hex from index i and then get first 6 bits
					self.track1 += str(unichr((ord(track1Hex[i:i+2].decode("hex")) >> 2)+ 32)) # don't know why add 32
				else:
					# take 2 hex from index i and then get last 6 bits
					self.track1 += str(unichr((ord(track1Hex[i:i+2].decode("hex")) & 63) + 32)) # HH & 3f  to get 
	
	def encryptData(self, data):
		from Crypto.Cipher import DES3
		gdSeedsKey = "0123456789abcdeffedcba9876543210"
		eng = DES3.new(gdSeedsKey.decode("hex"))
		return eng.encrypt(data).encode("hex")

	def __repr__(self):
		return ('track1 data: %s \n track2data: %s '%(self.track1,self.track2))
