import botocore.session

def uploadFile(path, bucket, object_name, content_type = 'application/pdf', public = False):
	if not ((type(path) in (unicode, str)) and \
			(type(bucket) in (unicode, str)) and \
			(type(object_name) in (unicode, str))):
		raise Exception('Invalid input')
	session = botocore.session.get_session()
	s3 = session.get_service('s3')
	operation = s3.get_operation('PutObject')
	endpoint = s3.get_endpoint('us-east-1')
	fp = open(path, 'rb')
	#Add expiry time to these objects so that congestion wouldn't happen after a while
	http_response, response_data = operation.call(endpoint,
											  bucket = bucket,
											  key = object_name,
											  body = fp, acl = 'public-read' if public else 'private',
											  content_type = content_type)
	return http_response.status_code == 200

def LookUpKey(bucket, object_name):
	if not ((type(bucket) in (unicode, str)) and \
			(type(object_name) in (unicode, str))):
		raise Exception('Invalid input')
	session = botocore.session.get_session()
	s3 = session.get_service('s3')
	operation = s3.get_operation('GetObject')
	endpoint = s3.get_endpoint('us-east-1')
	http_response, response_data = operation.call(endpoint,
											  bucket = bucket,
											  key = object_name)
	return http_response.status_code == 200