from src.utils.TLV import TagComparitor

"""
Don't require track1(e1,5f21) data. So, skipping it. Searching for 
	i. TLV Identifier(e1,48)
	ii. Track2 data (e1, 5f22)
	iii. Masked PAN (e1, dfae22)
"""
MSRData = [ TagComparitor([0xE1, 0x48], 
									[0xE1, 0x5F22],
									[0xE1, 0xDFAE22])
			]#TODO care of PIN similar to MSR_SRED

"""
Don't require track1(e1, dfae02, 5f21) data. So, skipping it. Searching for 
	i. TLV Identifier(e1,48)
	ii. Encrypted Track data (e1, dfae02); This data when unencrypted contains both 0x5f22(track2) and 0x5f21(track1) tags
	iii. KSN (e1, dfae03)
	iv. Masked PAN (e1, dfae22)
"""
MSRData_SRED = [
	TagComparitor(
		[0xE1, 0x48], [0xE1, 0xDFAE02],
		[0xE1, 0xDFAE03], [0xE1, 0xDFAE22]),
	TagComparitor(
		[0xE1, 0xDFAE02], [0xE1, 0xDFAE03],
		[0xE1, 0xDFAE04], [0xE1, 0xDFAE05])#PIN
	]

"""
#TODO Dont depend on this tag comparitor
"""
EMVData = [TagComparitor([0xE4, 0x5A], 
									[0xE4, 0x5F20],
									[0xE4, 0x5F24])
			]

"""
#TODO Dont depend on this tag comparitor
"""
EMVData_SRED = [TagComparitor([0xE4, 0xDFAE02], 
									[0xE4, 0xDFAE03],
									[0xE4, 0x5F20],
									[0xE4, 0x5F24])
				]

