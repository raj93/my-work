

# sampleTrack1 = "%B"+4545656456+'^'+chName+'^'+3454534+serviceCode+discretionaryData+'?'
class ParseTrackData:
	chName = ""
	cardNumber = ""
	YY = ""
	MM = ""

	def parse(self,trackData):
		caretPresent = False 
		equalPresent = False

		if trackData.find('^') != -1:
			caretPresent = True

		if trackData.find('=') != -1:
			equalPresent = True

		if caretPresent:
			cardData = trackData.split('^')

			self.chName = self.formatName(cardData[1])
			self.cardNumber = self.formatPan(cardData[0])
			self.YY = cardData[2][0:2]
			self.MM = cardData[2][2:4]

		elif equalPresent:
			cardData = trackData.split('=')

			self.cardNumber = self.formatPan(cardData[0])	
			self.YY = cardData[1][0:2]
			self.MM = cardData[1][2:4]
			

	def formatPan(self,cardNumber):

		numbers = []
		for i in range(10):
			numbers.append(str(i))

		formattedPan = ''
		for c in cardNumber:
			if c in numbers:
				formattedPan += c

		return formattedPan

	def formatName(self,name):
		if name.find('/') != -1:
			nameSplit = name.split('/')
			return nameSplit[1] + ' ' + nameSplit[0]
		else:
			return name

	def testParseFunction(self):
		track1 = '%B5176521005587557^sainath          /^10091010010000000772000000?'
		self.parse(track1);

	def __repr__(self):
		return ('chName %s ,cardNumber %s expiryMonth %s expiryYear %s'%
				(self.chName,self.cardNumber,self.MM,self.YY))
