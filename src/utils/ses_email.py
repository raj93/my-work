import botocore.session
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
import boto
from flask import current_app

def send(from_addr, subject, body, to_addr_list):
	"""
	Sends Email via AWS SES. We use botocore module to fetch
	credentials using IAM roles.

	:type from_addr: str
	:param from_addr: From email address

	:type subject: str
	:param subject: subject of the email

	:type body: str
	:param body: text content of the email

	:type to_addr_list: list
	:param to_addr_list: list of receivers' email addresses
	#TODO add support to send attachments
	"""

	destination = {'ToAddresses': to_addr_list}
	msgbody = {'Subject': {'Data': subject}, 'Body': {'Text': {'Data': body}}}
	session = botocore.session.get_session()
	ses = session.get_service('ses')
	operation = ses.get_operation('SendEmail')
	endpoint = ses.get_endpoint('us-east-1')
	http_response, response_data = operation.call(
											endpoint,
											source=from_addr,
											destination=destination,
											message=msgbody
											)

# send('sysadmins@aasaanpay.com', "test botocore", "works", ["pruthvi@aasaanpay.com", "shashank@aasaanpay.com"])

def send1(subject, to, body, filename, filename1):
	'''
		Used to send while testing when there is no access to IAM roles. 
		Uses raw boto along with AWS credentials(.boto config files)

		Params:
		subject -- Email subject
		to -- Sender Email Id. Format : string 
		body -- Email body
		filename -- File path to attach
		filename1 -- Name to be used for attachment
	'''
	try:
		msg = MIMEMultipart()
		msg['Subject'] = subject
		msg['From'] = 'support@aasaanpay.com'
		msg['To'] = to
		# what a recipient sees if they don't use an email reader
		msg.preamble = 'Multipart message.\n'

		# the message body
		part = MIMEText(body)
		msg.attach(part)
		
		# the attachment
		if filename != None:
			part = MIMEApplication(open(filename, 'rb').read())
			part.add_header('Content-Disposition', 'attachment', filename=filename1)
			msg.attach(part)

		# connect to SES
		connection = boto.connect_ses()

		# and send the message
		result = connection.send_raw_email(msg.as_string()
			, source=msg['From']
			, destinations=[msg['To']])
		return result
	except Exception as e:
		current_app.logger.error(e)