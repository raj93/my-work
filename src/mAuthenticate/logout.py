from flask import request
from datetime import datetime
import simplejson as json
  
from src.mAuthenticate import common
from ext import db
from src.models import posDetails
from common import Log,logEvent
from src.utils.validate import validateEntry

class Logout:

	LOGOUT_TAGS = {
		'terminalInfo': dict,
		'payload': dict,
		}	

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp': unicode,
		}

	PAYLOAD_TAGS = {
		'clientId': int,
		'sessionToken': 'SESSION'
		}

	def __init__(self):
		self.json_data = request.get_json()		
		
	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.LOGOUT_TAGS.items()):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['payload'] and validateEntry(self.json_data['payload'][k], v) \
							for k, v in self.PAYLOAD_TAGS.items()):
			return False

		return True

	def populateEntries(self):
		self.deviceId = self.json_data['terminalInfo']['deviceId']
		self.timeStamp = common.getCurrentTimeStamp()

		self.clientId = self.json_data['payload']['clientId']
		self.sessionToken = self.json_data['payload']['sessionToken']

	def authorise(self):
		details = posDetails.query.filter_by(clientId = self.clientId).first()

		if details.bitmap & posDetails.BITMAP['LOGIN_STATUS'] != 0:
			details.bitmap ^= posDetails.BITMAP['LOGIN_STATUS']
		else:
			current_app.logger.critical('Processing logout but login status is set to false')
			return common.errorMessage('INVALID_REQUEST')

		loggedInUser = details.loggedInUser
		details.update(commit=False, lastActivity=common.getCurrentTimeStamp(), loginTime=0, loggedInUser='')

		# logging log out event.
		event = 'Logged out'
		logEvent(event,loggedInUser,details,Log.info)

		db.session.commit()

		return json.dumps({'mesg' : 'Success'})
	
	def process(self):
		if not self.isValidRequest(): return common.errorMessage('INVALID_REQUEST')
		self.populateEntries()

		isAuthorised, statusCode = common.isAuthorisedRequest(self.clientId, self.sessionToken, self.deviceId)
		if isAuthorised == False:
			return common.errorMessage(statusCode)
		else:
			self.username = statusCode
		
		#self.validateEntries() 
		return self.authorise()
		