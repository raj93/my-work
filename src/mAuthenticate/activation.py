from flask import request
import simplejson as json

from src.mAuthenticate import common
from ext import db
from src.models import posDetails, smsOTP, posUsers
from src.models import LastTransaction
from flask import current_app
from src.utils.crypto import otp_generator, hash_passwd, generateRandomPassword
from src.utils.sms import sendPosCredentialsSms
from src.utils.ses_email import send1
from src.utils.validate import validateEntry
import hashlib, string
from common import Log,logEvent


class ActivatePOS():

	ACTIVATE_TAGS = {
		'terminalInfo': dict,
		'payload': dict,
		}

	TERMINAL_TAGS = {
		'deviceId' : 'DEVICE_ID',
		'deviceModel' : 'DEVICE_MODEL',
		}

	PAYLOAD_TAGS = {
		'mobileNumber': 'MOBILE',
		'otp': 'OTP',
		'registered': bool,
		} 

	def __init__(self):
		self.json_data = request.get_json()		
		
	def isValidRequest(self):
		if self.json_data == None:
			return False

		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.ACTIVATE_TAGS.items()):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['payload'] and validateEntry(self.json_data['payload'][k], v) \
							for k, v in self.PAYLOAD_TAGS.items()):
			return False
			
		return True

	def populateEntries(self):
		self.deviceId = self.json_data['terminalInfo']['deviceId']
		self.deviceModel = self.json_data['terminalInfo']['deviceModel']
		#self.timeStamp = self.json_data['terminalInfo']['timeStamp']
		#self.appVersion = self.json_data['terminalInfo']['appVersion']
		self.timeStamp = common.getCurrentTimeStamp()

		self.mobileNumber = self.json_data['payload']['mobileNumber']
		self.otp = self.json_data['payload']['otp']
		self.registered = self.json_data['payload']['registered']
		
	def first_time_activation(self):
		self.details.setActivated()
		self.details.update(commit=False, deviceId=self.deviceId, mobileModel=self.deviceModel)

		clientId = self.details.clientId

		if not current_app.config['DEBUG'] and not current_app.config['TESTING']:
			self.details.update(commit=False, sequenceCode=1, plutusSequenceCode=1)


		#making his state in last transaction table
		lastTransactionEntry = LastTransaction.query.filter_by(clientId=clientId).first()
		if lastTransactionEntry == None:
			entry = LastTransaction(clientId, common.ERROR_IN_TRANSACTION, 0, 0)
			entry.save(commit = False)

		# sending sms to main posUser
		posUser = posUsers.query.filter_by(username=self.details.mobileNumber, clientId=self.details.clientId).first()
		if posUser == None:
			current_app.logger.critical('no posuser with main posDetails mobileNumber');
			#TODO send alert message to user saying that there is some trouble with user credentials
		else:
			passwd = 'password9'#TODO Change to dynamic password. But, think of alternative communication channel(email)
			sendTempDetails = (self.details.bitmap & posDetails.BITMAP['SKIP_OTP_VERIFICATION'] == 0) & (not current_app.config['TESTING'])
			if sendTempDetails:
				passwd = generateRandomPassword(8)

			hashedPassword, salt = hash_passwd(hashlib.sha256(passwd).hexdigest())
			posUser.update(commit=False, shouldChangePassword=True, passwd=hashedPassword, salt=salt)
			# logging First time activation event
			event = 'First Time Activation with deviceId '+ self.deviceId + ' on model '+self.deviceModel
			logEvent(event,posUser.username,self.details,Log.info)

			# making sure before sending sms db entries are updated properly
			db.session.commit()

			if sendTempDetails:
				sendPosCredentialsSms(self.details.mobileNumber, posUser.username,passwd)

			# if current_app.config['TESTING']:
			# 	return json.dumps({'mesg': 'Success', 'clientId': clientId, 'sequenceCode': self.details.sequenceCode, \
			# 		'passwd': passwd})
				
		return json.dumps({'mesg': 'Success', 'clientId': clientId, 'sequenceCode': self.details.sequenceCode})

	def reactivation(self):
		clientId = self.details.clientId

		#making his state in last transaction table
		lastTransactionEntry = LastTransaction.query.filter_by(clientId=clientId).first()
		if lastTransactionEntry == None:
			entry = LastTransaction(clientId, common.ERROR_IN_TRANSACTION, 0, 0)
			entry.save(commit=False)

		# logging Reactivation event
		event = 'Re Activation with deviceId '+ self.deviceId + ' on model '+self.deviceModel 
		logEvent(event, self.details.mobileNumber, self.details, Log.info)

		return json.dumps({'mesg' : 'Success', 'clientId': clientId, 'sequenceCode': self.details.sequenceCode})

	def authorise(self):
		otp_entry = smsOTP.query.filter_by(mobileNumber = self.mobileNumber).first()

		#Activation request before otp request.crook access(soft request)
		if otp_entry == None:
			current_app.logger.warn('Activation request before OTP Request for Mobile Number: ' + str(self.mobileNumber) + '. No entry exists in smsOTP')
			return common.errorMessage('MOBILE_NUMBER_NOTREGISTERED')
		
		# number of unsuccesfful activations exceeded
		if otp_entry.numActivationRequests >= current_app.config['ACTIVATION_REQUEST_NUMTRIES_CAP']:
			return common.errorMessage('OTP_NUMTRIES_EXCEEDED')

		# otp did not match
		if otp_entry.otp != self.otp:
			otp_entry.update(commit = False,numActivationRequests = otp_entry.numActivationRequests + 1)
			return common.errorMessage('INCORRECT_OTP')

		# activation from a unregistered mobile number
		if self.registered == False:
			subject = 'Details of Customer Interested in aasaanpay services'
			to = current_app.config['SALES'][0]
			body = ('Probable Customer Details \n' + 'mobileNumber: %s \n' + 'Contact Time: %s') % \
			(self.mobileNumber, self.timeStamp)

			# sending an email to sales team
			send1(subject, to, body, None, None)
			
			otp_entry.update(commit=False, isSuccess=True, numActivationRequests=0)
			return json.dumps({'mesg' : common.INFO_CODES['SUCCESS']})

		# from here on registered mobile number with successful otp verification
		self.details = posDetails.query.filter_by(mobileNumber=self.mobileNumber).first()
		if self.details == None:
			current_app.logger.critical('Activation request for Mobile: ' + str(self.mobileNumber) + 
				'. No entry exists in posDetails')
			return common.errorMessage('ALREADY_REGISTERED')#Misguiding attacker with success response codes

		# incase a soft request with correct credentials
		isEligible, statusCode = common.validatePOSBitmap(self.details.bitmap, posDetails.VALIDATE_BITMAP_TYPE['PRE_ACTIVATION'])
		if not isEligible: return common.errorMessage(statusCode)

		# handling reactivation request
		if self.details.isActivated():
			if self.deviceId == self.details.deviceId:
				if otp_entry.isSuccess:
					current_app.logger.warn('Re-Activation request before OTP Request for Mobile Number: ' + str(self.mobileNumber))
					return common.errorMessage('INVALID_REQUEST')

				otp_entry.update(commit=False, isSuccess=True, numTries=0)
				return self.reactivation()
			else:
				current_app.logger.warn('Re-Activation request for a POS on a different mobile for Mobile Number: ' + str(self.mobileNumber))
				return common.errorMessage('ALREADY_REGISTERED')

		# first time activation
		otp_entry.update(commit=False, isSuccess=True, numTries=0)
		return self.first_time_activation()
		
	def process(self):
		if not self.isValidRequest(): return common.errorMessage('INVALID_REQUEST')
		self.populateEntries()

		response = self.authorise()
		db.session.commit()

		return response
