from flask import request, current_app
from datetime import datetime
#from distutils.version import StrictVersion
import simplejson as json
    
from src.mAuthenticate import common
from ext import db
from src.models import posDetails, posUsers
from src.utils.crypto import verify_passwd
from src.utils.validate import validateEntry
from common import Log, logEvent

class Login:

	LOGIN_TAGS = {
		'terminalInfo': dict,
		'payload': dict,
		}

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp': unicode,
		#'appVersion':unicode,
		}

	PAYLOAD_TAGS = {
		'clientId': int,
		'username': 'USER',
		'passwd': 'PASSWORD',
		}

	def __init__(self):
		self.json_data = request.get_json()		
		
	def isValidRequest(self):
		if self.json_data == None:
			return False

		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.LOGIN_TAGS.items()):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['payload'] and validateEntry(self.json_data['payload'][k], v) \
							for k, v in self.PAYLOAD_TAGS.items()):
			return False

		return True

	def populateEntries(self):
		self.deviceId = self.json_data['terminalInfo']['deviceId']
		#Note: Timestamp has to be initialised after generation of sessionToken as it is used for loginTime
		#self.appVersion = self.json_data['terminalInfo']['appVersion']

		self.clientId = self.json_data['payload']['clientId']
		self.username = self.json_data['payload']['username']
		self.passwd = self.json_data['payload']['passwd']#input is SHA-256 hash of plain password

	def authorise(self):
		details = posDetails.query.filter_by(clientId = self.clientId).first()
		if details == None: return common.errorMessage('INVALID_CREDENTIALS')
		posUser = posUsers.query.filter_by(username = self.username, clientId = self.clientId).first()
		if posUser == None:	return common.errorMessage('INVALID_CREDENTIALS')
		# To check temporary hold scenario
		if details.bitmap & posDetails.BITMAP['POS_LOCK']:
			diff = (datetime.now() - posUser.lastHoldedTime).seconds
			# Checking for temporary account hold
			if diff < current_app.config['USER_HOLD_TIME']:
				minutes = (current_app.config['USER_HOLD_TIME'] - diff)/60
				return json.dumps({'errorCode':'Account locked. Try after '+ str(minutes) + ' minutes'})
			else:
				# release the lock
				details.update(commit=False, bitmap=details.bitmap ^ posDetails.BITMAP['POS_LOCK'])
				db.session.commit()

		# checking bits
		isEligible, statusCode = common.validatePOSBitmap(details.bitmap,posDetails.VALIDATE_BITMAP_TYPE['PRE_LOGIN'])
		if not isEligible:
			return common.errorMessage(statusCode)

		if details.deviceId != self.deviceId:
			event = 'Login request for Client Id: ' + str(self.clientId) + ' from unauthorised deviceId: ' + str(self.deviceId)
			current_app.logger.warn(event, extra={'tags':{'clientId':self.clientId}})
			return common.errorMessage('INVALID_CREDENTIALS')
		
		if not verify_passwd(self.passwd, posUser.passwd, posUser.salt):
			posUser.update(commit=False, invalidAttempts=posUser.invalidAttempts + 1)
			db.session.commit()
			
			if posUser.invalidAttempts == current_app.config['INCORRECT_PASSWORD_ATTEMPTS']:
				posUser.update(commit=False, lastHoldedTime=common.getCurrentTimeStamp(), invalidAttempts=0)
				details.update(commit=False, bitmap = details.bitmap | posDetails.BITMAP['POS_LOCK']) # setting the lock
				db.session.commit()
				return json.dumps({'errorCode':'Account locked. Try after '+ str(current_app.config['USER_HOLD_TIME']/60) + ' minutes'})
			return common.errorMessage('INVALID_CREDENTIALS')
			

		# handling should change password for user.
		if posUser.shouldChangePassword:
			randomNumber = common.generateRandomNumber()
			posUser.update(commit = False, rn=randomNumber, invalidAttempts=0)

			# logging shuold change password event.
			event = 'Logged in with temporary credentials'
			logEvent(event, self.username, details, Log.info)

			sign = current_app.signer.sign(randomNumber)#TODO: remove rn from sign(rn.sign); we can remove rn entirely

			db.session.commit()
			
			return json.dumps({'mesg' : 'Success', 'sessionKey' : sign,'shouldChangePassword':True})

		#TODO #CRITICAL Store atleast the current app version for quick debugging
		'''
		#disabling as of now
		# Checking the app version on login -> checking it only during activation and login. #TODO do we need during each transaction? my view:not required
		if StrictVersion(status.appVersion) > StrictVersion(self.appVersion):
			return common.errorMessage('UPDATE_LATEST_VERSION')
				
		if StrictVersion(status.appVersion) < StrictVersion(self.appVersion):
				status.appVersion = self.appVersion
				newVersion = appVersion(self.clientId, self.appVersion)
				session.add(newVersion)
		'''
		sign = current_app.signer.sign(self.username)
		key, signTime = current_app.signer.unsign(sign, return_timestamp = True)
		'''
			Note this has to be after signing session token. Otherwise it would mess up the 
			checks in common.isAUthorised()
		'''
		self.timeStamp = signTime

		posUser.update(commit=False, invalidAttempts=0)
		bitmap = details.bitmap | posDetails.BITMAP['LOGIN_STATUS']
		details.update(commit=False, loggedInUser=self.username, bitmap=bitmap, lastActivity=self.timeStamp, loginTime=self.timeStamp)
		if details.getTransactionStatus():
			current_app.logger.warn('Transaction Status = True during login for clientId: ' + str(self.clientId) + ' user: ' + self.username)
		details.setTransactionStatus(False)#TODO TRANSACTIONSTATUS

		# logging shuold change password event.
		event = 'Logged in'
		logEvent(event, self.username, details, Log.info)

		db.session.commit()

		return json.dumps({'mesg' : 'Success', 'sessionKey' : sign})
	
	def process(self):
		if not self.isValidRequest(): return common.errorMessage('INVALID_REQUEST')
		self.populateEntries()
		return self.authorise()
