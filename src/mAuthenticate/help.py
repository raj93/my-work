from flask import request, current_app
import simplejson as json
from src.mAuthenticate import common  
from ext import db
from src.models import posDetails, mSupport
from src.utils.sms import sendSupportSms
from src.utils.validate import validateEntry
import requests


class Help:
	
	HELP_TAGS = {
		'terminalInfo': dict,
		'payload': dict,
		}	

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp': unicode,
		}

	PAYLOAD_TAGS = {
		'clientId': int,
		'query': 'QUERY',
		'contact': 'MOBILE',
		}

	def __init__(self):
		self.json_data = request.get_json()		
		
	def isValidRequest(self):
		if self.json_data == None:
			return False

		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.HELP_TAGS.items()):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['payload'] and validateEntry(self.json_data['payload'][k], v) \
							for k, v in self.PAYLOAD_TAGS.items()):
			return False
			
				
		return True

	def populateEntries(self):

		self.deviceId = self.json_data['terminalInfo']['deviceId']
		self.timeStamp = common.getCurrentTimeStamp()

		self.clientId = self.json_data['payload']['clientId']
		self.query = self.json_data['payload']['query']
		self.contact = self.json_data['payload']['contact']

	def process(self):
		if not self.isValidRequest(): return common.errorMessage('INVALID_REQUEST')

		self.populateEntries()

		details = posDetails.query.filter_by(clientId = self.clientId).first()
	
		if details == None: return common.errorMessage('INVALID_CREDENTIALS')
		if details.isActivated() == False: return common.errorMessage('POS_ACTIVATION')#TODO Preventing user from posting before it is activated.  But what if user wants help prior to activation
		if details.deviceId != self.deviceId: return common.errorMessage('INVALID_CREDENTIALS')
		if details.getTransactionStatus():
			current_app.logger.warn('Suspicious activity. Help request logged while some transacton activity is going on\n'+ 
					'Client ID:' + str(self.clientId), extra={'tags':{'clientId':self.clientId}})

		if not current_app.config['TESTING']:
			try:
				# freshdesk api key -> rLT5UM2kZNqqfpUvrOd

				helpDeskTicket ={
				  'helpdesk_ticket':{
					  'description': 'Contact number '+ self.contact + '\n' + self.query,
					  'subject' : 'Terminal Help',
					  'email' : details.merchantDetails.email,
					  'priority' : 1,
					  'status' : 2
				  },
				  'cc_emails':''
				}

				helpDeskJson = json.dumps(helpDeskTicket)

				# send it to help desk
				headers = {'content-type': 'application/json'}
				url = 'http://aasaanpay.freshdesk.com/helpdesk/tickets.json'
				r = requests.post(url,data = helpDeskJson,headers = headers, auth=(current_app.config['FRESH_DESK_API_KEY'],'fdsa'))
				response = r.json
				if r.status_code == 200:
					response = r.json()
					ticketId = response['helpdesk_ticket']['display_id']

					support = mSupport(self.clientId, self.timeStamp, self.contact, self.query, ticketId)
					support.save(commit = False)
					db.session.commit()

			except Exception as e:
				current_app.logger.error(str(e))
				return common.errorMessage('SERVER_ERROR')


		# send sms to the customer
		self.mobile = details.mobileNumber
		if current_app.config['TESTING'] or current_app.config['DEBUG']:#Overriding values during testing
			current_app.logger.debug(' SMS:' + current_app.config['SETTELEMENT_SMS'])
			self.mobile = current_app.config['SETTELEMENT_SMS']
		
		if not current_app.config['TESTING']:
			result = sendSupportSms(self.contact, self.mobile)

		# we don't care whether result is success or failure 
		# because all we need is to log the support and we did it above.		
		return json.dumps({'mesg' : 'Success'})
