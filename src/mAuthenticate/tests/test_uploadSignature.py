#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from src.transactions.tests.test_purchase import purchase
from src.transactions.tests.test_void import void
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login
from src.models import Transaction, LastTransaction, Settlement, Void, Tip, pendingTip
import simplejson as json
from datetime import datetime 
from src.mAuthenticate.common import errorMessageD
import os
from cStringIO import StringIO
from src.mAuthenticate.upload_signature import Upload

def uploadSignature(client, deviceId, clientId, sessionToken, transId, transType, signaturePath = 'mobileAPISimulator/sign.jpg'):

	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")

	terminalInfo = {'deviceId' : deviceId, 'timeStamp' : timeStamp}
	payload = {'transId' : transId, 'transType' : transType, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
	upload_data = {'terminalInfo' : terminalInfo, 'payload' : payload}

	return client.post(url_for('mServices.signature'),
						data = {'json' : (StringIO(json.dumps(upload_data)), 'json'),
							'signature' : (StringIO(open(signaturePath).read()), 'signature.' + signaturePath[-3:])}
						).json	

class testUploadSignature(KitTestCase):
	testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
	PURCHASE = Upload.PURCHASE
	VOID = Upload.VOID
	
	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)

		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP, posLogs, deviceIdCount
		from ext import db
		
		self.user.delete()
		deviceIdCount.query.delete()
		posLogs.query.delete()
		Tip.query.delete()
		pendingTip.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		self.details.delete()
		self.payment.delete()
		Void.query.delete()
		Transaction.query.delete()
		Settlement.query.delete()
		self.details.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		db.get_engine(current_app, bind='settled').dispose()
		
	def test1(self):
		'''
			Invalid request. Each request misses one of the elements (except one)
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''
		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		sessionToken = current_app.signer.sign('sessionToken')
		clientId = self.details.clientId
		transId = getTransID()
		transType = 0; #PURCHASE = 0; VOID = 1
		file('a.txt', 'w').close()
		#Valid request
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'transId' : transId, 'transType' : transType, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		upload_data = {'terminalInfo' : terminalInfo, 'payload' : payload}
		
		response_data = self.client.post(url_for('mServices.signature'),
						data = {'json' : (StringIO(json.dumps(upload_data)), 'json'),
							'signature' : (StringIO(open('a.txt').read()), 'signature')}
						).json
		self.assertNotEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing deviceId
		terminalInfo = {'timeStamp' : timeStamp}
		payload = {'transId' : transId, 'transType' : transType, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		upload_data = {'terminalInfo' : terminalInfo, 'payload' : payload}
		
		response_data = self.client.post(url_for('mServices.signature'),
						data = {'json' : (StringIO(json.dumps(upload_data)), 'json'),
							'signature' : (StringIO(open('a.txt').read()), 'signature')}
						).json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing transId
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'transType' : transType, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		upload_data = {'terminalInfo' : terminalInfo, 'payload' : payload}
		
		response_data = self.client.post(url_for('mServices.signature'),
						data = {'json' : (StringIO(json.dumps(upload_data)), 'json'),
							'signature' : (StringIO(open('a.txt').read()), 'signature')}
						).json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing transType
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'transId' : transId, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		upload_data = {'terminalInfo' : terminalInfo, 'payload' : payload}
		
		response_data = self.client.post(url_for('mServices.signature'),
						data = {'json' : (StringIO(json.dumps(upload_data)), 'json'),
							'signature' : (StringIO(open('a.txt').read()), 'signature')}
						).json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing clientId
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'transId' : transId, 'transType' : transType, 'sessionToken' : sessionToken}
		
		upload_data = {'terminalInfo' : terminalInfo, 'payload' : payload}
		
		response_data = self.client.post(url_for('mServices.signature'),
						data = {'json' : (StringIO(json.dumps(upload_data)), 'json'),
							'signature' : (StringIO(open('a.txt').read()), 'signature')}
						).json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing sessionToken
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'transId' : transId, 'transType' : transType, 'clientId' : clientId}
		
		upload_data = {'terminalInfo' : terminalInfo, 'payload' : payload}
		
		response_data = self.client.post(url_for('mServices.signature'),
						data = {'json' : (StringIO(json.dumps(upload_data)), 'json'),
							'signature' : (StringIO(open('a.txt').read()), 'signature')}
						).json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing terminalInfo
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'transId' : transId, 'transType' : transType, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		upload_data = {'payload' : payload}
		
		response_data = self.client.post(url_for('mServices.signature'),
						data = {'json' : (StringIO(json.dumps(upload_data)), 'json'),
							'signature' : (StringIO(open('a.txt').read()), 'signature')}
						).json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing payload
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'transId' : transId, 'transType' : transType, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		upload_data = {'terminalInfo' : terminalInfo}
		
		response_data = self.client.post(url_for('mServices.signature'),
						data = {'json' : (StringIO(json.dumps(upload_data)), 'json'),
							'signature' : (StringIO(open('a.txt').read()), 'signature')}
						).json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Missing json altogether
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'transId' : transId, 'transType' : transType, 'clientId' : clientId, 'sessionToken' : sessionToken}
		
		upload_data = {'terminalInfo' : terminalInfo, 'payload' : payload}
		
		response_data = self.client.post(url_for('mServices.signature'),
						data = {'signature' : (StringIO(open('a.txt').read()), 'signature')}
						).json
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))		

		#No signature file attached
		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'transId' : transId, 'transType' : self.PURCHASE, 'clientId' : self.details.clientId, 'sessionToken' : sessionToken}
		
		upload_data = {'terminalInfo' : terminalInfo, 'payload' : payload}
		
		response_data = self.client.post(url_for('mServices.signature'),
						data = {'json' : (StringIO(json.dumps(upload_data)), 'json')}
						).json	
		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		os.remove('a.txt')

	def test2(self):
		'''
			Tests two cases
			i. TransId = TransId of a disabled purchase
			ii. TransId which is not present
			iii. Invalid tranType
			Expected Output: {'errorCode' : INVALID_TRANSACTION}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])

		self.user.update(shouldChangePassword = False)
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		deviceType = 0
		conflict = False
		amount = '100'
		# response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		purchase_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)
		
		p = Transaction.query.first()
		
		#Invalid transType (giving Void for purchase)
		response_data = uploadSignature(self.client, self.deviceId, self.details.clientId, sessionKey, p.transId, self.VOID)
		self.assertEquals(response_data, errorMessageD('INVALID_TRANSACTION'))
		
		#Disabled purchase
		p.setEnabled(False)
		p.update()

		response_data = uploadSignature(self.client, self.deviceId, self.details.clientId, sessionKey, p.transId, self.PURCHASE)
		self.assertEquals(response_data, errorMessageD('INVALID_TRANSACTION'))

		#Settings transId = p.transId --> Unknown transId to the db

		response_data = uploadSignature(self.client, self.deviceId, self.details.clientId, sessionKey, getTransID(), self.PURCHASE)
		self.assertEquals(response_data, errorMessageD('INVALID_TRANSACTION'))


		p.setEnabled(True)
		p.update()
		#Repeating for void transaction
		transId = purchase_data['transactionInfo']['transId']
		response_data = void(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, transId)
		assert 'errorCode' not in response_data
		v = Void.query.first()
	
		#Disabled void
		v.setEnabled(False)
		v.update()

		response_data = uploadSignature(self.client, self.deviceId, self.details.clientId, sessionKey, v.transId, self.VOID)
		self.assertEquals(response_data, errorMessageD('INVALID_TRANSACTION'))

		#Settings transId = p.transId --> Unknown transId to the db
		response_data = uploadSignature(self.client, self.deviceId, self.details.clientId, sessionKey, getTransID(), self.VOID)
		self.assertEquals(response_data, errorMessageD('INVALID_TRANSACTION'))


	def test3(self):
		'''
			Valid upload signature for purchase
		'''	

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])

		self.user.update(shouldChangePassword = False)
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		deviceType = 0
		conflict = False
		amount = '100'
		# response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)
		
		transId = response_data['transactionInfo']['transId']
		response_data = uploadSignature(self.client, self.deviceId, self.details.clientId, sessionKey, transId, self.PURCHASE)
		assert 'mesg' in response_data
		
		p = Transaction.query.first()
		assert p.receiptId != ''

	def test4(self):
		'''
			Valid upload signature for void(jpg format)
		'''	

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])

		self.user.update(shouldChangePassword = False)
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		deviceType = 0
		conflict = False
		amount = '100'
		#response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)
		assert 'errorCode' not in response_data
		transId = response_data['transactionInfo']['transId']

		response_data = void(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, transId)
		
		transId = response_data['transactionInfo']['transId']
		response_data = uploadSignature(self.client, self.deviceId, self.details.clientId, sessionKey, transId, self.VOID)
		assert 'mesg' in response_data
		
		v = Void.query.first()
		assert v.receiptId != ''
	
	def test5(self):
		'''
			Invalid signature input
			i. Invalid file format
			ii. No signature file attached
			Expected Output: {'errorCode' : INVALID_FILE}
		'''

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])

		self.user.update(shouldChangePassword = False)
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		sessionKey = response_data['sessionKey']
		
		testCard = {'track1' : '%B5176521005587557^sainath          /^10091010010000000772000000?',\
			'track2' : ';5176521005587557=10091010017720?'}
		deviceType = 0
		conflict = False
		amount = '100'
		#response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)#Dummy purchase to set sequence code in alignment with pinelabs one
		response_data = purchase(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, self.details.sequenceCode, deviceType, conflict, testCard, amount)
		
		transId = response_data['transactionInfo']['transId']
		signaturePath = 'mobileAPISimulator/sign.jpg'
		import shutil
		shutil.copyfile(signaturePath, 'sign.ran')

		#Invalid file format
		response_data = uploadSignature(self.client, self.deviceId, self.details.clientId, sessionKey, transId, self.PURCHASE, 'sign.ran')
		self.assertEquals(response_data, errorMessageD('INVALID_FILE'))
		os.remove('sign.ran')

		self.assertEquals(response_data, errorMessageD('INVALID_FILE'))
