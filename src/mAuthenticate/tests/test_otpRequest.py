#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for
from testing import *
from src.utils.crypto import otp_generator
import simplejson as json
from datetime import datetime
from flask import current_app
from src.mAuthenticate.common import errorMessageD
from src.models import posLogs
		
def otpRequest(client, mobileNumber, deviceId, appVersion, registered, email = ''):
	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
	terminalInfo = {'deviceId': deviceId,'timeStamp': timeStamp,'appVersion': appVersion}       
	payload = {'mobileNumber': mobileNumber,"email": email,"registered": registered}
	verification_data = {"terminalInfo": terminalInfo,"payload": payload}
	
	response = client.post(url_for('mServices.otpRequest'),
					data = json.dumps(verification_data),
					content_type = 'application/json')

	response_data = response.json
	return response_data
	

class testOTPRequest(KitTestCase):
	
	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details,self.payment = getNewPOS(self.merchant)
		self.deviceId = getDeviceId()

	def tearDown(self):
		from src.models import deviceIdCount
		deviceIdCount.query.delete()
		posLogs.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		from src.models import smsOTP
		smsOTP.query.delete()
		from ext import db
		db.session.commit()
		db.engine.dispose()
	
	def testOTP1(self):
		'''
		Mobile Number is registered but sends Registered = False
		Expected value : {'mesg' : 'Success'} -> Kept it for privacy purposes
		'''
		registered = False
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, registered)
		self.assertEquals(response_data, dict(mesg = 'Success'))
	
	def testOTP2(self):
		'''
		Mobile Number is registered. Sends Registered = True (Fresh OTP Request)
		Expected value : {'mesg' : VALUE_OTP}
		'''

		registered = True
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, registered)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
	
	def testOTP3(self):
		'''
		Mobile Number is not registered. Sending registered = False
		Expected value: {'mesg' : VALUE_OTP}
		'''
		mobile = otp_generator(10, string.digits)
		registered = False
		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, registered)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

	def testOTP4(self):
		'''
		Mobile Number is not registered. Sending registered = True
		Expected value: {'errorCode' : errorMessage('MOBILE_NUMBER_NOTREGISTERED')}
		'''
		mobile = otp_generator(10, string.digits)
		registered = True
		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, registered)
		assert errorMessageD('MOBILE_NUMBER_NOTREGISTERED') == response_data
	
	def testOTP5(self):
		'''
		Mobile Number is registered. Fires a request, waits for TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST seconds and fires another request
		Expected values: 
		i. {'mesg' : VALUE_OTP}
		ii. {'mesg' : VALUE_OTP}
		'''

		registered = True
		
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, registered)

		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
		
		wait = current_app.config['TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST']
		import time
		time.sleep(wait)
		
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, registered)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
	
	def testOTP6(self):
		'''
		Mobile Number is not registered. Fires a request, waits for TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST seconds and fires another request
		Expected values:
		i. {'mesg' : VALUE_OTP}
		ii. {'mesg' : VALUE_OTP}
		'''

		registered = False
		mobile = otp_generator(10, string.digits)
		
		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, registered)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
		
		wait = current_app.config['TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST']
		import time
		time.sleep(wait)
		
		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, registered)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
	
	def testOTP7(self):
		'''
		Mobile Number is registered. Firing OTP Requests without time gap. It then waits for required time retrieved from the response and fires another OTP request 
		i. OTP request -> response
		ii. OTP request -> response, wait_time
		iii. Wait for wait_time
		iv. OTP_request
		Expected Values:
		i. {'mesg' : VALUE_OTP}
		ii. {'mesg' : 'Success', wait: WAIT_TIME}
		iii. {'mesg' : VALUE_OTP}
		'''
		registered = True

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, registered)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, registered)
		assert response_data['mesg'] == 'Success' and 'wait' in response_data
		
		wait = response_data['wait']
		import time
		time.sleep(wait + 1)
		
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, registered)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
	
	def testOTP8(self):
		'''
		Mobile Number is not registered. Firing OTP Requests without time gap. It then waits for required time retrieved from the response and fires another OTP request 
		i. OTP request -> response
		ii. OTP request -> response, wait_time
		iii. Wait for wait_time
		iv. OTP_request
		Expected Values:
		i. {'mesg' : VALUE_OTP}
		ii. {'mesg' : 'Success', wait: WAIT_TIME}
		iii. {'mesg' : VALUE_OTP}
		'''
		registered = False

		mobile = otp_generator(10, string.digits)
		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, registered)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, registered)
		assert response_data['mesg'] == 'Success' and 'wait' in response_data
		
		wait = response_data['wait']
		import time
		time.sleep(wait + 1)
		
		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, registered)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
	
	def testOTP9(self):
		'''
		Mobile Number is registered. One request with registered = True, second with registered = False
		Expected Values:
		i. {'mesg' : OTP_VALUE}
		ii. {'mesg' : 'Success'}
		'''

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, False)
		self.assertEquals(response_data, dict(mesg = 'Success'))
	
	def testOTP10(self):
		'''
		Mobile Number is registered. One request with registered = False, second with registered = True
		Expected Values:
		i. {'mesg' : 'Success'}
		ii. {'mesg' : OTP_VALUE}
		'''

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, False)
		self.assertEquals(response_data, dict(mesg = 'Success'))

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
	
	def testOTP11(self):
		'''
		Mobile Number is not registered. One request with registered = True, second with registered = False
		Expected Values:
		i. {'errorCode' : errorMessage('MOBILE_NUMBER_NOTREGISTERED')}
		ii. {'mesg' : OTP_VALUE}
		'''

		mobile = otp_generator(10, string.digits)
		
		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, True)
		from src.mAuthenticate.common import errorMessageD
		assert errorMessageD('MOBILE_NUMBER_NOTREGISTERED') == response_data
		
		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, False)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
	
	def testOTP12(self):
		'''
		Mobile Number is not registered. One request with registered = False, second with registered = True
		Expected Values:
		i. {'mesg' : OTP_VALUE}
		ii. {'errorCode' : errorMessage('MOBILE_NUMBER_NOTREGISTERED')}
		'''

		mobile = otp_generator(10, string.digits)
		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, False)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, True)
		from src.mAuthenticate.common import errorMessageD
		assert errorMessageD('MOBILE_NUMBER_NOTREGISTERED') == response_data
	
	def testOTP13(self):
		'''
		Mobile Number is registered. 
		Inputs: Registered = True
		DeviceIds are different for the requests
		Expected Values:
		i. {'mesg' : OTP_VALUE}
		ii. {'mesg' : OTP_VALUE}
		'''

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

		wait = current_app.config['TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST']#Waiting as the system disallows continuous requests from the same mobile
		import time
		time.sleep(wait)

		deviceId = getDeviceId()
		response_data = otpRequest(self.client, self.details.mobileNumber, deviceId, self.appVersion, True)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
	
	def testOTP14(self):
		'''
		Mobile Number is not registered. 
		Inputs: Registered = False
		DeviceIds are different for the requests
		Expected Values:
		i. {'mesg' : OTP_VALUE}
		ii. {'mesg' : OTP_VALUE}
		'''
		mobile = otp_generator(10, string.digits)
		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, False)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

		wait = current_app.config['TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST']#Waiting as the system disallows continuous requests from the same mobile
		import time
		time.sleep(wait)

		deviceId = getDeviceId()
		response_data = otpRequest(self.client, mobile, deviceId, self.appVersion, False)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
	
	def testOTP15(self):
		'''
		Mobile Number is registered. 
		Inputs: Registered = True
		DeviceIds are different for the requests. And change smsOTP isSuccess = True after first request
		Expected Values:
		i. {'mesg' : OTP_VALUE}
		ii. {'mesg' : OTP_VALUE}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

		from ext import db
		from src.models import smsOTP
		otp_entry = db.session.query(smsOTP).filter(smsOTP.mobileNumber == self.details.mobileNumber).first()
		
		assert otp_entry.isSuccess == False
		otp_entry.isSuccess = True
		otp_entry.update()

		deviceId = getDeviceId()
		wait = current_app.config['TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST']
		import time
		time.sleep(wait)

		response_data = otpRequest(self.client, self.details.mobileNumber, deviceId, self.appVersion, True)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
	
	def testOTP16(self):
		'''
		Mobile Number is not registered. 
		Inputs: Registered = False
		DeviceIds are different for the requests. And change smsOTP isSuccess = True after first request. Waits for TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST seconds
		Expected Values:
		i. {'mesg' : OTP_VALUE}
		ii. {'mesg' : OTP_VALUE}
		'''
	
		mobile = otp_generator(10, string.digits)
	
		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, False)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

		from ext import db
		from src.models import smsOTP
		otp_entry = db.session.query(smsOTP).filter(smsOTP.mobileNumber == mobile).first()
		
		assert otp_entry.isSuccess == False
		otp_entry.isSuccess = True
		otp_entry.update()

		wait = current_app.config['TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST']
		import time
		time.sleep(wait)

		deviceId = getDeviceId()
		response_data = otpRequest(self.client, mobile, deviceId, self.appVersion, False)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

	
	def testOTP17(self):
		'''
			OTP_NUMTRIES_EXCEEDED
			Inputs: Registered = False
			Expected Output: errorMessageD('OTP_NUMTRIES_EXCEEDED')
		'''
		
		wait = current_app.config['TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST']#Waiting as the system disallows continuous requests from the same mobile
		numTries = current_app.config['OTP_REQUEST_NUMTRIES_CAP']
		import time
		time.sleep(wait)
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		
		for i in range(numTries):
			assert 'mesg' in response_data
			time.sleep(wait)
			response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
	
		self.assertEquals(response_data, errorMessageD('OTP_NUMTRIES_EXCEEDED'))

	
	def testOTP18(self):
		'''
			Invalid request. Each request misses one of the elements
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''
		
		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		registered = True

		terminalInfo = {'deviceId': self.deviceId, 'timeStamp': timeStamp, 'appVersion': self.appVersion}       
		payload = {'mobileNumber': self.details.mobileNumber, "email": self.email, "registered": registered}
		

		# Missing deviceId
		terminalInfo = {'timeStamp': timeStamp, 'appVersion': self.appVersion}       
		verification_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.otpRequest'),
						data = json.dumps(verification_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
	
		#Missing timestamp
		terminalInfo = {'deviceId': self.deviceId, 'appVersion': self.appVersion}       
		verification_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.otpRequest'),
						data = json.dumps(verification_data),
						content_type = 'application/json').json

		self.assertNotEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Missing appVersion
		terminalInfo = {'deviceId': self.deviceId, 'timeStamp': timeStamp}       
		verification_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.otpRequest'),
						data = json.dumps(verification_data),
						content_type = 'application/json').json

		self.assertNotEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Missing terminalInfo
		verification_data = {"payload": payload}
		
		response_data = self.client.post(url_for('mServices.otpRequest'),
						data = json.dumps(verification_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Reinitialising Terminal Info
		terminalInfo = {'deviceId': self.deviceId, 'timeStamp': timeStamp, 'appVersion': self.appVersion}       
		
		#Missing MobileNumber
		payload = {"email": self.email, "registered": registered}
		verification_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.otpRequest'),
						data = json.dumps(verification_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Missing Email (Email is not a mandatory field. '' can be passwd as well)
		payload = {'mobileNumber': self.details.mobileNumber, "registered": registered}
		verification_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.otpRequest'),
						data = json.dumps(verification_data),
						content_type = 'application/json').json

		self.assertNotEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing Registered
		payload = {'mobileNumber': self.details.mobileNumber, "email": self.email}
		verification_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.otpRequest'),
						data = json.dumps(verification_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Missing Payload
		verification_data = {"terminalInfo": terminalInfo}
		
		response_data = self.client.post(url_for('mServices.otpRequest'),
						data = json.dumps(verification_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
	def testOTP19(self):
		#TODO review this and remove if not required
		'''
		Mobile Number is registered. 
		Inputs: Registered = True
		DeviceIds are same for the requests. And change smsOTP isSuccess = True after first request
		Expected Values:
		i. {'mesg' : OTP_VALUE}
		ii. {'mesg' : OTP_VALUE}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'


		from ext import db
		from src.models import smsOTP
		otp_entry = db.session.query(smsOTP).filter(smsOTP.mobileNumber == self.details.mobileNumber).first()
		
		assert otp_entry.isSuccess == False
		otp_entry.isSuccess = True
		otp_entry.update()

		wait = current_app.config['TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST']
		import time
		time.sleep(wait)

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
	
	def testOTP20(self):
		'''
		Mobile Number is not registered. 
		Inputs: Registered = False
		DeviceIds are same for the requests. And change smsOTP isSuccess = True after first request
		Expected Values:
		i. {'mesg' : OTP_VALUE}
		ii. {'mesg' : TO_PROMOTION}
		'''

		mobile = otp_generator(10, string.digits)
		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, False)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'


		from ext import db
		from src.models import smsOTP
		otp_entry = db.session.query(smsOTP).filter(smsOTP.mobileNumber == mobile).first()
		
		assert otp_entry.isSuccess == False
		otp_entry.isSuccess = True
		otp_entry.update()

		response_data = otpRequest(self.client, mobile, self.deviceId, self.appVersion, False)
		from src.mAuthenticate.common import errorMessageD
		self.assertEquals(response_data, errorMessageD('TO_PROMOTION'))		
