#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for
from testing import *
import simplejson as json
from datetime import datetime
from src.mAuthenticate.common import errorMessageD
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate

def help(client, clientId, query, contact, deviceId):

	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
	
	terminalInfo = {'deviceId' : deviceId, 'timeStamp' : timeStamp}
	payload = {'clientId' : clientId, 'query' : query, 'contact' : contact}
	
	help_data = {"terminalInfo" : terminalInfo, "payload" : payload}

	response = client.post(url_for('mServices.mHelp'),
					data = json.dumps(help_data),
					content_type = 'application/json')

	return response.json
	
class testHelp(KitTestCase):
	
	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)

		self.deviceId = otp_generator(16, string.hexdigits)
		self.otp = otp_generator(8, string.digits)
		self.query = otp_generator(random.randint(25, 200), string.letters + ' ')
		self.contact = otp_generator(10, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP, mSupport
		from src.models import LastTransaction, posLogs, deviceIdCount
		from ext import db
		
		mSupport.query.delete()
		self.user.delete()
		deviceIdCount.query.delete()
		posLogs.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
		
	def test1(self):
		'''
			Invalid request. Each request misses one of the elements
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''
	
		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		clientId = self.details.clientId
		
		#Valid tags
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'clientId' : clientId, 'query' : self.query, 'contact' : self.contact}
	
		help_data = {"terminalInfo" : terminalInfo, "payload" : payload}

		response = self.client.post(url_for('mServices.mHelp'),
					data = json.dumps(help_data),
					content_type = 'application/json')

		self.assertNotEquals(response.json, errorMessageD('INVALID_REQUEST'))		

		#Missing DeviceId
		terminalInfo = {'timeStamp' : timeStamp}
		payload = {'clientId' : clientId, 'query' : self.query, 'contact' : self.contact}
	
		help_data = {"terminalInfo" : terminalInfo, "payload" : payload}

		response = self.client.post(url_for('mServices.mHelp'),
					data = json.dumps(help_data),
					content_type = 'application/json')

		self.assertEquals(response.json, errorMessageD('INVALID_REQUEST'))		

		#Missing ClientId
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'query' : self.query, 'contact' : self.contact}
	
		help_data = {"terminalInfo" : terminalInfo, "payload" : payload}

		response = self.client.post(url_for('mServices.mHelp'),
					data = json.dumps(help_data),
					content_type = 'application/json')

		self.assertEquals(response.json, errorMessageD('INVALID_REQUEST'))		

		#Missing Query
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'clientId' : clientId, 'contact' : self.contact}
	
		help_data = {"terminalInfo" : terminalInfo, "payload" : payload}

		response = self.client.post(url_for('mServices.mHelp'),
					data = json.dumps(help_data),
					content_type = 'application/json')

		self.assertEquals(response.json, errorMessageD('INVALID_REQUEST'))		

		#Missing Contact
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'clientId' : clientId, 'query' : self.query}
	
		help_data = {"terminalInfo" : terminalInfo, "payload" : payload}

		response = self.client.post(url_for('mServices.mHelp'),
					data = json.dumps(help_data),
					content_type = 'application/json')

		self.assertEquals(response.json, errorMessageD('INVALID_REQUEST'))		

		#Missing TerminalInfo
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'clientId' : clientId, 'query' : self.query, 'contact' : self.contact}
	
		help_data = {"payload" : payload}

		response = self.client.post(url_for('mServices.mHelp'),
					data = json.dumps(help_data),
					content_type = 'application/json')

		self.assertEquals(response.json, errorMessageD('INVALID_REQUEST'))		

		#Missing Payload
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'clientId' : clientId, 'query' : self.query, 'contact' : self.contact}
	
		help_data = {"terminalInfo" : terminalInfo}

		response = self.client.post(url_for('mServices.mHelp'),
					data = json.dumps(help_data),
					content_type = 'application/json')

		self.assertEquals(response.json, errorMessageD('INVALID_REQUEST'))		

	def test2(self):
		'''
			Invalid clientId
			Expected Output: {'errorCode' : 'INVALID_CREDENTIALS'}
		'''
		response_data = help(self.client, self.details.clientId + 1, self.query, self.contact, self.deviceId)
		self.assertEquals(response_data, errorMessageD('INVALID_CREDENTIALS'))

	def test3(self):
		'''
			Inactive POS
			Expected Output: {'errorCode' : 'POS_ACTIVATION'}
		'''
		response_data = help(self.client, self.details.clientId, self.query, self.contact, self.deviceId)
		self.assertEquals(response_data, errorMessageD('POS_ACTIVATION'))

	def test4(self):
		'''
			Active POS but Invalid deviceId
			Expected Output: {'errorCode' : 'INVALID_CREDENTIALS'}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		
		deviceId = getDeviceId()
		while deviceId == self.deviceId:# Just in case new deviceId is equal to it #paranoid
			deviceId = getDeviceId()

		response_data = help(self.client, self.details.clientId, self.query, self.contact, deviceId)
		self.assertEquals(response_data, errorMessageD('INVALID_CREDENTIALS'))	
	
	def test5(self):
		'''
			Valid inputs
			Expected Output: {'mesg' : 'Success'}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		
		response_data = help(self.client, self.details.clientId, self.query, self.contact, self.deviceId)
		self.assertEquals(response_data, dict(mesg = 'Success'))
	