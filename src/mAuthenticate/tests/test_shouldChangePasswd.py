#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator, otp_generator1
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login
import simplejson as json

from datetime import datetime
from src.mAuthenticate.common import errorMessageD
import hashlib

def shouldChangePasswd(client, deviceId, username, clientId, sessionToken, oldPasswd, newPasswd):

	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
	oldPasswd = hashlib.sha256(oldPasswd).hexdigest()
	terminalInfo = {'deviceId' : deviceId, 'timeStamp' : timeStamp}
	payload = {'clientId' : clientId, 'username' : username, 'sessionToken' : sessionToken, 'oldPasswd' : oldPasswd, 'newPasswd' : newPasswd}
	
	request = {"terminalInfo" : terminalInfo, "payload" : payload}

	response = client.post(url_for('mServices.shouldChangePassword'),
					data = json.dumps(request),
					content_type = 'application/json')

	response_data = response.json
	return response_data

class testShouldChangePasswd(KitTestCase):
	'''
		Skipping traditional checks as they're already taken care in isAuthorised() and all the calls to this function calls isAuthorised()
	'''
	
	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)		
		
		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP
		from src.models import LastTransaction, posLogs, deviceIdCount
		from ext import db
		
		self.user.delete()
		deviceIdCount.query.delete()
		posLogs.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
	
	def test1(self):
		'''
			Invalid request. Each request misses one of the elements
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''
	
		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		sessionToken = current_app.signer.sign('sessionToken')
		newPasswd = getNewPassword()
		clientId = self.details.clientId
		username = self.details.mobileNumber

		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'clientId' : clientId,'username' : username, 'sessionToken' : sessionToken, 'oldPasswd' : self.passwd, 'newPasswd' : newPasswd}
		request = {"terminalInfo" : terminalInfo, "payload" : payload}

		# Missing deviceId
		terminalInfo = {'timeStamp' : timeStamp}
		payload = {'clientId' : clientId,'username' : username, 'sessionToken' : sessionToken, 'oldPasswd' : self.passwd, 'newPasswd' : newPasswd}
				
		request = {'terminalInfo' : terminalInfo, 'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing terminal Info

		request = {'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Reinitialising terminal Info
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}

		
		# Missing clientId
		payload = {'username' : username,'sessionToken' : sessionToken, 'oldPasswd' : self.passwd, 'newPasswd' : newPasswd}
		request = {'terminalInfo' : terminalInfo, 'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing username
		payload = {'clientId' : clientId, 'sessionToken' : sessionToken, 'oldPasswd' : self.passwd, 'newPasswd' : newPasswd}
		request = {'terminalInfo' : terminalInfo, 'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing SessionToken
		payload = {'clientId' : clientId,'username' : username, 'oldPasswd' : self.passwd, 'newPasswd' : newPasswd}
		request = {'terminalInfo' : terminalInfo, 'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing Old Password

		payload = {'clientId' : clientId, 'username' : username, 'sessionToken' : sessionToken, 'newPasswd' : newPasswd}
		request = {'terminalInfo' : terminalInfo, 'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))


		# Missing new Password
		payload = {'clientId' : clientId, 'username' : username, 'sessionToken' : sessionToken, 'oldPasswd' : self.passwd}
		request = {'terminalInfo' : terminalInfo, 'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing Payload
		request = {'terminalInfo' : terminalInfo}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
	
	def test2(self):
		'''
			Invalid username
			Expected Output: {'errorCode' : 'INVALID_CREDENTIALS'}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		assert response_data["shouldChangePassword"] == True and 'sessionKey' in response_data

		sessionKey = response_data['sessionKey']
		print sessionKey
		newPasswd = getNewPassword()
		invalidUserName = getUserName()
	
		response_data = shouldChangePasswd(self.client, self.deviceId, invalidUserName,self.details.clientId, sessionKey,self.passwd, newPasswd)
		self.assertEquals(response_data, errorMessageD('INVALID_CREDENTIALS'))

	def test3(self):
		'''
			Valid inputs
			Expected Output: {'mesg', 'Success'}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])

		# first time login to get the session token for change password
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		assert response_data["shouldChangePassword"] == True and 'sessionKey' in response_data

		# change new password
		newPassword = getNewPassword()
		response_data = shouldChangePasswd(self.client,self.deviceId,self.user.username,self.details.clientId,response_data["sessionKey"],self.passwd,newPassword)
		assert response_data['mesg'] == 'Success' and 'sessionKey' in response_data	
		assert self.user.shouldChangePassword == False

	def test4(self):
		'''
			New Password should not be same as old Password
			Note that since we are using 'password' here which is 
			Expected Output: {'errorCode' : 'NEW_PASSWORD_IS_OLD_PASSWORD'}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		
		# first time login to get the session token for change password
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		assert response_data["shouldChangePassword"] == True and 'sessionKey' in response_data

		# change new password
		response_data = shouldChangePasswd(self.client,self.deviceId,self.user.username,self.details.clientId,response_data["sessionKey"],self.passwd, self.passwd)
		self.assertEquals(response_data, errorMessageD('NEW_PASSWORD_IS_OLD_PASSWORD'))