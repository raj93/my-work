#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login
from src.mAuthenticate.tests.test_shouldChangePasswd import shouldChangePasswd
import simplejson as json
from datetime import datetime
from src.mAuthenticate.common import errorMessageD

def logout(client, deviceId, appVersion, clientId, sessionToken):

	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
	
	terminalInfo = {'deviceId' : deviceId, 'timeStamp' : timeStamp}
	payload = {'clientId' : clientId, 'sessionToken' : sessionToken}
	
	logout_data = {"terminalInfo" : terminalInfo, "payload" : payload}

	response = client.post(url_for('mServices.mLogout'),
					data = json.dumps(logout_data),
					content_type = 'application/json')

	response_data = response.json
	return response_data
	
class testLogout(KitTestCase):
	
	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)

		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):
		from src.models import smsOTP
		from src.models import LastTransaction, posLogs, deviceIdCount
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()
		posLogs.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
	
	def test1(self):
		'''
			Invalid request. Each request misses one of the elements
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''
	
		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		sessionToken = current_app.signer.sign('sessionToken')
		clientId = self.details.clientId

		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'clientId' : clientId, 'sessionToken' : sessionToken}
	
		# Missing deviceId
		terminalInfo = {'timeStamp' : timeStamp}
		logout_data = {"terminalInfo" : terminalInfo, "payload" : payload}
		
		response_data = self.client.post(url_for('mServices.mLogout'),
						data = json.dumps(logout_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
	
		# Missing terminalInfo
		logout_data = {"payload": payload}
		
		response_data = self.client.post(url_for('mServices.mLogout'),
						data = json.dumps(logout_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		# Reinitialising terminal Info
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		
		# Missing ClientId
		payload = {'sessionToken' : sessionToken}
		logout_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.mLogout'),
						data = json.dumps(logout_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		# Missing SessionToken
		payload = {'clientId' : clientId}
		logout_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.mLogout'),
						data = json.dumps(logout_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing payload
		logout_data = {"terminalInfo": terminalInfo}
		
		response_data = self.client.post(url_for('mServices.mLogout'),
						data = json.dumps(logout_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
	
	def test2(self):
		'''
			Valid inputs
			Expected Output: {'mesg', 'Success'}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		newPassword = getNewPassword()
		response_data = shouldChangePasswd(self.client,self.deviceId,self.user.username,self.details.clientId,response_data["sessionKey"],self.passwd,newPassword)
		self.passwd = newPassword # changing the variable to new password
		sessionKey = response_data['sessionKey']
		
		response_data = logout(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey)
		self.assertEquals(response_data, dict(mesg = 'Success'))

		