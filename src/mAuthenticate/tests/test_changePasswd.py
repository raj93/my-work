#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for
from testing import *
from src.utils.crypto import otp_generator, otp_generator1
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_login import login
from src.mAuthenticate.tests.test_shouldChangePasswd import shouldChangePasswd
import simplejson as json

from datetime import datetime
from src.mAuthenticate.common import errorMessageD
import hashlib

def changePasswd(client, deviceId, appVersion, clientId, sessionToken, oldPasswd, newPasswd):

	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
	oldPasswd = hashlib.sha256(oldPasswd).hexdigest()
	newPasswd = newPasswd
	
	terminalInfo = {'deviceId' : deviceId, 'timeStamp' : timeStamp}
	payload = {'clientId' : clientId, 'sessionToken' : sessionToken, 'oldPasswd' : oldPasswd, 'newPasswd' : newPasswd}
	
	request = {"terminalInfo" : terminalInfo, "payload" : payload}

	response = client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json')

	response_data = response.json
	return response_data
	
class testChangePasswd(KitTestCase):
	'''
		Skipping traditional checks as they're already taken care in isAuthorised() and all the calls to this function calls isAuthorised()
	'''
	
	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)
				
		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()


	def tearDown(self):
		from src.models import smsOTP
		from src.models import LastTransaction, posLogs, deviceIdCount
		from ext import db
		
		self.user.delete()
		deviceIdCount.query.delete()
		posLogs.query.delete()
		LastTransaction.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
	
	def test1(self):
		'''
			Invalid request. Each request misses one of the elements
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''
	
		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		sessionToken = 'sessionToken'
		newPasswd = getNewPassword()
		clientId = self.details.clientId

		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}
		payload = {'clientId' : clientId, 'sessionToken' : sessionToken, 'oldPasswd' : self.passwd, 'newPasswd' : newPasswd}
		request = {"terminalInfo" : terminalInfo, "payload" : payload}

		# Missing deviceId
		terminalInfo = {'timeStamp' : timeStamp}
		payload = {'clientId' : clientId, 'sessionToken' : sessionToken, 'oldPasswd' : self.passwd, 'newPasswd' : newPasswd}
				
		request = {'terminalInfo' : terminalInfo, 'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing timeStamp
		terminalInfo = {'deviceId' : self.deviceId}
		payload = {'clientId' : clientId, 'sessionToken' : sessionToken, 'oldPasswd' : self.passwd, 'newPasswd' : newPasswd}
				
		request = {'terminalInfo' : terminalInfo, 'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))


		# Missing terminal Info

		request = {'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Reinitialising terminal Info
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp}

		
		# Missing clientId
		payload = {'sessionToken' : sessionToken, 'oldPasswd' : self.passwd, 'newPasswd' : newPasswd}
		request = {'terminalInfo' : terminalInfo, 'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing SessionToken
		payload = {'clientId' : clientId, 'oldPasswd' : self.passwd, 'newPasswd' : newPasswd}
		request = {'terminalInfo' : terminalInfo, 'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing Old Password

		payload = {'clientId' : clientId, 'sessionToken' : sessionToken, 'newPasswd' : newPasswd}
		request = {'terminalInfo' : terminalInfo, 'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))


		# Missing new Password
		payload = {'clientId' : clientId, 'sessionToken' : sessionToken, 'oldPasswd' : self.passwd}
		request = {'terminalInfo' : terminalInfo, 'payload' : payload}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing Payload
		request = {'terminalInfo' : terminalInfo}

		response_data = self.client.post(url_for('mServices.changePassword'),
					data = json.dumps(request),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
	def test2(self):
		'''
			Invalid password
			Expected Output: {'errorCode' : 'INVALID_CREDENTIALS'}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])

		# first time login to get the session token for change password
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		assert response_data["shouldChangePassword"] == True and 'sessionKey' in response_data

		# change new password
		newPassword = getNewPassword()
		response_data = shouldChangePasswd(self.client, self.deviceId, self.user.username, self.details.clientId, \
			response_data["sessionKey"], self.passwd, newPassword)
		assert response_data['mesg'] == 'Success' and 'sessionKey' in response_data	
		assert self.user.shouldChangePassword == False
		
		sessionKey = response_data['sessionKey']

		newPasswd = getNewPassword()
		response_data = changePasswd(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, newPasswd, newPasswd)
		self.assertEquals(response_data, errorMessageD('INVALID_CREDENTIALS'))
	
	def test3(self):
		'''
			Valid inputs
			Expected Output: {'mesg', 'Success'}
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])

		# first time login to get the session token for change password
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		assert response_data["shouldChangePassword"] == True and 'sessionKey' in response_data

		# change new password
		newPassword = getNewPassword()
		response_data = shouldChangePasswd(self.client,self.deviceId,self.user.username,self.details.clientId,response_data["sessionKey"],self.passwd,newPassword)
		assert response_data['mesg'] == 'Success' and 'sessionKey' in response_data	
		assert self.user.shouldChangePassword == False
		self.passwd = newPassword	
		sessionKey = response_data['sessionKey']
		newPasswd = getNewPassword()
		response_data = changePasswd(self.client, self.deviceId, self.appVersion, self.details.clientId, sessionKey, newPassword, self.passwd)
#		self.assertEquals(response_data, dict(mesg = 'Success'))
	
	