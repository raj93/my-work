#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for
from testing import *
from src.utils.crypto import otp_generator, otp_generator1
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
import simplejson as json
from datetime import datetime
from src.mAuthenticate.common import errorMessageD,getErrorCode
import hashlib

def login(client, deviceId, appVersion, clientId, user, password):

	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
	hashedPasswd = hashlib.sha256(password).hexdigest()
	terminalInfo = {'deviceId' : deviceId, 'timeStamp' : timeStamp, 'appVersion' : appVersion}
	payload = {'clientId' : clientId, "username" : user, "passwd" : hashedPasswd}
	
	login_data = {"terminalInfo" : terminalInfo, "payload" : payload}

	response = client.post(url_for('mServices.mLogin'),
					data = json.dumps(login_data),
					content_type = 'application/json')

	response_data = response.json
	return response_data
	
class testLogin(KitTestCase):
	
	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)
		
		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()


	def tearDown(self):
		from src.models import smsOTP
		from src.models import LastTransaction, posLogs, deviceIdCount
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()
		posLogs.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
	
	def test1(self):
		'''
			Invalid request. Each request misses one of the elements
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''
	
		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		
		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		payload = {'clientId' :self.details.clientId, "username" : self.user.username, "passwd" : self.passwd}
	
		login_data = {"terminalInfo" : terminalInfo, "payload" : payload}

		response_data = self.client.post(url_for('mServices.mLogin'),
					data = json.dumps(login_data),
					content_type = 'application/json').json

		# Missing deviceId
		terminalInfo = {'timeStamp': timeStamp, 'appVersion': self.appVersion}
		login_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.mLogin'),
						data = json.dumps(login_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
	
		# Missing terminalInfo
		login_data = {"payload": payload}
		
		response_data = self.client.post(url_for('mServices.mLogin'),
						data = json.dumps(login_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Reinitialising Terminal Info
		terminalInfo = {'deviceId': self.deviceId, 'timeStamp': timeStamp, 'appVersion': self.appVersion}       

		# Missing Client Id
		payload = {"username" : self.user.username, "passwd" : self.passwd}
		login_data = {"terminalInfo" : terminalInfo, "payload" : payload}

		response_data = self.client.post(url_for('mServices.mLogin'),
					data = json.dumps(login_data),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		# Missing self.user
		payload = {'clientId' :self.details.clientId, "passwd" : self.passwd}
		login_data = {"terminalInfo" : terminalInfo, "payload" : payload}

		response_data = self.client.post(url_for('mServices.mLogin'),
					data = json.dumps(login_data),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		# Missing self.passwd
		payload = {'clientId' :self.details.clientId, "username" : self.user.username}
		login_data = {"terminalInfo" : terminalInfo, "payload" : payload}

		response_data = self.client.post(url_for('mServices.mLogin'),
					data = json.dumps(login_data),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		# Missing payload
		login_data = {"terminalInfo" : terminalInfo}

		response_data = self.client.post(url_for('mServices.mLogin'),
					data = json.dumps(login_data),
					content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
	
	def test2(self):
		'''
		Valid Request. Unallocated ClientId
		Expected Output: {'errorCode' : 'INVALID_CREDENTIALS'}
		'''	
		clientId = self.details.clientId + 1 # some clientId other than the current one. Works as the only clientId present in the db is self.details.clientId
		response_data = login(self.client, self.deviceId, self.appVersion, clientId, self.user.username, self.passwd)
		self.assertEquals(response_data, errorMessageD('INVALID_CREDENTIALS'))
	
	def test3(self):
		'''
		Valid Request. Allocated ClientId. Not Activated
		Expected Output: {'errorCode' : 'POS_ACTIVATION'}
		'''
		
		#self.details.bitmap = 31
		#self.details.save()
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		errorCode =	getErrorCode(response_data['errorCode'])
		assert errorCode in posDetails.PRE_LOGIN_BITMAP

	
	def test4(self):
		'''
		Valid Request. Allocated ClientId. POS Activation. Invaild DeviceId
		Expected Output: {'errorCode' : 'INVALID_CREDENTIALS'}
		'''
		#OTP_Request + Activation
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		otp = response_data['mesg']

		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, otp)

		#Login
		deviceId = getDeviceId()
		while deviceId == self.deviceId:# Just in case new deviceId is equal to it #paranoid
			deviceId = getDeviceId()

		response_data = login(self.client, deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		self.assertEquals(response_data, errorMessageD('INVALID_CREDENTIALS'))
	
	def test5(self):
		'''
		Valid Request. Allocated ClientId. POS Activation. Valid DeviceId. Invalid Username
		Expected Output: {'errorCode' : 'INVALID_CREDENTIALS'}
		'''
		#OTP_Request + Activation
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		otp = response_data['mesg']

		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, otp)

		#Login
		username = getUserName()
		while username == self.user.username:
			username = getUserName()

		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, username, self.passwd)
		self.assertEquals(response_data, errorMessageD('INVALID_CREDENTIALS'))
		
	def test6(self):
		'''
		Valid Request. Allocated ClientId. POS Activation. Valid DeviceId. Invalid Password
		Expected Output: {'errorCode' : 'INVALID_CREDENTIALS'}
		'''
		#OTP_Request + Activation
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		otp = response_data['mesg']

		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, otp)

		#Login
		password = getNewPassword()
		while password == self.passwd:# Just in case new password is equal to it #paranoid
			password = getNewPassword()

		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, password)
		self.assertEquals(response_data, errorMessageD('INVALID_CREDENTIALS'))
	

	def test7(self):
		'''
		Valid Request. Allocated ClientId. POS Activation. Valid DeviceId. Valid Password , Should Change Password simulation
		Expected Output: ({'mesg' : 'Success', 'sessionKey' : sessionKey, 'shouldChangePassword' : True}
		'''

		#OTP_Request + Activation
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		otp = response_data['mesg']

		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, otp)#ShouldchangePassword will be set to True during activation request.

		# login request
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		assert response_data['mesg'] == 'Success' and 'sessionKey' in response_data and response_data['shouldChangePassword'] == True

	def test8(self):
		'''
		Valid Request. Allocated ClientId. POS Activation. Valid DeviceId. Valid Password , Should Change Password simulation
		Expected Output: ({'mesg' : 'Success', 'sessionKey' : sessionKey}
		'''
		#Kept this import here to prevent circular import
		from src.mAuthenticate.tests.test_shouldChangePasswd import shouldChangePasswd
	
		#OTP_Request + Activation
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		otp = response_data['mesg']

		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, otp)

		# simulating first time change password

		posUser = db.session.query(posUsers).filter_by(username = self.details.mobileNumber,clientId =self.details.clientId).first()
		posUser.shouldChangePassword = True
		posUser.update()

		# first time login to get the session token for change password
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)

		assert response_data["shouldChangePassword"] == True and 'sessionKey' in response_data

		# change new password
		newPassword = getNewPassword()
		response_data = shouldChangePasswd(self.client, self.deviceId, self.user.username, self.details.clientId, response_data["sessionKey"], self.passwd, newPassword)
		
		assert response_data['mesg'] == 'Success' and 'sessionKey' in response_data
		
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, newPassword)
		assert response_data['mesg'] == 'Success' and 'sessionKey' in response_data
		assert self.details.loggedInUser == self.user.username