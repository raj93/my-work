#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
from src.mAuthenticate.tests.test_activation import activate
from src.mAuthenticate.tests.test_shouldChangePasswd import shouldChangePasswd
from src.mAuthenticate.tests.test_login import login
from src.mAuthenticate.common import isAuthorisedRequest


class testIsAuthorised(KitTestCase):
	'''
	Adds test cases for isAuthorised function present in src/mAuthenticate/common.py. Note that its not a view and REST requests aren't necessary to test it
	'''

	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)

		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()

	def tearDown(self):

		from src.models import smsOTP
		from src.models import LastTransaction, posLogs, deviceIdCount
		from ext import db
		
		deviceIdCount.query.delete()
		self.user.delete()
		posLogs.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == self.details.clientId
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
	
	def test1(self):
		'''
			Non-existing clientId. Valid sessionKey and valid deviceId of a different clientId :D
			Expected output: False, INVALID_CREDENTIALS
		'''
		
		#OTP_Request + Activation + Login

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		newPassword = getNewPassword()
		response_data = shouldChangePasswd(self.client,self.deviceId,self.user.username,self.details.clientId,response_data["sessionKey"],self.passwd,newPassword)
		self.passwd = newPassword

		sessionKey = response_data['sessionKey']

		clientId = self.details.clientId + 1 # some clientId other than the current one. Works as the only clientId present in the db is self.details.clientId
		isAuthorised, responseCode = isAuthorisedRequest(clientId, sessionKey, self.deviceId)
		assert isAuthorised == False and responseCode == 'INVALID_CREDENTIALS'
	
	def test2(self):
		'''	
			Valid clientId. POS not activated
			Sessionkey = 'sessionkey'
			Expected Output: False, POS_ACTIVATION
		'''
		isAuthorised, responseCode = isAuthorisedRequest(self.details.clientId, 'sessionKey', self.deviceId)
		assert isAuthorised == False and responseCode == 'POS_ACTIVATION'
	
	def test3(self):
		'''	
			Valid clientId. POS activated. Incorrect deviceId
			Sessionkey = 'sessionkey'
			Expected Output : False, INVALID_CREDENTIALS
		'''
		deviceId = getDeviceId()
		while deviceId == self.deviceId:# Just in case new deviceId is equal to it #paranoid
			deviceId = getDeviceId()

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		newPassword = getNewPassword()
		response_data = shouldChangePasswd(self.client,self.deviceId,self.user.username,self.details.clientId,response_data["sessionKey"],self.passwd,newPassword)
		self.passwd = newPassword

		sessionKey = response_data['sessionKey']
	
		isAuthorised, responseCode = isAuthorisedRequest(self.details.clientId, sessionKey, deviceId)
		assert isAuthorised == False and responseCode == 'INVALID_CREDENTIALS'
	
	def test4(self):
		'''
			Activated clientId & valid deviceId. Invalid session token - bad signature
			Expected Output: False, INVALID_CREDENTIALS
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		#Ignoring login request as its main intent is to retrieve sesssion Key. We anyway need to use a invalid session key
		sessionKey = 'sessionKey'
	
		isAuthorised, responseCode = isAuthorisedRequest(self.details.clientId, sessionKey, self.deviceId)
		assert isAuthorised == False and responseCode == 'LOGIN_STATUS'
	
	def test5(self):
		'''	
			Activated clientId & valid deviceId. Request before login request
			Expected Output: False, NOT_LOGGED_IN
		'''
		
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		sessionKey = current_app.signer.sign(self.user.username)
		isAuthorised, responseCode = isAuthorisedRequest(self.details.clientId, sessionKey, self.deviceId)
		assert isAuthorised == False and responseCode == 'LOGIN_STATUS'
	
	def test6(self):
		'''	
			Activated clientId & valid deviceId. 
			i. Login request with user1
			ii. Login request with user2
			iii. isAuthorised with user1 & sessionToken(user1)
			Expected Output: False, NOT_LOGGED_IN
		'''
		self.user1, self.passwd1 = getNewPOSUser(self.details.clientId)
		self.user1.username = "otheruser"
		self.user1.save()

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		newPassword = getNewPassword()
		response_data = shouldChangePasswd(self.client,self.deviceId,self.user.username,self.details.clientId,response_data["sessionKey"],self.passwd,newPassword)
		self.passwd = newPassword

		sessionKey = response_data['sessionKey']
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user1.username, self.passwd1)
		
		isAuthorised, responseCode = isAuthorisedRequest(self.details.clientId, sessionKey, self.deviceId)
		assert isAuthorised == False and responseCode == 'NOT_LOGGED_IN'

		self.user1.delete()
	
	def test7(self):
		'''	
			Activated clientId, valid deviceId. Using old sessiontoken
			Expected Output : False, INVALID_CREDENTIALS
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		newPassword = getNewPassword()
		response_data = shouldChangePasswd(self.client, self.deviceId, self.user.username, \
			self.details.clientId, response_data["sessionKey"], self.passwd, newPassword)
		self.passwd = newPassword

		sessionKey = response_data['sessionKey']
		import time
		time.sleep(3)#Using this as 1 second old session token is valid 
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId,\
		 	self.user.username, self.passwd)
		assert 'mesg' in response_data
		isAuthorised, responseCode = isAuthorisedRequest(self.details.clientId, sessionKey, self.deviceId)
		assert isAuthorised == False and responseCode == 'INVALID_CREDENTIALS'

	def test8(self):
		'''	
			Activated clientId, valid deviceId. Valid session token but expired
			Expected Output : False, SESSION_TIMEOUT
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		newPassword = getNewPassword()
		response_data = shouldChangePasswd(self.client,self.deviceId,self.user.username,self.details.clientId,response_data["sessionKey"],self.passwd,newPassword)
		self.passwd = newPassword

		sessionKey = response_data['sessionKey']
		import time
		time.sleep(current_app.config['POS_SESSION_TIME'] + 1)#Note that +1 is used

		isAuthorised, responseCode = isAuthorisedRequest(self.details.clientId, sessionKey, self.deviceId)
		assert isAuthorised == False and responseCode == 'SESSION_TIMEOUT'

	def test9(self):
		'''
			Valid isAuthorised Request
			Expected Output: True, Username
		'''

		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, response_data['mesg'])
		response_data = login(self.client, self.deviceId, self.appVersion, self.details.clientId, self.user.username, self.passwd)
		newPassword = getNewPassword()
		response_data = shouldChangePasswd(self.client,self.deviceId,self.user.username,self.details.clientId,response_data["sessionKey"],self.passwd,newPassword)
		self.passwd = newPassword

		sessionKey = response_data['sessionKey']
		
		isAuthorised, username = isAuthorisedRequest(self.details.clientId, sessionKey, self.deviceId)
		assert isAuthorised and username == self.user.username
	
	