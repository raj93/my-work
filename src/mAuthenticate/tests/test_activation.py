#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from flask import url_for, current_app
from testing import *
from src.utils.crypto import otp_generator
from src.mAuthenticate.tests.test_otpRequest import otpRequest
import simplejson as json

from datetime import datetime
from src.mAuthenticate.common import errorMessageD

def activate(client, mobileNumber, deviceId, appVersion, registered, otp):
	deviceModel = random.choice(['samsung', 'htc', 'lg a', 'nexus'])

	timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
	
	terminalInfo = {'deviceId' : deviceId,'deviceModel' : deviceModel, 'timeStamp' : timeStamp, 'appVersion' : appVersion}
	payload = {'mobileNumber' : mobileNumber, "otp" : otp, "registered" : registered}
	
	activation_data = {"terminalInfo" : terminalInfo, "payload" : payload}

	response = client.post(url_for('mServices.activationProcess'),
					data = json.dumps(activation_data),
					content_type = 'application/json')

	response_data = response.json
	return response_data
	

class testActivate(KitTestCase):
	
	def setUp(self):
		self.email = otp_generator(random.randint(6,8), string.letters) + '@aasaanpay.com'#TODO not using it

		self.company, self.companyLogin = getNewCompany()
		self.companyLogin.save()
		self.company.save()
		
		self.merchant, self.merchantLogin = getNewMerchant(self.company.username)
		self.merchantLogin.save()
		self.merchant.save()

		self.details, self.payment = getNewPOS(self.merchant)

		self.deviceId = getDeviceId()
		self.otp = otp_generator(8, string.digits)

		self.user, self.passwd = getNewPOSUser(self.details.clientId)
		self.user.save()
		
	def tearDown(self):

		from src.models import smsOTP
		from src.models import LastTransaction, posLogs, deviceIdCount
		from ext import db
		
		self.user.delete()
		deviceIdCount.query.delete()
		LastTransaction.query.delete()#Or delete a single row with clientId == details.clientId
		posLogs.query.delete()
		self.details.delete()
		self.payment.delete()
		self.merchant.delete()
		self.merchantLogin.delete()
		self.company.delete()
		self.companyLogin.delete()
		smsOTP.query.delete()
		db.session.commit()
		db.engine.dispose()
	
	def activate1(self, registered = True):
		'''
			Invalid request. Each request misses one of the elements
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''
		if registered:
			mobileNumber = self.details.mobileNumber
		else:
			mobileNumber = otp_generator(10, string.digits)

		
		timeStamp = datetime.utcnow().strftime("%Y-%m-%d %X")
		registered = True

		terminalInfo = {'deviceId' : self.deviceId, 'timeStamp' : timeStamp, 'appVersion' : self.appVersion}
		payload = {'mobileNumber' : mobileNumber, "otp" : self.otp, "registered" : registered}
	
		activation_data = {"terminalInfo" : terminalInfo, "payload" : payload}

		# Missing deviceId
		terminalInfo = {'timeStamp': timeStamp, 'appVersion': self.appVersion}
		activation_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.activationProcess'),
						data = json.dumps(activation_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
	
		#Missing timestamp
		terminalInfo = {'deviceId': self.deviceId, 'appVersion': self.appVersion}
		activation_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.activationProcess'),
						data = json.dumps(activation_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Missing appVersion
		terminalInfo = {'deviceId': self.deviceId, 'timeStamp': timeStamp}       
		activation_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.activationProcess'),
						data = json.dumps(activation_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Missing terminalInfo
		activation_data = {"payload": payload}
		
		response_data = self.client.post(url_for('mServices.activationProcess'),
						data = json.dumps(activation_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Reinitialising Terminal Info
		terminalInfo = {'deviceId': self.deviceId, 'timeStamp': timeStamp, 'appVersion': self.appVersion}       
		
		#Missing MobileNumber
		payload = {"otp" : self.otp, "registered" : registered}

		activation_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.activationProcess'),
						data = json.dumps(activation_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Missing OTP
		payload = {'mobileNumber' : mobileNumber, "registered" : registered}
		activation_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.activationProcess'),
						data = json.dumps(activation_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

		#Missing Registered
		payload = {'mobileNumber' : mobileNumber, "otp" : self.otp}
		activation_data = {"terminalInfo": terminalInfo, "payload": payload}
		
		response_data = self.client.post(url_for('mServices.activationProcess'),
						data = json.dumps(activation_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
		#Missing Payload
		activation_data = {"terminalInfo": terminalInfo}
		
		response_data = self.client.post(url_for('mServices.activationProcess'),
						data = json.dumps(activation_data),
						content_type = 'application/json').json

		self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))

	def test1(self):
		'''
			Invalid Request missing each of the elements required for the request. Tests all the variants.
			Tests for both Registered = True & Registered = False. Doesn't care about OTP Request
			Expected Output: {'errorCode' : 'INVALID_REQUEST'}
		'''
		self.activate1()
		self.activate1(False)
	
	def activate2(self, registered = True):
		if registered:
			mobileNumber = self.details.mobileNumber
		else:
			mobileNumber = otp_generator(10, string.digits)
		from src.utils.validate import validateEntry
		print validateEntry(self.deviceId, 'DEVICE_ID')
		print validateEntry(self.otp, 'OTP')
		#TODO Keep a check on warning logs
		response_data = activate(self.client, mobileNumber, self.deviceId, self.appVersion, registered, self.otp)
		self.assertEquals(response_data, errorMessageD('MOBILE_NUMBER_NOTREGISTERED'))

	def test2(self):
		'''
			Activation request before OTP Request
			Expected Output: {'errorCode' : 'MOBILE_NUMBER_NOTREGISTERED'}
		'''
		self.activate2()
		self.activate2(False)
	
	def activate3(self, registered = True):
		if registered:
			mobileNumber = self.details.mobileNumber
		else:
			mobileNumber = otp_generator(10, string.digits)
		
		response_data = otpRequest(self.client, mobileNumber, self.deviceId, self.appVersion, registered)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'
		response_data = activate(self.client, mobileNumber, self.deviceId, self.appVersion, registered, self.otp)
		self.assertEquals(response_data, errorMessageD('INCORRECT_OTP'))

	def test3(self):
		''' Incorrect OTP
			Expected Output: {'errorCode' : 'INCORRECT_OTP'}
		'''
		self.activate3()
		self.activate3(False)	
	
	def test4(self):
		'''
			Valid OTP activation for a non-registered mobile
			Expected output: {'mesg' : 'Success'}
		'''
		
		mobileNumber = otp_generator(10, string.digits)
		response_data = otpRequest(self.client, mobileNumber, self.deviceId, self.appVersion, False)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

		otp = response_data['mesg']
		assert len(otp) == 8

		response_data = activate(self.client, mobileNumber, self.deviceId, self.appVersion, False, otp)
		self.assertEquals(response_data, dict(mesg = 'Success'))
	
	def test5(self):
		'''
			Valid OTP activation for a registered mobile
			Expected output: {'mesg' : 'Success', 'clientId': clientId, 'sequenceCode' : sequenceCode}
		'''
		
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

		otp = response_data['mesg']
		assert len(otp) == 8

		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, otp)
		assert 'mesg' in response_data and response_data['mesg'] == 'Success' and \
				'clientId' in response_data and 'sequenceCode' in response_data
	
	def test6(self):
		'''
			Activation of Non-registered mobile with inputs:
				i. registered = True 
				ii. Valid OTP Verification(registered = False in otpRequest)

			Expected Output: {'errorCode' : 'ALREADY_REGISTERED'}
		'''

		mobileNumber = otp_generator(10, string.digits)
		response_data = otpRequest(self.client, mobileNumber, self.deviceId, self.appVersion, False)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

		otp = response_data['mesg']
		assert len(otp) == 8

		response_data = activate(self.client, mobileNumber, self.deviceId, self.appVersion, True, otp)
		self.assertEquals(response_data, errorMessageD('ALREADY_REGISTERED'))
	
	def test7(self):
		'''
			Reactivation of a registered mobile with different different deviceId. Second Otp Request is irrelevant in this case. #TODO Probably need to add for granularity
			i. otp = OtpRequest
			ii. activate(deviceId)
			iv. activate(deviceId1)
		'''
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

		otp = response_data['mesg']
		assert len(otp) == 8

		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, otp)
		assert 'mesg' in response_data and response_data['mesg'] == 'Success' and \
				'clientId' in response_data and 'sequenceCode' in response_data

		wait = current_app.config['TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST']
		import time
		time.sleep(wait)
		
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

		otp = response_data['mesg']
		assert len(otp) == 8

		response_data = activate(self.client, self.details.mobileNumber, getDeviceId(), self.appVersion, True, otp)
		self.assertEquals(response_data, errorMessageD('ALREADY_REGISTERED'))		
	
	def activate8(self, secondOTPRequest = True):
		response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
		assert 'mesg' in response_data and response_data['mesg'] != 'Success'

		otp = response_data['mesg']
		assert len(otp) == 8

		response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, otp)
		assert 'mesg' in response_data and response_data['mesg'] == 'Success' and \
				'clientId' in response_data and 'sequenceCode' in response_data

		if secondOTPRequest:
			#Wait for TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST as it is restricted to send multiple otp requests with that time interval
			wait = current_app.config['TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST']
			import time
			time.sleep(wait)
			
			response_data = otpRequest(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True)
			assert 'mesg' in response_data and response_data['mesg'] != 'Success'

			otp = response_data['mesg']
			assert len(otp) == 8

			response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, otp)
			assert 'mesg' in response_data and response_data['mesg'] == 'Success' and \
					'clientId' in response_data and 'sequenceCode' in response_data
		else:
			response_data = activate(self.client, self.details.mobileNumber, self.deviceId, self.appVersion, True, otp)
			self.assertEquals(response_data, errorMessageD('INVALID_REQUEST'))
		
	def test8(self):
		'''
			Re-activation of a registered mobile using same deviceId.
			Case 1: Without 2nd otp request i.e., sending a new activation request with old OTP
				i. otp = OtpRequest
				ii. activate(otp)
				iii. activate(otp)
				Expected Output: {'errorCode' : 'INVALID_REQUEST'}
			Case 2: With 2nd otp request i.e., 
				i. otp = OtpRequest
				ii. activate(otp)
				iii. otp1 = otpRequest()
				iv. activate(otp1)
				Expected output: {'mesg' : 'Success', 'clientId': clientId, 'sequenceCode' : sequenceCode}
		'''	
		self.activate8()
		#Wait for TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST as it is restricted to send multiple otp requests with that time interval
		wait = current_app.config['TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST']
		import time
		time.sleep(wait)
		self.activate8(False)
	