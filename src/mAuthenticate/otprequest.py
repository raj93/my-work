from flask import request, current_app
from datetime import datetime
import simplejson as json
	
import common
from ext import db
from src.models import posDetails, smsOTP, deviceIdCount
from src.utils.sms import send
from src.utils.crypto import otp_generator
from src.utils.validate import validateEntry
import string

class OtpRequest():
	
	OTP_TAGS = {
		'terminalInfo': dict,
		'payload': dict,
		}

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp' : unicode,
		#'appVersion' : unicode,
		}

	PAYLOAD_TAGS = {
		'mobileNumber': 'MOBILE',
		#'email' : unicode,#Not mandated. Will be validated during population
		'registered': bool,
		}

	PAYLOAD_OPTIONAL_TAGS = {
		'email': 'EMAIL',
		}	

	def __init__(self):
		self.json_data = request.get_json()

	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.OTP_TAGS.items()):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['payload'] and validateEntry(self.json_data['payload'][k], v) \
							for k, v in self.PAYLOAD_TAGS.items()):
			return False

		for k, v in self.PAYLOAD_OPTIONAL_TAGS.items():
			if k in self.json_data['payload'] and self.json_data['payload'][k] !='' and \
				not validateEntry(self.json_data['payload'][k], v):#Check happens only if entry is not null
				return False
				
		return True

	def populateEntries(self):
		self.deviceId = self.json_data['terminalInfo']['deviceId']
		self.timestamp = common.getCurrentTimeStamp() # storing server time stamp
		#self.appVersion = self.json_data['terminalInfo']['appVersion']

		self.mobileNumber = self.json_data['payload']['mobileNumber']
		
		self.email = self.json_data['payload']['email'] \
					if 'email' in self.json_data['payload'] else ''

		self.registered = self.json_data['payload']['registered']

	def changeDeviceIdCountState(self):
		'''This is to track the count the requests from a particular device.
		#TODO: These values are not used in checking requests
		Verify this implementation before it is used
		'''
	
		ddetails = deviceIdCount.query.filter_by(deviceId=self.deviceId).first()
		if ddetails == None:
			dentry = deviceIdCount(self.deviceId)
			dentry.save(commit=False)
		else:
			numTries = ddetails.numTries + 1
			ddetails.update(commit=False, numTries=numTries)

	def generateOTP(self):
		'''Generates an otp of length(otp_length)'''
		return otp_generator(8, string.digits)

	def processOTPRequest(self):

		otp_entry = smsOTP.query.filter_by(mobileNumber = self.mobileNumber).first()
		otp = self.generateOTP()

		if otp_entry == None:
			otp_entry = smsOTP(self.mobileNumber, self.email, self.deviceId, otp, common.getCurrentTimeStamp())#numTries = 1 (default value)
			otp_entry.save(commit = False)
		
		else:
			#checking  the time stamp with the latest otp verification request
			ticks = (datetime.now() - otp_entry.lastSMSTime).seconds

			if ticks < current_app.config['TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST']:
				wait_ticks = current_app.config['TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST'] -ticks
				return json.dumps({'mesg' : 'Success', 'wait' : wait_ticks})#
			
			#check num tries of that mobile number
			if otp_entry.numTries >= current_app.config['OTP_REQUEST_NUMTRIES_CAP']:
				return common.errorMessage('OTP_NUMTRIES_EXCEEDED')
			
			numTries = otp_entry.numTries + 1

			# resetting activation requests count to zero 
			otp_entry.update(commit=False, deviceId=self.deviceId, otp=otp, \
				lastSMSTime=common.getCurrentTimeStamp(), numTries=numTries, isSuccess=False, \
				numActivationRequests=0)
						
		self.changeDeviceIdCountState()
		
		return self.sendOTP(otp)

	def sendOTP(self, otp):
		'''Sends or Returns OTP based on run level(production/testing)'''
		# Incase of TESTIn we send otp as the message
		if current_app.config['TESTING']:
			return json.dumps({'mesg':otp})
		
		# In case of SKIP_OTP_VERIFICATION we send sms in the format 'Success#otp'
		dtls = posDetails.query.filter_by(mobileNumber=self.mobileNumber).first()
		if dtls != None and (dtls.bitmap & posDetails.BITMAP['SKIP_OTP_VERIFICATION'] != 0) :
			return json.dumps({'mesg':'Success'+'#'+otp[:4]+'-'+otp[4:]})

		# Incase of DEBUG we just send to particular numbers
		if current_app.config['DEBUG'] and self.mobileNumber not in current_app.config['ALLOWED_SMS_LIST']:
			return common.errorMessage('SMS not allowed to: ' + str(self.mobileNumber))

		payload={'To':self.mobileNumber, 'Body':'Activation Code: %s-%s' % (otp[:4], otp[4:])}
		response = send(payload)
		return json.dumps({'mesg' : response})

	def authorise(self):
		# getting details of terminal mobile number if any
		details = posDetails.query.filter_by(mobileNumber = self.mobileNumber).first()

		if self.registered == False:
			if details != None:
				# he is telling unregisterd but has entered registered number
				self.changeDeviceIdCountState() 
				return json.dumps({'mesg' : common.INFO_CODES['SUCCESS']}) # to remove the distinction of reg and unreg
			else:
				otp_entry = smsOTP.query.filter_by(mobileNumber = self.mobileNumber).first()
				if otp_entry == None:
					otp = self.generateOTP()
					otp_entry = smsOTP(self.mobileNumber,self.email,self.deviceId,otp,common.getCurrentTimeStamp())
					otp_entry.save(commit = False)

					self.changeDeviceIdCountState()

					return self.sendOTP(otp)
				else:
					if otp_entry.isSuccess and otp_entry.deviceId == self.deviceId:#Mobile already verified. Take to promotion page
						return common.errorMessage('TO_PROMOTION')
					else:
						return self.processOTPRequest()
		else:
			if details == None:
				#if there is a matching device id it means he has entered wrong mobile number
				details = posDetails.query.filter_by(deviceId = self.deviceId).first()
				if details != None:
					return common.errorMessage('MOBILE_NUMBER_ERROR')
				else:
					return common.errorMessage('MOBILE_NUMBER_NOTREGISTERED')

			# checking bits
			isEligible, statusCode = common.validatePOSBitmap(details.bitmap, posDetails.VALIDATE_BITMAP_TYPE['PRE_ACTIVATION'])
			if not isEligible:
				return common.errorMessage(statusCode)

			#first time otp request
			if details.isActivated() == False:
				return self.processOTPRequest() 
			
			if details.deviceId == self.deviceId:
				return self.processOTPRequest() #resend otp request
			else:
				#we don't allow up and running pos mobile to us his sim from other device
				return common.errorMessage('ALREADY_REGISTERED')

	def process(self):
		if not self.isValidRequest(): return common.errorMessage('INVALID_REQUEST')
		
		self.populateEntries()
		output = self.authorise()
		db.session.commit()
		return output
