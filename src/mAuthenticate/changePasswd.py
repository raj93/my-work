from datetime import datetime
from flask import request
import hashlib

from ext import db
from src.mAuthenticate import common
from src.models import posDetails, posUsers
from src.utils import crypto
from src.utils.validate import validateEntry
from common import Log,logEvent

import simplejson as json

class ChangePasswd:

	CHANGE_PASSWD_TAGS = {
		'terminalInfo': dict,
		'payload': dict,
		}#removed appversion tag.. in the assumption that checking appVersion during login request is good to go.

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		}

	PAYLOAD_TAGS = {
		'clientId': int,
		'sessionToken': 'SESSION',
		'oldPasswd': 'PASSWORD',
		'newPasswd': 'NEW_PASSWORD',
		}

	def __init__(self):
		self.json_data = request.get_json()		
		
	def isValidRequest(self):
		if self.json_data == None:
			return False
		
		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.CHANGE_PASSWD_TAGS.items()):
			return False 
		

		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		#TODO send special error messages; INVALID_NEW_PASSWD for invalid password	
		if not all(k in self.json_data['payload'] and validateEntry(self.json_data['payload'][k], v) \
							for k, v in self.PAYLOAD_TAGS.items()):
			return False

		return True

	def populateEntries(self):
		self.deviceId = self.json_data['terminalInfo']['deviceId']
		self.timeStamp = common.getCurrentTimeStamp()

		self.clientId = self.json_data['payload']['clientId']
		self.sessionToken = self.json_data['payload']['sessionToken']
		self.oldPasswd = self.json_data['payload']['oldPasswd']#input is SHA-256 hash of plain password
		self.newPasswd = self.json_data['payload']['newPasswd']#input is plain password
		
	def authorise(self):
		details = posDetails.query.filter_by(clientId = self.clientId).first()

		user = posUsers.query.filter_by(username = self.username, clientId = self.clientId).first()

		if user is None: return common.errorMessage('INVALID_CREDENTIALS')

		if not crypto.verify_passwd(self.oldPasswd, user.passwd, user.salt):
			return common.errorMessage('INVALID_CREDENTIALS')

		# Making sure new password is not same as old password.
		passwd, salt = crypto.hash_passwd(crypto.sha256(self.newPasswd), user.salt)
		if passwd == user.passwd: return common.errorMessage('NEW_PASSWORD_IS_OLD_PASSWORD')

		# updating to new password and new salt
		print self.newPasswd
		passwd, salt = crypto.hash_passwd(crypto.sha256(self.newPasswd))
		user.update(commit=False, passwd=passwd, salt=salt, lastActivity=self.timeStamp)

		# logging changePassword event
		event = 'Changed Password'
		logEvent(event,self.username,details,Log.info)
		
		db.session.commit()
		return json.dumps({'mesg' : 'Success'})	

	def process(self):
		if not self.isValidRequest(): return common.errorMessage('INVALID_REQUEST')

		self.populateEntries()
		isAuthorised, statusCode = common.isAuthorisedRequest(self.clientId, \
			self.sessionToken, self.deviceId)
		if isAuthorised == False:
			return common.errorMessage(statusCode)
		else:
			self.username = statusCode
		return self.authorise()
