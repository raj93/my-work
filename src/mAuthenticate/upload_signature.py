from flask import request, current_app, render_template
from sqlalchemy import and_
from distutils.version import StrictVersion
import os, re
# Use simplejson or Python 2.6 json, prefer simplejson.
import simplejson as json

from src.mAuthenticate import common
from src.utils.pdf import convertHtmlToPdf
from ext import db
from src.models import Transaction, Void
from src.utils.sms import sendTransactionSms
from src.utils.s3 import uploadFile, LookUpKey
from src.utils.crypto import otp_generator
from src.utils.ses_email import send1
from src.utils.receipt import generateReceipt
from src.utils.validate import validateEntry


class Upload:
	PURCHASE = 0;
	VOID = 1;

	UPLOAD_TAGS = {
		'terminalInfo': dict,
		'payload': dict,
		}

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp': str,
		}

	PAYLOAD_TAGS = {
		'clientId': int,
		'sessionToken': 'SESSION',
		'transId': 'TRANSID',
		'transType': int,
		}

	PAYLOAD_OPTIONAL_TAGS = {
		'email': 'EMAIL',
		'sms' : 'MOBILE',
		}

	CONTENT_TYPE = {
		'jpg': 'jpeg',
		'png': 'png',
		'jpeg': 'jpeg',
		'gif': 'gif',
		}

	#sms & email Tags are optional
	def __init__(self):
		if 'json' in request.files.keys() and 'signature' in request.files.keys():
			self.json_data = json.loads(request.files['json'].read())
		else:
			self.json_data = None

	def isValidRequest(self):
		if self.json_data == None:
			return False

		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.UPLOAD_TAGS.items()):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['payload'] and validateEntry(self.json_data['payload'][k], v) \
							for k, v in self.PAYLOAD_TAGS.items()):
			return False

		#These shouldn't be hard checks, since card holder verification might be missed. But, only applications should be allowed, so they can be! #TODO DECIDE
		# for k, v in self.PAYLOAD_OPTIONAL_TAGS.items():
		# 	if k in self.json_data['payload'] and \
		# 		not validateEntry(self.json_data['payload'][k], v):
		# 		return False	

		return True
		
	def populateEntries(self):

		self.deviceId = self.json_data['terminalInfo']['deviceId']
		#self.timeStamp = self.json_data['terminalInfo']['timeStamp']

		#TODO #validate the following entries -> length and chars in the input
		self.clientId = self.json_data['payload']['clientId']
		self.sessionToken = self.json_data['payload']['sessionToken']
		self.transId = self.json_data['payload']['transId']
		self.transType = self.json_data['payload']['transType']
		
		self.email = self.mobile = ''
		
		if 'sms' in self.json_data['payload'] and \
				validateEntry(self.json_data['payload']['sms'], 'MOBILE'):
			self.mobile = self.json_data['payload']['sms']

		if 'email' in self.json_data['payload'] and \
				validateEntry(self.json_data['payload']['email'], 'EMAIL'):
			self.email = self.json_data['payload']['email']
			#validation of the email/sms have to be done on the client. If they're found invalid on the server, request to email/sms are discarded and signature is saved

		if not (self.transType == self.PURCHASE or self.transType == self.VOID):
			return False

		return True
		
	def allowed_file(self, filename):
		#return '.' in filename and filename.rsplit('.', 1)[1] == 'png'
		 return '.' in filename and \
            filename.rsplit('.', 1)[1] in current_app.config['SIGNATURE_EXTENSIONS_ALLOWED']

	def authorise(self):
		
		if self.transType == self.PURCHASE:
			purchase = transaction = Transaction.query.filter(Transaction.clientId == self.clientId, \
				Transaction.transId == self.transId, Transaction.conditions('SIMPLE_PURCHASE')).first()
			transType = 'SALE'

		else:#Only two values are possible, PURCHASE, VOID
			transaction = Void.query.filter(Void.clientId == self.clientId, \
				Void.transId == self.transId, Void.conditions('VALID')).first()
			if transaction: purchase = transaction.purchaseDetails
			transType = 'VOID'

		if transaction is None:
			return common.errorMessage('INVALID_TRANSACTION')
		
		if transaction.receiptId:
			return common.errorMessage('SIGNATURE_EXISTS')

		signature = request.files['signature']
		if signature and self.allowed_file(signature.filename):
			merchantDetails = transaction.terminalDetails.merchantDetails

			'''
			i. Generates Transaction Receipt, receiptId for public link
			ii. Upload signature and transaction receipt to respective buckets.
			iii. Update receiptId in database
			'''
	
			receiptId = otp_generator(8)
			count = 0
			while LookUpKey(current_app.config['RECEIPTS_BUCKET'], receiptId):
				receiptId = otp_generator(8)
				if count >  5:
					current_app.logger.critical('Receipt id space full. Increase the signature ')
				count += 1	
				#TODO Instead of this, keep a expiry time on each object when uploading to prevent congestion

			receiptPath = current_app.config['TEMP_FOLDER'] + receiptId + ".jpeg"
			signaturePath = current_app.config['TEMP_FOLDER'] + self.transId

			signature.save(signaturePath)

			generateReceipt(purchase, merchantDetails, signaturePath, receiptPath)
		
			'''
				Note we're using transId as signature names for convenience. We are using receiptId in case of receipts
				only to abstract other files from evesdroppers
			'''
			content_type = 'image/' + self.CONTENT_TYPE[signature.filename.rsplit('.', 1)[1]]
			receipt_content_type = 'image/jpeg'
			if not (uploadFile(signaturePath, current_app.config['SIGNATURES_BUCKET'], self.transId, content_type = content_type) and #Private
		 			uploadFile(receiptPath, current_app.config['RECEIPTS_BUCKET'], receiptId, content_type= receipt_content_type, public = True)):#Public object
				current_app.logger.error('Unable to upload S3 objects')

			# send sms to the customer
			current_app.logger.debug('Email: ' + str(self.email) + ' SMS:' + str(self.mobile))

			if current_app.config['TESTING']:
				self.email = current_app.config['DEVELOPERS'][0]#List doesn't work here
				self.mobile = current_app.config['SETTELEMENT_SMS']

			if self.mobile:
				receiptLink = 'http://' + current_app.config['RECEIPTS_BUCKET'] + '/' + receiptId
				sendTransactionSms(merchantDetails.nameOnReceipt, transType, receiptLink, (purchase.amount + purchase.tipAmount), \
					purchase.maskedPan[-4:], transaction.rrn, transaction.invoice, self.mobile)

			if self.email:
				attachmentName = transType + '_' + transaction.rrn + '.jpeg'
				body = '''Greetings, 
				\rPlease find e-Receipt of your transaction attached.
				\r
				\rBest Wishes,
				\rAasaanPay
				'''
				subject = 'e-Receipt for your transaction at ' + merchantDetails.nameOnReceipt
				try:
					send1(subject, self.email, body, receiptPath, attachmentName)
				except Exception as e:
					current_app.logger.error('Following exception occured while sending email to '+to+'\n'+str(e))
				
			transaction.update(commit = False, receiptId = receiptId, receiptMobile = self.mobile, receiptEmail = self.email)

			db.session.commit()

			# removing the file path
			os.remove(receiptPath)
			os.remove(signaturePath)
			return json.dumps({'mesg' : 'Successfully Uploaded'})
		else:
			return common.errorMessage('INVALID_FILE')
	
	def process(self):
		if not self.isValidRequest() or not self.populateEntries():
			return common.errorMessage('INVALID_REQUEST')
		
		#validateentries -> only trantype = 0,1 are valid etc..
		isAuthorised, statusCode = common.isAuthorisedRequest(self.clientId, self.sessionToken, self.deviceId, False)
		if isAuthorised == False:
			return common.errorMessage(statusCode)
		else:
			self.username = statusCode
		#self.validateEntries() 
		return self.authorise()
