from flask import current_app, request, render_template
from datetime import datetime
import re, os, itsdangerous
import simplejson as json

from ext import db
from src.models import Transaction, Void, Tip, Settlement as settlement_table, LastTransaction,posDetails
from src.utils.pdf import convertHtmlToPdf
from src.utils.ses_email import send1
from src.utils.crypto import otp_generator
from src.utils.sms import sendSettlementSms

from sqlalchemy import and_

#Last settlement flags
PURCHASE = 0
VOID = 1
TIP = 2
SETTLEMENT = 3
ERROR_IN_TRANSACTION = 4
SETTLEMENT_REPORT = 5
VOID_SEARCH = 6
REFUND = 7
PREAUTH = 8
BALANCE_ENQUIRY = 9

RELAXED = 0
TIP_BUSY = 1
BUSY = 2


LAST_SETTLEMENT_SUMMARY = 'last'
BATCH_TOTALS_REPORT = 'current'


INFO_CODES = {
	'SUCCESS' : 'Success',
	'FAILURE' : 'Failure'
}

ERROR_CODES = {
	'TO_PROMOTION' : 'Taking you to promotion page',
	'SYSTEM_ERROR' : 'System Error. Please try again',
	'SERVER_ERROR' : 'Server error. Please Contact customer care',
	'INVALID_REQUEST' : 'INVALID_REQUEST_JSON',
	'INVALID_CREDENTIALS' : 'Invalid credentials',
	'NEW_PASSWORD_IS_OLD_PASSWORD' : 'New Password should not be same as old Password',
	'WRONG_PASSWD' : 'Wrong Password',
	'NOT_LOGGED_IN' : 'User not logged in',
	'SESSION_TIMEOUT' : 'Session Timed out',
	'INVALID_TRANSACTION' : 'Invalid transaction details',
	'INVALID_FILE' : 'Invalid file. Only png format supported',
	'SIGNATURE_EXISTS' : 'Signature Already exists',
	'UPDATE_LATEST_VERSION' : 'Update to the latest version',
	'ALREADY_REGISTERED' : 'Already registered on different device',
	'MOBILE_NUMBER_ERROR' : 'Check your mobile number',
	'MOBILE_NUMBER_NOTREGISTERED' : 'mobile number not registered',
	'INCORRECT_OTP' : 'Otp verification failed',
	'OTP_NUMTRIES_EXCEEDED' : 'Number of tries exceeded. Contact Customer care',
	'ALREADY_LOGGED' : 'Already logged with us',	
	'PROCESSOR_BUSY' : 'Processor busy, please try again',
	'SERVER_BUSY' : 'Server busy, please try again',
	'INVALID_SEQUENCE_CODE' : 'Invalid sequence Code. Please try again',
	'INVALID_SEQUENCE_CODE1' : 'Invalid sequence Code',
	'MERCHANT_VERIFICATION' : 'KYC Verification pending',
	'POS_VERIFICATION' : 'POS verification pending',
	'PAYMENT_VERIFICATION' : 'Payment for POS not processed',
	'MID_ALLOTED' : 'MID not allocated by bank',
	'TID_ALLOTED' : 'TID not allocated by bank',
	'POS_ACTIVATION' : 'POS not activated',
	'LOGIN_STATUS' : 'User not logged in',
	'MERCHANT_LOCK' : 'MID locked',
	'MERCHANT_DISABLE' : 'MID deactivated',
	'POS_LOCK' : 'POS locked',
	'POS_DISABLE' : 'POS deactivated',
	'REQUIRE_PIN' : 'PIN Required'
}

errorMessage = lambda errorCode: json.dumps({'errorCode' : ERROR_CODES[errorCode] 
									if ERROR_CODES.has_key(errorCode) else errorCode})
errorMessageD = lambda errorCode: {'errorCode' : ERROR_CODES[errorCode] 
									if ERROR_CODES.has_key(errorCode) else errorCode}

class Log:
	info = 1
	debug = 2
	warn = 3
	error = 4

import random
RANDOM_NUMBER_LENGTH = 5
def generateRandomNumber():
	return str(random.randint(10**(RANDOM_NUMBER_LENGTH-1),10**RANDOM_NUMBER_LENGTH-1))
									
import hashlib
def getSha256Hash(input):
	if input:
		return hashlib.sha256(input).hexdigest()
	return input

# user is when the user is not logged in but event is related to that user (forgot password)
def logEvent(event,posUser,details,type):
	tags = {'clientId': str(details.clientId),
			'posUser' : posUser,
			'company': details.company,
			'merchant':details.merchant,
			}
	if details.agent:
		tags.update({'agent':details.agent})

	if type == Log.info:
		current_app.logger.info(event, extra={'tags':tags})
	elif type == Log.warn:
		current_app.logger.warn(event, extra={'tags':tags})
	elif type == Log.debug:
		current_app.logger.debug(event, extra={'tags':tags})
	elif type == Log.error:
		current_app.logger.error(event, extra={'tags':tags})
	else:
		current_app.logger.info(event, extra={'tags':tags})



def getCurrentTimeStamp():
	return datetime.utcnow().strftime("%Y-%m-%d %X")

def getTotalAmount(line):
	results = re.findall(r'(Rs.)([\d]+).([\d]+)',line)
	return int(results[0][1]+(results[0][2]))


def validatePOSBitmap(bitmap,validationType = posDetails.VALIDATE_BITMAP_TYPE["POST_LOGIN"]):#login = True
	
	validValue = None
	validBitmap = None

	table = posDetails.VALIDATE_BITMAP_TYPE
	if validationType == table["PRE_ACTIVATION"]:
		validValue = posDetails.VALID_PRE_ACTIVATION_VALUE
		validBitmap = posDetails.PRE_ACTIVATION_BITMAP
	elif validationType == table["PRE_LOGIN"]:
		validValue = posDetails.VALID_PRE_LOGIN_VALUE
		validBitmap = posDetails.PRE_LOGIN_BITMAP
	elif validationType == table["POST_LOGIN"]:
		validValue = posDetails.POSITIVE_BITMAP_VALUE
		validBitmap = posDetails.POSITIVE_BITMAP
	else:
		validValue = posDetails.POSITIVE_BITMAP_VALUE
		validBitmap = posDetails.POSITIVE_BITMAP

	# checking for validity
	isValid = (bitmap & validValue) == validValue
	isNegative = (bitmap & posDetails.NEGATIVE_BITMAP_VALUE) != 0

	if isValid and not isNegative:
		return True,None

	# sending appropriate error code
	if not isValid:
		'''
			Finds the least bit that is set to 0
			format(value, '0' + len() + 'b') -> bitmap in binary format padded with 0s if required
			format().rfind('0') -> gives right index where '0' is encountered for the first time
			[-len(validBitmap):] -> We have to do rfind only on the substring which is of length validBitmap from right
			len() - format() -> we need index calculated from right side. so len - index
			len() - format() - 1 -> list indexing starts from 0
		'''
		
		return False, validBitmap[len(validBitmap) - (format(bitmap, '0' + str(len(validBitmap)) + 'b')[-len(validBitmap):].rfind('0')) - 1]

	bitmap >>= len(posDetails.POSITIVE_BITMAP + posDetails.NEUTRAL_BITMAP)

	if isNegative:
		#Finds the least bit that is set to 1. Check above description
		return False, posDetails.NEGATIVE_BITMAP[len(posDetails.NEGATIVE_BITMAP) - format(bitmap, '0' + str(len(posDetails.NEGATIVE_BITMAP)) + 'b').rfind('1') - 1]
	
'''
	It authorizes the request
'''
def isAuthorisedRequest(clientId, sessionToken, deviceId, updateLastActivity = True):
	'''
		updateLastActivity is used for logout request
	'''

	#Validation of clientId
	details = db.session.query(posDetails).filter(posDetails.clientId == clientId).first()
	if details == None: return False, 'INVALID_CREDENTIALS'

	isValid, errorCode = validatePOSBitmap(details.bitmap)
	if not isValid: return False, errorCode
	
	#Validation of DeviceId
	if details.deviceId != deviceId: return False, 'INVALID_CREDENTIALS'

	username = ''
	try:
		'''
			Validation of sessionToken:
			1. Time is in UTC
			2. Only one user may be logged into a POS terminal at a point of time
		'''
		username, signTime = current_app.signer.unsign(sessionToken, return_timestamp = True)
		
		if details.loggedInUser == '' or details.loginTime == None or \
			username != details.loggedInUser:
			return False, 'NOT_LOGGED_IN'

		'''
			Using timestamp and loginTime to prevent user from using previous session tokens
			#TODO Note that a session token generated just 1 second prior to login time would be valid. Only this time can be brought down to milli seconds but not removed entirely
			Storing the session token in db would remove that barrier as well. But, it would remove the main purpose of not storing the session Keys. Think of a permanent solution for this.
			Note: Second case isn't possbile. just wrote it in case some one gets the secret key and generates sessionTokens with later timestamps.. doesn't affect the efficiency because of OR implementation
		'''
		if details.loginTime != signTime:
			return False, 'INVALID_CREDENTIALS'
			
	except itsdangerous.BadSignature:
		return False, 'INVALID_CREDENTIALS'
			
	if (datetime.utcnow() - details.lastActivity).seconds > current_app.config['POS_SESSION_TIME']:
		return False, 'SESSION_TIMEOUT' #session timed out.. send instruction to logout & destroy the session token.

	if updateLastActivity == True:
		details.lastActivity = datetime.now().strftime("%Y-%m-%d %X")
	
	return True, username

def canIdoTransaction(clientId, ttype, username):
	'''
		This function assumes that the request is already authenticated using isAuthorised()
		Locks the posDetails row for updating the current transaction type and releases.
		Note: db.session.commit() has to be executed before control leaving this very function
	'''
	details = None
	try:
		#This call comes after authorization so status can't be None
		details = db.session.query(posDetails).filter(posDetails.clientId == clientId).with_lockmode('update').first()#TODO can timeout be used for lock waiting?
	
		if details == None:
			current_app.logger.critical('posDetails is None\n' + 'Client ID: ' + str(clientId))
			return False, None

		if details.getTransactionStatus():
			if details.transactionType == TIP:
				if ttype in [PURCHASE, VOID, REFUND, BALANCE_ENQUIRY, PREAUTH]:
					details.transactionType = ttype
				db.session.commit()
				return False, None
		
			current_app.logger.warn('Suspicious activity. Transaction request(' + str(ttype) + ') while some transacton activity is going on\n'+ 
					'Client ID:' + str(clientId) + ' User: ' + username)
			db.session.commit()
			return False, None
		
		details.setTransactionStatus(True)
		details.transactionType = ttype
		db.session.commit()
		return True, details
		
	except Exception as e:#db.session.query returns OperationError for both Lock wait timeout exceeded & Unknown Mysql server host; sqlalchemy.exc.Operation can be used to catch them as well
		#when try for the lock fails return busy
		print e
		try:
			details = db.session.query(posDetails).filter(posDetails.clientId == clientId).first()

			if details and details.transactionType == TIP:
				if ttype == PURCHASE:
					details.transactionType = ttype#TODO why is only Purchase stored here?
				db.session.commit()
				return False, None
			else:
				current_app.logger.warn('Suspicious activity. Transaction request(' + str(ttype) + ') while some transacton activity is going on\n'+ 
					'Client ID:' + str(clientId) + ' User: ' + username)
				db.session.commit()
				return False, None
		except Exception as e:
			return False, None

	return False, None
			
def validateSequenceCode(seqCode, details):
	'''
		This function assumes that the request is already authenticated using isAuthorised()
		and permitted using canIdoTransaction()
		Validates the sequenceCode(local)
	'''

	if seqCode < details.minSequenceCode or seqCode > details.sequenceCode:
		if not current_app.config['TESTING']:
			current_app.logger.critical('Invalid Sequence code for Client Id: ' + str(details.clientId) + str(details))
		else:
			current_app.logger.warning('Invalid Sequence code for Client Id: ' + str(details.clientId) + str(details))
		output = {'errorCode' : ERROR_CODES['INVALID_SEQUENCE_CODE'], 'sequenceCode' : details.sequenceCode}
		details.setTransactionStatus(False) #Changing the status of ClientId to Relaxed
		db.session.commit()
		return False, json.dumps(output)

	return True, details


def disableLastTransaction(clientId, ttype, seqCode, rowId = 0):#0 for settle request and settle report
	'''
		Checks if there is already a transaction done with sequenceCode = seqCode
		If present, disables the respective transaction. Else, ignore
		Modify the Last Transaction entry to point to current transaction
		
		Note: 
		1. LastTransaction table is used keeping other transactions such as refund, pre-auth in mind 
		and for easy extension. A simple solution would suffice for Purchase/Void/Tip transactions. THINK
		2. DB Commit is required after calling this function
	'''
	lastTransaction = db.session.query(LastTransaction).filter(LastTransaction.clientId == clientId).first()
		
	if lastTransaction == None:
		#This case shouldn't arise as an entry should've been added in activate request
		#Note that in case there is a transaction with this sequenceCode, it will not be disabled in this case
		current_app.logger.critical('Last transaction Entry not found for clientId : ' + str(clientId))
		lastTransaction = LastTransaction(clientId, ttype, seqCode, rowId)
		db.session.add(lastTransaction)
		return
	
	if lastTransaction.sequenceCode == seqCode:#If request has sequence code of last transaction, disable the previous transaction
		if lastTransaction.transactionType == PURCHASE:
			txn = Transaction.query.filter_by(id = lastTransaction.rowId, ttype = Transaction.TTYPE['PURCHASE']).first()
			if txn is None: 
				current_app.logger.critical('Invalid Transaction status for ' + str(clientId) + ':' + str(txn.status))
				return
			txn.setEnabled(False)

		elif lastTransaction.transactionType == VOID:
			voidEntry = Void.query.filter_by(id=lastTransaction.rowId).first()
			voidEntry.setEnabled(False)

			txn = Transaction.query.filter_by(id=voidEntry.purchaseId).first()
			if not txn.isVoided():
				current_app.logger.critical('Invalid Transaction status for ' + str(clientId) + ':' + str(txn.status))
				return
			txn.setVoid(False)

		elif lastTransaction.transactionType == TIP:

			tip = db.session.query(Tip).filter(Tip.id == lastTransaction.rowId).first()
			tip.setEnabled(False)
			
			txn = Transaction.query.filter_by(id = tip.purchaseId).first()
			if not txn.isTipped():
				current_app.logger.critical('Invalid Transaction status for ' + str(clientId) + ':' + str(txn.status))
				return
			txn.setTip(False)

	#Change the last transaction entry details in database
	lastTransaction.transactionType = ttype
	lastTransaction.sequenceCode = seqCode
	lastTransaction.rowId = rowId

def updateToNewBatch(details_entry, report, username, clientId, sequenceCode, plutusSequenceCode):
	'''
		#NOTE not being used currently
		Changes the status of settled transactions to settled and moves them to settled database
		This function returns {} if successfully executed. Else returns a {'errorCode' : 'ERROR_CODE'} 
	'''
	if report.has_key('errorCode'):
		return report
	#variable initializations
	numSales, mynumSales = 0, 0
	saleAmount, mysaleAmount = 0, 0
	numTips, mynumTips = 0, 0
	tipAmount, mytipAmount = 0, 0
	numVoids, mynumVoids = 0, 0
	voidAmount, myVoidAmount = 0, 0
	totalAmount, mytotalAmount = 0, 0


	#list of entries to be added to database
	settlement_entries = []

	for bankName in report:
		if not isinstance(report[bankName], dict):
			continue
				
		for TID in report[bankName]:
			
			summary = report[bankName][TID]
			batchNumber = int(summary['batch'])
			tid = TID
			mid = summary['mid']

			s, sa = int(summary['sale']['count']), getTotalAmount(summary['sale']['total'])
			#t,ta = summary['tip']['count'],summary['tip']['total']
			t, ta = 0,0
			v, va = int(summary['void']['count']), getTotalAmount(summary['void']['total'])
			nta = getTotalAmount(summary['netTotal'])

			numSales += s
			saleAmount += sa
			#numTips += t
			#tipAmount += ta
			numVoids += v
			voidAmount += va
			totalAmount += nta

			settlement_entries.append(settlement_table(username, clientId, sequenceCode, plutusSequenceCode, getCurrentTimeStamp(), \
					bankName, batchNumber, tid, mid, s, v, t, sa, va, ta, nta, details_entry.batch))
	

	#your amounts,i am anxious!
	purchaseEntries = Transaction.query.filter_by(clientId=clientId).all()
	
	for txn in purchaseEntries:
		#Only approved transactions come into reconciliation
		if txn.isApproved():
			if txn.isNormalPurchase():
				mynumSales += 1
				mysaleAmount += int(txn.amount)
			elif txn.isTippedPurchase():
				mynumSales += 1
				mynumTips += 1
				mysaleAmount += int(txn.amount)
				mytipAmount += int(txn.tipAmount)
			elif txn.isVoidedPurchase():
				mynumSales += 1
				mynumVoids += 1
				myVoidAmount += int(txn.amount)
				mysaleAmount += int(txn.amount)
			elif txn.isVoidedTipTxn():
				mynumSales += 1
				mynumVoids += 1
				#mynumTips += 1
				mysaleAmount += int(txn.amount)
				myVoidAmount += int(txn.amount) + int(txn.tipAmount)
				#mytipAmount += int(txn.tipAmount)	
			else:
				current_app.logger.warn('Status of txn entry is set to invalid value of ' + str(txn.status) + \
							'\n. Client Id: ' + str(clientId) + ' | Row ID: ' + str(txn.id))

	mytotalAmount = mysaleAmount - myVoidAmount #ERRORPRONE		
	voidCount = Void.query.filter(Void.clientId == clientId, Void.conditions('VALID')).count()#TODO This doesn't move disabled transactions
	tipCount = Tip.query.filter(Tip.clientId == clientId, Tip.conditions('VALID')).count()
	
	if voidCount != mynumVoids:
		current_app.logger.critical('Number of void entries mismatch with Plutus settle req ' + str(clientId))	#TODO write settlement response to a file and link it here; can be sent as attachment to the email also
		return errorMessageD('SERVER_ERROR')

	if tipCount != mynumTips:
		current_app.logger.critical('Number of tip entries mismatch with Plutus settle req ' + str(clientId))
		return errorMessageD('SERVER_ERROR')					

	voidEntries = Void.query.filter_by(clientId=clientId).all()
	tipEntries = Tip.query.filter_by(clientId=clientId).all()

	if numSales != mynumSales or saleAmount != mysaleAmount or \
		numTips != mynumTips or tipAmount!= mytipAmount or \
		numVoids != mynumVoids or voidAmount != myVoidAmount or \
		totalAmount != mytotalAmount:
		
		current_app.logger.critical('cross check with plutus not ok ' + str(clientId))
		return errorMessageD('SERVER_ERROR')
			
	
	#add to the settlement tables after verification is successful
	for settlement_row in settlement_entries:
		db.session.add(settlement_row)

	#Change status of Purchase entries to Settled		
	for purchase in purchaseEntries:
		purchase.setSettled(True)

	#Add recently settled entries to settled database
	from src.models import SettledTransaction, SettledVoid, SettledTip

	for purchase_row in purchaseEntries:
		stldPurchase = SettledTransaction(purchase_row, details_entry.batch)
		db.session.add(stldPurchase)

	for void_row in voidEntries:#ERRORPRONE foreign key restraint..
		stldVoid = SettledVoid(void_row)
		db.session.add(stldVoid)

	for tip_row in tipEntries:
		stldTip = SettledTip(tip_row)
		db.session.add(stldTip)

	db.session.commit() #TODO can remove

	#Cleaning up settled entries in unsettled database
	for tip_row in tipEntries:
		db.session.delete(tip_row)

	for void_row in voidEntries:
		db.session.delete(void_row)

	for purchase_row in purchaseEntries:
		db.session.delete(purchase_row)

		# Update batch count in posDetails. This count is common in the series of settlements made by this settlement request
	details_entry.batch += 1
	notifyMerchant(details_entry, settlement_entries, purchaseEntries)

	db.session.commit()
	#TODO send a sms and email to the merchant regarding settlement!
	return {}


def resolveConflict_dummy(details, username, conflict, reqSequenceCode):
	'''
		Resolves the conflict of a particular clientId.
		Currently used in purchase, VoidSearch modules
		#TODO move to another package
	'''
	output = {}
	if conflict:
		if details.sequenceCode == reqSequenceCode + 1:
			output['settleCode'] = 'l'
		else:
			current_app.logger.critical('Conflict and sequenceCode imply conflicting states. Request:\n' + str(request.get_json()) + \
			'\n SeqCode: ' + str(details.sequenceCode))
			output = {'errorCode': ERROR_CODES['INVALID_SEQUENCE_CODE']}

	return output

def resolveConflict(details, username, conflict, reqSequenceCode):
	'''
		Resolves the conflict of a particular clientId.
		Currently used in purchase, VoidSearch modules
		#TODO Plutus not in use!! Doesn't work with current code
	'''
	if not current_app.config['PLUTUS_STATUS']:
		return resolveConflict_dummy(details, username, conflict, reqSequenceCode)

	output = {}

	if details.inSettleConflict:
		'''
			If there is a conflict at server side, it has to resolved regardless of the client's state.
			Note: 
			i. we send the settleCode tag even if conflict = False is sent in the request. 
			ii. Code enters this block only if last transaction is SettleRequest #TODO move it into a function
		'''
		from src.transactions import settleReport#Dont take it outside #CIRCULAR IMPORT
		settleRep = settleReport.SettleReport()
		settleReportResponse = settleRep.getSettleStatus(details)
		if 'isSettled' in settleReportResponse:
			if settleReportResponse['isSettled']:# Update db -> move settled transactions to settled db		
				
				output = updateToNewBatch(details, settleReportResponse, username, details.clientId, \
								details.sequenceCode - 1, details.plutusSequenceCode - 1)

				if not len(output):#No error found during updateToNewBatch. 
					details.inSettleConflict = False#Note that egsttle conflict is not removed if there is any error found
				
				output['settleCode'] = 'l'
			else:
				output['settleCode'] = 'c'
				details.inSettleConflict = False
		else:
			output = settleReportResponse

		if not conflict:
			current_app.logger.critical('Fraud attempt or wrong implementation at client side.\nConflict at server, but request contains conflict = False. Request: \n' + str(request.get_json()))
			output = {'errorCode': ERROR_CODES['INVALID_TRANSACTION']}
	else:
		if conflict:
			if details.sequenceCode == reqSequenceCode + 1:
				output['settleCode'] = 'l'
			else:
				current_app.logger.critical('Conflict and sequenceCode imply conflicting states. Request:\n' + str(request.get_json()) + \
				'\n SeqCode: ' + str(details.sequenceCode))
				output = {'errorCode': ERROR_CODES['INVALID_SEQUENCE_CODE']}

	return output


def notifyMerchant(details, settlements, txns):

	class CumulativeEntry:
		numSales = 0
		numVoids = 0
		numTips = 0
		numRefunds = 0
		numPreAuths = 0

		saleAmount = 0
		voidAmount = 0
		tipAmount = 0
		refundAmount = 0
		preAuthAmount = 0

		totalAmount = 0


	final = CumulativeEntry()
	for settlement in settlements:
		final.numSales += settlement.numSales
		final.numVoids += settlement.numVoids
		final.numTips += settlement.numTips
		final.numRefunds += settlement.numRefunds
		final.numPreAuths += settlement.numPreAuths

		final.saleAmount += settlement.saleAmount
		final.voidAmount += settlement.voidAmount
		final.tipAmount += settlement.tipAmount
		final.refundAmount += settlement.refundAmount
		final.preAuthAmount += settlement.preAuthAmount

		final.totalAmount += settlement.totalAmount
	
	rend_html = render_template('reports/settlement_report.html', settlements = settlements, final = final, txns = txns)
	tempFile = current_app.config['TEMP_FOLDER'] + str(details.clientId) + otp_generator(12)
	convertHtmlToPdf(rend_html, tempFile)

	############### Sending email and sms to merchant###############
	#TODO move this to a different thread. These are blocking calls
	to = details.merchantDetails.email
	mobile = details.mobileNumber

	if current_app.config['TESTING'] or current_app.config['DEBUG']:
		current_app.logger.debug('Email: ' + str(to) + ' SMS:' + str(details.mobileNumber))
		to = current_app.config['DEVELOPERS'][0]#List doesn't work here
		mobile = current_app.config['SETTELEMENT_SMS']

	try:
		send1('Settlement Report', to, 'Please find it attached', tempFile, 'Settlement.pdf')
	except Exception as e:
		current_app.logger.error('Following exception occured while sending email to '+to+'\n'+str(e))
		
	sendSettlementSms(final.totalAmount, mobile)
	os.remove(tempFile)

# return error code for a message or returns None if not found.
def getErrorCode(errorMessage):
	for entry in ERROR_CODES:
		if ERROR_CODES[entry] == errorMessage:
			return entry
	return None
