from datetime import datetime
from flask import request,current_app
import hashlib

from ext import db
from src.mAuthenticate import common
from src.models import posDetails, posUsers
from src.utils import crypto
from src.utils.validate import validateEntry

from common import Log,logEvent
import simplejson as json

class ShouldChangePasswd:

	SHOULD_CHANGE_PASSWORD_TAGS = {
		'terminalInfo': dict,
		'payload': dict,
		}#removed appversion tag.. in the assumption that checking appVersion during login request is good to go.

	TERMINAL_TAGS = {
		'deviceId': 'DEVICE_ID',
		#'timeStamp': unicode,
		}

	PAYLOAD_TAGS = {
		'clientId': int ,
		'username': 'USER',
		'sessionToken': 'SESSION',
		'oldPasswd': 'PASSWORD', # oldPasswd is necessary to invalidate soft requests
		'newPasswd': 'NEW_PASSWORD',
		}
	
	def __init__(self):
		self.json_data = request.get_json()		
		
	def isValidRequest(self):
		if self.json_data == None:
			return False
	
		if not all (k in self.json_data and validateEntry(self.json_data[k], v) \
							for k, v in self.SHOULD_CHANGE_PASSWORD_TAGS.items()):
			return False 
		
		if not all(k in self.json_data['terminalInfo'] and validateEntry(self.json_data['terminalInfo'][k], v) \
							for k, v in self.TERMINAL_TAGS.items()):
			return False

		if not all(k in self.json_data['payload'] and validateEntry(self.json_data['payload'][k], v) \
							for k, v in self.PAYLOAD_TAGS.items()):
			return False

		return True

	def populateEntries(self):
		self.deviceId = self.json_data['terminalInfo']['deviceId']
		self.timeStamp = common.getCurrentTimeStamp()

		self.clientId = self.json_data['payload']['clientId']
		self.username = self.json_data['payload']['username']
		self.sessionToken = self.json_data['payload']['sessionToken']
		self.oldPasswd = self.json_data['payload']['oldPasswd']#input is SHA-256 hash of plain password
		self.newPasswd = self.json_data['payload']['newPasswd']#input is plain password
		
	def authorise(self):
		details = posDetails.query.filter_by(clientId=self.clientId).first()

		user = posUsers.query.filter_by(username=self.username, clientId=self.clientId).first()
		if user is None: return common.errorMessage('INVALID_CREDENTIALS')

		if not crypto.verify_passwd(self.oldPasswd, user.passwd, user.salt):
			return common.errorMessage('INVALID_CREDENTIALS')

		# this endpoint request should be processed only when shouldChange password is set to true
		if not user.shouldChangePassword: return common.errorMessage('INVALID_CREDENTIALS')

		# getting the random number set in the session token
		randomNumber, signTime = current_app.signer.unsign(self.sessionToken, return_timestamp=True)

		# verifying the random number
		if randomNumber != user.rn:
			return common.errorMessage('INVALID_CREDENTIALS')
		
		# Making sure new password is not same as old password.
		passwd, salt = crypto.hash_passwd(crypto.sha256(self.newPasswd), user.salt)
		if passwd == user.passwd: return common.errorMessage('NEW_PASSWORD_IS_OLD_PASSWORD')

		#Storing password with new salt
		passwd, salt = crypto.hash_passwd(crypto.sha256(self.newPasswd))
		rn = common.generateRandomNumber()
		user.update(commit=False, passwd=passwd, salt=salt, shouldChangePassword=False, rn=rn)

		# making the user login	
		bitmap = details.bitmap | posDetails.BITMAP['LOGIN_STATUS']
		details.update(commit = False, loggedInUser = self.username, bitmap = bitmap,\
		 lastActivity = self.timeStamp, loginTime = self.timeStamp)
		
		if details.getTransactionStatus():
			current_app.logger.warn('Transaction Status = True during login for clientId: ' + 
				str(self.clientId) + ' user: ' + self.username)
		details.setTransactionStatus(False)

		# logging should change password event
		event = 'changed temporary credentials and logged in'
		logEvent(event,user.username,details,Log.info)

		sign = current_app.signer.sign(self.username)

		db.session.commit()
		return json.dumps({'mesg':'Success', 'sessionKey':sign})


	def process(self):
		if not self.isValidRequest(): return common.errorMessage('INVALID_REQUEST')
		self.populateEntries()
		return self.authorise()
