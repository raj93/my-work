from flask import render_template, request, flash, redirect, url_for,current_app
from flask.ext.login import current_user

from src.utils.crypto import hash_passwd

from ext import db
from src.portal import ADMIN, COMPANY
from src.models import portalLogin, Company

from forms import AddAgentForm, AddCompanyForm, EditCompanyForm

from principalConstants import *
from src.utils.ses_email import send1
from portalHelper import getActionListForCompany,populateBaseRoleFields,logEvent

"""
	View all the companies added in the hierarchy.
	Company is the topmost level in the hierarchy.
"""
def viewCompanies():

	# checking permission
	if not view_company_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	companies = []
	if current_user.role == ADMIN:
		companies = Company.query.all()
	else:
		#code should not come here if permissions are right
		return redirect(url_for('basePortal.viewProfile'))

	logEvent("Viewing all companies")
	return render_template('users/view_companies.html',companies=companies)

"""
	Add a company.
"""
def addCompany():
	#checking permission
	if not add_company_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	form = AddCompanyForm()

	if form.validate_on_submit():
		
		# adding entry to portal login
		import hashlib
		from src.utils.crypto import generateRandomPassword
		password = generateRandomPassword(10)
		hashedPassword,salt = hash_passwd(hashlib.sha256(password).hexdigest())

		login = portalLogin(form.username.data,hashedPassword,salt,COMPANY,0,True,True);
		login.save(commit = False)

		#adding entry to company table
		company = Company(form.username.data,form.fullname.data,'end of universe',form.mobileNumber.data,form.email.data, \
			form.bankName.data,form.bankBranch.data,form.ifsc.data,form.acNumber.data,form.MDR_Debit.data, \
			form.MDR_Credit.data,form.MDR_CreditGold.data,form.address1.data,form.address2.data,form.city.data, \
			form.district.data,form.state.data,form.pincode.data,form.country.data)
		company.save(commit = False)

		# send password to email
		subject = "Your credentials with temporary password"
		to = company.email
			
		body = "username and password are "+"\n"+ \
				"username: "+ company.username + "\n"+ \
				"password: "+ password + "\n" +\
				"You can access your portal at "+\
				request.url_root[:len(request.url_root)-1] + url_for('basePortal.login')

		logEvent("added company %s"% company.username)

		try:
			db.session.commit()
		except Exception as e:
			current_app.logger.critical('Following exception occured while commiting changes to db')
			current_app.logger.critical(str(e))

			flash("server has faced problem in processing this request. Please try again after some time")
			return redirect(url_for('basePortal.viewProfile'))

		send1(subject, to, body,None,None)

		flash("added company successfully")
		return redirect(url_for('basePortal.viewCompany',name=company.username))

	if current_app.config['TESTING']:
		from portalHelper import populateUserForm
		populateUserForm(form)

	return render_template("company/add_company.html", form=form)

"""
	Edit a given company
"""
def editCompany(company):
	#2 choices are there to provide company argument.
	#one is by url routing and other is by adding an argumnet to get request

	#checking permission
	if not edit_company_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# checking whether requested company exists or not
	company = Company.query.filter_by(username = company).first()
	if company is None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	form = EditCompanyForm(obj=company)
	
	if form.validate_on_submit():
		company = populateBaseRoleFields(company,form)
		company.update(commit = False)
		logEvent("edited company %s"% company.username)
		db.session.commit()

		flash('updated company '+company.username + ' successfully')
		return redirect(url_for('basePortal.viewCompany',name=company.username))

	return render_template("company/edit_company.html",form=form,company=company)

"""
	View a given company
"""
def viewCompany(company):

	# view_company_permission can be given only to user with base role admin
	#checking permission
	if not view_company_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	company = Company.query.filter_by(username = company).first()
	if company is None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))
	
	# adding necessary actions
	actionList = getActionListForCompany(company)

	logEvent("viewed company %s"% company.username,onActionUser=company.username)
	db.session.commit()

	return render_template("company/view_company.html", company=company, actionList=actionList)

