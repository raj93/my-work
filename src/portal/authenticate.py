from flask import render_template, request, flash, redirect, url_for, current_app,session
from flask.ext.login import login_user, logout_user, current_user
from src.models import portalLogin,portalUserLogs
from src.utils.crypto import verify_passwd,hash_passwd,otp_generator
from datetime import datetime
from ext import db

from src.portal import ADMIN, COMPANY, AGENT, MERCHANT
from flask.ext.principal import identity_changed, Identity, AnonymousIdentity

from forms import LoginForm,ChangePasswordForm,BaseProfileForm,ShouldChangePasswordForm,ForgotPasswordForm,ResetPasswordForm

from principalConstants import *
from portalHelper import getDetailsOfPortalLoginEntry,logEvent, getCurrentTimeStamp, getSha256Hash
import string,random


"""
	Login has two purposes to fulfil
	1) Logging in a user.
	2) set up changePassword on user if he has to change password
"""

def login():
	form = LoginForm()

	if form.validate_on_submit():
		user = portalLogin.query.filter_by(username = form.username.data).first()

		# checking for correct credentials
		if user is None:
			flash("Invalid credentials")
			return render_template('common/login.html',form=form)

	
		# Checking for temporary account hold
		if not user.isActivated:
			diff = (datetime.utcnow() - user.lastHoldedTime).seconds
			if diff < current_app.config['USER_HOLD_TIME']:
				flash("your account has been temporarily blocked, Please try after " + str((current_app.config['USER_HOLD_TIME']-diff)/60) + ' minutes')
				return render_template('common/login.html',form=form)
			else:
				user.update(commit=False,isActivated=True)
				db.session.commit()

		if verify_passwd(form.password.data, user.password, user.salt) == False:
			user.update(commit=False, invalidAttempts=user.invalidAttempts+1)
			db.session.commit()
			
			if user.invalidAttempts == current_app.config['INCORRECT_PASSWORD_ATTEMPTS']:
				flash("your account has been blocked for "+ str(current_app.config['USER_HOLD_TIME']/60) + ' minutes')
				user.update(commit=False, lastHoldedTime=getCurrentTimeStamp(), invalidAttempts=0, isActivated=False)
				db.session.commit()
			else:
				flash("Invalid credentials")
				
			return render_template('common/login.html',form=form)

				# resetting session keys
		session.pop('username',None)
		session.pop('rn',None)
		
		#checking whether the user needs to change his password
		if user.shouldChangePassword == True:
			rn = random.randint(1,1000) #should make it more secure
			session['username'] = user.username
			session['rn'] = str(rn)

			user.update(commit = False,rn = rn)
			db.session.commit()

			return redirect(url_for('basePortal.shouldChangePassword'))
		
		if login_user(user):
			session['loginTime'] = getCurrentTimeStamp()
			logEvent("LOGGED_IN")

			user.update(commit=False,loginTime=session['loginTime'])
			db.session.commit()

			identity_changed.send(current_app._get_current_object(), identity=Identity(user.id))
			print session['loginTime'],user.loginTime
			flash('logged in successfully')
			return redirect(url_for('basePortal.viewProfile'))

	return render_template('common/login.html',form=form)
		

"""
logs out a user.
"""

def logout():
	# logging the event and making aprprt changes
	# only when requested by an non anonymous user
	if not current_user.is_anonymous():		
		logEvent("LOGGED_OUT")
		session.pop('loginTime',None)

		try:
			current_user.update(commit=False,loginTime=None)
			db.session.commit()
		except Exception as e:
			current_app.logger.error(str(e))
		


		logout_user()

		# Remove session keys set by Flask-Principal
		for key in ('identity.id', 'identity.auth_type'):
			session.pop(key, None)

		# Tell Flask-Principal the user is anonymous
		identity_changed.send(current_app._get_current_object(), identity=AnonymousIdentity())
		
		flash('you have successfully logged out')
	return redirect(url_for('basePortal.login'))

"""
	Change Password for the user
"""

def changePassword():
	form = ChangePasswordForm()
	if form.validate_on_submit():

		user = portalLogin.query.filter_by(username = current_user.username).first()

		# checking the existence of user		
		if user == None:
			current_app.logger.warn("user %s is meddling with changePassword functionlaity" % user.username)
			return redirect(url_for('basePortal.viewProfile'))

		
		passwd,salt = hash_passwd(getSha256Hash(form.newPassword.data),user.salt)
		user.update(commit = False,password = passwd)

		logEvent("Changed password")
		db.session.commit()

		flash("password changed successfully")
		return redirect(url_for('basePortal.viewProfile'))
	return render_template('common/changepasswd.html',form=form)

"""
	View Profile of a user.
"""
def viewProfile():
	print session
	# mapping the required entry
	entry = getDetailsOfPortalLoginEntry(current_user)	

	if entry == None:
		flash("please login")
		return redirect(url_for('basePortal.login'))
	
	return render_template('common/vanilla_profile.html',entry=entry)

"""
	Edit Profile of a user.
"""

def editProfile():

	# mapping the required entry
	entry = getDetailsOfPortalLoginEntry(current_user)

	if entry == None:
		flash("please login")
		return redirect(url_for('basePortal.login'))
	
	form = BaseProfileForm(obj=entry)

	if form.validate_on_submit():
		#get data from the form and update in db
		form.populate_obj(entry)
		entry.update(commit = False)
		logEvent("edited profile")
		db.session.commit()

		return redirect(url_for('basePortal.viewProfile'))

	return render_template('common/edit_profile.html',form=form)


"""
	When we want a user to change his password we set "shouldChangePassword" flag
	in the login entry. Now when the user tries to login his "username" and "rn"
	(random gen num) are set in the session and is redirected to this page.
"""

def shouldChangePassword():
	
	if 'username' in session and 'rn' in session and current_user.is_anonymous():
		user = portalLogin.query.filter_by(username = session['username']).first()

		# To avoid soft requests
		if user is None or user.shouldChangePassword == False:
			return redirect(url_for('basePortal.login'))

		#check rn matches
		if session['rn'] == user.rn:
			form = ShouldChangePasswordForm()
			if form.validate_on_submit():
				passwd,salt = hash_passwd(getSha256Hash(form.newPassword.data),user.salt)

				# checking whether the newly set password is not equal to old password
				if user.password == passwd:
					#form.confirmPassword.data = ""
					#form.newPassword.data = ""
					form.confirmPassword.errors.append('new password should not be same as old password')
					return render_template('common/should_change_password.html',form=form)

				#change the password,random number 
				user.update(commit=False, password=passwd, shouldChangePassword=False)				
				user.update(commit = False)
				logEvent("Should change password condition fulfilled")
				db.session.commit()

				#removing temp keys from session
				session.pop('username',None)
				session.pop('rn',None)

				# logging in user
				if login_user(user):
					logEvent("LOGGED_IN")
					session['loginTime'] = getCurrentTimeStamp()
					user.update(commit=False,loginTime=session['loginTime'])
					db.session.commit()

					identity_changed.send(current_app._get_current_object(), identity=Identity(user.id))
					
					flash('logged in successfully')
					return redirect(url_for('basePortal.viewProfile'))

				return redirect(url_for('basePortal.viewProfile'))
			return render_template('common/should_change_password.html',form=form)
		
	return redirect(url_for('basePortal.login'))

from itsdangerous import TimestampSigner

"""
	Forgot Password functionlaity for a user.
"""

def forgotPassword():
	form = ForgotPasswordForm()	

	if form.validate_on_submit():

		# this is tested in the form 
		user = portalLogin.query.filter_by(username = form.username.data).first()
		if user is None:
			print 'here'
			return redirect(url_for('basePortal.login'))

		# get the email of logged in user
		entry = getDetailsOfPortalLoginEntry(user)
		email = None # entry may be present but not email
		if entry != None:
			email = entry.email

		# if email is not set log the event
		if email == None:
			current_app.logger.critical("user "+ user.username +" exists in the data base with out email entry present.")

		# generate token
		token = otp_generator(25,chars=string.ascii_lowercase+string.ascii_uppercase + string.digits)

		# sign the token
		ts = TimestampSigner(current_app.config['FORGOT_PASSWORD_TOKEN_KEY'])
		signedToken = ts.sign(token)
		parts = signedToken.split('.')

		tosend = parts[1]
		for i in range(2,len(parts)):
			tosend += '.'+parts[i]

		# store the token and signed token
		user.update(commit=False, resetToken=token, resetSignedToken=tosend)
		logEvent("reset password token requested",user=user)
		db.session.commit()
		
		if 'https' in request.url:
			email_link = 'https://'+request.host+url_for('basePortal.resetPassword')+"?token="+tosend
		else:
			email_link = 'http://'+request.host+url_for('basePortal.resetPassword')+"?token="+tosend

		subject = "Password reset request"
		body = "Click the following link to reset your password  "+email_link # TODO make body beautiful
		print email_link
		from src.utils.ses_email import send1
		try:
			if email:
				to = email
				send1(subject, to, body,None,None)
		except Exception as e:
			current_app.logger.error('Following exception occured while sending email to '+ to +'\n'+str(e))

		flash('reset link successfully sent to associated username')
		return redirect(url_for('basePortal.login'))

	return render_template('common/forgot_password.html',form=form)


"""
	Reset password once the user logs in.
"""

def resetPassword():
	
	if 'token' in request.args:
		signedToken = request.args['token']
		entry = portalLogin.query.filter_by(resetSignedToken=signedToken).first()
		ts = TimestampSigner(current_app.config['FORGOT_PASSWORD_TOKEN_KEY'])

		try:
			birthTimeStamp =  ts.unsign(entry.resetToken+'.'+signedToken,return_timestamp = True)[1]

			# linke expired
			if (datetime.now() -birthTimeStamp).seconds > current_app.config['FORGOT_PASSWORD_LINK_EXPIRY_TIME']:
				flash('link expired')
				return redirect(url_for('basePortal.login'))
			else:
				form = ResetPasswordForm()
				if form.validate_on_submit():

					# hash it add salt and store it in password field
					finalPassword,salt = hash_passwd(getSha256Hash(form.password.data),entry.salt)

					# Making sure new password is not same as old password
					if entry.password == finalPassword:
						form.confirmPassword.errors.append('new password should not be same as old password')
						return render_template('common/reset_password.html',form=form,token=request.args['token'])

					# this link should be expired now.
					# entry.resetSignedToken = None # 1 in 62^20 but why to take a chance.
					entry.update(commit=False, password=finalPassword, resetSignedToken=None,shouldChangePassword=False)
					logEvent("successfuly used the reset password link", user=entry)
					db.session.commit()

					flash('password updated successfuly')
					return redirect(url_for('basePortal.login'))

				return render_template('common/reset_password.html',form=form,token=request.args['token'])

		except Exception as e:
			flash('link expired')
			return redirect(url_for('basePortal.login'))
	else:
		return redirect(url_for('basePortal.login'))