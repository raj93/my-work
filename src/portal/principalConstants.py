"""
This file should have the permissions that we create to restrict access.
"""

from flask.ext.principal import Permission, RoleNeed, ActionNeed

class URLPermission(Permission):
	url = ""
	name = ""
	

admin_need = RoleNeed('admin')
company_need = RoleNeed('company')
agent_need = RoleNeed('agent')
merchant_need = RoleNeed('merchant')

admin_permission = Permission(admin_need)
company_permission = Permission(company_need)
agent_permission = Permission(agent_need)
merchant_permission = Permission(merchant_need)

view_transactions_need = ActionNeed('viewTransactions')
view_transactions_permission = Permission(view_transactions_need)

view_stats_need = ActionNeed('viewStats')
view_stats_permission = Permission(view_stats_need)

call_need = ActionNeed('call')
call_permission = Permission(call_need)#Only given to Admin Customer care


#ave company
view_company_need = ActionNeed(ActionNeed('viewCompany'))
view_company_permission = Permission(view_company_need)

add_company_need = ActionNeed('createCompany')
add_company_permission = Permission(add_company_need)

edit_company_need = ActionNeed('editCompany')
edit_company_permission = Permission(edit_company_need)#Manage company also comes into this
edit_company_permission.name = "edit"
edit_company_permission.url = 'basePortal.editCompany'

#ave agent
view_agent_need = ActionNeed('viewAgent')
view_agent_permission = Permission(view_agent_need)

add_agent_need = ActionNeed('createAgent')
add_agent_permission = Permission(add_agent_need)

edit_agent_need = ActionNeed('editAgent')
edit_agent_permission = Permission(edit_agent_need)
edit_agent_permission.name = "edit"
edit_agent_permission.url = 'basePortal.editAgent'

#ave merchant
view_merchant_need = ActionNeed('viewMerchant')
view_merchant_permission = Permission(view_merchant_need)

add_merchant_need = ActionNeed('createMerchant')
add_merchant_permission = Permission(add_merchant_need)

edit_merchant_need = ActionNeed('editMerchant')
edit_merchant_permission = Permission(edit_merchant_need)
edit_merchant_permission.name = "edit"
edit_merchant_permission.url = 'basePortal.editMerchant'

verify_merchant_need = ActionNeed('verifyMerchant')
verify_merchant_permission = Permission(verify_merchant_need)

lock_merchant_need = ActionNeed('lockMerchant')
lock_merchant_permission = Permission(lock_merchant_need)

disable_merchant_need = ActionNeed('disableMerchant')
disable_merchant_permission = Permission(disable_merchant_need)


#ave employee
view_employee_need = ActionNeed('viewEmployee')
view_employee_permission = Permission(view_employee_need)
view_employee_permission.name = "View employees"
view_employee_permission.url = "basePortal.viewEmployees"

add_employee_need = ActionNeed('addEmployee')
add_employee_permission = Permission(add_employee_need)
add_employee_permission.name = "Add employee"
add_employee_permission.url = 'basePortal.addEmployee'

edit_employee_need = ActionNeed('editEmployee')
edit_employee_permission = Permission(edit_employee_need)
edit_employee_permission.name = "Edit"
edit_employee_permission.url = 'basePortal.editEmployee'

# ave terminal
view_terminal_need = ActionNeed('viewTerminal')
view_terminal_permission = Permission(view_terminal_need)
view_terminal_permission.name = "View terminals"
view_terminal_permission.url = "basePortal.viewTerminals"

add_terminal_need = ActionNeed('addTerminal')
add_terminal_permission = Permission(add_terminal_need)
add_terminal_permission.name = "Add Terminal"
add_terminal_permission.url = 'basePortal.addPOS'

edit_terminal_need = ActionNeed('editTerminal')
edit_terminal_permission = Permission(edit_terminal_need)

lock_terminal_need = ActionNeed('lockTerminal')
lock_terminal_permission = Permission(lock_terminal_need)


# ave posuser
view_pos_users_need = ActionNeed('viewPosUsers')
view_pos_users_permission  = Permission(view_pos_users_need)

delete_pos_user_need = ActionNeed('deletePosUser')
delete_pos_user_permission = Permission(delete_pos_user_need)

add_pos_user_need = ActionNeed('addPosUser')
add_pos_user_permission = Permission(add_pos_user_need)

lock_pos_user_need = ActionNeed('lockPosUser')
lock_pos_user_permission = Permission(lock_pos_user_need)

unlock_pos_user_need = ActionNeed('unlockPosUser')
unlock_pos_user_permission = Permission(unlock_pos_user_need)


change_pos_user_password_need = ActionNeed('changePosPassword')
change_pos_user_password_permission = Permission(change_pos_user_password_need)



# client id and dongle permission
add_clientid_need = ActionNeed('addClientId')
add_clientid_permission = Permission(add_clientid_need)

add_dongle_need = ActionNeed('addDongle')
add_dongle_permission = Permission(add_dongle_need)



# hierarchial queries
get_agents_need = ActionNeed('getAgents')
get_agents_permission = Permission(get_agents_need)

get_merchants_of_company_need = ActionNeed('getCompanyMerchants')
get_merchants_of_company_permission = Permission(get_merchants_of_company_need)

get_merchants_of_agent_need = ActionNeed('getAgentMerchants')
get_merchants_of_agent_permission = Permission(get_merchants_of_agent_need)


# payment verification
get_payments_need = ActionNeed('getPayments')
get_payments_permission = Permission(get_payments_need)

verify_payment_need = ActionNeed('verifyPayment')
verify_payment_permission = Permission(verify_payment_need)

verify_merchant_need = ActionNeed('verifyMerchant')
verify_merchant_permission = Permission(verify_merchant_need)

verify_pos_need = ActionNeed('verifyPos')
verify_pos_permission = Permission(verify_pos_need)


# support permissions
search_merchant_need = ActionNeed('searchMerchantNeed')
search_merchant_permission = Permission(search_merchant_need)

search_terminal_need = ActionNeed('searchTerminalNeed')
search_terminal_permission = Permission(search_terminal_need)

# allot mid tid permissions
allot_mid_need = ActionNeed('AllotMid')
allot_mid_permission = Permission(allot_mid_need)

allot_tid_need = ActionNeed('AllotTid')
allot_tid_permission = Permission(allot_tid_need)


# allot dongle,search dongle permissions
allot_dongle_need = ActionNeed('AllotDongle')
allot_dongle_permission = Permission(allot_dongle_need)

unallot_dongle_need = ActionNeed('UnAllotDongle')
unallot_dongle_permission = Permission(unallot_dongle_need)

search_dongle_need = ActionNeed('SearchDongle')
search_dongle_permission = Permission(search_dongle_need)


