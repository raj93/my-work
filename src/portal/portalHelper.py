from src.portal import ADMIN, COMPANY, AGENT, MERCHANT
from principalConstants import *
from flask import url_for,current_app
from flask.ext.login import current_user
from src.models import *
from datetime import datetime
import hashlib
from flask import flash

def accessDeniedMessage():
	flash('Access denied.Redirected to profile page','danger')

def populateUserForm(form):
	###################### start testing ###############
	
	from testing import getNewCompany
	company,company_login = getNewCompany()
	form.username.data = company.username
	form.fullname.data = company.fullname
	form.mobileNumber.data = company.mobileNumber
	form.email.data = company.email

	# bank details
	form.bankName.data = company.bankName
	form.bankBranch.data = company.bankBranch
	form.ifsc.data = company.ifsc
	form.acNumber.data = company.acNumber
	form.MDR_Debit.data = company.MDR_Debit
	form.MDR_Credit.data = company.MDR_Credit
	form.MDR_CreditGold.data = company.MDR_CreditGold

	form.address1.data = company.address1
	form.address2.data = company.address2
	form.city.data = company.city
	form.district.data = company.district
	form.state.data = company.state
	form.pincode.data = company.pincode	

	
	from testing import getPaymentPayload
	payload = getPaymentPayload()

	if 'micr' in form.data: # it means merchant form
		form.micr.data = payload['micr']
		form.paymentAcNumber.data = payload['paymentAcNumber']
		form.serial.data = payload['serial']
		form.amount.data = payload['amount']
	

def getSha256Hash(input):
	if input:
		return hashlib.sha256(input).hexdigest()
	return input

# Returns the key if the value is present else returns None
def getKeyOfValueInDict(inputDict,value):
	for key in inputDict:
		if inputDict[key] == value:
			return key
	return None

def getCurrentTimeStamp():
	return datetime.utcnow().strftime("%Y-%m-%d %X")

# user is when the user is not logged in but event is related to that user (forgot password)
def logEvent(event,user=None, onActionUser=None):
	
	# portal log
	if not current_user.is_anonymous():
		if not onActionUser:
			onActionUser = current_user.username

		current_app.logger.warn(event + " acting up on " + onActionUser, extra={'tags':{'username':current_user.username }})
		"""
		log = portalUserLogs(current_user.username,current_user.role,current_user.subRole,onActionUser,event,getCurrentTimeStamp())
		log.save(commit = False)
		"""
	else:
		# actions on any user cannot be performed while he is not logged in.
		if user != None:

			if not onActionUser:
				onActionUser = user.username

			current_app.logger.warn(event + " acting up on " + onActionUser, extra={'tags':{'username':user.username }})
			"""
			log = portalUserLogs(user.username,user.role,user.subRole,user.username,event,getCurrentTimeStamp())
			log.save(commit = False)
			"""
		else:
			current_app.logger.warn("portalUserlog Event called for anonymous user with event ''%s''" % event)

def isHeAdmin():
	if current_user.role == ADMIN and current_user.subRole == 0:
		return True
	return False

def canHeMeddleWithThisAgent(agent):
	# he should be able to view agents of his own
	if current_user.role == ADMIN:
		pass
	elif current_user.role == COMPANY:
		if current_user.subRole == 0:
			if agent.company != current_user.username:
				return False
		else:
			if agent.company != current_user.companyEmployee.company:
				return False
	else:
		current_app.logger.warn("problem with assigning permissions. Current user %s with role (%d,%d) can meddle with agent %s" % (current_user.username,current_user.role,current_user.subRole,agent.username))
		return False

	return True

def getActionListForAgent(agent):
	actionList = []

	# edit is separate bcos of its url 
	if edit_agent_permission.can():
		actionList.append({'name':edit_agent_permission.name,'url':url_for(edit_agent_permission.url,agent=agent.username)})

	# view employees check
	if view_employee_permission.can():
		actionList.append({'name':'View employees','url':url_for('basePortal.viewEmployees')+'?username='+agent.username})

	if add_employee_permission.can():
		actionList.append({'name':add_employee_permission.name,'url':url_for(add_employee_permission.url)+'?username='+agent.username})

	if view_merchant_permission.can():
		actionList.append({'name':'View Merchants','url':url_for('basePortal.viewMerchants')+'?username='+agent.username})

	return actionList

def populateBaseRoleFields(user,form):
	# personal details
	user.fullname = form.fullname.data 
	user.mobileNumber = form.mobileNumber.data

	# bank details
	user.bankName = form.bankName.data
	user.bankBranch = form.bankBranch.data
	user.ifsc = form.ifsc.data
	user.acNumber = form.acNumber.data
	user.MDR_Debit = form.MDR_Debit.data
	user.MDR_Credit = form.MDR_Credit.data
	user.MDR_CreditGold = form.MDR_CreditGold.data

	# address details
	user.address1 = form.address1.data
	user.address2 = form.address2.data
	user.district = form.district.data
	user.city = form.city.data
	user.state = form.state.data
	user.pincode = form.pincode.data
	user.country = form.country.data
	
	return user

def getDetailsOfPortalLoginEntry(user):
	role = user.role
	subRole = user.subRole

	entry = None
	if subRole == 0:
		if role == ADMIN:
			entry = user.admin
		elif role == COMPANY:
			entry = user.company
		elif role == AGENT:
			entry = user.agent
		elif role == MERCHANT:
			entry = user.merchant
	elif subRole > 0:
		if role == ADMIN:
			entry = user.adminEmployee
		elif role == COMPANY:
			entry = user.companyEmployee
		elif role == AGENT:
			entry = user.agentEmployee
		elif role == MERCHANT:
			entry = user.merchantEmployee

	return entry

def getHierarchyDetails():
	merchant = None
	agent = None
	company = None

	username = current_user.username
	
	if current_user.role == ADMIN:
		pass
	elif current_user.role == COMPANY:
		if current_user.subRole == 0:
			companyName = current_user.username
		else:
			companyName = current_user.companyEmployee.company
		company = companyName
	elif current_user.role == AGENT:
		if current_user.subRole == 0:
			agentName = current_user.username
			agentEntry = current_user.agent
		else:
			agentEntry = current_user.agentEmployee.parentDetails
			agentName = agentEntry.username

		agent= agentName
		company = agentEntry.company

	elif current_user.role == MERCHANT:
		if current_user.subRole == 0:
			merchantName = current_user.username
			merchantEntry = current_user.merchant
		else:
			merchantEntry = current_user.merchantEmployee.parentDetails
			merchantName = merchantEntry.username

		merchant = merchantName
		agent = merchantEntry.agent
		company = merchantEntry.company
	
	return company,agent,merchant

def canHeMeddleWithThisMerchant(merchant):
	# checking whether the current user can access this particular payment.
	if current_user.role == ADMIN:
		# he can verify any payment
		pass
	elif current_user.role == COMPANY:
		if current_user.subRole == 0:
			if merchant.company != current_user.username:
				return False
		else:
			if merchant.company != current_user.company:
				return False

	elif current_user.role == AGENT:
		if current_user.subRole == 0:
			if merchant.agent != current_user.username:
				return False
		else:
			if merchant.agent != current_user.agent:
				return False
	else:
		current_app.logger.warn("problem with assigning permissions. Current user %s with role (%d,%d) can meddle with merchant %s" % (current_user.username,current_user.role,current_user.subRole,merchant.username))
		return False
	return True

def getActionListForMerchant(merchant):
	actionList = []

	if edit_merchant_permission.can():
		actionList.append({'name':edit_merchant_permission.name,'url':url_for(edit_merchant_permission.url,merchant=merchant.username)})

	# view employees no needs check
	if view_employee_permission.can():
		actionList.append({'name':view_employee_permission.name,'url':url_for(view_employee_permission.url)+'?username='+merchant.username})
	
	if view_terminal_permission.can():
		actionList.append({'name':view_terminal_permission.name,'url':url_for(view_terminal_permission.url)+'?username='+merchant.username})

	if add_terminal_permission.can():
		actionList.append({'name':add_terminal_permission.name,'url':url_for(add_terminal_permission.url)+'?merchant='+merchant.username})
	
	# adding delete only if he is admin
	if isHeAdmin():
		actionList.append({'name': 'delete','url':url_for('basePortal.deleteMerchant',merchant=merchant.username)})		

	if add_employee_permission.can():
		actionList.append({'name':add_employee_permission.name,'url':url_for(add_employee_permission.url)+'?username='+merchant.username})

	if lock_merchant_permission.can():
		if merchant.bitmap & Merchant.BITMAP['MERCHANT_LOCK'] == 0:
			actionList.append({'name':'Lock','url':url_for('basePortal.changeMerchantState',merchant=merchant.username)})
		else:
			actionList.append({'name':'Un Lock','url':url_for('basePortal.changeMerchantState',merchant=merchant.username)})

	return actionList

def getActionListForCompany(company):
	actionList = []
	# view employees no needs check
	if edit_company_permission.can():
		actionList.append({'name':edit_company_permission.name,'url':url_for(edit_company_permission.url,company = company.username)})
		
	if view_employee_permission.can():
		actionList.append({'name':'View employees','url':url_for('basePortal.viewEmployees')+'?username='+company.username})
	
	if view_agent_permission.can():
		actionList.append({'name':'View Agents','url':url_for('basePortal.viewAgents')+'?username='+company.username})

	if add_employee_permission.can():
		actionList.append({'name':add_employee_permission.name,'url':url_for(add_employee_permission.url)+'?username='+company.username})

	if view_merchant_permission.can():
		actionList.append({'name':'View Merchants','url':url_for('basePortal.viewMerchants')+'?username='+company.username})

	return actionList

def canHeMeddleWithThisTerminal(terminal):
	# checking for access
	if current_user.role == ADMIN:
		pass
	elif current_user.role == COMPANY:
		if current_user.subRole == 0:
			companyName = current_user.username
		else:
			companyName = current_user.companyEmployee.company
	
		if terminal.company != companyName:
			return False

	elif current_user.role == AGENT:
		# filling agentName based on the subRole
		if current_user.subRole == 0:
			agentName = current_user.username
		else:
			agentName = current_user.agentEmployee.agent

		if terminal.agent != agentName:
			return False
	elif current_user.role == MERCHANT:
		# filling merchantName based on the subRole
		if current_user.subRole == 0:
			merchantName = current_user.username
		else:
			merchantName = current_user.merchantEmployee.merchant

		if terminal.merchant != merchantName:
			return False
	else:
		return False

	return True

def getActionListForTerminal(terminal):
	actionList = []

	# view pos users permission
	if view_pos_users_permission.can():
		actionList.append({'name':'View Pos Users','url':url_for('basePortal.viewPosUsers')+'?terminal='+str(terminal.clientId)})
		
	# add pos user permission
	if add_pos_user_permission.can():
		actionList.append({'name':'Add Pos User','url':url_for('basePortal.addPosUser')+'?terminal='+str(terminal.clientId)})

	# lock terminal permission
	if lock_terminal_permission.can():
		if terminal.bitmap & posDetails.BITMAP['POS_LOCK'] == 0:
			actionList.append({'name':'Lock','url':url_for('basePortal.changeTerminalState',merchant=terminal.merchant,clientId = terminal.clientId)})
		else:
			actionList.append({'name':'Un Lock','url':url_for('basePortal.changeTerminalState',merchant=terminal.merchant,clientId = terminal.clientId)})

	return actionList

