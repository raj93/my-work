"""
Role is a basic entity in AasaanPay. It defines the access level in AasaanPay system. 
Subroles define the resources 
are stored in an integer variable
Right most 8 bits(25-32) : SubRole -> 2^8 : 256 possible sub-roles
Bits 17-24 represent the base role -> 2^8 : 256 possible roles
 
Defined Roles: 
	* Administrator 
	* Company
	* Agent
	* Merchant

Subrole is a employee under Role. It defines the type of resources/operations of the role, the employee has access to. Role has access to all the resources.

Sub roles may vary role to role. Example of Defined Sub-Roles:

Administrator has:
	* Financial Manager
	* Customer care
	* DB Manager

Company has:
	* Financial Manager
	* Field Agent
	* Customer care
	
Agent has:
	* Financial Manager
	* Field Agent
	* Customer care
	
Merchant has:
	* Financial Manager
	
First 8 sub-roles are reserved for pre-defined sub-roles. Start index from 1. 0 conflicts with base role. If any role has custom sub-roles, assign starting from index 8
"""

ADMIN = 0
COMPANY = 1
AGENT = 2
MERCHANT = 3

MAIN_ROLE = 0
ACCOUNTS_MANAGER = 1
CUSTOMER_CARE = 2
SALES_AGENT = 3

# useful while inserting an employee
ADMIN_ROLE = {'MAIN_ROLE':MAIN_ROLE,'ACCOUNTS_MANAGER' : ACCOUNTS_MANAGER, 'CUSTOMER_CARE' : CUSTOMER_CARE, 'SALES_AGENT' : SALES_AGENT}
COMPANY_ROLE = {'MAIN_ROLE':MAIN_ROLE,'ACCOUNTS_MANAGER' : ACCOUNTS_MANAGER, 'CUSTOMER_CARE' : CUSTOMER_CARE, 'SALES_AGENT' : SALES_AGENT}
AGENT_ROLE = {'MAIN_ROLE':MAIN_ROLE,'ACCOUNTS_MANAGER' : ACCOUNTS_MANAGER, 'CUSTOMER_CARE' : CUSTOMER_CARE, 'SALES_AGENT' : SALES_AGENT}
MERCHANT_ROLE = {'MAIN_ROLE':MAIN_ROLE,'ACCOUNTS_MANAGER' : ACCOUNTS_MANAGER}

# useful while inserting an employee
ADMIN_EMPLOYEE_ROLE = {'ACCOUNTS_MANAGER' : ACCOUNTS_MANAGER, 'CUSTOMER_CARE' : CUSTOMER_CARE, 'SALES_AGENT' : SALES_AGENT}
COMPANY_EMPLOYEE_ROLE = {'ACCOUNTS_MANAGER' : ACCOUNTS_MANAGER, 'CUSTOMER_CARE' : CUSTOMER_CARE, 'SALES_AGENT' : SALES_AGENT}
AGENT_EMPLOYEE_ROLE = {'ACCOUNTS_MANAGER' : ACCOUNTS_MANAGER, 'CUSTOMER_CARE' : CUSTOMER_CARE, 'SALES_AGENT' : SALES_AGENT}
MERCHANT_EMPLOYEE_ROLE = {'ACCOUNTS_MANAGER' : ACCOUNTS_MANAGER}

# useful for testing purposes
ADMIN_ROLE_LIST = [MAIN_ROLE,ACCOUNTS_MANAGER,CUSTOMER_CARE,SALES_AGENT]
COMPANY_ROLE_LIST = [MAIN_ROLE,ACCOUNTS_MANAGER,CUSTOMER_CARE,SALES_AGENT]
AGENT_ROLE_LIST = [MAIN_ROLE,ACCOUNTS_MANAGER,CUSTOMER_CARE,SALES_AGENT]
MERCHANT_ROLE_LIST = [MAIN_ROLE,ACCOUNTS_MANAGER]


QUERY = 0
COMPLAINT = 1
SUGGESTION = 2