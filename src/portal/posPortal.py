from flask import render_template, request, flash, redirect, url_for, current_app
from flask.ext.login import current_user

from ext import db
from src.portal import ADMIN, COMPANY, AGENT, MERCHANT
from src.models import Company, Agent,Merchant,payment,posDetails,clientIdInventory
from src.mAuthenticate import common
from src.utils.crypto import hash_passwd

from forms import AddTerminalForm

from sqlalchemy import and_
from principalConstants import *
from portalHelper import *
import json

"""
	Allot TID for a particular terminal
"""
def allotTid(terminal):
	NO_ACCESS = "you don't have access to perform this action"
	# checking the permission
	if not allot_tid_permission.can():
		return NO_ACCESS

	# making sure tid is present in form request
	if not 'tid' in request.form:
		return 'failure'

	terminal = posDetails.query.filter_by(clientId = terminal).first()
	if terminal is None:
		return 'failure'

	# checking authorization 
	if not canHeMeddleWithThisTerminal(terminal):
		return NO_ACCESS
	
	if terminal.bitmap & posDetails.BITMAP['TID_ALLOTED'] != 0:
		return 'tid is already alloted'

	tid = request.form['tid']
	bitmap = terminal.bitmap | posDetails.BITMAP['TID_ALLOTED']
	terminal.update(commit=False, tid=tid, bitmap=bitmap)

	# logging tid allocation event
	event = "TID alloted"
	common.logEvent(event,terminal.mobileNumber,terminal,common.Log.info)

	db.session.commit()

	return 'success'

"""
	Veiw terminals of a particular merchant
"""
def viewTerminals():
	# check for permission
	if not view_terminal_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# check for the existence of requested merchant
	if 'username' in request.args:
		username = request.args['username']
	else:
		username = current_user.username

	# checking for the existence of the merchant
	merch = db.session.query(Merchant).filter_by(username = username).first()
	
	# unauthorized access
	if merch is None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# checking access rights
	if current_user.role == ADMIN:
		pass
	elif current_user.role == COMPANY:
		if current_user.subRole == 0:
			companyName = current_user.username
		else:
			companyName = current_user.companyEmployee.company

		if companyName != merch.company:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))

	elif current_user.role == AGENT:
		if current_user.subRole == 0:
			agentName = current_user.username
		else:
			agentName = current_user.agentEmployee.agent

		if agentName != merch.agent:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))

	elif current_user.role == MERCHANT:
		if current_user.subRole == 0:
			merchantName = current_user.username
		else:
			merchantName = current_user.MerchantEmployee.merchant

		if merchantName != merch.username:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))

	else:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# get details from posDetails
	terminals = posDetails.query.filter_by(merchant = merch.username).all()	

	actionListEntries = []
	allotTidList = []

	for terminal in terminals:
		actionList = []
		# view pos users permission
		if view_pos_users_permission.can():
			actionList.append({'name':'View Pos Users','url':url_for('basePortal.viewPosUsers')+'?terminal='+str(terminal.clientId)})
			
		# add pos user permission
		if add_pos_user_permission.can():
			actionList.append({'name':'Add Pos User','url':url_for('basePortal.addPosUser')+'?terminal='+str(terminal.clientId)})

		# lock terminal permission
		if lock_terminal_permission.can():
			if terminal.bitmap & posDetails.BITMAP['POS_LOCK'] == 0:
				actionList.append({'name':'Lock','url':url_for('basePortal.changeTerminalState',merchant=terminal.merchant,clientId = terminal.clientId)})
			else:
				actionList.append({'name':'Un Lock','url':url_for('basePortal.changeTerminalState',merchant=terminal.merchant,clientId = terminal.clientId)})
		
		if allot_tid_permission.can():
			if terminal.bitmap & posDetails.BITMAP['TID_ALLOTED'] == 0:
				allotTidList.append(True)
			else:
				allotTidList.append(False)

		actionListEntries.append(actionList)

	return render_template('/users/view_terminals.html',entries = terminals,actionListEntries = actionListEntries,allotTidList=allotTidList)

"""
	Swap the status of the terminal from lock->unlock or reverse.
"""
def changeTerminalState(merchant,clientId):
	# merchant variable is present to imporve security because clientId are just rowId's of database

	# check for permission
	if not lock_terminal_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# check the existence of terminal and the given merchant name should match
	terminal = posDetails.query.filter_by(clientId = clientId).first()
	if terminal == None or (terminal.merchant != merchant):
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	if not canHeMeddleWithThisTerminal(terminal):
		# terminal is not in user's scope
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	bitmap = terminal.bitmap ^ posDetails.BITMAP['POS_LOCK']
	terminal.update(commit=False, bitmap=bitmap)

	# logging terminal lock/unlock events
	if terminal.bitmap & posDetails.BITMAP['POS_LOCK'] != 0:
		event = "POS Locked"
	else:
		event = "POS UnLocked"

	common.logEvent(event,terminal.mobileNumber,terminal,common.Log.info)

	db.session.commit()

	return redirect(url_for('basePortal.viewTerminals')+'?username='+ terminal.merchant)

"""
	Verifies the details of a particualr terminal
"""
def verifyPOS():

	response = {'status':False,'feedback':''}

	if not verify_pos_permission.can():
		response['feedback']  = "Permisssion denied"
		return json.dumps(response)

	#check whether get request has id argument
	if 'id' not in request.form:
		response['feedback'] = "id parameter not present in request"
		return json.dumps(response)

	id = request.form['id']

	# check whether entry exists or not
	entry =  posDetails.query.filter_by(id = id).first()

	if entry is None:
		# accessing non existent
		response['feedback'] = "requested pos not present"
		return json.dumps(response)

	if not canHeMeddleWithThisTerminal(entry):
		# terminal is not in user's scope
		response['feedback'] = "requested pos not present"
		return json.dumps(response)

	if entry.bitmap & posDetails.BITMAP['POS_VERIFICATION'] != 0 :
		# already verified
		response['feedback'] = "POS is verified already"
		return json.dumps(response) 
	
	# check whether the payment for that particular entry is verified or not.
	if entry.bitmap & posDetails.BITMAP['PAYMENT_VERIFICATION'] == 0:
		paymentEntry = entry.paymentDetails
		response['feedback'] = 'payment with micr '+paymentEntry.micr+' needs to be verified'
		return json.dumps(response) 

	# change bitmap to verified state
	bitmap = entry.bitmap | posDetails.BITMAP['POS_VERIFICATION']
	entry.update(commit=False, bitmap=bitmap)

	event = "POS verified"

	try:
		db.session.commit()
	except Exception as e:
		current_app.logger.error(str(e))
		response['feedback'] = "server error"
		return json.dumps(response)	

	common.logEvent(event,entry.mobileNumber,entry,common.Log.info)

	response['status'] = True
	response['feedback'] = "POS verified successfully"
	return json.dumps(response)	

"""
	Shows the view with a list of terminals need to be verified.
"""
def POSVerification():

	#only users with admin as base role can do merchant verification process
	if current_user.role != ADMIN:
		return redirect(url_for('basePortal.viewProfile'))
	
	terminals = db.session.query(posDetails).filter(posDetails.bitmap.op('&')(posDetails.BITMAP['POS_VERIFICATION']) == 0).all()

	return render_template('verification/pos_verification.html',terminals=terminals)

"""
	Adds the terminal to the database
"""
def addPosToDB(form,m,posList):
	# adding in to payment table	
	try:
		p = payment(m.username,form.paymentMode.data,form.micr.data,'end of universe',form.paymentAcNumber.data,form.serial.data,form.amount.data,len(posList))
		p.save()

		# adding in to pos tables
		for pos in posList:
			# details entry
			d = posDetails(m.username,m.agent,m.company,p.id,pos,'',m.currency)
			d.save()
			if m.bitmap & Merchant.BITMAP['MERCHANT_VERIFICATION'] != 0:
				d.bitmap |= posDetails.BITMAP['MERCHANT_VERIFICATION']
			
			if m.bitmap & Merchant.BITMAP['MERCHANT_MID_ALLOTED'] != 0:
				d.bitmap |= posDetails.BITMAP['MID_ALLOTED']

			if m.bitmap & Merchant.BITMAP['MERCHANT_LOCK'] != 0:
				d.bitmap |= posDetails.BITMAP['MERCHANT_LOCK']

			if m.bitmap & Merchant.BITMAP['MERCHANT_DISABLE'] != 0:
				d.bitmap |= posDetails.BITMAP['MERCHANT_DISABLE']
				
			d.clientId = d.id
			d.plutusClientId = d.clientId

			#Add pos user entry
			# this is the default password we set.
			# When the pos is activated for the very first time we generate a random password
			# and set the state of pos to "shouldChangePassword" and send the temporary credentials
			import hashlib
			passwd,salt = hash_passwd(hashlib.sha256("password").hexdigest())

			u = posUsers(pos,passwd,salt,d.clientId,True)
			u.save()
	except Exception as e:
		current_app.logger.error(str(e))
		# deleting all terminal deatils
		for pos in posList:
			posUsers.query.filter_by(username = pos).delete()
			posDetails.query.filter_by(mobileNumber = pos).delete()
		p.delete()

		return False
	return True
	
"""
	Add pos for a particular merchant
"""
def addPOS():

	# checking permission
	if not add_terminal_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	if 'merchant' not in request.args:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	merchantName = request.args['merchant']

	# check whether merchant exists or not
	merchant =  Merchant.query.filter_by(username = merchantName).first()
	if merchant == None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	if not canHeMeddleWithThisMerchant(merchant):
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	form = AddTerminalForm()

	# getting pos list
	posList = []
	if form.posData.data != None:
		posListString = form.posData.data
		posListString = posListString[:len(posListString)-1] #if we don't fill posData ';' is assigned

		if len(posListString) != 0:
			posList = posListString.split(';')

	if form.validate_on_submit():
		if not addPosToDB(form,merchant,posList): 
			flash("server faced error while adding pos, try again after some time")
			return redirect(url_for('basePortal.viewProfile'))
		flash('added pos device successfully')
		return redirect(url_for('basePortal.viewMerchant',name=merchant.username))

	return render_template('pos/add_pos.html',form=form,merchantName=merchantName,posList=posList)


"""
	View the given terminal
"""
def viewPOS(clientId):

	# checking permission
	if not view_terminal_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))
	
	# check whether pos exists or not
	terminal =  posDetails.query.filter_by(clientId = clientId).first()
	if terminal == None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# checking whether pos can be accessed by logged in user
	if current_user.role == ADMIN:
		# any admin has access if he has permission
		pass
	elif current_user.role == COMPANY:
		# filling companyName based on the subRole
		if current_user.subRole == 0:
			companyName = current_user.username
		else:
			companyName = current_user.companyEmployee.company
	
		if terminal.company != companyName:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))

	elif current_user.role == AGENT:
		# filling agentName based on the subRole
		if current_user.subRole == 0:
			agentName = current_user.username
		else:
			agentName = current_user.agentEmployee.agent

		if terminal.agent != agentName:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))

	elif current_user.role == MERCHANT:
		if current_user.subRole == 0:
			merchantName = current_user.username
		else:
			merchantName = current_user.MerchantEmployee.merchant

		if terminal.merchant != merchantName:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))
	else:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	actionList = getActionListForTerminal(terminal)
	
	allotTid = False	
	if allot_tid_permission.can():
		if terminal.bitmap & posDetails.BITMAP['TID_ALLOTED'] == 0:
			allotTid = True
		else:
			allotTid = False

	# bitmap status
	bitmapStatuses = []
	for ele in posDetails.BITMAP:
		bitmapStatuses.append([ele,(terminal.bitmap & posDetails.BITMAP[ele] > 0)])
		
	return render_template('pos/view_pos.html',details = terminal, actionList = actionList, allotTid = allotTid,bitmapStatuses = bitmapStatuses)

"""
	Search for terminal based on the query keyword
"""
def searchTerminals():
	if not  search_terminal_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	terminal = None
	if request.method == 'POST':
		if 'keyword' not in request.form:
			return render_template('support/search_terminals.html',terminal=terminal)
		keyword = request.form['keyword']
		terminal = posDetails.query.filter_by(mobileNumber = keyword).first()

		if terminal is not None:
			if not canHeMeddleWithThisTerminal(terminal):
				terminal = None # requested terminal not under his scope

	return render_template('support/search_terminals.html',terminal = terminal)
	
"""
	Experimental !
	Add terminal by traversing the tree from company to merchant at one spot.
"""
from src.models import vanillaHierarchy
def hierarchyDemo():

	showCompanyList = False
	showAgentList = False
	showMerchantList = False
	companyList = []
	agentList = []
	merchantList = []

	if current_user.role == ADMIN:
		showCompanyList = True
		showAgentList = True
		showMerchantList = True

		#companies
		entries = db.session.query(Company).all()
		for entry in entries:
			companyList.append(entry.username)

	elif current_user.role == COMPANY:
		showCompanyList = True
		showAgentList = True
		showMerchantList = True

		#agents
		entries = db.session.query(Agent).filter(Agent.company == current_user.username ).all()
		for entry in entries:
			agentList.append(entry.username)

		if current_user.subRole == 0:
			companyList.append(current_user.username)
		else:
			companyList.append(current_user.companyEmployee.company)

	elif current_user.role == AGENT:
		showMerchantList = True

		#merchants
		entries = db.session.query(Merchant).filter(Merchant.agent == current_user.username).all()
		for entry in entries:
			merchantList.append(entry.username)

	elif current_user.role == MERCHANT:
		pass
	else:
		# code should not come here
		pass

	form = AddTerminalForm()
	if form.validate_on_submit():
		
		#check boxes are posted to server only if they are selected not other wise !
		if 'forAgent' in request.form.keys():
			throughAgent = True
		else:
			throughAgent = False

		
		if current_user.role == ADMIN:
			# getting the inputs
			c_name = request.form['companies']
			a_name = request.form['agents']
			m_name = request.form['merchants']

			#checking whether the merchant exists or not
			m_entry = db.session.query(Merchant).filter(Merchant.username == m_name).first()

			if m_entry is None:
				return redirect(url_for('basePortal.viewProfile'))

			#checking whether merchant's company is same as mentioned
			if m_entry.company != c_name:
				return redirect(url_for('basePortal.viewProfile'))

			#if it is being created unded agent,agent name should match the requested
			if throughAgent:
				if m_entry.agent != a_name:
					return redirect(url_for('basePortal.viewProfile'))
			else:
				a_name = None

			#TODO add to db c_name,a_name,m_name
			v = vanillaHierarchy(c_name,a_name,m_name)
			v.save()
			
		elif current_user.role == COMPANY:

			if current_user.subRole == 0:
				c_name = current_user.username
			else:
				c_name = current_user.companyEmployee.company

			a_name = request.form['agents']
			m_name = request.form['merchants']

			m_entry = db.session.query(Merchant).filter(Merchant.username == m_name).first()

			if m_entry is None:
				return redirect(url_for('basePortal.viewProfile'))

			#checking whether merchant's company is same as mentioned
			if m_entry.company != c_name:
				return redirect(url_for('basePortal.viewProfile'))

			#if it is being created unded agent,agent name should match the requested
			if throughAgent:
				if m_entry.agent != a_name:
					return redirect(url_for('basePortal.viewProfile'))
			else:
				a_name = None

			v = vanillaHierarchy(current_user.username,a_name,m_name)
			v.save()
		
		elif current_user.role == AGENT:
			if current_user.subRole == 0:
				a_name = current_user.username
			else:
				a_name = current_user.agentEmployee.agent

			m_name = request.form['merchants']
			m_entry = db.session.query(Merchant).filter(Merchant.username == m_name).first()

			if m_entry is None:
				return redirect(url_for('basePortal.viewProfile'))

			if m_entry.agent != a_name:
					return redirect(url_for('basePortal.viewProfile'))

			#TODO add to db current_user.company,current_user.username,m_name

			v = vanillaHierarchy(m_entry.company,current_user.username,m_name)
			v.save()
		elif current_user.role == MERCHANT:
			# TODO add to db current_user.merchant.company, .agent, .username
			
			if current_user.subRole == 0:
				m_entry = current_user.merchant
			else:
				m_entry = current_user.merchantEmployee.parentDetails

			v = vanillaHierarchy(m_entry.company,m_entry.agent,m_entry.username)
			v.save()
			pass
		else:
			print 'code should not come here'
			pass

		flash("added successfully")

	print showCompanyList,showAgentList,showMerchantList
	return render_template('hierarchy/add_terminal.html',form=form,companyList=companyList,agentList=agentList,merchantList=merchantList, showCompanyList=showCompanyList,showAgentList=showAgentList,showMerchantList=showMerchantList)

def changeTerminalBitState(clientId,bitName):
	entry  = posDetails.query.filter_by(clientId = clientId).first()

	if entry == None:
		return redirect(url_for('basePortal.viewProfile'))		

	# making pos ready for pre activation
	if bitName == "PREPARE_FOR_ACTIVATION":
		entry.bitmap = entry.bitmap | posDetails.VALID_PRE_ACTIVATION_VALUE
		entry.update()
		return redirect(url_for('basePortal.viewPOS',clientId = clientId))

	# changing a bit state
	entry.bitmap = entry.bitmap ^ posDetails.BITMAP[bitName]
	entry.update()
	return redirect(url_for('basePortal.viewPOS',clientId = clientId))
