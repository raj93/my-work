from wtforms.fields import StringField,PasswordField,FloatField,SelectField,IntegerField,HiddenField,\
					TextAreaField,FileField
from wtforms.validators import DataRequired,ValidationError,EqualTo,Email, Length
from src.models import portalLogin,clientIdInventory,dongleInventory,posDetails,support
# 0-Query 1-Complaint 2-Suggestion
from src.models import support
from src.utils.crypto import verify_passwd
from flask.ext.login import current_user
from formConstants import countryChoices,currencyChoices

from flask import current_app, session
from flask.ext.wtf import Form,RecaptchaField

from ext import db
import hashlib
from validationHelper import isPasswordStrong,isThisADigitChain,doesThisHaveSpecialCharacters,isThisAnAlphabetChain

########## constants ##########
userNameLength=6
usernameMaxLength = 20

passwordLength = 8
passwordMaxLength = 20

maxInputLength = 30

supportMessageLength = 25
supportMessageMaxLength = 200

mobileNumberMinLength = 10
mobileNumberMaxLength = 15

########## Basic forms ##########

# login form
class LoginForm(Form):
	username = StringField('username', [DataRequired()])
	password = StringField('password', [DataRequired()])

#when we force the user to change password
class ShouldChangePasswordForm(Form):
	newPassword  = PasswordField('NewPassword', [DataRequired(), Length(min = passwordLength, max = passwordMaxLength)])
	confirmPassword = PasswordField('ConfirmPassword', [DataRequired(), Length(min = passwordLength, max = passwordMaxLength), EqualTo('newPassword', message='Passwords must match')])

	def validate_newPassword(form,field):
		if not isPasswordStrong(field.data):
			raise ValidationError('Password should have atleast 1 numeric or caps or special character')
		return True


class BaseProfileForm(Form):
	#fullname = StringField('fullname',[DataRequired()])
	#mobileNumber = StringField('mobileNumber',[DataRequired()])
	email = StringField('email', [DataRequired(), Email(), Length(max = maxInputLength)])


#change password form
class ChangePasswordForm(Form):
	password = PasswordField('password',[DataRequired()])
	newPassword  = PasswordField('NewPassword',[DataRequired(),Length(min=passwordLength, max = passwordMaxLength)])
	confirmPassword = PasswordField('ConfirmPassword',[DataRequired(),EqualTo('newPassword',message='Passwords must match')])

	def validate_password(form,field):
		hashed_passwd = hashlib.sha256(field.data).hexdigest()
		if verify_passwd(hashed_passwd, current_user.password, current_user.salt) == False:
			raise ValidationError('Invalid password')
		return True

	def validate_newPassword(form,field):
		if not isPasswordStrong(field.data):
			raise ValidationError('Password should have atleast 1 numeric or caps or special character')

		if field.data == form.password.data:
			raise ValidationError('New password should be different from old password')
		return True


# forgot password form
class ForgotPasswordForm(Form):
	username = StringField('username',[DataRequired()])
	recaptcha = RecaptchaField()
	
	# moving to the code to avoid two db requests
	def validate_username(form,field):
		entry = db.session.query(portalLogin).filter(portalLogin.username == field.data).first()
		if entry is None:
			raise ValidationError('Invalid username')
		return True


class ResetPasswordForm(Form):
	password  = PasswordField('Password',[DataRequired(), Length(min=passwordLength, max = passwordMaxLength)])
	confirmPassword = PasswordField('ConfirmPassword',[DataRequired(), Length(min=passwordLength, max = passwordMaxLength),EqualTo('password',message='Passwords must match')])

	def validate_password(form,field):
		if not isPasswordStrong(field.data):
			raise ValidationError('Password should have atleast 1 numeric or caps or special character')
		return True


class SupportForm(Form):
	contactNumber = StringField('mobile number', [DataRequired(), Length(min=mobileNumberMinLength, max = mobileNumberMaxLength)]) # digits only
	email = StringField('email',[DataRequired(),Email(),Length(max=maxInputLength)])
	feedbackType = SelectField(u'feedback Type',choices=support.feedbackChoices)
	message = TextAreaField(u'message',[DataRequired(),Length(min=supportMessageLength,max=supportMessageMaxLength)]) # min length 25

	def validate_contactNumber(form,field):
		if not isThisADigitChain(field.data):
			raise ValidationError('Only numeric characters')


########## Employee related forms ##########

class EditEmployeeForm(Form):	
	email = StringField('Email', [Email(), Length(max=maxInputLength)])


class AddEmployeeForm(EditEmployeeForm):
	username = StringField('Username',[DataRequired(),Length(min=userNameLength,max=usernameMaxLength)])
	fullname = StringField('Fullname',[DataRequired(),Length(max=maxInputLength)])
	mobileNumber = StringField('Mobile number',[DataRequired(),Length(min=mobileNumberMinLength, max=mobileNumberMaxLength)])

	def validate_mobileNumber(form,field):
		if not isThisADigitChain(field.data):
			raise ValidationError('Only numeric characters')

	def validate_username(form,field):
		entry = db.session.query(portalLogin).filter(portalLogin.username == field.data).first()
		if entry is not None:
			print 'validation error'+entry.username
			raise ValidationError('Username is already taken try other')
		return True

########## posUser related forms ##########
class AddPosUserForm(Form):
	username = StringField('username',[DataRequired(),Length(min=userNameLength, max=usernameMaxLength)])
	password  = PasswordField('password',[DataRequired(),Length(min=passwordLength, max=passwordMaxLength)])
	confirmPassword = PasswordField('confirmPassword',[DataRequired(),Length(min=passwordLength, max=passwordMaxLength),EqualTo('password',message='Passwords must match')])

	def validate_password(form,field):
		if not isPasswordStrong(field.data):
			raise ValidationError('Password should have atleast 1 numeric or caps or special character')
		return True

#change password form pos user
class PosUserChangePasswordForm(Form):
	newPassword  = PasswordField('NewPassword',[DataRequired(),Length(min=passwordLength, max=passwordMaxLength)])
	confirmPassword = PasswordField('ConfirmPassword',[DataRequired(),Length(min=passwordLength, max=passwordMaxLength),EqualTo('newPassword',message='Passwords must match')])

	def validate_newPassword(form,field):
		if not isPasswordStrong(field.data):
			raise ValidationError('Password should have atleast 1 numeric or caps or special character')
		return True

########## Inventory related forms #########

class AddClientIdForm(Form):
	clientID = IntegerField('clientId',[DataRequired()])
	securityToken = StringField('securityToken',[DataRequired(), Length(max=maxInputLength)])

	def validate_clientID(form,field):
		entry = db.session.query(clientIdInventory).filter(clientIdInventory.clientID == field.data).first()
		if entry is not None:
			raise ValidationError('Client Id alerady present with us')
		return True


class AddDongleForm(Form):
	dongleSerial = StringField('dongleSerial',[DataRequired(),Length(max=maxInputLength)])

	def validate_dongleSerial(form,field):
		entry = db.session.query(dongleInventory).filter(dongleInventory.dongleSerial == field.data).first()
		if entry is not None:
			raise ValidationError('Dongle alerady present with us')
		return True

########## portal user related forms #########
class EditUserForm(Form):

	fullname = StringField('fullname',[DataRequired(),Length(max=maxInputLength)])
	mobileNumber = StringField('mobile number', [DataRequired(), Length(min=mobileNumberMinLength, max=mobileNumberMaxLength)])

	#bank details
	bankName = StringField('bankName',[DataRequired(),Length(max=maxInputLength)]) # alpha
	bankBranch = StringField('bankBranch',[DataRequired(),Length(max=maxInputLength)]) # alpha
	ifsc = StringField('ifsc',[DataRequired(),Length(max=maxInputLength)]) # digits and letters
	acNumber = StringField('acNumber',[DataRequired(),Length(max=maxInputLength)]) #digits
	MDR_Debit= FloatField('MDR_Debit',[DataRequired()]) #float
	MDR_Credit= FloatField('MDR_Credit',[DataRequired()]) #float
	MDR_CreditGold= FloatField('MDR_Special_Card',[DataRequired()]) #float

	#address details
	address1 = StringField('address1', [DataRequired(), Length(max=maxInputLength)])
	address2 = StringField('address2', [Length(max=maxInputLength)])
	city = StringField('city',[DataRequired(), Length(max=maxInputLength)]) # alpha
	district = StringField('district',[DataRequired(), Length(max=maxInputLength)])
	state = StringField('state',[DataRequired(), Length(max=maxInputLength)]) # alpha
	pincode = StringField('pincode',[DataRequired(), Length(min=6,max=7)]) # numeric

	country = SelectField(u'country',choices = countryChoices)

	def validate_bankName(form,field):
		if not isThisAnAlphabetChain(field.data):
			raise ValidationError('Only Alphabets')

	def validate_bankBranch(form,field):
		if not isThisAnAlphabetChain(field.data):
			raise ValidationError('Only Alphabets')

	def validate_ifsc(form,field):
		if doesThisHaveSpecialCharacters(field.data):
			raise ValidationError('Alpha numeric only')

	def validate_acNumber(form,field):
		if not isThisADigitChain(field.data):
			raise ValidationError('Only numerics')

	def validate_city(form,field):
		if not isThisAnAlphabetChain(field.data):
			raise ValidationError('Only Alphabets')

	def validate_state(form,field):
		if not isThisAnAlphabetChain(field.data):
			raise ValidationError('Only Alphabets')

	def validate_pincode(form,field):
		if not isThisADigitChain(field.data):
			raise ValidationError('Only numeric characters')


class AddUserForm(EditUserForm):

	#user details
	username = StringField('username', [DataRequired(), Length(min=userNameLength, max=usernameMaxLength)])
	email = StringField('email', [DataRequired(), Email(), Length(max=maxInputLength)])
	

	def validate_username(form,field):
		entry = db.session.query(portalLogin).filter(portalLogin.username == field.data).first()
		if entry is not None:
			raise ValidationError('Username is already taken try other')
		return True

######### company #######

class EditCompanyForm(EditUserForm):
	# TODO
	#other details specific to company (editable)
	pass

# form for adding company
class AddCompanyForm(EditCompanyForm,AddUserForm):
	# TODO
	#other details specific to company (non-editable)
	pass


######### Agent #########

class EditAgentForm(EditUserForm):
	# TODO
	#other details specific to agent (editable)
	pass


# form for adding agent
class AddAgentForm(EditAgentForm,AddUserForm):
	# TODO
	#other details if any (non editable)
	pass

######### Merchant ##########


class EditMerchantWithPosForm(EditUserForm):
	# TODO
	#other details specific to merchant (editable)
	pass


class AddMerchantWithPosForm(EditMerchantWithPosForm,AddUserForm):

	#other details specific to merchant (non-editable)

	# TODO settle them 
	dob = StringField('dob') # dd/mm/yyyy # DataRequired
	idProof = StringField('idProof')
	nameOnReceipt = StringField('nameOnReceipt') # DataRequired

	# for proofs
	idProofImage = FileField(u'idProofImage')

	# pos details are sent as json
	posData = HiddenField('posData',[DataRequired()])

	# payment details
	currency = SelectField(u'currency',choices = currencyChoices)
	paymentMode = SelectField(u'paymentMode',choices=[('0','cheque'),('1','DD')])
	micr = StringField('micr',[DataRequired(),Length(max=maxInputLength)]) # alphanum
	paymentAcNumber = StringField('acNumber',[DataRequired(),Length(max=maxInputLength)]) # numeric
	serial = StringField('serial',[DataRequired(),Length(max=maxInputLength)]) # alphanum
	amount = FloatField('amount',[DataRequired()]) # float

	def validate_micr(form,field):
		if doesThisHaveSpecialCharacters(field.data):
			raise ValidationError('Only Alphabets')

	def validate_paymentAcNumber(form,field):
		if not isThisADigitChain(field.data):
			raise ValidationError('Alpha numeric only')

	def validate_serial(form,field):
		if doesThisHaveSpecialCharacters(field.data):
			raise ValidationError('Only numerics')


	def validate_posData(form,field):
		posListString = field.data
		posListString = posListString[:len(posListString)-1]
		if len(posListString) != 0:
			posList = posListString.split(';')
			
			if len(posList) != len(set(posList)):
				raise ValidationError('duplicates in the pos numbers')

			for pos in posList:
				posEntry = db.session.query(posDetails).filter_by(mobileNumber = pos).first()

				if posEntry is not None:
					raise ValidationError('pos number '+pos+' is already registered')
			return True
		else:
			raise ValidationError('atleast one pos should be filled')


####### Terminal

class AddTerminalForm(Form):
	# pos details are sent as json
	posData = HiddenField('posData',[DataRequired()])

	# payment details
	paymentMode = SelectField(u'paymentMode',choices=[('0','cheque'),('1','DD')])
	micr = StringField('micr',[DataRequired(),Length(max=maxInputLength)]) # alphanum
	paymentAcNumber = StringField('acNumber',[DataRequired(),Length(max=maxInputLength)]) # numeric
	serial = StringField('serial',[DataRequired(),Length(max=maxInputLength)]) # alphanum
	amount = FloatField('amount',[DataRequired()]) # float

	def validate_micr(form,field):
		if doesThisHaveSpecialCharacters(field.data):
			raise ValidationError('Only Alphabets')

	def validate_paymentAcNumber(form,field):
		if not isThisADigitChain(field.data):
			raise ValidationError('Alpha numeric only')

	def validate_serial(form,field):
		if doesThisHaveSpecialCharacters(field.data):
			raise ValidationError('Only numerics')

	def validate_posData(form,field):
		posListString = field.data
		posListString = posListString[:len(posListString)-1]

		if len(posListString) != 0:
			posList = posListString.split(';')
			if len(posList) != len(set(posList)):
				raise ValidationError('duplicates in the pos numbers')

			for pos in posList:
				posEntry = db.session.query(posDetails).filter_by(mobileNumber = pos).first()

				if posEntry is not None:
					raise ValidationError('pos number '+pos+' is already registered')
			return True
		else:
			raise ValidationError('atleast one pos should be filled')



