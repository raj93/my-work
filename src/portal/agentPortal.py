from flask import render_template, request, flash, redirect, url_for, current_app
from flask.ext.login import current_user
import simplejson as json

from ext import db
from src.portal.forms import AddAgentForm, EditAgentForm
from src.portal.principalConstants import get_agents_permission,add_agent_permission, \
		edit_agent_permission,view_agent_permission,view_employee_permission,add_employee_permission,\
		view_merchant_permission
from src.models import portalLogin, Company, Agent
from src.portal import ADMIN, COMPANY, AGENT
from src.utils.crypto import hash_passwd
from src.utils.ses_email import send1
from portalHelper import canHeMeddleWithThisAgent,getActionListForAgent,populateBaseRoleFields,logEvent, accessDeniedMessage

"""
	Agents come under a company.
	Input : company name
	Output : Agents under the input company

"""
def getCompanyAgents(companyName):
	agents = Agent.query.filter_by(company = companyName).all()

	agentList = []
	for agent in agents: 
		agentList.append(agent.username)

	return agentList

"""
	Given a company return the list of agents under him.
	'company' is the post argument.
"""

def getAgents():
	
	companyName = request.form['company']

	emptyJSON = json.dumps({"agents":[]})
	# returning an empty list if the use
	if not get_agents_permission.can():
		# log unauthorized request.
		return emptyJSON

	# check for company
	if current_user.role == COMPANY:
		if current_user.subRole == 0:
			if current_user.username != companyName:
				return emptyJSON
		else:
			if current_user.companyEmployee.company != companyName:
				return emptyJSON

	data = {"agents":getCompanyAgents(companyName)}

	return json.dumps(data)

"""
	view Agents for a given company username
"""

def viewAgents():

	# checking permission
	if not view_agent_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	if 'username' in request.args:
		username = request.args['username']
	else:
		username = current_user.username # company logged in and coming through menu.

	company = Company.query.filter_by(username = username).first()

	#checking the existence of the agent/company
	if company == None:
		return redirect(url_for('basePortal.viewProfile'))

	# allowing to view agents of his own company
	if current_user.role == ADMIN:
		pass
	elif current_user.role == COMPANY:
		if current_user.subRole == 0:
			if current_user.username != company.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		else:
			if current_user.username != current_user.companyEmployee.company:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
	else:
		current_app.logger.warn("problem with assigning permissions. Current user %s with role (%d,%d) can access viewAgents function" \
			% (current_user.username,current_user.role,current_user.subRole))
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# getting agents
	agents = Agent.query.filter_by(company = username).all()

	logEvent("viewed agents of company %s"% username, onActionUser=username)

	return render_template('users/view_agents.html',agents=agents)

"""
	Add agent under a company
"""

def addAgent():

	#checking permission
	if not add_agent_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	showCompanyList = True
	companyList = []

	# if it is admin list all companies
	if current_user.role == ADMIN:
		companies = Company.query.all()
		for company in companies:
			companyList.append(company.username)
	elif current_user.role == COMPANY:
		if current_user.subRole == 0:
			companyList.append(current_user.username)
		else:
			companyList.append(current_user.companyEmployee.company)

	form = AddAgentForm()
	if form.validate_on_submit():
		companyName = request.form['companies']

		#check whether the companyName exists
		company = Company.query.filter_by(username = companyName).first()
		if company is None:
			flash("select a  company")
			return render_template("agent/add_agent.html", form=form,showCompanyList=showCompanyList,companyList=companyList)
	
		# allowing to add agent to his own company
		if current_user.role == COMPANY:
			if current_user.subRole == 0:
				if current_user.username != companyName:
					accessDeniedMessage()
					return redirect(url_for('basePortal.viewProfile'))
			else:
				if current_user.companyEmployee.company != companyName:
					accessDeniedMessage()
					return redirect(url_for('basePortal.viewProfile'))

		# adding entry to portal login
		import hashlib
		from src.utils.crypto import generateRandomPassword
		password = generateRandomPassword(10)
		hashedPassword,salt = hash_passwd(hashlib.sha256(password).hexdigest())
		entry = portalLogin(form.username.data,hashedPassword,salt,AGENT,0,True,True);
		entry.save(commit = False)

		#adding agent to Agent table
		agent = Agent(form.username.data,form.fullname.data,'end of universe',form.mobileNumber.data,form.email.data, \
			form.bankName.data,form.bankBranch.data,form.ifsc.data,form.acNumber.data,form.MDR_Debit.data, \
			form.MDR_Credit.data,form.MDR_CreditGold.data,form.address1.data,form.address2.data,form.city.data, \
			form.district.data,form.state.data,form.pincode.data,form.country.data,companyName)
		agent.save(commit = False)
		
		# send password to email
		subject = "Your credentials with temporary password"
		to = agent.email
			
		body = "username and password are "+"\n"+ \
				"username: "+ agent.username + "\n"+ \
				"password: "+ password + "\n" +\
				"You can access your portal at "+\
				request.url_root[:len(request.url_root)-1] + url_for('basePortal.login')
						
		# loggin the event
		logEvent("added agent %s under company %s"% (agent.username,companyName), onActionUser=companyName)	

		try:
			db.session.commit()
		except Exception as e:
			current_app.logger.critical(str(e))
			flash("server has faced problem in processing this request. Please try again after some time")
			return redirect(url_for('basePortal.viewProfile'))

		# sending email after entries are updated.
		# Incase if he does not get email he can always do forgot password
		send1(subject, to, body,None,None)
		
		flash("added agent successfully")
		return redirect(url_for('basePortal.viewAgent',name=agent.username))

	if current_app.config['TESTING']:
		from portalHelper import populateUserForm
		populateUserForm(form)
	

	return render_template("agent/add_agent.html", form=form,showCompanyList=showCompanyList,companyList=companyList)

"""
	Edit Agent under a company
"""

def editAgent(agent):

	# checking permission
	if not edit_agent_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# requested one should exists
	agent = Agent.query.filter_by(username = agent).first()
	if agent is None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# he should be able to edit agent of his own
	if not canHeMeddleWithThisAgent(agent):
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	form = EditAgentForm(obj=agent)
	
	if form.validate_on_submit():
		agent = populateBaseRoleFields(agent,form)
		agent.update(commit = False)
		logEvent("edited agent %s"% agent.username, onActionUser=agent.username)
		db.session.commit()

		flash('updated agent '+agent.username + ' successfully')
		return redirect(url_for('basePortal.viewAgent',name=agent.username))

	return render_template("agent/edit_agent.html",form=form,agent=agent)

"""
	View Agent under a company
"""

def viewAgent(agent):

	# he should have permission
	if not view_agent_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# the requested one should exists
	agent = Agent.query.filter_by(username = agent).first()
	if agent is None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# he should be able to view agents of his own
	if not canHeMeddleWithThisAgent(agent):
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# getting necessary action
	actionList = getActionListForAgent(agent)

	logEvent("viewed agent %s"% agent.username, onActionUser=agent.username)
	db.session.commit()

	return render_template("agent/view_agent.html",agent=agent,actionList=actionList)
