#from subprocess import _args_from_interpreter_flags
from flask import url_for


from src.models import portalLogin
import hashlib
from src.portal import ADMIN, COMPANY, AGENT, MERCHANT,ADMIN_ROLE_LIST,COMPANY_ROLE_LIST,AGENT_ROLE_LIST,MERCHANT_ROLE_LIST
from src.portal import MAIN_ROLE, CUSTOMER_CARE, SALES_AGENT
from testing import *

from ext import db


class TestMiscFunctionality(KitTestCase):
	
	def setUp(self):
		pass

	def tearDown(self):
		pass

	# TODO for viewAgents,viewMerchants

	def viewAgentsTest(self,baseRole,subRole):
		# set up entity
		entity,login = createEntryForRole(baseRole,subRole)

		# login entity
		username = login.username
		password = hashlib.sha256(PASSWORD).hexdigest()

		# login (baseRole,subRole) entry
		payload = {'username':username,'password':password}
		response = self.client.post(url_for('basePortal.login'),data=payload)

		#check whether it is redirecting to view profile
		self.assertRedirects(response,url_for('basePortal.viewProfile'))

		### start functionality
		company,company_login = createEntryForRole(COMPANY,MAIN_ROLE)

		if baseRole == ADMIN:
			self.process_viewAgentsTest(company.username,True) # admin can view agents of any company
		elif baseRole == COMPANY:
			if subRole == MAIN_ROLE:
				self.process_viewAgentsTest(entity.username,True) # he can view his own
				self.process_viewAgentsTest(company.username,False) # not others
			elif subRole == CUSTOMER_CARE:
				# subrole with access to this functionality
				self.process_viewAgentsTest(entity.parentDetails.username,True) # he can view his company's
				self.process_viewAgentsTest(company.username,False) # not others
			elif subRole == SALES_AGENT:
				# subroles with out access to this functionality
				self.process_viewAgentsTest(entity.parentDetails.username,False) # he cannot view his company's
				self.process_viewAgentsTest(company.username,False) # not others
		else:
			# no way users below company can access viewAgents of own or others
			if subRole == MAIN_ROLE:
				self.process_viewAgentsTest(entity.company,False)
				self.process_viewAgentsTest(company.username,False)
			else:
				self.process_viewAgentsTest(entity.parentDetails.company,False)
				self.process_viewAgentsTest(company.username,False)
				
		cleanUpEntryOfRole(company.username,company_login)
		### end of functionality

		# logout entity 
		response = self.client.get(url_for('basePortal.logout'))

		# clean up entity
		if db.session.query(portalLogin).filter_by(username = entity.username).first() != None:
			cleanUpEntryOfRole(entity.username,login)		

	def process_viewAgentsTest(self,companyName,has_permission):
		response = self.client.get(url_for('basePortal.viewAgents')+'?username='+companyName)
		if has_permission:
			self.assertTemplateUsed('users/view_agents.html')
		else:
			self.assertRedirects(response,url_for('basePortal.viewProfile'))
	"""
	def testViewAgents(self):
		#For each possible ordered pair it runs the tests
		allUsers = [[ADMIN,ADMIN_ROLE_LIST],[COMPANY,COMPANY_ROLE_LIST],[AGENT,AGENT_ROLE_LIST],[MERCHANT,MERCHANT_ROLE_LIST]]

		# admin roles
		for entry in allUsers:
			for role in entry[1]:
				self.viewAgentsTest(entry[0],role)
				print entry[0],role,' TESTED'
	"""

	def viewMerchantsTest(self,baseRole,subRole):
		# set up entity
		entity,login = createEntryForRole(baseRole,subRole,companyMerchant = False)

		# login entity
		username = login.username
		password = hashlib.sha256(PASSWORD).hexdigest()

		# login (baseRole,subRole) entry
		payload = {'username':username,'password':password}
		response = self.client.post(url_for('basePortal.login'),data=payload)

		#check whether it is redirecting to view profile
		self.assertRedirects(response,url_for('basePortal.viewProfile'))

		### start functionality

		# TODO viewMerchant neuro path ??

		
		company_merchant,company_merchant_login = createEntryForRole(MERCHANT,MAIN_ROLE,companyMerchant = False)
		
		if baseRole == ADMIN:
			self.process_viewMerchantsTest(company_merchant.company,True) # merchant directly under a company
			self.process_viewMerchantsTest(company_merchant.agent,True) # merchant directly under an agent
		elif baseRole == COMPANY:
			agent  = None
			agent_login = None

			if subRole == MAIN_ROLE:
				agent,agent_login = createNewAgent(entity.username)
			else:
				agent,agent_login = createNewAgent(entity.parentDetails.username)

			if subRole == MAIN_ROLE:
				self.process_viewMerchantsTest(entity.username,True) # merchants under him
				self.process_viewMerchantsTest(agent.username,True) # merchants under his agent

			elif subRole == CUSTOMER_CARE:
				# sub roles with access to this functionality
				parent = entity.parentDetails
				self.process_viewMerchantsTest(parent.username,True) # merchants under him

				self.process_viewMerchantsTest(agent.username,True) # merchants under his agent
			elif subRole == SALES_AGENT:
				# sub roles with out access to this functionality
				parent = entity.parentDetails
				self.process_viewMerchantsTest(parent.username,False) # merchants under him

				self.process_viewMerchantsTest(agent.username,False) # merchants under his agent

			agent.delete()
			agent_login.delete()

			self.process_viewMerchantsTest(company_merchant.company,False) # merchants directly under a company
			self.process_viewMerchantsTest(company_merchant.agent,False) # merchants directly under an agent
		elif baseRole == AGENT:
			if subRole == MAIN_ROLE:
				self.process_viewMerchantsTest(entity.company,False) # merchants under his company
				self.process_viewMerchantsTest(entity.username,True) # merchants under him
			elif subRole == CUSTOMER_CARE:
				# sub roles with access to this functionality
				parent = entity.parentDetails
				self.process_viewMerchantsTest(parent.company,False) # merchants under him
				self.process_viewMerchantsTest(parent.username,True) # merchants under his agent
			elif subRole == SALES_AGENT:
				# sub roles with out access to this functionality
				parent = entity.parentDetails
				self.process_viewMerchantsTest(parent.company,False) # merchants under him
				self.process_viewMerchantsTest(parent.username,False) # merchants under his agent

			self.process_viewMerchantsTest(company_merchant.company,False) # merchants directly under a company
			self.process_viewMerchantsTest(company_merchant.agent,False) # merchants directly under an agent
		elif baseRole == MERCHANT:
			if subRole == MAIN_ROLE:
				self.process_viewMerchantsTest(entity.company,False) # merchants under his company
				self.process_viewMerchantsTest(entity.agent,False) # merchants under his agent
			elif subRole == ACCOUNTS_MANAGER:
				# sub roles with access to this functionality
				parent = entity.parentDetails
				self.process_viewMerchantsTest(parent.company,False) # merchants under him
				self.process_viewMerchantsTest(parent.agent,False) # merchants under his agent

		# clean up entries
		cleanUpEntryOfRole(company_merchant.username,company_merchant_login)

		### end of functionality

		# logout entity 
		response = self.client.get(url_for('basePortal.logout'))

		# clean up entity
		if db.session.query(portalLogin).filter_by(username = entity.username).first() != None:
			cleanUpEntryOfRole(entity.username,login)		

	def process_viewMerchantsTest(self,parentName,has_permission):
		response = self.client.get(url_for('basePortal.viewMerchants')+'?username='+parentName)
		if has_permission:
			self.assert_template_used('users/view_merchants.html')
		else:
			self.assertRedirects(response,url_for('basePortal.viewProfile'))

	def testViewMerchants(self):
		allUsers = [[ADMIN,ADMIN_ROLE_LIST],[COMPANY,COMPANY_ROLE_LIST],[AGENT,AGENT_ROLE_LIST],[MERCHANT,MERCHANT_ROLE_LIST]]
		"""
			For each possible ordered pair it runs the tests
		"""
		# admin roles
		for entry in allUsers:
			for role in entry[1]:
				print 'running ',entry[0],role
				self.viewMerchantsTest(entry[0],role)
				



