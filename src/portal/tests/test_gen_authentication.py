#from subprocess import _args_from_interpreter_flags
from flask import url_for

from src.portal import ADMIN, COMPANY, AGENT, MERCHANT
from src.portal import ADMIN_ROLE_LIST, COMPANY_ROLE_LIST, AGENT_ROLE_LIST, MERCHANT_ROLE_LIST
from src.utils.crypto import verify_passwd
from testing import createEntryForRole,cleanUpEntryOfRole,KitTestCase
from ext import db
import hashlib
from src.models import portalLogin
from testing import *

def getSha256Hash(input):
		if input:	
			return hashlib.sha256(input).hexdigest()
		return input

class TestAuthentication(KitTestCase):
	def setUp(self):
		pass

	def tearDown(self):
		pass

	def editProfileTest(self,entity):
		### edit profile
		payload = {'fullname':entity.fullname,'mobileNumber':'6534560011117','email':'testingemail@apay.com'}
		response = self.client.post(url_for('basePortal.editProfile'),data=payload)

		# get profile
		entity.save()
		assert(entity.email == "testingemail@apay.com" )

	def changePasswordTest(self,login):
		### change password
		password = PASSWORD

		newPassword = CHANGED_PASSWORD
		payload = {'password':password,'newPassword':newPassword,'confirmPassword':newPassword}
		response = self.client.post(url_for('basePortal.changePassword'),data=payload)

		# save portal Login
		login.save()
		assert ((verify_passwd(getSha256Hash(newPassword),login.password,login.salt) == True))

	def shouldChangePasswordTest(self,login):
		login.shouldChangePassword = True
		login.save()

		username = login.username
		password = FORGOT_PASSWORD

		payload = {'username':username,'password':getSha256Hash(password)}
	
		# login
		response = self.client.post(url_for('basePortal.login'),data=payload)
		#check whether it is redirecting to view profile
		self.assertRedirects(response,url_for('basePortal.shouldChangePassword'))


		#change password
		newPassword = SHOULD_CHANGE_PASSWORD
		payload = {'newPassword':newPassword,'confirmPassword':newPassword}
		response = self.client.post(url_for('basePortal.shouldChangePassword'),data=payload)

		login.save()
		assert ((verify_passwd(getSha256Hash(newPassword),login.password,login.salt) == True))

		# logout
		response = self.client.get(url_for('basePortal.logout'))

	def forgotPasswordTest(self,login):
		### forgot password
		# forgot password request
		username = login.username
		payload = {'username':username}
		response = self.client.post(url_for('basePortal.forgotPassword'),data=payload)

		login.save()

		token = login.resetToken
		tosend = login.resetSignedToken

		# change the password using the link
		password = FORGOT_PASSWORD
		payload = {"password":password,"confirmPassword":password}
		response = self.client.post((url_for('basePortal.resetPassword')+"?token="+tosend),data=payload)

		login.save()
		assert verify_passwd(getSha256Hash(password), login.password, login.salt)

		# verfiy the new password by logging in.
		payload = {'username':username, 'password':getSha256Hash(password)}
		response = self.client.post(url_for('basePortal.login'),data=payload)
		#check whether it is redirecting to view profile
		self.assertRedirects(response,url_for('basePortal.viewProfile'))

		# logout entity 
		response = self.client.get(url_for('basePortal.logout'))


	"""
	Authentication story :

	User logs in,edits his porfile,changes his password and logs out.
	Now he forgot his password and for security reasons we want
	to change his password next time he logs in.
	"""

	def authenticationTests(self,baseRole,subRole):

		# creating a entry who logsin
		entity,login = createEntryForRole(baseRole,subRole)

		username = login.username
		password = hashlib.sha256(PASSWORD).hexdigest()

		# login (baseRole,subRole) entry
		payload = {'username':username,'password':password}
		response = self.client.post(url_for('basePortal.login'),data=payload)

		#check whether it is redirecting to view profile
		self.assertRedirects(response,url_for('basePortal.viewProfile'))

		self.editProfileTest(entity)
		self.changePasswordTest(login)

		# logout entity 
		response = self.client.get(url_for('basePortal.logout'))
		
		self.forgotPasswordTest(login)
		
		### should change password
		self.shouldChangePasswordTest(login)
		
	
		# clean up entity
		if db.session.query(portalLogin).filter_by(username = entity.username).first() != None:
			cleanUpEntryOfRole(entity.username,login)


	def testAuth(self):
		allUsers = [[ADMIN,ADMIN_ROLE_LIST],[COMPANY,COMPANY_ROLE_LIST],[AGENT,AGENT_ROLE_LIST],[MERCHANT,MERCHANT_ROLE_LIST]]

		# admin roles
		for entry in allUsers:
			for role in entry[1]:
				self.authenticationTests(entry[0],role)