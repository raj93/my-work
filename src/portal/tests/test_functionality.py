#from subprocess import _args_from_interpreter_flags
from src.models import AdminEmployee, Company, CompanyEmployee, Agent, AgentEmployee, MerchantEmployee, portalLogin


from testing import *
from ext import db
from flask import url_for

import hashlib
from src.utils.crypto import otp_generator, otp_generator1
from src.portal import ADMIN, COMPANY, AGENT, MERCHANT
from src.portal import ADMIN_ROLE_LIST, COMPANY_ROLE_LIST, AGENT_ROLE_LIST, MERCHANT_ROLE_LIST
from src.portal import MAIN_ROLE,ACCOUNTS_MANAGER,CUSTOMER_CARE,SALES_AGENT

"""
Includes tests for

addCompany
addAgent
addEmployee
add and delete entry module

"""

class TestFunctionality(KitTestCase):
	def setUp(self):
		pass
		
	def tearDown(self):
		pass
	
	# just to test whether add and delete entry of any role is working fine or not
	def testAddAndDeleteEntry(self):
		entity,entity_login = createEntryForRole(ADMIN,MAIN_ROLE)
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(ADMIN,ACCOUNTS_MANAGER)
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(ADMIN,CUSTOMER_CARE)
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(ADMIN,SALES_AGENT)
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(COMPANY,MAIN_ROLE)
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(COMPANY,ACCOUNTS_MANAGER)
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(COMPANY,CUSTOMER_CARE)
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(COMPANY,SALES_AGENT)		
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(AGENT,MAIN_ROLE)
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(AGENT,ACCOUNTS_MANAGER)
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(AGENT,CUSTOMER_CARE)
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(AGENT,SALES_AGENT)
		cleanUpEntryOfRole(entity.username)
		

		entity,entity_login = createEntryForRole(MERCHANT,MAIN_ROLE,True)
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(MERCHANT,ACCOUNTS_MANAGER,True)
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(MERCHANT,MAIN_ROLE,False)
		cleanUpEntryOfRole(entity.username)

		entity,entity_login = createEntryForRole(MERCHANT,ACCOUNTS_MANAGER,False)
		cleanUpEntryOfRole(entity.username)

	def addCompanyTest(self,baseRole,subRole,has_permission):

		# set up entity
		entity,login = createEntryForRole(baseRole,subRole)

		# login entity
		username = login.username
		password = hashlib.sha256(PASSWORD).hexdigest()

		# login (baseRole,subRole) entry
		payload = {'username':username,'password':password}
		response = self.client.post(url_for('basePortal.login'),data=payload)

		#check whether it is redirecting to view profile
		self.assertRedirects(response,url_for('basePortal.viewProfile'))

		#--- functionlaity --- #

		# Add company request
		company,company_login = getNewCompany()

		payload = getCommonPayload(company)
		response = self.client.post(url_for('basePortal.addCompany'),data=payload)

		entry = db.session.query(Company).filter_by(username = company.username).first()

		if has_permission:
			assert(entry is not None)
			assert(entry.username == company.username)

			entry_login = entry.loginDetails
			entry.delete()
			entry_login.delete()

		else:
			assert(entry is None)

		# --- end of functionality --- #

		# logout entity 
		response = self.client.get(url_for('basePortal.logout'))

		# clean up entity
		if db.session.query(portalLogin).filter_by(username = entity.username).first() != None:
			cleanUpEntryOfRole(entity.username)
	
	def testAddCompany(self):

		for role in ADMIN_ROLE_LIST:
			if role == MAIN_ROLE:
				self.addCompanyTest(ADMIN,role,True)
			else:
				self.addCompanyTest(ADMIN,role,False)
		
		for role in COMPANY_ROLE_LIST:
			self.addCompanyTest(COMPANY,role,False)

		for role in AGENT_ROLE_LIST:
			self.addCompanyTest(AGENT,role,False)

		for role in MERCHANT_ROLE_LIST:
			self.addCompanyTest(MERCHANT,role,False)
	

	def addAgentTest(self,baseRole,subRole,ofOwn,has_permission):
		# set up entity
		entity,login = createEntryForRole(baseRole,subRole)

		# login entity
		username = login.username
		password = hashlib.sha256(PASSWORD).hexdigest()

		# login (baseRole,subRole) entry
		payload = {'username':username,'password':password}
		response = self.client.post(url_for('basePortal.login'),data=payload)

		#check whether it is redirecting to view profile
		self.assertRedirects(response,url_for('basePortal.viewProfile'))

		if ofOwn:

			if baseRole == COMPANY:
				if subRole == MAIN_ROLE:
					companyName = entity.username
					agent,agent_login = getNewAgent(companyName)
					agentName = agent.username
					
					payload = getCommonPayload(agent)
					payload.update({"companies":companyName})

					self.process_addAgentTest(payload,companyName,agentName,True)
				elif subRole != subRole:
					# this is place for company employee having permission to 
					# add agent to his own company
					pass
				else:
					companyName = entity.parentDetails.username
					agent,agent_login = getNewAgent(companyName)
					agentName = agent.username

					payload = getCommonPayload(agent)
					payload.update({"companies":companyName})

					self.process_addAgentTest(payload,companyName,agentName,False)
			elif baseRole == AGENT or baseRole == MERCHANT:
				# no agent has permission to addAgent to his or other company
				if subRole == MAIN_ROLE:
					companyName = entity.company
				else:
					companyName = entity.parentDetails.company

				agent,agent_login = getNewAgent(companyName)
				agentName = agent.username
				
				payload = getCommonPayload(agent)
				payload.update({"companies":companyName})

				self.process_addAgentTest(payload,companyName,agentName,False)

		else:
			company,company_login = createNewCompany()
			agent,agent_login = getNewAgent(company.username)

			agentName = agent.username
			companyName = company.username

			payload = getCommonPayload(agent)
			payload.update({"companies":companyName})

			if baseRole == ADMIN and subRole == MAIN_ROLE:
				# only admin has permission to add agent to some company
				self.process_addAgentTest(payload,companyName,agentName,True)
			else:
				self.process_addAgentTest(payload,companyName,agentName,False)

			cleanUpEntryOfRole(company_login.username,company_login)

		# logout entity 
		response = self.client.get(url_for('basePortal.logout'))

		# clean up entity
		l_entry = db.session.query(portalLogin).filter_by(username = entity.username).first()
		if l_entry != None:
			cleanUpEntryOfRole(l_entry.username,l_entry)

	def testAddAgent(self):
		# admin tests

		for role in ADMIN_ROLE_LIST:
			if role == MAIN_ROLE:
				self.addAgentTest(ADMIN,role,False,True)
			else:
				self.addAgentTest(ADMIN,role,False,False)
		
		# company tests
		
		for role in COMPANY_ROLE_LIST:
			if role == MAIN_ROLE:
				self.addAgentTest(COMPANY,role,True,True)
				self.addAgentTest(COMPANY,role,False,False)
			else:
				self.addAgentTest(COMPANY,role,True,False)
				self.addAgentTest(COMPANY,role,False,False)

		# agent tests
		
		for role in AGENT_ROLE_LIST:
			self.addAgentTest(AGENT,role,True,False)
			self.addAgentTest(AGENT,role,False,False)

		# merchant tests

		for role in MERCHANT_ROLE_LIST:
			self.addAgentTest(MERCHANT,role,True,False)
			self.addAgentTest(MERCHANT,role,False,False)

	def process_addAgentTest(self,payload,companyName,agentName,has_permission):
		# if agent is added remove it
		
		response = self.client.post(url_for('basePortal.addAgent'),data=payload)
		entry = db.session.query(Agent).filter_by(username = agentName).first()

		if has_permission:
			assert(entry is not None)
			assert(entry.username == agentName)

			entry_login = entry.loginDetails
			entry.delete()
			entry_login.delete()

		else:
			assert(entry is None)
	
	# user with baseRole logs in 
	def addEmployeeTest(self,baseRole,subRole,onActionRole,ofOwn):
		# employee can only be added to users whose subRole is MAIN_ROLE

		# creating a entry who logsin
		entity,login = createEntryForRole(baseRole,subRole)

		username = login.username
		password = hashlib.sha256(PASSWORD).hexdigest()

		# login (baseRole,subRole) entry
		payload = {'username':username,'password':password}
		response = self.client.post(url_for('basePortal.login'),data=payload)

		# details of to be added employee
		empUserName = otp_generator(7)
		payload = {'username':empUserName,'fullname':otp_generator1(7),'mobileNumber':'1234567890','email':otp_generator(6)+'@apay.com','employeeType':'ACCOUNTS_MANAGER'}

		if baseRole == ADMIN:
			# main role
			if subRole == MAIN_ROLE:
				adminName = entity.username
				self.process_add_employee_byAdminAsBaseroleTest(payload,empUserName,onActionRole,adminName,ofOwn,True)

			# subroles with permission
			elif subRole == ACCOUNTS_MANAGER:
				adminName = entity.parentDetails.username
				self.process_add_employee_byAdminAsBaseroleTest(payload,empUserName,onActionRole,adminName,ofOwn,True)

			# subRoles with out permission				
			elif subRole == SALES_AGENT or subRole == CUSTOMER_CARE:
				adminName = entity.parentDetails.username
				self.process_add_employee_byAdminAsBaseroleTest(payload,empUserName,onActionRole,adminName,ofOwn,False)
				
		elif baseRole == COMPANY:
			# main role
			if subRole == MAIN_ROLE:
				companyName = entity.username
				self.process_add_employee_byCompanyAsBaseroleTest(payload,empUserName,onActionRole,companyName,ofOwn,True)

			# sub role with permission
			elif subRole == ACCOUNTS_MANAGER:
				companyName = entity.parentDetails.username
				self.process_add_employee_byCompanyAsBaseroleTest(payload,empUserName,onActionRole,companyName,ofOwn,True)

			# sub role with out permission
			elif subRole == SALES_AGENT or subRole == CUSTOMER_CARE:
				companyName = entity.parentDetails.username
				self.process_add_employee_byCompanyAsBaseroleTest(payload,empUserName,onActionRole,companyName,ofOwn,False)

		elif baseRole == AGENT:
			if subRole == MAIN_ROLE:

				companyName = entity.company
				agentName = entity.username

				self.process_add_employee_byAgentAsBaseroleTest(payload,empUserName,onActionRole,companyName,agentName,ofOwn,True)
			
			# sub role with permission
			elif subRole != subRole:
				agnt = entity.parentDetails
				companyName = agnt.company
				agentName = agnt.username

				self.process_add_employee_byAgentAsBaseroleTest(payload,empUserName,onActionRole,companyName,agentName,ofOwn,True)

			# sub role with out permission				
			elif subRole == ACCOUNTS_MANAGER or subRole == CUSTOMER_CARE or subRole == SALES_AGENT:
				agnt = entity.parentDetails
				companyName = agnt.company
				agentName = agnt.username

				self.process_add_employee_byAgentAsBaseroleTest(payload,empUserName,onActionRole,companyName,agentName,ofOwn,False)

		elif baseRole == MERCHANT:
			# has_permission is the last argument
			if subRole == MAIN_ROLE:

				companyName = entity.company
				agentName = entity.agent
				merchantName = entity.username
				
				self.process_add_employee_byMerchantAsBaseroleTest(payload,empUserName,onActionRole,companyName,agentName,merchantName,ofOwn,True)
			# sub role with permission
			elif subRole != subRole:

				merchant = entity.parentDetails
				companyName = merchant.company
				agentName = merchant.agent
				merchantName = merchant.username
				
				self.process_add_employee_byMerchantAsBaseroleTest(payload,empUserName,onActionRole,companyName,agentName,merchantName,ofOwn,True)

			# sub role with out permission				
			elif subRole == ACCOUNTS_MANAGER or subRole == CUSTOMER_CARE or subRole == SALES_AGENT:

				merchant = entity.parentDetails
				companyName = merchant.company
				agentName = merchant.agent
				merchantName = merchant.username
				
				self.process_add_employee_byMerchantAsBaseroleTest(payload,empUserName,onActionRole,companyName,agentName,merchantName,ofOwn,False)

		else:
			print "problem with the baseRole allocation of add employee module"


		# logout
		response = self.client.get(url_for('basePortal.logout'))

		if db.session.query(portalLogin).filter_by(username = login.username).first() != None:
			cleanUpEntryOfRole(login.username)
	
	def testAddEmployee(self):
		onActionList = [ADMIN,COMPANY,AGENT,MERCHANT]
		
		# admin tests
		for le in onActionList:
			self.addEmployeeTest(ADMIN,MAIN_ROLE,le,True)
			self.addEmployeeTest(ADMIN,ACCOUNTS_MANAGER,le,True)
			self.addEmployeeTest(ADMIN,CUSTOMER_CARE,le,True)
			
			if le == ADMIN:
				self.addEmployeeTest(ADMIN,MAIN_ROLE,le,False)
				self.addEmployeeTest(ADMIN,ACCOUNTS_MANAGER,le,False)
				self.addEmployeeTest(ADMIN,CUSTOMER_CARE,le,False)
				
		
		# company tests
		for le in onActionList:
			self.addEmployeeTest(COMPANY,MAIN_ROLE,le,True)
			self.addEmployeeTest(COMPANY,ACCOUNTS_MANAGER,le,True)
			self.addEmployeeTest(COMPANY,CUSTOMER_CARE,le,True)
			
			if le != ADMIN:
				self.addEmployeeTest(COMPANY,MAIN_ROLE,le,False)
				self.addEmployeeTest(COMPANY,ACCOUNTS_MANAGER,le,False)
				self.addEmployeeTest(COMPANY,CUSTOMER_CARE,le,False)
		

		# agent tests
		for le in onActionList:
			self.addEmployeeTest(AGENT,MAIN_ROLE,le,True)
			self.addEmployeeTest(AGENT,ACCOUNTS_MANAGER,le,True)
			self.addEmployeeTest(AGENT,CUSTOMER_CARE,le,True)
			
			if le != ADMIN:
				self.addEmployeeTest(AGENT,MAIN_ROLE,le,False)
				self.addEmployeeTest(AGENT,ACCOUNTS_MANAGER,le,False)
				self.addEmployeeTest(AGENT,CUSTOMER_CARE,le,False)

		
		# merchant tests
		for le in onActionList:
			if le != AGENT:
				self.addEmployeeTest(MERCHANT,MAIN_ROLE,le,True)
				self.addEmployeeTest(MERCHANT,ACCOUNTS_MANAGER,le,True)
			if le != ADMIN:
				self.addEmployeeTest(MERCHANT,MAIN_ROLE,le,False)
				self.addEmployeeTest(MERCHANT,ACCOUNTS_MANAGER,le,False)
			
		
	
	def process_add_employee_byAdminAsBaseroleTest(self,payload,empUserName,onActionRole,adminName,ofOwn,has_permission):
		if onActionRole == ADMIN:
					
			if ofOwn:
				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+adminName,data=payload)
				admin_emp = db.session.query(AdminEmployee).filter_by(username = empUserName).first()

				if has_permission:
					assert(admin_emp is not None)
					admin_emp_login = admin_emp.loginDetails

					admin_emp.delete()
					admin_emp_login.delete()
				else:
					assert(admin_emp is None)

			else:
				# create new admin
				admin,admin_login = createEntryForRole(ADMIN,MAIN_ROLE)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+admin.username,data=payload)
				admin_emp = db.session.query(AdminEmployee).filter_by(username = empUserName).first()

				if has_permission:
					assert(admin_emp is not None)
					cleanUpEntryOfRole(admin_emp.username)
				else:
					assert(admin_emp is None)
					admin.delete()
					admin_login.delete()

		if onActionRole == COMPANY:
			# create new company
			company,company_login = createEntryForRole(COMPANY,MAIN_ROLE)

			# asserting addEmployee funcitonality
			response = self.client.post(url_for('basePortal.addEmployee')+'?username='+company.username,data=payload)
			company_emp = db.session.query(CompanyEmployee).filter_by(username = empUserName).first()

			if has_permission:
				assert(company_emp is not None)
				cleanUpEntryOfRole(company_emp.username)
			else:
				assert(company_emp is None)
				company.delete()
				company_login.delete()
				
			
		if onActionRole == AGENT:
			# create new agent
			agent,agent_login = createEntryForRole(AGENT,MAIN_ROLE)

			# asserting addEmployee funcitonality
			response = self.client.post(url_for('basePortal.addEmployee')+'?username='+agent.username,data=payload)
			agent_emp = db.session.query(AgentEmployee).filter_by(username = empUserName).first()

			if has_permission:
				assert(agent_emp is not None)
				cleanUpEntryOfRole(agent_emp.username)
			else:
				assert(agent_emp is None)
				company = Company.query.filter_by(username=agent.company).first()
				company_login = company.loginDetails

				agent.delete()
				agent_login.delete()

				company.delete()
				company_login.delete()


		if onActionRole == MERCHANT:
			# create new company merchant
			merchant,merchant_login = createNewCompanyMerchant()

			# asserting addEmployee funcitonality
			response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
			merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()
			
			if has_permission:
				assert(merchant_emp is not None)
				cleanUpEntryOfRole(merchant_emp.username)
			else:
				assert(merchant_emp is None)
				company = merchant.companyDetails
				company_login = company.loginDetails

				merchant.delete()
				merchant_login.delete()

				company.delete()
				company_login.delete()

				

			# create new agent merchant
			merchant,merchant_login = createNewAgentMerchant()

			# asserting addEmployee funcitonality
			response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
			merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()
			
			if has_permission:
				assert(merchant_emp is not None)
				cleanUpEntryOfRole(merchant_emp.username)
			else:
				assert(merchant_emp is None)
				agent = Agent.query.filter_by(username=merchant.agent).first()
				agent_login = agent.loginDetails

				company = merchant.companyDetails
				company_login = company.loginDetails

				merchant.delete()
				merchant_login.delete()

				agent.delete()
				agent_login.delete()

				company.delete()
				company_login.delete()

				

	def process_add_employee_byCompanyAsBaseroleTest(self,payload,empUserName,onActionRole,companyName,ofOwn,has_permission):
		if onActionRole == ADMIN:
			# create new admin
			admin,admin_login = createEntryForRole(ADMIN,MAIN_ROLE)

			# asserting addEmployee funcitonality
			response = self.client.post(url_for('basePortal.addEmployee')+'?username='+admin.username,data=payload)
			admin_emp = db.session.query(AdminEmployee).filter_by(username = empUserName).first()
			assert(admin_emp is None)

			cleanUpEntryOfRole(admin.username)

		if onActionRole == COMPANY:
			if ofOwn:
				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+companyName,data=payload)
				company_emp = db.session.query(CompanyEmployee).filter_by(username = empUserName).first()
				
				if has_permission:
					assert(company_emp is not None)

					company_emp_login = company_emp.loginDetails
					company_emp.delete()
					company_emp_login.delete()
					
				else:
					assert(company_emp is None)
			else:
				# create new company
				company,company_login = createEntryForRole(COMPANY,MAIN_ROLE)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+company.username,data=payload)
				company_emp = db.session.query(CompanyEmployee).filter_by(username = empUserName).first()
				assert(company_emp is None)

				cleanUpEntryOfRole(company.username)
			
		if onActionRole == AGENT:
			if ofOwn:
				# create new agent
				agent,agent_login = createNewAgent(companyName)
				
				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+agent.username,data=payload)
				agent_emp = db.session.query(AgentEmployee).filter_by(username = empUserName).first()

				if has_permission:
					assert(agent_emp is not None)
					
					agent_emp_login = agent_emp.loginDetails
					agent_emp.delete()
					agent_emp_login.delete()

				else:
					assert(agent_emp is None)

				agent.delete()
				agent_login.delete()

			else:
				# create new agent
				agent,agent_login = createEntryForRole(AGENT,MAIN_ROLE)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+agent.username,data=payload)
				agent_emp = db.session.query(AgentEmployee).filter_by(username = empUserName).first()
				assert(agent_emp is None)

				cleanUpEntryOfRole(agent.username)

			
		if onActionRole == MERCHANT:
			if ofOwn:
				# creating merchant directly under him
				merchant,merchant_login = createNewCompanyMerchant(companyName)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
				merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()
				
				if has_permission:		
					assert(merchant_emp is not None)

					merchant_emp_login = merchant_emp.loginDetails
					merchant_emp.delete()
					merchant_emp_login.delete()
					
				else:
					assert(merchant_emp is None)

				merchant.delete()
				merchant_login.delete()



				# creating merchant directly under his agent
				agent,agent_login = createNewAgent(companyName)
				merchant,merchant_login = createNewAgentMerchant(agent.username)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
				merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()

				if has_permission:		
					assert(merchant_emp is not None)

					merchant_emp_login = merchant_emp.loginDetails
					merchant_emp.delete()
					merchant_emp_login.delete()
				
				else:
					assert(merchant_emp is None)


				merchant.delete()
				merchant_login.delete()

				agent.delete()
				agent_login.delete()
				

			else:
				# create new company merchant
				merchant,merchant_login = createNewCompanyMerchant()

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
				merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()
				assert(merchant_emp is None)

				cleanUpEntryOfRole(merchant.username)

				# create new agent merchant
				merchant,merchant_login = createNewAgentMerchant()

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
				merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()
				assert(merchant_emp is None)

				cleanUpEntryOfRole(merchant.username)

	def process_add_employee_byAgentAsBaseroleTest(self,payload,empUserName,onActionRole,companyName,agentName,ofOwn,has_permission):

		if onActionRole == ADMIN:
			### for an admin

			# create new admin
			admin,admin_login = createEntryForRole(ADMIN,MAIN_ROLE)

			# asserting addEmployee funcitonality
			response = self.client.post(url_for('basePortal.addEmployee')+'?username='+admin.username,data=payload)
			admin_emp = db.session.query(AdminEmployee).filter_by(username = empUserName).first()
			assert(admin_emp is None)

			cleanUpEntryOfRole(admin.username)

		if onActionRole == COMPANY:
			if ofOwn:
				### for his own company

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+companyName,data=payload)
				company_emp = db.session.query(AdminEmployee).filter_by(username = empUserName).first()
				assert(company_emp is None)

			else:
				### for other company

				# create new company
				company,company_login = createEntryForRole(COMPANY,MAIN_ROLE)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+company.username,data=payload)
				company_emp = db.session.query(CompanyEmployee).filter_by(username = empUserName).first()
				assert(company_emp is None)

				cleanUpEntryOfRole(company.username)
			
		if onActionRole == AGENT:
			if ofOwn:
				### for himself

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+agentName,data=payload)
				agent_emp = db.session.query(AgentEmployee).filter_by(username = empUserName).first()

				if has_permission:
					assert(agent_emp is not None)

					agent_emp_login = agent_emp.loginDetails
					agent_emp.delete()
					agent_emp_login.delete()
					
				else:
					assert(agent_emp is None)
			else:
				### for an other agent under same company

				agent,agent_login = createNewAgent(companyName)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+agent.username,data=payload)
				agent_emp = db.session.query(AgentEmployee).filter_by(username = empUserName).first()
				assert(agent_emp is None)

				# it will take back to starting line of this "else"
				agent_login.delete()
				agent.delete()

				### for an agent under different company

				agent,agent_login = createEntryForRole(AGENT,MAIN_ROLE)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+agent.username,data=payload)
				agent_emp = db.session.query(AgentEmployee).filter_by(username = empUserName).first()
				assert(agent_emp is None)

				cleanUpEntryOfRole(agent.username)

		if onActionRole == MERCHANT:
			if ofOwn:
				### for a merchant directly under him

				merchant,merchant_login = createNewAgentMerchant(agentName)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
				merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()

				if has_permission:
					assert(merchant_emp is not None)

					merchant_emp_login = merchant_emp.loginDetails
					merchant_emp.delete()
					merchant_emp_login.delete()
				else:
					assert(merchant_emp is None)
				
				merchant_login.delete()
				merchant.delete()

			else:
				### for a merchant under his company

				# create new company merchant
				merchant,merchant_login = createNewCompanyMerchant(companyName)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
				merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()
				assert(merchant_emp is None)

				merchant_login.delete()
				merchant.delete()


				### for a merchant under other company

				# create new company merchant
				merchant,merchant_login = createNewCompanyMerchant()

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
				merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()
				assert(merchant_emp is None)

				cleanUpEntryOfRole(merchant.username)

				### for a merchant under other agent.

				# create new agent merchant
				merchant,merchant_login = createNewAgentMerchant()

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
				merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()
				assert(merchant_emp is None)

				cleanUpEntryOfRole(merchant.username)				
	
	def process_add_employee_byMerchantAsBaseroleTest(self,payload,empUserName,onActionRole,companyName,agentName,merchantName,ofOwn,has_permission):
		if onActionRole == ADMIN:
			### for an admin

			# create new admin
			admin,admin_login = createEntryForRole(ADMIN,MAIN_ROLE)

			# asserting addEmployee funcitonality
			response = self.client.post(url_for('basePortal.addEmployee')+'?username='+admin.username,data=payload)
			admin_emp = db.session.query(AdminEmployee).filter_by(username = empUserName).first()
			assert(admin_emp is None)

			cleanUpEntryOfRole(admin.username)

		if onActionRole == COMPANY:
			if ofOwn:
				### for his own company

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+companyName,data=payload)
				company_emp = db.session.query(AdminEmployee).filter_by(username = empUserName).first()
				assert(company_emp is None)

			else:
				### for other company

				# create new company
				company,company_login = createEntryForRole(COMPANY,MAIN_ROLE)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+company.username,data=payload)
				company_emp = db.session.query(CompanyEmployee).filter_by(username = empUserName).first()
				assert(company_emp is None)

				cleanUpEntryOfRole(company.username)
			
		if onActionRole == AGENT:
			if ofOwn:
				### for his agent

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+agentName,data=payload)
				agent_emp = db.session.query(AgentEmployee).filter_by(username = empUserName).first()
				assert(agent_emp is None)

			else:
				### for an other agent under same company

				agent,agent_login = createNewAgent(companyName)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+agent.username,data=payload)
				agent_emp = db.session.query(AgentEmployee).filter_by(username = empUserName).first()
				assert(agent_emp is None)

				# it will take back to starting line of this "else"
				agent.delete()
				agent_login.delete()

				### for an agent under different company

				agent,agent_login = createEntryForRole(AGENT,MAIN_ROLE)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+agent.username,data=payload)
				agent_emp = db.session.query(AgentEmployee).filter_by(username = empUserName).first()
				assert(agent_emp is None)

				# it will take back to starting line of this "else"
				agent.delete()
				agent_login.delete()
				

				company = Company.query.filter_by(username=agent.company).first()
				company_login = company.loginDetails

				company.delete()
				company_login.delete()
				


		if onActionRole == MERCHANT:
			if ofOwn:
				### for himself/ for his merchant if his employee calls this method

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchantName,data=payload)
				print response.data
				merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()

				if has_permission:
					assert(merchant_emp is not None)

					merchant_emp_login = merchant_emp.loginDetails
					merchant_emp.delete()
					merchant_emp_login.delete()
					
				else:
					assert(merchant_emp is None)
					

			else:
				### for a merchant under other company
				
				# create new company merchant
				merchant,merchant_login = createNewCompanyMerchant()

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
				merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()
				assert(merchant_emp is None)

				cleanUpEntryOfRole(merchant.username)

				### for a merchant under other agent.

				# create new agent merchant
				merchant,merchant_login = createNewAgentMerchant()

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
				merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()
				assert(merchant_emp is None)

				cleanUpEntryOfRole(merchant.username)

				
				### for a merchant under his company

				# create new company merchant
				merchant,merchant_login = createNewCompanyMerchant(companyName)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
				merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()
				assert(merchant_emp is None)

				merchant.delete()
				merchant_login.delete()

				
				### for a merchant under his agent

				### createEntryForRole(Merchant,subRole) by default creates a companyMerchant
				agent,agent_login = createNewAgent(companyName)
				merchant,merchant_login = createNewAgentMerchant(agent.username)

				# asserting addEmployee funcitonality
				response = self.client.post(url_for('basePortal.addEmployee')+'?username='+merchant.username,data=payload)
				merchant_emp = db.session.query(MerchantEmployee).filter_by(username = empUserName).first()
				assert(merchant_emp is None)

				merchant.delete()
				merchant_login.delete()

				agent.delete()
				agent_login.delete()

				

				

				
