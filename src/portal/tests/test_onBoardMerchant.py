#from subprocess import _args_from_interpreter_flags #Required at the top of the file as per the following link. #Non threaded environment??
# Following note taken from https://github.com/pydata/conda/issues/303 
# This function is only used by multiprocessing and the test suite, but it's here so that it can be imported when Python is compiled without threads.
from src.models import Merchant, portalLogin


from testing import *
from ext import db
from flask import url_for

import hashlib
from src.portal import ADMIN, COMPANY, AGENT, MERCHANT
from src.portal import ADMIN_ROLE_LIST, COMPANY_ROLE_LIST, AGENT_ROLE_LIST, MERCHANT_ROLE_LIST
from src.portal import MAIN_ROLE,ACCOUNTS_MANAGER,CUSTOMER_CARE,SALES_AGENT



class MerchantTest(KitTestCase):
	def setUp(self):
		pass

	def tearDown(self):
		payment.query.delete() # temp patch
		db.session.commit()

	def addMerchantToNewCompanyTest(self,has_permission):
		company,company_login = createEntryForRole(COMPANY,MAIN_ROLE) # create new company
		payload = getOnBoardMerchantPayload(company.username)

		self.process_onBoardMerchant(payload,has_permission)
		cleanUpEntryOfRole(company.username,company_login)

	def addMerchantToNewAgentTest(self,has_permission):
		# agentName # companyName
		agent,agent_login = createEntryForRole(AGENT,MAIN_ROLE) # create new agent
		payload = getOnBoardMerchantPayload(agent.company,agent.username)

		self.process_onBoardMerchant(payload,has_permission)
		cleanUpEntryOfRole(agent.username,agent_login)


	def onBoardMerchantTest(self,baseRole,subRole,onActionRole,ofOwn):
		# creating a entry who logsin
		entity,login = createEntryForRole(baseRole,subRole)

		username = login.username
		password = hashlib.sha256(PASSWORD).hexdigest()

		# login (baseRole,subRole) entry
		payload = {'username':username,'password':password}
		response = self.client.post(url_for('basePortal.login'),data=payload)

		#check whether it is redirecting to view profile
		self.assertRedirects(response,url_for('basePortal.viewProfile'))

		### testing functionality

		if baseRole == ADMIN:
			# sub roles with permissions
			if subRole == MAIN_ROLE or subRole == ACCOUNTS_MANAGER:

				if onActionRole == COMPANY:
					self.addMerchantToNewCompanyTest(True)
				elif onActionRole == AGENT:
					self.addMerchantToNewAgentTest(True)

			elif subRole == SALES_AGENT or subRole == CUSTOMER_CARE:

				if onActionRole == COMPANY:
					self.addMerchantToNewCompanyTest(False)
				elif onActionRole == AGENT:
					self.addMerchantToNewAgentTest(False)
			
		elif baseRole == COMPANY:
			if subRole == MAIN_ROLE:
				# company main role
				if onActionRole == COMPANY:
					if ofOwn:
						companyName = entity.username
						payload = getOnBoardMerchantPayload(companyName)

						self.process_onBoardMerchant(payload,True)
					else:
						self.addMerchantToNewCompanyTest(False)

				if onActionRole == AGENT:
					if ofOwn:
						companyName = entity.username
						agent,agent_login = createNewAgent(companyName)

						agentName = agent.username
						payload = getOnBoardMerchantPayload(companyName,agentName)

						self.process_onBoardMerchant(payload,True)

						#clean up agent
						cleanUpEntryOfRole(agent.username,agent_login)
					else:
						self.addMerchantToNewAgentTest(False)
						
			elif subRole == ACCOUNTS_MANAGER:
				# company employees with permission
				if onActionRole == COMPANY:
					if ofOwn:
						companyName = entity.parentDetails.username
						payload = getOnBoardMerchantPayload(companyName)

						self.process_onBoardMerchant(payload,True)
					else:
						self.addMerchantToNewCompanyTest(False)

				if onActionRole == AGENT:
					if ofOwn:
						companyName = entity.parentDetails.username
						agent,agent_login = createNewAgent(companyName)

						agentName = agent.username
						payload = getOnBoardMerchantPayload(companyName,agentName)

						self.process_onBoardMerchant(payload,True)

						#clean up agent
						agent.delete()
						agent_login.delete()
					else:
						self.addMerchantToNewAgentTest(False)
				pass
			elif subRole == CUSTOMER_CARE or subRole == SALES_AGENT:
				# company employees with no permission
				if onActionRole == COMPANY:
					if ofOwn:
						companyName = entity.parentDetails.username
						payload = getOnBoardMerchantPayload(companyName)

						self.process_onBoardMerchant(payload,False)
					else:
						self.addMerchantToNewCompanyTest(False)

				if onActionRole == AGENT:
					if ofOwn:
						companyName = entity.parentDetails.username
						agent,agent_login = createNewAgent(companyName)

						agentName = agent.username
						payload = getOnBoardMerchantPayload(companyName,agentName)

						self.process_onBoardMerchant(payload,False)

						#clean up agent
						agent.delete()
						agent_login.delete()
					else:
						self.addMerchantToNewAgentTest(False)
				
		elif baseRole == AGENT:
			# roles having permission
			payload = getOnBoardMerchantPayload()
			if subRole == MAIN_ROLE or subRole == ACCOUNTS_MANAGER:
				self.process_onBoardMerchant(payload,True)
			else:
				self.process_onBoardMerchant(payload,False)

		elif baseRole == MERCHANT:
			payload = getOnBoardMerchantPayload()
			self.process_onBoardMerchant(payload,False)

		### logout and clean up logged in entry

		# logout entity 
		response = self.client.get(url_for('basePortal.logout'))

		# clean up entity
		if db.session.query(portalLogin).filter_by(username = entity.username).first() != None:
			cleanUpEntryOfRole(entity.username)

	def test_OnBoardMerchant(self):
		
		for role in ADMIN_ROLE_LIST:
			self.onBoardMerchantTest(ADMIN,role,COMPANY,True)
			self.onBoardMerchantTest(ADMIN,role,AGENT,True)
		
		for role in COMPANY_ROLE_LIST:
			self.onBoardMerchantTest(COMPANY,role,COMPANY,True)
			self.onBoardMerchantTest(COMPANY,role,COMPANY,False)

			self.onBoardMerchantTest(COMPANY,role,AGENT,True)
			self.onBoardMerchantTest(COMPANY,role,AGENT,False)

		# agent tests
		# does not matter what actionRole is and ofOwn is.
		for role in AGENT_ROLE_LIST:
			self.onBoardMerchantTest(AGENT,role,COMPANY,True)		

		#merchant tests
		# does not matter what actionRole is and ofOwn is.
		for role in MERCHANT_ROLE_LIST:
			self.onBoardMerchantTest(MERCHANT,role,COMPANY,True)	


	def rollBackOnBoardMerchant(self,merchant):
		# what are all the entries that are added

		for pos in merchant.posDetails:
			# deleting users
			for user in pos.users:
				user.delete()
			pos.delete() # self destruct

		# deleting payments
		for payment in merchant.payments:
			merchant.delete()

		merchant_login = merchant.loginDetails

		# delting merchant details
		merchant.delete()
		merchant_login.delete()


	def process_onBoardMerchant(self,payload,has_permission):
	
		response = self.client.post(url_for('basePortal.addMerchantWithPos'),data=payload)
		merchant = db.session.query(Merchant).filter_by(username = payload['username']).first()
		if has_permission:
			assert(merchant is not None)
			self.rollBackOnBoardMerchant(merchant)
		else:
			assert(merchant is None)
