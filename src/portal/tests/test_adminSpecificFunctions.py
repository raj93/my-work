#from subprocess import _args_from_interpreter_flags
from flask import url_for

from src.models import portalLogin, clientIdInventory, dongleInventory
import hashlib
from src.portal import ADMIN, COMPANY, AGENT, MERCHANT,ADMIN_ROLE_LIST,COMPANY_ROLE_LIST,AGENT_ROLE_LIST,MERCHANT_ROLE_LIST
from src.portal import MAIN_ROLE
from testing import *

from ext import db
from src.utils.crypto import otp_generator1, otp_generator

# TESTS
# Add ClientId
# Add Dongle
# View Companies

class TestAdminSpecificFuncs(KitTestCase):
	
	def setUp(self):
		pass

	def tearDown(self):
		pass

	def addClientIdTest(self,baseRole,subRole):
		# set up entity
		entity,login = createEntryForRole(baseRole,subRole)

		# login entity
		username = login.username
		password = hashlib.sha256(PASSWORD).hexdigest()

		# login (baseRole,subRole) entry
		payload = {'username':username,'password':password}
		response = self.client.post(url_for('basePortal.login'),data=payload)

		#check whether it is redirecting to view profile
		self.assertRedirects(response,url_for('basePortal.viewProfile'))

		### start functionality
		clientId = otp_generator(5,string.digits)
		payload = {'clientID':int(clientId),'securityToken':otp_generator1(10)}
		response = self.client.post(url_for('adminPortal.addClientId'),data=payload)

		entry = db.session.query(clientIdInventory).filter_by(clientID = clientId).first()
		
		permitted_users = [[ADMIN,MAIN_ROLE]]
		if [login.role,login.subRole] in permitted_users:
			assert(entry is not None)
			entry.delete()
		else:
			assert(entry is None)

		### end of functionality
		# logout entity 
		response = self.client.get(url_for('basePortal.logout'))

		# clean up entity
		if db.session.query(portalLogin).filter_by(username = entity.username).first() != None:
			cleanUpEntryOfRole(entity.username)

	# testing add clientid functionality
	def testAddClientId(self):
		for role in ADMIN_ROLE_LIST:
			self.addClientIdTest(ADMIN,role)

		for role in COMPANY_ROLE_LIST:
			self.addClientIdTest(COMPANY,role)

		for role in AGENT_ROLE_LIST:
			self.addClientIdTest(AGENT,role)

		for role in MERCHANT_ROLE_LIST:
			self.addClientIdTest(MERCHANT,role)
		

	def addDongleTest(self,baseRole,subRole):
		# set up entity
		entity,login = createEntryForRole(baseRole,subRole)

		# login entity
		username = login.username
		password = hashlib.sha256(PASSWORD).hexdigest()

		# login (baseRole,subRole) entry
		payload = {'username':username,'password':password}
		response = self.client.post(url_for('basePortal.login'),data=payload)

		#check whether it is redirecting to view profile
		self.assertRedirects(response,url_for('basePortal.viewProfile'))

		### start functionality
		dongleSerial = otp_generator1(10)
		payload = {'dongleSerial':dongleSerial}

		response = self.client.post(url_for('adminPortal.addDongle'),data=payload)

		entry = db.session.query(dongleInventory).filter_by(dongleSerial = dongleSerial).first()

		permitted_users = [[ADMIN,MAIN_ROLE]]

		if [login.role,login.subRole] in permitted_users:
			assert(entry is not None)
			entry.delete()
		else:
			assert(entry is None)

		### end of functionality
		# logout entity 
		response = self.client.get(url_for('basePortal.logout'))

		# clean up entity
		if db.session.query(portalLogin).filter_by(username = entity.username).first() != None:
			cleanUpEntryOfRole(entity.username)


	# testing add Dongle functionality
	def testAddDongle(self):
		
		for role in ADMIN_ROLE_LIST:
			self.addDongleTest(ADMIN,role)

		for role in COMPANY_ROLE_LIST:
			self.addDongleTest(COMPANY,role)

		for role in AGENT_ROLE_LIST:
			self.addDongleTest(AGENT,role)

		for role in MERCHANT_ROLE_LIST:
			self.addDongleTest(MERCHANT,role)

	def viewCompaniesTest(self,baseRole,subRole,has_permission):
		# set up entity
		entity,login = createEntryForRole(baseRole,subRole)

		# login entity
		username = login.username
		password = hashlib.sha256(PASSWORD).hexdigest()

		# login (baseRole,subRole) entry
		payload = {'username':username,'password':password}
		response = self.client.post(url_for('basePortal.login'),data=payload)

		#check whether it is redirecting to view profile
		self.assertRedirects(response,url_for('basePortal.viewProfile'))

		### start functionality
		response = self.client.get(url_for('basePortal.viewCompanies'))
		if has_permission:
			self.assertTemplateUsed('users/view_companies.html')
		else:
			self.assertRedirects(response,url_for('basePortal.viewProfile'))

		### end of functionality
		# logout entity 
		response = self.client.get(url_for('basePortal.logout'))

		# clean up entity
		if db.session.query(portalLogin).filter_by(username = entity.username).first() != None:
			cleanUpEntryOfRole(entity.username)		


	# view companies functionality
	def testViewCompanies(self):
		self.viewCompaniesTest(ADMIN,MAIN_ROLE,True)

		for role in ADMIN_ROLE_LIST:
			self.addDongleTest(ADMIN,role)

		for role in COMPANY_ROLE_LIST:
			self.addDongleTest(COMPANY,role)

		for role in AGENT_ROLE_LIST:
			self.addDongleTest(AGENT,role)

		for role in MERCHANT_ROLE_LIST:
			self.addDongleTest(MERCHANT,role)

	