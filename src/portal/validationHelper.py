import re

OTHER_ALPHABET = 1
NUMERIC_ALPHABET = 2
CAPITAL_ALPHABET = 4
SMALL_ALPHABET = 8

# returns the type of the character
def getType(ascii):
	if 48 <= ascii <= 57:
		return NUMERIC_ALPHABET
	if 65 <= ascii <= 90:
		return CAPITAL_ALPHABET
	if 97 <= ascii <= 122:
		return SMALL_ALPHABET
	return OTHER_ALPHABET


# validates the password 
def isPasswordStrong(passwd):
	val = 0
	for c in passwd:
		val |= getType(ord(c))

	if val > SMALL_ALPHABET:
		return True

	return False

# checking whether a string is all digits
def isThisADigitChain(chain):
	val = 0
	for c in chain:
		val |= getType(ord(c))

	if val == NUMERIC_ALPHABET:
		return True
	return False

def isThisAnAlphabetChain(chain):
	val = 0
	for c in chain:
		val |= getType(ord(c))
	
	if val >= CAPITAL_ALPHABET and (val % CAPITAL_ALPHABET) == 0:
		return True

	return False

def doesThisHaveSpecialCharacters(chain):
	val = 0
	for c in chain:
		val |= getType(ord(c))

	if val & OTHER_ALPHABET != 0:
		return True

	return False
	

