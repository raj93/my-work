from flask import Blueprint, render_template, request
from flask.ext.login import current_user
from math import ceil

from ext import db, db1

from sqlalchemy import and_,or_
import calendar
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from src.models import Transaction, SettledTransaction
from src.portal import ADMIN, COMPANY, AGENT, MERCHANT

transaction = Blueprint('transactionPortal', __name__, url_prefix='/transaction')

from principalConstants import view_transactions_permission

PER_PAGE = 5


def getCurrentTime():
	# return the time at which query is issued in india
	return (datetime.utcnow() + timedelta(hours=5, minutes=30))

"""
	Class used for paginating the result entries.
"""
class Pagination(object):

	def __init__(self, page, per_page, total_count):
		self.page = page
		self.per_page = per_page
		self.total_count = total_count

	@property
	def pages(self):
		return int(ceil(self.total_count / float(self.per_page)))

	@property
	def has_prev(self):
		return self.page > 1

	@property
	def has_next(self):
		return self.page < self.pages

	def iter_pages(self, left_edge=2, left_current=2,
				   right_current=5, right_edge=3):
		last = 0
		for num in xrange(1, self.pages + 1):
			if num <= left_edge or \
			   (num > self.page - left_current - 1 and \
				num < self.page + right_current) or \
			   num > self.pages - right_edge:
				if last + 1 != num:
					yield None
				yield num
				last = num

"""
	wrapper of show_transactions view with filter[in view context]
"""
@transaction.route('/viewtransactions')
def viewTransactions():
	return render_template('transaction/view_transactions.html')

"""
	Get the un settled transactions
"""
def getUnsettledTransactions():
	#by default everything
	#unsettled
	ttypeCondition = None
	dtypeCondition = None
	authTypeCondition = None

	# basic units before doing and and or
	baseConditions = []

	# authorization filter
	authTypeConditions = []

	if current_user.role == ADMIN:
		pass
	elif current_user.role == COMPANY:
		if current_user.subRole == 0:
			authTypeConditions.append(Transaction.company == current_user.username)
		else:
			authTypeConditions.append(Transaction.company == current_user.companyEmployee.company)
	elif current_user.role == AGENT:
		if current_user.subRole == 0:
			authTypeConditions.append(Transaction.agent == current_user.username)
		else:
			authTypeConditions.append(Transaction.agent == current_user.agentEmployee.agent)
	elif current_user.role == MERCHANT:
		if current_user.subRole == 0:
			authTypeConditions.append(Transaction.merchant == current_user.username)
		else:
			authTypeConditions.append(Transaction.merchant == current_user.merchantEmployee.merchant)
	else:
		#TODO code should not come here.
		pass
	authTypeCondition = and_(*authTypeConditions)
	baseConditions.append(authTypeCondition)

	#TODO don't show disabled transactions . add a condition to filter them
	# transaction filter
	# type=all | type=sale | type=void ?ttype=sale&ttype=void&ttype=all&
	if 'ttype' in request.args:
		reqTypes = request.args.getlist('ttype') # request 
		if 'all' in reqTypes:
			# by default all so we can pass the buck
			pass
		else:
			ttypeConditions = []
			# TODO map statuses to names (bit structuring remember!)
			if 'sale' in reqTypes:
				ttypeConditions.append(Transaction.conditions('SIMPLE_PURCHASE'))
				ttypeConditions.append(Transaction.conditions('TIPPED_PURCHASE'))
			if 'void' in reqTypes:
				ttypeConditions.append(Transaction.conditions('VOIDED_PURCHASE'))
				ttypeConditions.append(Transaction.conditions('VOIDED_TIP'))

			ttypeCondition = or_(*ttypeConditions)
			baseConditions.append(ttypeCondition)

	current_time = getCurrentTime()
	indianTimeDelta = timedelta(hours=5,minutes=30)

	# date filter
	if 'dtype' in request.args:
		dateType = request.args['dtype']

		dtypeConditions = []

		if dateType == 'today':
			today = (current_time.replace(hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta).strftime("%Y-%m-%d %X")
			dtypeConditions.append(Transaction.transTime > today)
		elif dateType == 'yesterday':
			todayDt = current_time.replace(hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta
			today = todayDt.strftime("%Y-%m-%d %X")
			yesterday = (todayDt + timedelta(days=-1)).strftime("%Y-%m-%d %X")

			#> yesterday < today
			dtypeConditions.append(Transaction.transTime > yesterday)
			dtypeConditions.append(Transaction.transTime < today)

		elif dateType == 'thisweek':
			thisweek = ((current_time+timedelta(days=-1*current_time.weekday())).replace(hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta).strftime("%Y-%m-%d %X")
			dtypeConditions.append(Transaction.transTime > thisweek)

		elif dateType == 'lastweek':
			thisweekDt = (current_time + timedelta(days=-1*current_time.weekday())).replace(hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta 
			thisweek = thisweekDt.strftime("%Y-%m-%d %X")
			lastweek = (thisweekDt + timedelta(days=-7)).strftime("%Y-%m-%d %X")
			
			# >lastweek <thisweek
			dtypeConditions.append(Transaction.transTime < thisweek)
			dtypeConditions.append(Transaction.transTime > lastweek)

		elif dateType == 'thismonth':
			thismonth = (current_time.replace(day=1,hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta).strftime("%Y-%m-%d %X")

			dtypeConditions.append(Transaction.transTime > thismonth)

		elif dateType == 'lastmonth':
			thismonth = (current_time.replace(day=1,hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta).strftime("%Y-%m-%d %X")
			dtypeConditions.append(Transaction.transTime < thismonth)

			lastmonth = ((current_time - relativedelta(months=1)).replace(day=1,hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta).strftime("%Y-%m-%d %X")

			dtypeConditions.append(Transaction.transTime > lastmonth)
		elif dateType == 'range':
			dateFrom = request.args['dfrom']
			dateTo = request.args['dto']

			dateFrom += " 00:00:00"
			dateTo += " 00:00:00"

			dateFrom = (datetime.strptime(dateFrom,"%Y-%m-%d %X") - indianTimeDelta ).strftime("%Y-%m-%d %X")
			dateTo = (datetime.strptime(dateTo,"%Y-%m-%d %X") - indianTimeDelta + timedelta(days=1)).strftime("%Y-%m-%d %X")
			
			dtypeConditions.append(Transaction.transTime <= dateTo)
			dtypeConditions.append(Transaction.transTime >= dateFrom)

		elif dateType == 'irrespective':
			# by default irrespective
			pass
		else:
			# by default irrespective
			pass
		dtypeCondition = and_(*dtypeConditions)
		baseConditions.append(dtypeCondition)

	else:
		# if 'dtype' is not there then show today's transactions 
		dtypeConditions = []

		today = (current_time.replace(hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta).strftime("%Y-%m-%d %X")
		dtypeConditions.append(Transaction.transTime > today)
		dtypeCondition = and_(*dtypeConditions)
		baseConditions.append(dtypeCondition)

	# final punch
	# unsettled
	finalCondition = and_()
	for condition in baseConditions:
		finalCondition = and_(finalCondition,condition)

	# getting the page if present else default to 1
	if 'page' in request.args:
		page = int(request.args['page'])
	else:
		page = 1

	
	# sorting stuff
	operations = []

	# date and amount
	if 'orderType' in request.args:
		if request.args['orderType'] == 'timedesc':
			operations.append(Transaction.transTime.desc())
		elif request.args['orderType'] == 'timeasc':
			operations.append(Transaction.transTime.asc())
		elif request.args['orderType'] == 'amountdesc':
			operations.append(Transaction.amount.desc())
		elif request.args['orderType'] == 'amountasc':
			operations.append(Transaction.amount.asc())
		else:
			# by default descening
			operations.append(Transaction.transTime.desc())
	else:
		# by default descening
		operations.append(Transaction.transTime.desc())

	count = Transaction.query.filter(finalCondition).count()
	print page
	transactions = Transaction.query.filter(finalCondition).order_by(*operations).limit(PER_PAGE).offset((page-1)*PER_PAGE)

	return count,transactions	

"""
	Get the settled transactions
"""
def getSettledTransactions():
	#settled
	stld_ttypeCondition = None
	stld_dtypeCondition = None
	stld_authTypeCondition = None

	# basic units before doing and and or
	stld_baseConditions = []

	# authorization filter
	stld_authTypeConditions = []

	if current_user.role == ADMIN:
		pass
	elif current_user.role == COMPANY:
		if current_user.subRole == 0:
			stld_authTypeConditions.append(SettledTransaction.company == current_user.username)
		else:
			stld_authTypeConditions.append(SettledTransaction.company == current_user.companyEmployee.company)
	elif current_user.role == AGENT:
		if current_user.subRole == 0:
			stld_authTypeConditions.append(SettledTransaction.agent == current_user.username)
		else:
			stld_authTypeConditions.append(SettledTransaction.agent == current_user.agentEmployee.agent)
	elif current_user.role == MERCHANT:
		if current_user.subRole == 0:
			stld_authTypeConditions.append(SettledTransaction.merchant == current_user.username)
		else:
			stld_authTypeConditions.append(SettledTransaction.merchant == current_user.merchantEmployee.merchant)
	else:
		#TODO code should not come here.
		pass
	
	stld_authTypeCondition = and_(*stld_authTypeConditions)
	stld_baseConditions.append(stld_authTypeCondition)

	# transaction filter
	# ttype=all | ttype=sale | ttype=void ?ttype=sale&ttype=void&ttype=all&
	if 'ttype' in request.args:
		reqTypes = request.args.getlist('ttype') # request 
		if 'all' in reqTypes:
			# by default all so we can pass the buck
			pass
		else:
			stld_ttypeConditions = []
			# TODO map statuses to names (bit structuring remember!)
			if 'sale' in reqTypes:
				stld_ttypeConditions.append(SettledTransaction.conditions('SIMPLE_PURCHASE'))
				stld_ttypeConditions.append(SettledTransaction.conditions('TIPPED_PURCHASE'))
			if 'void' in reqTypes:
				stld_ttypeConditions.append(SettledTransaction.conditions('VOIDED_PURCHASE'))
				stld_ttypeConditions.append(SettledTransaction.conditions('VOIDED_TIP'))

			stld_ttypeCondition = or_(*stld_ttypeConditions)
			stld_baseConditions.append(stld_ttypeCondition)


	# date filter
	current_time = getCurrentTime()
	indianTimeDelta = timedelta(hours=5, minutes=30)

	if 'dtype' in request.args:
		dateType = request.args['dtype']

		dtypeConditions = []
		stld_dtypeConditions = []

		if dateType == 'today':
			today = (current_time.replace(hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta).strftime("%Y-%m-%d %X")
			stld_dtypeConditions.append(SettledTransaction.transTime > today)

		elif dateType == 'yesterday':
			todayDt = current_time.replace(hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta
			today = todayDt.strftime("%Y-%m-%d %X")
			yesterday = (todayDt + timedelta(days=-1)).strftime("%Y-%m-%d %X")
			
			stld_dtypeConditions.append(SettledTransaction.transTime > yesterday)
			stld_dtypeConditions.append(SettledTransaction.transTime < today)
		elif dateType == 'thisweek':
			thisweek = ((current_time+timedelta(days=-1*current_time.weekday())).replace(hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta).strftime("%Y-%m-%d %X")
			
			stld_dtypeConditions.append(SettledTransaction.transTime > thisweek)
		elif dateType == 'lastweek':
			thisweekDt = (current_time + timedelta(days=-1*current_time.weekday())).replace(hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta 
			thisweek = thisweekDt.strftime("%Y-%m-%d %X")
			lastweek = (thisweekDt + timedelta(days=-7)).strftime("%Y-%m-%d %X")
			# >lastweek <thisweek

			stld_dtypeConditions.append(SettledTransaction.transTime < thisweek)
			stld_dtypeConditions.append(SettledTransaction.transTime > lastweek)
		elif dateType == 'thismonth':
			thismonth = (current_time.replace(day=1,hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta).strftime("%Y-%m-%d %X")

			stld_dtypeConditions.append(SettledTransaction.transTime > thismonth)
		elif dateType == 'lastmonth':
			thismonth = (current_time.replace(day=1,hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta).strftime("%Y-%m-%d %X")

			stld_dtypeConditions.append(SettledTransaction.transTime < thismonth)

			lastmonth = ((current_time - relativedelta(months=1)).replace(day=1,hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta).strftime("%Y-%m-%d %X")

			stld_dtypeConditions.append(SettledTransaction.transTime > lastmonth)
		elif dateType == 'range':

			# format of 'dfrom' and 'dto' "YYYY-MM-DD 00:00:00"
			# TODO check whether arguments are present
			# check whether the format is present

			dateFrom = request.args['dfrom']
			dateTo = request.args['dto']
			
			dateFrom += " 00:00:00"
			dateTo += " 00:00:00"

			dateFrom = (datetime.strptime(dateFrom,"%Y-%m-%d %X") - indianTimeDelta).strftime("%Y-%m-%d %X")
			dateTo = (datetime.strptime(dateTo,"%Y-%m-%d %X") - indianTimeDelta + timedelta(days=1)).strftime("%Y-%m-%d %X")

			stld_dtypeConditions.append(SettledTransaction.transTime <= dateTo)
			stld_dtypeConditions.append(SettledTransaction.transTime >= dateFrom)

		elif dateType == 'irrespective':
			# by default irrespective
			pass
		else:
			# by default irrespective
			pass

		stld_dtypeCondition = and_(*stld_dtypeConditions)
		stld_baseConditions.append(stld_dtypeCondition)
	else:
		# if 'dtype' is not there then show today's transactions 
		stld_dtypeConditions = []

		today = (current_time.replace(hour=0, minute=0, second=0, microsecond=0) - indianTimeDelta).strftime("%Y-%m-%d %X")

		stld_dtypeConditions.append(SettledTransaction.transTime > today)
		stld_dtypeCondition = and_(*stld_dtypeConditions)
		stld_baseConditions.append(stld_dtypeCondition)


	# final punch
	# settled
	stld_finalCondition = and_()
	for condition in stld_baseConditions:
		stld_finalCondition = and_(stld_finalCondition,condition)


	# getting the page if present else default to 1
	if 'page' in request.args:
		page = int(request.args['page'])
	else:
		page = 1

	
	# sorting stuff
	stld_operations = []

	# date and amount
	if 'orderType' in request.args:
		if request.args['orderType'] == 'timedesc':
			stld_operations.append(SettledTransaction.transTime.desc())
		elif request.args['orderType'] == 'timeasc':
			stld_operations.append(SettledTransaction.transTime.asc())
		elif request.args['orderType'] == 'amountdesc':
			stld_operations.append(SettledTransaction.amount.desc())
		elif request.args['orderType'] == 'amountasc':
			stld_operations.append(SettledTransaction.amount.asc())
		else:
			# by default descening
			stld_operations.append(SettledTransaction.transTime.desc())
	else:
		# by default descening
		stld_operations.append(SettledTransaction.transTime.desc())
	
	stld_count = SettledTransaction.query.filter(stld_finalCondition).count()
	print page
	stld_transactions = SettledTransaction.query.filter(stld_finalCondition).order_by(*stld_operations).limit(PER_PAGE).offset((page-1)*PER_PAGE)

	return stld_count,stld_transactions	

"""
	returns the transactions that match the user's selected options.
"""
@transaction.route('/transactions/')
def show_transactions():

	#checking permission
	if not view_transactions_permission.can():
		return 'No transactions found'

	# getting the page if present else default to 1
	if 'page' in request.args:
		page = int(request.args['page'])
	else:
		page = 1

	if 'status' in request.args:
		if request.args['status'] == 'settled':
			count,transactions = getSettledTransactions()
		elif request.args['status'] == 'unsettled':
			count,transactions = getUnsettledTransactions()
		else:
			# this happens when requesting transactions page 
			# with out any arguments.
			count = 0
			transactions = []
	else:
		# by default get unsettled transactions
		count,transactions = getUnsettledTransactions()



	# no entries and requesting later pages abort
	if not transactions and page != 1:
		abort(404)
	pagination = Pagination(page, PER_PAGE, count)

	return render_template('transaction/transactions.html',
		pagination=pagination,
		transactions=transactions
	)
	