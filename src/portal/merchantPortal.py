from flask import render_template, request, flash, redirect, url_for, current_app
from flask.ext.login import current_user
from flask.ext.principal import Permission

from src.utils.crypto import hash_passwd
from src.utils.s3 import uploadFile

from ext import db
from src.portal import ADMIN, COMPANY, AGENT, MERCHANT
from src.models import portalLogin, Company, Agent,Merchant,Transaction,payment,posDetails,posUsers,clientIdInventory,\
						merchantProofs
from src.models import LastTransaction,Void,Tip,pendingTip,Settlement

from forms import AddMerchantWithPosForm,EditMerchantWithPosForm
from src.utils.crypto import otp_generator
from sqlalchemy import and_,or_,desc
import random,json
from principalConstants import *
from src.utils.ses_email import send1
import os
from portalHelper import *

"""
	Delete the given merchant
"""

def deleteMerchant(merch):
	"""
	delete from posUsers; delete from posStatus; delete from lastTransaction; delete from void; delete from tip; delete from transactions;
	delete from pendingTips; delete from settle; delete from posDetails; 
	delete from merchant; delete from agent; delete from company; delete from admin; delete from portalLogin
	"""
	merchant = Merchant.query.filter_by(username = merch).first()

	if merchant is None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	if not isHeAdmin():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	for employee in merchant.employees:
		employee.delete(commit = False)

	# only admin has permission

	for pos in merchant.posDetails:
		# deleting users
		for user in pos.users:
			user.delete(commit = False)

		last = db.session.query(LastTransaction).filter_by(clientId = pos.clientId).first()
		if last != None:
			last.delete(commit = False)

		# deleting voids
		voids = db.session.query(Void).filter_by(clientId = pos.clientId).all()
		for void in voids:
			void.delete(commit = False)

		# deleting tips
		tips = db.session.query(Tip).filter_by(clientId = pos.clientId).all()
		for tip in tips:
			tip.delete(commit = False)

		# deleting purhcases
		txns = db.session.query(Transaction).filter_by(clientId = pos.clientId).all()
		for purchase in txns:
			purchase.delete(commit = False)

		# pending tips
		pendingTips = db.session.query(pendingTip).filter_by(clientId = pos.clientId).all()
		for ptip in pendingTips:
			ptip.delete(commit = False)

		# settle
		settlements = db.session.query(Settlement).filter_by(clientId = pos.clientId).all()
		for settlement in settlements:
			settlement.delete(commit = False)

		logs = posLogs.query.filter_by(clientId = pos.clientId).all()
		for log in logs:
			log.delete(commit = False)

		pos.delete(commit = False) # self destruct

	# deleting payments
	for payment in merchant.payments:
		payment.delete(commit = False)

	merchant_login = merchant.loginDetails

	proofs = db.session.query(merchantProofs).filter_by(merchant = merchant.username).all()
	for proof in proofs:
		proof.delete(commit = False)

	# delting merchant details
	merchant.delete(commit = False)
	merchant_login.delete(commit = False)

	logEvent("deleted merchant %s" % merch)
	db.session.commit()

	flash('deleted merchant '+merch+' successfully')
	return redirect(url_for('basePortal.viewProfile'))

"""
	Allot mid to the merchant.
"""
def allotMid(merchant):
	NO_ACCESS = "you don't have access to perform this action"
	if not allot_mid_permission.can():
		return NO_ACCESS

	# check whether required entry is present or not.
	merchant = Merchant.query.filter_by(username = merchant).first()
	if merchant == None:
		return 'failure'

	# checking authorization 
	if not canHeMeddleWithThisMerchant(merchant):
		return NO_ACCESS
	
	# check whether it is already verified
	if merchant.bitmap & Merchant.BITMAP['MERCHANT_MID_ALLOTED'] != 0:
		return 'mid already alloted'

	# updating bitmap.
	mid = request.form['mid']
	bitmap = merchant.bitmap | Merchant.BITMAP['MERCHANT_MID_ALLOTED']
	merchant.update(commit = False, bitmap=bitmap, mid=mid)

	# set the flag in terminals
	terminals = merchant.posDetails
	for terminal in terminals:
		bitmap = terminal.bitmap | posDetails.BITMAP['MID_ALLOTED']
		terminal.update(commit = False, bitmap=bitmap)

	logEvent("Alloted MID",onActionUser=merchant.username)
	db.session.commit()

	return 'success'

"""
	Change the given merchant state from (lock -> unlock) or (unlock -> lock)
"""
def changeMerchantState(merchant):
	# check for permission
	if not lock_merchant_permission.can():
		return redirect(url_for('basePortal.viewProfile'))

	# check the existence of terminal and the given merchant name should match
	merch = db.session.query(Merchant).filter_by(username=merchant).first()
	if merch == None:
		return redirect(url_for('basePortal.viewProfile'))

	# checking authorization 
	if not canHeMeddleWithThisMerchant(merch):
		return redirect(url_for('basePortal.viewProfile'))

	# reverse the lock bit of merchant
	bitmap = merch.bitmap ^ Merchant.BITMAP['MERCHANT_LOCK']
	merch.update(commit=False, bitmap=bitmap)
	
	# reverse the lock bit of terminals 
	terminals = merch.posDetails
	for terminal in terminals:
		bitmap = terminal.bitmap ^ posDetails.BITMAP['MERCHANT_LOCK']
		terminal.update(commit=False, bitmap=bitmap)

	if bitmap & Merchant.BITMAP['MERCHANT_LOCK']:
		logEvent("locked merchant", onActionUser=merch.username)
	else:
		logEvent("un locked merchant", onActionUser=merch.username)


	db.session.commit()
	
	return redirect(url_for('basePortal.viewMerchant', name=merchant))

"""
	verifies the merchant that his personal and address details are true.
"""
def verifyMerchant():
	response = {'status':False,'feedback':''}

	if not verify_merchant_permission.can():
		response['feedback']  = "Permisssion denied"
		return json.dumps(response)

	if 'id' not in request.form:
		response['feedback'] = "id parameter not present in request"
		return json.dumps(response)

	id = request.form['id']

	
	merchant = Merchant.query.filter_by(id = id).first()

	# checking whether requested merchant exists or not
	if merchant is None:
		# accessing non existent
		response['feedback'] = "requested merchant not present"
		return json.dumps(response) 

	# checking authorization 
	if not canHeMeddleWithThisMerchant(merchant):
		response['feedback'] = "permission denied"
		return json.dumps(response) 

	# checking whether merchant is already verified
	if merchant.bitmap & Merchant.BITMAP['MERCHANT_VERIFICATION'] != 0:
		response['feedback'] = "merchant already verified"
		return json.dumps(response) 

	bitmap = merchant.bitmap | Merchant.BITMAP['MERCHANT_VERIFICATION']
	merchant.update(commit = False, bitmap=bitmap)

	# setting the flag in terminals 
	terminals = merchant.posDetails
	for terminal in terminals:
		bitmap = terminal.bitmap | posDetails.BITMAP['MERCHANT_VERIFICATION']
		terminal.update(commit = False, bitmap=bitmap)

	db.session.commit()
	logEvent("verified merchant",onActionUser=merchant.username)
	
	response['status'] = True
	response['feedback'] = "merchant verified successfully"
	return json.dumps(response) 

"""
	Displays the page with list of merchants need to be verified.
"""
def merchantVerification():

	merchants = [] 

	#only users with admin as base role can do merchant verification process
	if current_user.role != ADMIN:
		return redirect(url_for('basePortal.viewProfile'))

	merchants = db.session.query(Merchant).filter(Merchant.bitmap.op('&')(Merchant.BITMAP['MERCHANT_VERIFICATION']) == 0).all()	

	return render_template('verification/merchant_verification.html',merchants=merchants)

"""
	View Merchants of an agent/company user.
"""
def viewMerchants():
	# checking permission
	if not view_merchant_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	#checking the existence of the agent/company
	if 'username' in request.args:
		username = request.args['username']
	else:
		username = current_user.username

	entry = portalLogin.query.filter_by(username = username).first()

	# making sure 
	# requested user present
	# role of request is company or agent
	if entry is None or not (entry.subRole == 0 and (entry.role == COMPANY or entry.role == AGENT)):
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	if entry.role == AGENT:
		req = entry.agent
	else:
		req = entry.company

	# checking access rights
	if current_user.role == ADMIN:
		pass
	elif current_user.role == COMPANY:
		if current_user.subRole == 0:
			companyName = current_user.username
		else:
			companyName = current_user.companyEmployee.company

		if entry.role == AGENT:
			if companyName != req.company:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		else:
			if companyName != req.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))

	elif current_user.role == AGENT:
		if current_user.subRole == 0:
			agentName = current_user.username
		else:
			agentName = current_user.agentEmployee.agent

		if entry.role == AGENT:
			if agentName != req.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		else:
			# agent cannot view  merchants of a company
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))

	else:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	#getting merchants
	if entry.role == AGENT:
		merchants = Merchant.query.filter_by(agent = req.username).all()
	else:
		merchants = Merchant.query.filter_by(company = req.username).all()

	logEvent("view merchants", onActionUser=req.username)

	return render_template('users/view_merchants.html',merchants=merchants)

"""
	Adds the merchant onboarding details to the database.
"""

def addMerchantWithPosToDB(form,c_name,a_name,posList):

	# adding entry to portal login
	import hashlib
	from src.utils.crypto import generateRandomPassword
	password = generateRandomPassword(10)
	hashedPassword,salt = hash_passwd(hashlib.sha256(password).hexdigest())

	a = portalLogin(form.username.data,hashedPassword,salt,MERCHANT,0,True,True);
	a.save(commit = False)

	# adding in to merchant table
	m = Merchant(form.username.data,form.fullname.data,'end of universe',form.mobileNumber.data,form.email.data, \
			form.bankName.data,form.bankBranch.data,form.ifsc.data,form.acNumber.data,form.MDR_Debit.data,form.MDR_Credit.data,form.MDR_CreditGold.data,\
			form.dob.data,form.idProof.data,form.address1.data,form.address2.data,form.city.data,form.district.data,form.state.data,\
			form.pincode.data,form.country.data, form.currency.data, c_name, agent = a_name)
	m.save(commit = False)

	# adding in to payment table
	p = payment(m.username,form.paymentMode.data,form.micr.data,'end of universe',form.paymentAcNumber.data,form.serial.data,form.amount.data,len(posList))
	p.save(commit = False)
	
	db.session.commit()

	try:
		# adding in to pos tables
		for pos in posList:

			d = posDetails(m.username,m.agent,m.company,p.id,pos,'',form.currency.data)
			d.save()
			d.clientId = d.id

			# pos user entry
			import hashlib
			# this is the default password we set. Why is this set because incase of skipotp 
			# we can ask user to use "password" as his password
			# When the pos is activated for the very first time we generate a random password
			# and set the state of pos to "shouldChangePassword"
			passwd,salt = hash_passwd(hashlib.sha256("password").hexdigest())
			u = posUsers(pos,passwd,salt,d.clientId,True)
			u.save()
	except Exception as e:
		current_app.logger.error(str(e))
		# deleting all terminal deatils
		for pos in posList:
			posUsers.query.filter_by(username = pos).delete()
			posDetails.query.filter_by(mobileNumber = pos).delete()
		p.delete(commit = False)
		m.delete(commit = False)
		a.delete(commit = False)

		db.session.commit()
		return False

	# send a mail to merchant's email
	# send password to email
	subject = "Your credentials with temporary password"
	to = m.email
		
	body = "username and password are "+"\n"+ \
			"username: "+ m.username + "\n"+ \
			"password: "+ password + "\n" +\
			"You can access your portal at "+\
			request.url_root[:len(request.url_root)-1] + url_for('basePortal.login')
	
	# sending credentials mail
	send1(subject, to, body,None,None)
	
	# adding proofs to s3
	if form.idProofImage.name in request.files:
		try:
			file = request.files[form.idProofImage.name]
			
			# try to upload if there is a file
			if file != None:
				object_name = file.filename + otp_generator(8)
				path = os.path.join(current_app.config['TEMP_FOLDER'], object_name)
				file.save(path)

				content_type = "image/png"

				if uploadFile(path, current_app.config['UPLOADS_BUCKET'], object_name, content_type):
					proof = merchantProofs(form.username.data, file.filename, object_name)
					proof.save()
					
				os.remove(path)
		except Exception as e:
			current_app.logger.warn('Following exception occured while uploading file to apay.co.in: '+'\n'+str(e))

	return True

"""
	Add merchant to an agent/company
"""
def addMerchantWithPos():

	# he should have permission
	if not add_merchant_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	companyList = []
	agentList = []
	showCompanyList = False
	showAgentList = False

	if current_user.role == ADMIN:
		showCompanyList = True
		showAgentList = True
		
		entries = db.session.query(Company).all()
		for entry in entries:
			companyList.append(entry.username)

	elif current_user.role == COMPANY:
		showCompanyList = True
		showAgentList = True
		if current_user.subRole == 0:
			companyList.append(current_user.username)

			entries = db.session.query(Agent).filter(Agent.company == current_user.username).all()
			for entry in entries:
				agentList.append(entry.username)
		else:			
			companyList.append(current_user.companyEmployee.company)

			entries = db.session.query(Agent).filter(Agent.company == current_user.companyEmployee.company).all()
			for entry in entries:
				agentList.append(entry.username)

	elif current_user.role == AGENT:
		showCompanyList = False
		showAgentList = False
	else:
		return redirect(url_for('basePortal.viewProfile'))

	form = AddMerchantWithPosForm()

	# getting pos list
	posList = []
	if form.posData.data != None:
		posListString = form.posData.data
		posListString = posListString[:len(posListString)-1] #if we don't fill posData ';' is assigned

		if len(posListString) != 0:
			posList = posListString.split(';')
			

	if form.validate_on_submit():

		# upload proof to boto
		import random
		import os

		#check boxes are posted to server only if they are selected not other wise !
		if 'forAgent' in request.form.keys():
			throughAgent = True
		else:
			throughAgent = False

		getView = render_template('merchant/add_merchant_with_pos.html',form=form,companyList=companyList,agentList=agentList,showCompanyList=showCompanyList,showAgentList=showAgentList,posList=posList)

		#check for validity of form.
		if current_user.role == ADMIN:
			c_name = request.form['companies']
			a_name = request.form['agents']

			# check the existence of company
			if db.session.query(Company).filter_by(username = c_name).first() == None:
				flash("company should be selected")
				return getView

			# checking for the requested agent
			if throughAgent:
				if a_name not in getCompanyAgents(c_name):
					flash("agent should be selected")
					return getView
			else:
				a_name = None		
			
			# add merchant to db 
			if not addMerchantWithPosToDB(form,c_name,a_name,posList): return getView
			
			# add merchant to db 
			#company -> c_name
			#agent -> a_name (if any)
		elif current_user.role == COMPANY:
			c_name = request.form['companies']
			a_name = request.form['agents']

			# filling companyName based on the subRole
			if current_user.subRole == 0:
				companyName = current_user.username
			else:
				companyName = current_user.companyEmployee.company

			# restricting company subRole to add to his own and not others
			if c_name != companyName:
				return redirect(url_for('basePortal.viewProfile'))

			if throughAgent:
				# restricting company subRole to add to his own agent and not others.
				if a_name not in getCompanyAgents(companyName):
					return redirect(url_for('basePortal.viewProfile'))
			else:
				a_name = None

			# add merchant to db 
			if not addMerchantWithPosToDB(form,c_name,a_name,posList): return getView

		elif current_user.role == AGENT:
			c_name = None
			a_name = None
			# add merchant to db 
			if current_user.subRole == 0:
				c_name = current_user.agent.company
				a_name = current_user.username
			else:
				c_name = current_user.agentEmployee.parentDetails.company
				a_name = current_user.agentEmployee.parentDetails.username

			if not addMerchantWithPosToDB(form,c_name,a_name,posList): return getView

		else:
			return redirect(url_for('basePortal.viewProfile'))

		if not a_name:
			logEvent("added merchant under company %s"%c_name,onActionUser=c_name)
		else:
			logEvent("added merchant under agent %s"%a_name,onActionUser=a_name)
		db.session.commit()

		flash('added merchant successfully')
		return redirect(url_for('basePortal.viewMerchant',name=form.username.data))

	if current_app.config['TESTING']:
		from portalHelper import populateUserForm
		populateUserForm(form)
	

	return render_template('merchant/add_merchant_with_pos.html',form=form,companyList=companyList,agentList=agentList,showCompanyList=showCompanyList,showAgentList=showAgentList,posList=posList)

"""
	Edit a given merchant
"""
def editMerchant(merchant):

	# checking permission
	if not edit_merchant_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# checking whether requested merchant exists or not
	merchant = db.session.query(Merchant).filter(Merchant.username == merchant).first()
	if merchant is None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile')) # accessing non existent

	# checking whether he has access to the merchant
	if not canHeMeddleWithThisMerchant(merchant):
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))
	

	form = EditMerchantWithPosForm(obj=merchant)
	
	if form.validate_on_submit():

		merchant = populateBaseRoleFields(merchant,form)
		merchant.update(commit = False)
		logEvent("edited merchant",onActionUser=merchant.username)
		db.session.commit()

		flash('updated merchant '+merchant.username+' successfully')
		return redirect(url_for('basePortal.viewMerchant',name = merchant.username))

	return render_template("merchant/edit_merchant.html",form=form,merchant = merchant)

"""
	View a given merchant
"""
def viewMerchant(merchant):
	#checking permission
	if not  view_merchant_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# checking whether requested merchant exists or not
	merchant = Merchant.query.filter_by(username = merchant).first()
	if merchant is None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile')) # accessing non existent

	# union of list is addTerminal and addEmployee
	if not canHeMeddleWithThisMerchant(merchant):
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile')) 

	actionList = getActionListForMerchant(merchant)
	
	# dongle allocation
	showAllotDongleBlock = False
	if allot_dongle_permission.can():
		showAllotDongleBlock = True

	# mid allocation
	showAllotMidBlock = False
	if allot_mid_permission.can():
		if merchant.bitmap & Merchant.BITMAP['MERCHANT_MID_ALLOTED'] == 0:
			showAllotMidBlock = True
		
	# verify merchant
	showVerifyMerchant = False
	if verify_merchant_permission.can():
		if merchant.bitmap & Merchant.BITMAP['MERCHANT_VERIFICATION'] == 0:
			showVerifyMerchant = True

	# verify payment 
	paymentList = []
	if verify_payment_permission.can():
		payments = merchant.payments
		for payment in payments:
			if not payment.isVerified:
				paymentList.append(payment)

	# verify pos
	posList = []
	if verify_pos_permission.can():
		# getting all unverified terminals
		payments = merchant.payments
		
		for payment in payments:
			terminals = payment.terminals 
			for terminal in terminals:
				if terminal.bitmap & posDetails.BITMAP['POS_VERIFICATION'] == 0:
					posList.append(terminal)

	logEvent("viewed merchant",onActionUser=merchant.username)
	db.session.commit()
	
	return render_template("merchant/view_merchant.html",merchant=merchant,actionList=actionList,posList=posList,paymentList = paymentList,showVerifyMerchant = showVerifyMerchant,showAllotMidBlock=showAllotMidBlock,showAllotDongleBlock=showAllotDongleBlock)

"""
	Given an agent it return list of merchants directly under him.
"""
def getAgentMerchants():
	emptyJSON =  json.dumps({"merchants":[]})

	#checking permission
	if not get_merchants_of_agent_permission.can():
		return emptyJSON

	if not 'agent' in request.form:
		return emptyJSON

	agentName = request.form['agent']
	#he has permission he is asking no know agent username
	agent = Agent.query.filter_by(username == agentName).first()
	if agent is None:
		return emptyJSON

	companyName = agent.company

	# hierarchial checks
	if current_user.role == ADMIN:
		#he can access any agent , he is admin!!
		pass
	elif current_user.role == COMPANY:
		#checking whether agent is in his company
		if current_user.subRole == 0:
			if companyName != current_user.username:
				return emptyJSON
		else:
			if companyName != current_user.companyEmployee.company:
				return emptyJSON

	elif current_user.role == AGENT:
		#checking whether the logged in user is agent itself
		# or his employee
		if current_user.subRole == 0:
			if agentName != current_user.username:
				return emptyJSON
		else:
			if agentName != current_user.agentEmployee.agent:
				return emptyJSON
	else:
		# TODO log
		return emptyJSON

	# agent is not none and can be accessible by the currently logged in user.
	entries = Merchant.query.filter_by(agent = agentName).all()

	for entry in entries:
		merchantList.append(entry.username)

	return json.dumps({"merchants":merchantList})

"""
	Given a company it returns list of merchants directly under him.
"""
def getCompanyMerchants():
	emptyJSON =  json.dumps({"merchants":[]})

	#checking permission
	if not  get_merchants_of_company_permission.can():
		return emptyJSON

	if 'company' not in request.form:
		return emptyJSON

	companyName = request.form['company']
	
	# hierarchial checks
	if current_user.role == ADMIN:
		pass
	elif current_user.role == COMPANY:
		#checking whether agent is in his company
		if current_user.subRole == 0:
			if companyName != current_user.username:
				return emptyJSON
		else:
			if companyName != current_user.companyEmployee.company:
				return emptyJSON
	else:
		return emptyJSON

	#getting entries from db
	entries = Merchant.query.filter_by(company = companyName).all()

	merchantList = []
	for entry in entries:
		merchantList.append(entry.username)

	return json.dumps({"merchants":merchantList})

"""
	Given a (company,agent) ordered pair, it returns list of merchants under it.
"""
def getMerchants():

	if not  get_merchants_of_company_permission.can() and \
			get_merchants_of_agent_permission.can():
		data = {"merchants":[]}
		return json.dumps(data)


	companyName = request.form['company']
	agentName = request.form['agent']
	merchantList = []

	if agentName == 'null':
		agentName = None
	

	if agentName is None:
		if current_user.role != ADMIN or current_user.role != COMPANY:
			data = {"merchants":[]}
			return json.dumps(data)


	#getting merchants of an agent
	if companyName is not None:
		if agentName is not None:
			a = db.session.query(Agent).filter_by(username=agentName).first()
			if a is None or a.company != companyName:
				data = {"merchants":[]}
				return json.dumps(data)

			entries = Merchant.query.filter_by(agent=agentName).all()

			for entry in entries:
				merchantList.append(entry.username)

			
		else: # merchant who are directly under company
			entries = Merchant.query.filter(and_(Merchant.company == companyName,Merchant.agent == None)).all()

			for entry in entries:
				merchantList.append(entry.username)

	data = {"merchants":merchantList}
	return json.dumps(data)


"""
	Given a company it returns list of agents under him.
"""
def getCompanyAgents(companyName):
	agents = Agent.query.filter_by(company = companyName).all()

	agentList = []
	for agent in agents:
		agentList.append(agent.username)

	return agentList

"""
	Searches for a merchant using the requested query keyword
"""
def searchMerchants():
	merchant = None

	if not  search_merchant_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	if request.method == 'POST':
		if 'keyword' not in request.form:
			return render_template('support/search_merchants.html',merchant=merchant)

		keyword = request.form['keyword']
		merchant = Merchant.query.filter(or_(Merchant.username == keyword,Merchant.mobileNumber == keyword)).first()
		if merchant is not None and not canHeMeddleWithThisMerchant(merchant):
			merchant = None # requested merchant not under his scope
			
		return render_template('support/search_merchants.html',merchant=merchant)

	return render_template('support/search_merchants.html',merchant = merchant)


