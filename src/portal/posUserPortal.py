from flask import render_template, request, flash, redirect, url_for
from flask.ext.login import current_user

from src.utils.crypto import verify_passwd,hash_passwd

from ext import db
from src.portal import COMPANY, AGENT, MERCHANT
from src.models import posDetails, posUsers
from forms import PosUserChangePasswordForm,AddPosUserForm

from src.mAuthenticate import common

from principalConstants import *
from portalHelper import canHeMeddleWithThisTerminal,getSha256Hash,accessDeniedMessage
import json

def addPosUser():
	# check for permission
	if not add_pos_user_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	if 'terminal' not in request.args:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	clientId = request.args['terminal']
	terminal = posDetails.query.filter_by(clientId = clientId).first()

	# check for existence of terminal
	if terminal == None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# checking for eligibility
	isEligible,statusCode = common.validatePOSBitmap(terminal.bitmap,posDetails.VALIDATE_BITMAP_TYPE["PRE_LOGIN"])
	if not isEligible:
		flash(common.ERROR_CODES[statusCode]+' for the terminal','danger')
		return redirect(url_for('basePortal.viewPosUsers')+'?terminal='+clientId)

	# check whether the logged in user has access
	if canHeMeddleWithThisTerminal(terminal) == False:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	form = AddPosUserForm()
	if form.validate_on_submit():
		hashPasswd = getSha256Hash(form.password.data)
		password,salt = hash_passwd(hashPasswd)
		entry = posUsers.query.filter_by(clientId=clientId, username=form.username.data).first()
		if entry is not None:
			flash('username '+entry.username+' already exists','danger')
			return redirect(url_for('basePortal.viewPosUsers')+'?terminal='+request.args['terminal'])

		posUser = posUsers(form.username.data,password,salt,clientId)
		posUser.save(commit = False)

		# logging pos user creation event.
		event = "pos user "+ posUser.username +" created"
		common.logEvent(event,posUser.username,terminal,common.Log.info)

		db.session.commit()

		flash('added pos user '+posUser.username+ ' successfully')
		return redirect(url_for('basePortal.viewPosUsers')+'?terminal='+request.args['terminal'])

	return render_template('pos_user/add_pos_user.html',form=form,clientId = clientId)

def deletePosUser():

	if not delete_pos_user_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	if 'terminal' not in request.args or 'username' not in request.args:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	clientId = request.args['terminal']
	username = request.args['username']

	posUser = posUsers.query.filter_by(clientId = clientId,username = username).first()
	
	if posUser == None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	terminalDetails = posUser.details

	# checking for eligibility
	isEligible,statusCode = common.validatePOSBitmap(terminalDetails.bitmap,posDetails.VALIDATE_BITMAP_TYPE["PRE_LOGIN"])
	if not isEligible:
		flash(common.ERROR_CODES[statusCode]+' for the terminal','danger')
		return redirect(url_for('basePortal.viewPosUsers')+'?terminal='+clientId)

	# main posUser cannot be deletd
	if posUser.username == terminalDetails.mobileNumber:
		flash(' Main pos user '+ posUser.username +' cannot be deleted','danger')
		return redirect(url_for('basePortal.viewPosUsers')+'?terminal='+str(clientId))

	terminal = posUser.details
	# checking access for the logged in user.
	if canHeMeddleWithThisTerminal(terminal) == False:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))
	
	# logging pos user creation event.
	posUser.delete(commit = False)
	event = "pos user "+posUser.username+ " deleted"
	common.logEvent(event,posUser.username,terminal,common.Log.info)

	db.session.commit()

	flash('deleted pos user '+ posUser.username +' successfully')
	return redirect(url_for('basePortal.viewPosUsers')+'?terminal='+str(clientId))

def viewPosUsers():
	"""
		get the terminal and show posUsers
	"""
	# check for permission
	if not view_pos_users_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))
	
	if 'terminal' not in request.args:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	clientId = request.args['terminal']
	terminalDetails = posDetails.query.filter_by(clientId = clientId).first()

	if terminalDetails == None or canHeMeddleWithThisTerminal(terminalDetails) == False:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))


	users = terminalDetails.users
	lp = lock_pos_user_permission.can()
	ulp = unlock_pos_user_permission.can()
	lock_bricks = []
	for user in users:
		brick = {}
		if user.isLocked():
			if ulp:
				brick['name'] = 'Un Lock'
				brick['href'] = url_for('basePortal.unlockPosUser')+'?terminal='+ str(user.clientId) +'&username='+ str(user.username)
			else:
				brick = None
		else:
			if lp:
				brick['name'] = 'Lock'
				brick['href'] = url_for('basePortal.lockPosUser')+'?terminal='+ str(user.clientId) +'&username='+ str(user.username)
			else:
				brick = None
		lock_bricks.append(brick)


	# adding necessary actions
	addPosUser = add_pos_user_permission.can()
	deletePosUser = delete_pos_user_permission.can()
	changePasswordForPosUser = change_pos_user_password_permission.can()

	return render_template('users/view_pos_users.html',terminalDetails = terminalDetails,users=users,lock_bricks=lock_bricks,addPosUser=addPosUser,deletePosUser=deletePosUser,changePasswordForPosUser=changePasswordForPosUser)

def changePosUserPassword():
	# check for permission
	if not change_pos_user_password_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	if 'terminal' not in request.args or 'username' not in request.args:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	clientId = request.args['terminal']
	username = request.args['username']

	# check whether posUser exists or not.
	posUser = posUsers.query.filter_by(clientId = clientId,username = username).first()

	if posUser == None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))
	
	terminalDetails = posUser.details

	# checking for eligibility
	isEligible,statusCode = common.validatePOSBitmap(terminalDetails.bitmap,posDetails.VALIDATE_BITMAP_TYPE["PRE_LOGIN"])
	if not isEligible:
		flash(common.ERROR_CODES[statusCode]+' for the terminal','danger')
		return redirect(url_for('basePortal.viewPosUsers')+'?terminal='+clientId)

	# checking access for the logged in user.
	if canHeMeddleWithThisTerminal(posUser.details) == False:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	form = PosUserChangePasswordForm()
	if form.validate_on_submit():
		hashPasswd = getSha256Hash(form.newPassword.data)
		password,salt = hash_passwd(hashPasswd,posUser.salt)
		posUser.update(commit=False, passwd=password, salt=salt)

		event = 'Changed password for pos user '+posUser.username+' successfully'
		# logging pos user creation event.
		details = posUser.details
		common.logEvent(event,posUser.username,details,common.Log.info)

		db.session.commit()

		flash(event)
		return redirect(url_for('basePortal.viewPosUsers')+'?terminal='+clientId)
	
	return render_template('pos_user/change_password.html',form = form,terminal = clientId,username = username)

def lockPosUser():
	# check for permission
	if not lock_pos_user_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	if 'terminal' not in request.args or 'username' not in request.args:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))
		
	clientId = request.args['terminal']
	username = request.args['username']

	# check whether posUser exists or not.
	posUser = posUsers.query.filter_by(clientId = clientId,username = username).first()

	if posUser == None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))
	
	terminalDetails = posUser.details

	# checking for eligibility
	isEligible,statusCode = common.validatePOSBitmap(terminalDetails.bitmap,posDetails.VALIDATE_BITMAP_TYPE["PRE_LOGIN"])
	if not isEligible:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# checking access for the logged in user.
	if canHeMeddleWithThisTerminal(posUser.details) == False:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))
	
	posUser.lock()
	db.session.commit()
	
	flash('Locked pos user '+ posUser.username +' successfully','danger')
	return redirect(url_for('basePortal.viewPosUsers')+'?terminal='+str(clientId))

	return json.dumps(jsonResponse)

def unlockPosUser():
	# check for permission
	if not unlock_pos_user_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	if 'terminal' not in request.args or 'username' not in request.args:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))
		
	clientId = request.args['terminal']
	username = request.args['username']

	# check whether posUser exists or not.
	posUser = posUsers.query.filter_by(clientId = clientId,username = username).first()

	if posUser == None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))
	
	terminalDetails = posUser.details

	# checking for eligibility
	isEligible,statusCode = common.validatePOSBitmap(terminalDetails.bitmap,posDetails.VALIDATE_BITMAP_TYPE["PRE_LOGIN"])
	if not isEligible:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# checking access for the logged in user.
	if canHeMeddleWithThisTerminal(posUser.details) == False:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))
	
	posUser.unlock()
	db.session.commit()
	
	flash('Unlocked pos user '+ posUser.username +' successfully','success')
	return redirect(url_for('basePortal.viewPosUsers')+'?terminal='+str(clientId))
