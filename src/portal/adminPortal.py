from flask import render_template, request, flash, redirect, url_for, current_app
from flask.ext.login import current_user

from src.portal import ADMIN
from src.models import clientIdInventory,dongleInventory, Merchant

from forms import AddClientIdForm,AddDongleForm

from principalConstants import add_clientid_permission,add_dongle_permission,allot_dongle_permission,unallot_dongle_permission
from ext import db
from portalHelper import isHeAdmin,canHeMeddleWithThisMerchant,logEvent,accessDeniedMessage
import json

""" This is add new clientId to the clientId Inventory table"""
def addClientId():
	
	# checking for permission
	if not add_clientid_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# only allowing admin base role 
	if current_user.role != 0: # checking whether user with base role admin
		current_app.logger.warn('you gave add_clientd_permission to a user who is not main admin')
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	form = AddClientIdForm()
	if form.validate_on_submit():
		c = clientIdInventory(form.clientID.data,form.securityToken.data)
		c.save(commit=False)
		event = "Added client Id to inventory"
		logEvent(event+str(form.clientID.data))
		db.session.commit()

		flash(event)
		return redirect(url_for('basePortal.viewProfile'))

	return render_template("inventory/add_clientid.html", form=form)
	
""" This is add new dongle to the dongle Inventory table """
def addDongle():
	# checking for permission
	if not add_dongle_permission.can():
		return redirect(url_for('basePortal.viewProfile'))

	# only allowing admin base role 
	if not current_user.role == 0: # checking whether user with base role admin
		current_app.logger.critical('you gave add_dongle_permission to other who is not admin')
		return redirect(url_for('basePortal.viewProfile'))

	form = AddDongleForm()

	if form.validate_on_submit():
		d = dongleInventory(form.dongleSerial.data)
		d.save(commit=False)
		event = "Added dongle to inventory"
		logEvent(event +"(%s)"%form.dongleSerial.data)
		db.session.commit()

		flash(event)
		return redirect(url_for('basePortal.viewProfile'))

	return render_template("inventory/add_dongle.html", form=form)


def getDongleResponseJson(status = False,notes = ""):
	jsonResponse = {"status":status,"notes":notes}
	return json.dumps(jsonResponse)

def allotDongle(merchant):

	if 'dongleSerial' not in request.form:
		return getDongleResponseJson(False,"invalid request")		

	if not allot_dongle_permission.can():
		return getDongleResponseJson(False,"you don't have necessary permissions")
		
	# check whether required entry is present or not.
	merchant = Merchant.query.filter_by(username = merchant).first()
	if merchant == None:
		return getDongleResponseJson(False,"not a valid merchant")

	# checking authorization
	if not canHeMeddleWithThisMerchant(merchant):
		return getDongleResponseJson(False,"you cannot meddle with this merchant")

	dongleSerial = request.form['dongleSerial']
	dongle = dongleInventory.query.filter_by(dongleSerial = dongleSerial).first()

	if not dongle:
		return getDongleResponseJson(False,"request dongle serial not found")

	if dongle.isAlloted:
		return getDongleResponseJson(False,'Dongle with serial %s is alloted to other merchant'% dongleSerial)

	# update dongle entry
	dongle.update(commit=False, isAlloted=True, merchant=merchant.username, agent=merchant.agent, company=merchant.company)
	logEvent("dongle %s alloted"%dongleSerial, onActionUser=merchant.username)	
	db.session.commit()

	return getDongleResponseJson(True,'Alloted dongle %s successfully'% dongleSerial)

def unallotDongle():
	# checking for the existence of form key
	if 'dongleSerial' not in request.form:
		return getDongleResponseJson(False,"invalid request")

	# checking whether the user has permission or not
	if not unallot_dongle_permission.can():
		return getDongleResponseJson(False,"you don't have necessary permissions")

	dongleSerial = request.form['dongleSerial']
	dongle = dongleInventory.query.filter_by(dongleSerial = dongleSerial).first()

	# checking for the existence of the requested dongle
	if not dongle:
		return getDongleResponseJson(False,"request dongle serial not found")

	# checked whether the dongle is alloted to some one.
	if not dongle.isAlloted:
		return getDongleResponseJson(False,'Dongle with serial %s is not alloted to any merchant'% dongleSerial)

	# check whether dongle merchant is present or not.
	merchant = dongle.owner
	if merchant == None:
		return getDongleResponseJson(False,"not a valid merchant")

	# checking use authorization on that merchant
	if not canHeMeddleWithThisMerchant(merchant):
		return getDongleResponseJson(False,"you cannot meddle with this merchant")

	dongle.update(commit=False, isAlloted=False, merchant=None, agent=None, company=None)
	logEvent("dongle %s unalloted"%dongleSerial, onActionUser=merchant.username)	
	db.session.commit()

	return getDongleResponseJson(True,'Un Alloted dongle %s successfully'% dongleSerial)
