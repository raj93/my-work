from flask import render_template, request, flash, redirect, url_for, current_app
from flask.ext.login import current_user

from ext import db
from src.portal import ADMIN, COMPANY, AGENT, MERCHANT
from src.models import payment, support, posDetails
from src.mAuthenticate.common import Log,logEvent

from forms import SupportForm

from sqlalchemy import and_
import simplejson as json
from datetime import datetime
from principalConstants import *

from portalHelper import canHeMeddleWithThisMerchant, getHierarchyDetails, getCurrentTimeStamp

import requests

"""
	A user can use this functionality to tell/ask something to aasaanpay.
"""

def addSupport():

	# no permission because any user can do support query

	company,agent,merchant = getHierarchyDetails()

	form = SupportForm()
	if form.validate_on_submit():
		try:
			# freshdesk api key -> rLT5UM2kZNqqfpUvrOd
			feedbackType = support.FEED_BACK_VALUES[int(form.feedbackType.data)]

			helpDeskTicket ={
			  "helpdesk_ticket":{
				  "description": form.message.data + "\n contactNumber:"+ form.contactNumber.data,
				  "subject" : feedbackType,
				  "email" : form.email.data,
				  "priority" : 1,
				  "status" : 2
			  },
			  "cc_emails":""
			}

			helpDeskJson = json.dumps(helpDeskTicket)

			# send it to help desk
			headers = {'content-type': 'application/json'}
			url = "http://aasaanpay.freshdesk.com/helpdesk/tickets.json"
			r = requests.post(url,data = helpDeskJson,headers = headers, auth=(current_app.config['FRESH_DESK_API_KEY'],"fdsa"))
			response = r.json
			if r.status_code == 200:
				response = r.json()
				ticketId = response["helpdesk_ticket"]["display_id"]

				s = support(current_user.username,getCurrentTimeStamp(),form.contactNumber.data,form.message.data,form.email.data,\
				form.feedbackType.data,ticketId,merchant,agent,company)
				s.save(commit = False)
				db.session.commit()

		except Exception as e:
			current_app.logger.warn("freshdesk api request failed")
			current_app.logger.error(str(e))
			flash("Your request could not be processed.Please try later.")
			return redirect(url_for('basePortal.viewProfile'))

		

		flash("Reported your support request.Our support team will contact you soon")
		return redirect(url_for('basePortal.viewProfile'))

	return render_template("support/add_support.html", form=form)


"""
	When the merchant is onBoarded, there is payment that need to be made
	for the terminals associated. This payment verification is done on this page.
"""
def paymentVerification():
	# where payment verifications are done.
	return render_template('verification/payment_verification.html')

"""
	Given a cheque or dd number it returns all the payments satisfies the request and accessibility 
"""
def getPayments():
	
	emptyJSON =  json.dumps({"payments":[]})

	# check for permission
	if not get_payments_permission.can():
		return emptyJSON

	paymentNumber = request.form['paymentNumber']

	entries  = payment.query.filter(and_(payment.micr == paymentNumber,payment.isVerified == False)).all()
	output = []

	# for each entry check whether the entry is accessible by the logged in user or not.
	for entry in entries:
		merchant = entry.merchantDetails

		if canHeMeddleWithThisMerchant(merchant):
			# it means he has access
			tempJson  ={"id":entry.id,"amount":entry.amount,"merchant":merchant.username}
			output.append(tempJson)

	finalDict = {"payments":output}
	return json.dumps(finalDict)

"""
	verify a payment using his paymentId
"""
def verifyPayment():
	response = {'status':False,'feedback':''}

	# check for permission
	if not verify_payment_permission.can():
		response['feedback']  = "Permisssion denied"
		return json.dumps(response)

	# getting payment id
	if 'paymentId' not in request.form:
		response['feedback'] = "paymentId parameter not present in request"
		return json.dumps(response)

	paymentId = request.form['paymentId']

	paymentEntry = payment.query.filter_by(id = paymentId).first()

	# checking whether entry exists or not.
	if not paymentEntry:
		# accessing non existent
		response['feedback'] = "requested payment not present"
		return json.dumps(response) 

	merchant = paymentEntry.merchantDetails

	if not canHeMeddleWithThisMerchant(merchant):
		# accessing non existent
		response['feedback'] = "Permisssion denied"
		return json.dumps(response) 


	# setting verified status to true for payment entry.
	paymentEntry.update(commit=False, isVerified=True)

	terminals = paymentEntry.terminals
	# setting payment done for terminals associated to the payment.
	event = "payment verified"

	print posDetails.BITMAP['PAYMENT_VERIFICATION']
	for terminal in terminals:
		terminal.update(commit=False,bitmap = terminal.bitmap|terminal.BITMAP['PAYMENT_VERIFICATION'])
		

	db.session.commit()
	for terminal in terminals:
		logEvent(event,terminal.mobileNumber,terminal,Log.info)

	response['status'] = True
	response['feedback'] = "payment verified successfully"
	url = url_for('basePortal.viewMerchant',name=merchant.username)
	response.update({'redirect_url':url}) # present only if verification is success
	return json.dumps(response)
