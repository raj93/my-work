from flask import render_template, request, flash, redirect, url_for
from flask.ext.login import current_user

from src.utils.crypto import hash_passwd,generateRandomPassword

from ext import db
from src.portal import ADMIN, COMPANY, AGENT, MERCHANT, ADMIN_EMPLOYEE_ROLE, COMPANY_EMPLOYEE_ROLE, AGENT_EMPLOYEE_ROLE, MERCHANT_EMPLOYEE_ROLE
from src.models import portalLogin,AdminEmployee,CompanyEmployee,AgentEmployee,MerchantEmployee, \
						Admin, Company, Agent,Merchant,payment,posDetails

from forms import AddEmployeeForm, EditEmployeeForm

from principalConstants import *
import hashlib
from portalHelper import logEvent,getKeyOfValueInDict,accessDeniedMessage
from src.utils.ses_email import send1

# add/edit/view employee
"""
	Returns employees of a particular user.
	User might be Admin,Company,Agent or Merchant.
"""
def getEmployees(reqEntry):
	employees = []

	if reqEntry.role == ADMIN:
		employees = AdminEmployee.query.filter_by(admin = reqEntry.username).all()
	elif reqEntry.role == COMPANY:
		employees = CompanyEmployee.query.filter_by(company = reqEntry.username).all()
	elif reqEntry.role == AGENT:
		employees = AgentEmployee.query.filter_by(agent = reqEntry.username).all()
	elif reqEntry.role == MERCHANT:
		employees = MerchantEmployee.query.filter_by(merchant = reqEntry.username).all()
	else:
		# code should not come here
		pass
	return employees

"""
	View employees of a particular user
"""
def viewEmployees():
	
	
	if 'username' not in request.args:
		username = current_user.username
	else:
		username = request.args['username']

	entry =	portalLogin.query.filter_by(username = username).first()

	# we should have the requested user with us and
	# an employee cannot have employees
	if entry == None and entry.subRole > 0:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# NOTE: an employee can have access to view employees of base users

	# checking whether the logged in user has access to view employees of the requested
	if current_user.role == ADMIN:
		# admin can view employees of any one.		
		pass
	elif current_user.role == COMPANY:
		# filling companyName variable
		if current_user.subRole == 0:
			companyName = current_user.username
		else:
			companyName = current_user.companyEmployee.company
		
		# company cannot view admin details
		if entry.role == ADMIN:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))
		elif entry.role == COMPANY:
			# company cannot view employees of other company
			if companyName != entry.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		elif entry.role == AGENT:
			# company cannot view employees of agent of other company
			if companyName != entry.company:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		elif entry.role == MERCHANT:
			# company cannot view employees of merchant who are not in his scope.
			if companyName != entry.company:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		else:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))
		
	elif current_user.role == AGENT:
		# filling agentName variable
		if current_user.subRole == 0:
			agentName = current_user.username
		else:
			agentName = current_user.agentEmployee.agent
		
		if entry.role == ADMIN or entry.role == COMPANY:
			#agent cannot view admin employees
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))
		elif entry.role == AGENT:
			#agent cannot view agent of other company
			if agentName != entry.agent:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		elif entry.role == MERCHANT:
			# company cannot view merchant not under him
			if agentName != entry.agent:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		else:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))
		
	elif current_user.role == MERCHANT:
		# filling merchantName variable
		if current_user.subRole == 0:
			merchantName = current_user.username
		else:
			merchantName = current_user.merchantEmployee.merchant
		
		if entry.role == ADMIN or entry.role == COMPANY or entry.role == AGENT:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))
		elif entry.role == MERCHANT:
			# merchant cannot view employees of other merchants
			if merchantName != entry.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		else:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))
	else:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# getting the employees 
	employees = []
	employees = getEmployees(entry)

	logEvent("viewed employees",onActionUser=entry.username)
	db.session.commit()

	return render_template('users/view_employees.html',employees=employees)

"""
	Add Employee to a user with subRole = 0.
"""
def addEmployee():
	# checking add employee permission
	if not add_employee_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	if 'username' not in request.args:
		username = current_user.username
	else:
		username = request.args['username']

	# for whom we add
	entry =	portalLogin.query.filter_by(username = username).first()

	# it should satisfy (we have that user and he is not an employee)
	if entry == None or entry.subRole > 0:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# based on the role of the entry output a list of employee types
	if entry.role == ADMIN:
		employeeTypeList = ADMIN_EMPLOYEE_ROLE
	elif entry.role == COMPANY:
		employeeTypeList = COMPANY_EMPLOYEE_ROLE
	elif entry.role == AGENT:
		employeeTypeList = AGENT_EMPLOYEE_ROLE
	elif entry.role == MERCHANT:
		employeeTypeList = MERCHANT_EMPLOYEE_ROLE
	else:
		pass

	form = AddEmployeeForm()
	#post and validate
	if form.validate_on_submit():
		#we need to check whether the logged in user can add employee or not to requested 'username'
		if current_user.role == ADMIN:
			# admin can add employee to any one
			pass
		elif current_user.role == COMPANY:

			# getting the company name
			if current_user.subRole == 0:
				companyName = current_user.username
			else:
				# even company employee can add to company if he has permission
				companyName = current_user.companyEmployee.company

			if entry.role == ADMIN:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))

			elif entry.role == COMPANY:
				if companyName != entry.username:
					accessDeniedMessage()
					return redirect(url_for('basePortal.viewProfile'))

			elif entry.role == AGENT:
				if companyName != entry.agent.company:
					accessDeniedMessage()
					return redirect(url_for('basePortal.viewProfile'))

			elif entry.role == MERCHANT:
				if companyName != entry.merchant.company:
					accessDeniedMessage()
					return redirect(url_for('basePortal.viewProfile'))

			else:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))

		elif current_user.role == AGENT:
			# getting the agent name
			if current_user.subRole == 0:
				agentName = current_user.username
			else:
				# even company employee can add to company if he has permission
				agentName = current_user.agentEmployee.agent

			if entry.role == ADMIN or entry.role == COMPANY:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
			elif entry.role == AGENT:
				if agentName != entry.username:
					accessDeniedMessage()
					return redirect(url_for('basePortal.viewProfile'))
			elif entry.role == MERCHANT:
				if agentName != entry.merchant.agent:
					accessDeniedMessage()
					return redirect(url_for('basePortal.viewProfile'))
			else:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))

		elif current_user.role == MERCHANT:
			# getting the merchant name
			if current_user.subRole == 0:
				merchantName = current_user.username
			else:
				# even company employee can add to company if he has permission
				merchantName = current_user.merchantEmployee.merchant

			if entry.role != MERCHANT:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
			else:
				if entry.username != merchantName:
					accessDeniedMessage()
					return redirect(url_for('basePortal.viewProfile'))
		else:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))


		#add to the appropriate table based on the role
		password = generateRandomPassword(10)
		hashedPassword,salt = hash_passwd(hashlib.sha256(password).hexdigest())

		employeeType = request.form['employeeType']
		if entry.role == ADMIN:
			if employeeType in ADMIN_EMPLOYEE_ROLE:
				subRole = ADMIN_EMPLOYEE_ROLE[employeeType]
			else:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))

			# adding entry to portal login
			login = portalLogin(form.username.data,hashedPassword,salt,ADMIN,subRole,True,True);
			#a.shouldChangePassword = True # for changing password when logged in for first time
			login.save()

			# add to admin employee table
			employee = AdminEmployee(form.username.data,form.fullname.data,'end of universe',\
				form.mobileNumber.data,form.email.data,entry.username)
			employee.save()

		elif entry.role == COMPANY:
			if employeeType in COMPANY_EMPLOYEE_ROLE:
				subRole = COMPANY_EMPLOYEE_ROLE[employeeType]
			else:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
			
			# adding entry to portal login
			login = portalLogin(form.username.data,hashedPassword,salt,COMPANY,subRole,True,True);
			login.save(commit = False)

			# add to company employee table
			employee = CompanyEmployee(form.username.data,form.fullname.data,'end of universe',\
				form.mobileNumber.data,form.email.data,entry.username)
			employee.save(commit = False)

		elif entry.role ==  AGENT:
			if employeeType in AGENT_EMPLOYEE_ROLE:
				subRole = AGENT_EMPLOYEE_ROLE[employeeType]
			else:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))

			# adding entry to portal login
			login = portalLogin(form.username.data,hashedPassword,salt,AGENT,subRole,True,True);
			login.save(commit = False)

			# add to agent employee table
			employee = AgentEmployee(form.username.data,form.fullname.data,'end of universe',\
				form.mobileNumber.data,form.email.data,entry.username)
			employee.save(commit = False)
		elif entry.role == MERCHANT:
			if employeeType in MERCHANT_EMPLOYEE_ROLE:
				subRole = MERCHANT_EMPLOYEE_ROLE[employeeType]
			else:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))

			# adding entry to portal login
			login = portalLogin(form.username.data,hashedPassword,salt,MERCHANT,subRole,True,True);
			login.save(commit = False)

			# add to merchant employee table
			employee = MerchantEmployee(form.username.data,form.fullname.data,'end of universe',\
				form.mobileNumber.data,form.email.data,entry.username)
			employee.save(commit = False)
			
		else:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))

		try:
			db.session.commit()
		except Exception as e:
			current_app.logger.critical('Following exception occured while commiting changes to db')
			current_app.logger.critical(str(e))

			flash("server has faced problem in processing this request. Please try again after some time",'danger')
			return redirect(url_for('basePortal.viewProfile'))

		# send password to email
		subject = "Your credentials with temporary password"
		to = employee.email
			
		body = "username and password are "+"\n"+ \
				"username: "+ employee.username + "\n"+ \
				"password: "+ password + "\n" +\
				"You can access your portal at "+\
				request.url_root[:len(request.url_root)-1] + url_for('basePortal.login')

		logEvent("added employee %s"% employee.username)

		send1(subject, to, body,None,None)

		flash('employee' + employee.username + ' added successfully as '+request.form['employeeType'])
		return redirect(url_for('basePortal.viewProfile'))

	return render_template('employee/add_employee.html',form=form,employeeTypeList = employeeTypeList,username =username)

"""
	Edit a given employee
"""
def editEmployee(employee):
	# checking permission
	if not edit_employee_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# checking existence of requested employee
	e = portalLogin.query.filter_by(username = employee).first()
	if e is None:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	#getting the right entry
	emp = None
	if e.role == ADMIN:
		emp = e.adminEmployee
		empIncharge = portalLogin.query.filter_by(username = emp.admin).first()
	elif e.role == COMPANY:
		emp = e.companyEmployee
		empIncharge = portalLogin.query.filter_by(username = emp.company).first()
	elif e.role == AGENT:
		emp = e.agentEmployee
		empIncharge = portalLogin.query.filter_by(username = emp.agent).first()
	elif e.role == MERCHANT:
		emp = e.merchantEmployee
		empIncharge = portalLogin.query.filter_by(username = emp.merchant).first()
	else:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))


	# checking the access permission of logged in user
	if current_user.role == ADMIN:
		pass
	elif current_user.role == COMPANY:
		if empIncharge.role == COMPANY:
			if not empIncharge.username == current_user.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		elif empIncharge.role == AGENT or empIncharge.role == MERCHANT:
			if empIncharge.company != current_user.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		else:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))

	elif current_user.role == AGENT:
		if empIncharge.role == AGENT:
			if not empIncharge.username == current_user.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		elif empIncharge.role == MERCHANT:
			if empIncharge.agent != current_user.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		else:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))

	elif current_user.role == MERCHANT:
		if empIncharge.role == MERCHANT:
			if empIncharge.username != current_user.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		else:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))
	
	#edit employee entry 
	form = EditEmployeeForm(obj=emp)
	if form.validate_on_submit():
		emp.email = form.email.data
		emp.update()
		
		flash('updated employee '+emp.username+' successfully')
		return redirect(url_for('basePortal.viewEmployee',name = emp.username))

	return render_template("employee/edit_employee.html",form=form,employee = emp)

"""
	View a given employee
"""
def viewEmployee(employee):
	# checking permission
	if not view_employee_permission.can():
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	# checking existence of requested employee
	e = portalLogin.query.filter_by(username = employee).first()

	if e is None or e.subRole == 0:
		accessDeniedMessage()
		return redirect(url_for('basePortal.viewProfile'))

	subRole = None
	#getting the right entry
	if e.role == ADMIN:
		emp = e.adminEmployee
		empIncharge = portalLogin.query.filter_by(username = emp.admin).first()
		subRole = getKeyOfValueInDict(ADMIN_EMPLOYEE_ROLE,e.subRole)
	elif e.role == COMPANY:
		emp = e.companyEmployee
		empIncharge = portalLogin.query.filter_by(username = emp.company).first()
		subRole = getKeyOfValueInDict(COMPANY_EMPLOYEE_ROLE,e.subRole)
	elif e.role == AGENT:
		emp = e.agentEmployee
		empIncharge = portalLogin.query.filter_by(username = emp.agent).first()
		subRole = getKeyOfValueInDict(AGENT_EMPLOYEE_ROLE,e.subRole)
	elif e.role == MERCHANT:
		emp = e.merchantEmployee
		empIncharge = portalLogin.query.filter_by(username = emp.merchant).first()
		subRole = getKeyOfValueInDict(MERCHANT_EMPLOYEE_ROLE,e.subRole)
	else:
		return redirect(url_for('basePortal.viewProfile'))


	# checking the access permission of logged in user
	if current_user.role == ADMIN:
		pass
	elif current_user.role == COMPANY:
		if empIncharge.role == COMPANY:
			if not empIncharge.username == current_user.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		elif empIncharge.role == AGENT or empIncharge.role == MERCHANT:
			if empIncharge.company != current_user.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		else:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))

	elif current_user.role == AGENT:
		if empIncharge.role == AGENT:
			if not empIncharge.username == current_user.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		elif empIncharge.role == MERCHANT:
			if empIncharge.agent != current_user.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		else:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))

	elif current_user.role == MERCHANT:
		if empIncharge.role == MERCHANT:
			if empIncharge.username != current_user.username:
				accessDeniedMessage()
				return redirect(url_for('basePortal.viewProfile'))
		else:
			accessDeniedMessage()
			return redirect(url_for('basePortal.viewProfile'))

	# adding necessary actions
	actionList = []
	# edit is separate bcos of its url 
	if edit_employee_permission.can():
		actionList.append({'name':edit_employee_permission.name,'url':url_for(edit_employee_permission.url,employee=emp.username)})


	return render_template("employee/view_employee.html",subRole=subRole, employee=emp,actionList = actionList)	

"""
	Expermintal code.
	An admin can add employee to any user with subRole = 0.
	This module can be later used for usecases like, 
	# Admin wants to add an Agent to a company
	# Admin wants to add Merchant to an Company/Agent
	# Admin wants to add a terminal to a merchant
	etc..
"""
def addEmpByHierarchy():

	#check permission
	
	showRoleList = False
	showCompanyList = False
	showAgentList = False
	showMerchantList = False
	showOthers = True

	roleList = []
	companyList = []
	agentList = []
	merchantList = []

	if current_user.role == ADMIN:
		roleList = ['company','agent','merchant']

		showRoleList = True
		showCompanyList = True
		showAgentList = True
		showMerchantList = True

		#adding companies to start the tree selection
		entries = Company.query.all()
		for entry in entries:
			companyList.append(entry.username)
		
	elif current_user.role == COMPANY:
		roleList = ['agent','merchant']

		showRoleList = True
		showAgentList = True
		showMerchantList = True

		if current_user.subRole == 0:
			companyName = current_user.username
		else:
			companyName = current_user.company

		#adding agents to start the tree selection
		entries = Agent.query.filter_by(company = companyName).all()
		for entry in entries:
			agentList.append(entry.username)

	elif current_user.role == AGENT:
		roleList = ['merchant']

		showRoleList = False
		showMerchantList = True

		if current_user.subRole == 0:
			agentName = current_user.username
		else:
			agentName = current_user.agent

		# adding merchants to select from
		entries = Merchant.query.filter_by(agent = agentName).all()
		for entry in entries:
			merchantList.append(entry.username)
	elif current_user.role == MERCHANT:
		showOthers = False
	else:
		pass

	form = AddEmployeeForm(request.form)

	if form.validate_on_submit():
		# 'forOthers' is the id of the check box
		# if it is in the form list it means it is checked( that's how the rule is)
		if 'forOthers' in request.form.keys():
			forOthers = True
		else:
			forOthers = False

		if 'forAgent' in request.form.keys():
			forAgent = True
		else:
			forAgent = False

		etype_name = request.form['employeeType']

		#check for valid employee type first
		if forOthers:
			r_name = request.form['roles']
			# if etype_name in employees[r_name]:
			# proceed
			# else:
			# return redirect(url_for('basePortal.viewProfile'))

		if current_user.role == ADMIN:
			if forOthers:
				# getting the inputs
				r_name = request.form['roles']
				c_name = request.form['companies']
				a_name = request.form['agents']
				m_name = request.form['merchants']

				if r_name == "company":
					c_entry = Company.query.filter_by(username = c_name).first()
					if c_entry is None:
						return redirect(url_for('basePortal.viewProfile'))
					# add to portal login with form.username.data
					# add to compnay employee table	with form.username.data,c_name and etype_name
				elif r_name == "agent":
					a_entry = Agent.query.filter_by(username = a_name).first()
					if a_entry is None:
						return redirect(url_for('basePortal.viewProfile'))
					# add to portal login with form.username.data
					# add to agent employee table with form.username.data,a_name and etype_name
				elif r_name == "merchant":
					m_entry = Merchant.query.filter_by(username = m_name).first()
					if m_entry is None:
						return redirect(url_for('basePortal.viewProfile'))
					# add to portal login with form.username.data
					# add to merchant employee table with form.username.data , m_name and etype_name
				else:
					return redirect(url_for('basePortal.viewProfile'))

			else:
				# add to portal login with form.username.data
				# add to admin employee table with form.username.data,current_user.username
				pass
		elif current_user.role == COMPANY:
			#getting inputs
			r_name = request.form['roles']
			a_name = request.form['agents']
			m_name = request.form['merchants']

			if forOthers:
				if r_name == "agent":
					a_entry = Agent.query.filter_by(username = a_name).first()
					if a_entry is None:
						return redirect(url_for('basePortal.viewProfile'))
					# add to portal login with form.username.data
					# add to agent employee table with form.username.data,a_name and etype_name
				elif r_name == "merchant":
					m_entry = Merchant.query.filter_by(username = m_name).first()
					if m_entry is None:
						return redirect(url_for('basePortal.viewProfile'))
					# add to portal login with form.username.data
					# add to merchant employee table with form.username.data , m_name and etype_name
				else:
					return redirect(url_for('basePortal.viewProfile'))
			else:
				# add to portal login with form.username.data
				# add to company employee table with form.username.data,current_user.username	
				pass

		elif current_user.role == AGENT:
			#getting inputs
			r_name = request.form['roles']
			m_name = request.form['merchants']

			if forOthers:
				if r_name == "merchant":
					m_entry = Merchant.query.filter_by(username = m_name).first()
					if m_entry is None:
						return redirect(url_for('basePortal.viewProfile'))
					# add to portal login with form.username.data
					# add to merchant employee table with form.username.data , m_name and etype_name
				else:
					return redirect(url_for('basePortal.viewProfile'))
			else:
				# add to portal login with form.username.data
				# add to company employee table with form.username.data,current_user.username	
				pass
		elif current_user.role == MERCHANT:
			# add to portal login with form.username.data
			# add to merchant employee table with form.username.data,current_user.username
			pass
			
		return redirect(url_for('basePortal.viewProfile'))
	
	return render_template("hierarchy/add_employee.html", form=form,companyList =companyList,agentList=agentList,merchantList = merchantList,showCompanyList=showCompanyList,showAgentList=showAgentList,showMerchantList=showMerchantList,showRoleList=showRoleList,roleList=roleList,showOthers=showOthers)
