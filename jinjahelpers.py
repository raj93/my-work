"""
	This module has all helper methods
	requiered for jinja templates
"""

from datetime import datetime, timedelta
from flask import url_for, request

# helper method for pagination
def url_for_other_page(page):
	arguments = '?'
	for a in request.args:
		if a != 'page':
			arguments += a+'='+request.args[a]+'&'
	ret = url_for(request.endpoint)+arguments+'page='+str(page)
	return ret

# return indian time corresponding to given utctime.
def get_indian_time(utctime):
	return (datetime.strptime(utctime,"%Y-%m-%d %X") + timedelta(hours=5,minutes=30)).strftime("%Y-%m-%d %X")