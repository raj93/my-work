import os
from logging import getLogger
from src.utils.misc import ensure_dir

class Config(object):
	basedir = os.path.abspath(os.path.dirname(__file__))#app package name
	
	################################# Folder Paths ####################################
	TEMP_FOLDER = basedir + '/temp/'
	UPLOAD_FOLDER= basedir + '/proofs/'
	LOG_FILENAME = basedir + '/logs/debug'
	TEMPLATE_FOLDER = basedir +'/templates'
	RECEIPT_RESOURCES_FOLDER = basedir + '/static/receipt/'

	ensure_dir(TEMP_FOLDER)
	ensure_dir(UPLOAD_FOLDER)
	ensure_dir(TEMPLATE_FOLDER)
	ensure_dir(LOG_FILENAME)
	ensure_dir(RECEIPT_RESOURCES_FOLDER)
	

	# File Extensions allowed during merchant registration or signatures(related to transactions)#
	SIGNATURE_EXTENSIONS_ALLOWED = set(['png', 'jpg', 'jpeg', 'gif'])
	PROOFS_EXTENSIONS_ALLOWED = set(['pdf', 'png', 'jpg', 'jpeg', 'gif'])
	

	############################# Contact Emails ###################################
	DEVELOPERS = ['developers@aasaanpay.com']
	SALES = ['venkat@aasaanpay.com']


	############################ S3 Buckets #########################################
	SIGNATURES_BUCKET = 'ap-signatures-dev'
	RECEIPTS_BUCKET = 'apay.co.in'
	UPLOADS_BUCKET = 'ap-upload-dev'


	########### Session/Cookie Management #########################
	SECRET_KEY = 'x$ow].em(2mI$,c0g'
	SESSION_PROTECTION = 'strong'#https://github.com/maxcountryman/flask-login/blob/master/docs/index.rst#session-protection
	ITS_DANGEROUS = "#f]F<<;9hp01nhJq*Z\\I2`teyf@J^=NK"#validate the security of itsdangerous
	REMEMBER_COOKIE_NAME = 'token'#https://github.com/maxcountryman/flask-login/blob/master/docs/index.rst#cookie-settings	
	SESSION_COOKIE_NAME = 'session'#The secure cookie uses this for the name of the session cookie.
	POS_SESSION_TIME = 30*60 # 30 minutes
	REMEMBER_COOKIE_DURATION = 7 #days #TODO
	SESSION_COOKIE_HTTPONLY = True
	SESSION_COOKIE_SECURE = True ##NOTE doesn't work with http connections


	########## SQLAlchemy configuration ############################
	SQLALCHEMY_DATABASE_URI = 'mysql://user:password@sample.com/dev'
	SQLALCHEMY_BINDS = { 'settled': 'mysql://user:password@sample.com/dev_settled'}
	SQLALCHEMY_POOL_SIZE = 5
	SQLALCHEMY_POOL_RECYCLE = 60*60
	SQLALCHEMY_POOL_TIMEOUT = 20
	SQLALCHEMY_ECHO = False
	

	######################## Request Caps/Timings ##############################
	TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST = 60
	OTP_REQUEST_NUMTRIES_CAP = 5
	ACTIVATION_REQUEST_NUMTRIES_CAP = 5
	INCORRECT_PASSWORD_ATTEMPTS = 5
	USER_HOLD_TIME = 60*20 # 20 minutes


	######### Configuration required by helpers to create Flask application #########
	BLUEPRINTS = ['src.api.base.base',
	 			'src.api.mobileServices.mServices',
	 			'src.api.mobileTransactions.transactions',
	 			'src.api.basePortal.common',
	 			'src.api.adminPortal.admin',
	 			'src.api.transactionPortal.transaction']

	EXTENSIONS = ['ext.db',
				'ext.db1',
				'ext.loginManager',
				'ext.principals',
				'ext.signer',
				'ext.csrf']
	
	LOGGERS = [getLogger('sqlalchemy'),
				getLogger('xhtml2pdf')]#TODO check the getlogger of sqlalchemy

	HANDLERS = ['src.utils.handlers.mail_handler',
				'src.utils.handlers.file_handler'] # sentry handler added only in production

	SIGNALS = {'flask.ext.principal.identity_loaded' : 'signals.on_identity_loaded'}#Signal to connect to: Function to be connected
	

	########################## Forgot password ##################################
	FORGOT_PASSWORD_FROM_EMAIL = "aasaanpay.com"
	FORGOT_PASSWORD_SUBJECT = "Reset password link"
	FORGOT_PASSWORD_TOKEN_KEY = "iamforgotpasswordkey@#$%^&*("
	FORGOT_PASSWORD_LINK_EXPIRY_TIME = 3600*10 # 10 hours
	RECAPTCHA_PUBLIC_KEY = '6LeYIbsSAAAAACRPIllxA7wvXjIE411PfdB2gt2J'
	RECAPTCHA_PRIVATE_KEY = '6LeYIbsSAAAAAJezaIq3Ft_hSTo0YtyeFG-JgRtu'
	RECAPTCHA_USE_SSL = True

	#################### Misc Development configuration ###############################
	DEBUG = False
	TESTING = False
	PLUTUS_STATUS = True
	STRIPE_LIVE = False
	ALLOWED_SMS_LIST = ['9030997944','9908306120', '9550473408', '7382457644','9848876174']
	

	##################### Third party application details ######################
	SENTRY_DSN = 'https://f5b3229e37364b9b84c73ba63c678c94:2b7894f926044f8c99af848bff0203c1@app.getsentry.com/22134'
	FRESH_DESK_API_KEY = 'rLT5UM2kZNqqfpUvrOd'
	KEY_TIER_URL = 'http://54.251.106.159/keyTier/'

	STRIPE_LIVE_KEY = 'sk_live_lylwyp0EQVdYixYdWOlgVKpO'#rXn8L8t7AQOv30Vw72RUnF0bN0PKFvIA'
	STRIPE_DEMO_KEY = 'sk_test_njwmHzfGYURxbToQU19tvE4D'# j590LNM0G7iun5At6QHOv6PrYiLbAmPM'
	STRIPE_EXCHANGE_RATE = 63.00#TODO Used temporarily during stripe/Refund as it supports only USD

	################## Uploads $###################
	MAX_CONTENT_LENGTH = 5 * 1024 * 1024 # 5MB is the max upload size


class ProductionConfig(Config):
	HANDLERS = ['src.utils.handlers.mail_handler',
				'src.utils.handlers.file_handler',
				'src.utils.handlers.sentry_handler']
	
	########## SQLAlchemy configuration ############################
	SQLALCHEMY_POOL_SIZE = 10
	SQLALCHEMY_POOL_RECYCLE = 60*60
	SQLALCHEMY_POOL_TIMEOUT = 20
	SQLALCHEMY_ECHO = False

	######################## Request Caps/Timings ##############################
	TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST = 60
	OTP_REQUEST_NUMTRIES_CAP = 5
	ACTIVATION_REQUEST_NUMTRIES_CAP = 5


	############################ S3 Buckets #########################################
	SIGNATURES_BUCKET = 'ap-signatures-prod'
	RECEIPTS_BUCKET = 'apay.in'
	UPLOADS_BUCKET = 'ap-upload-prod'


	####################### Misc #############################
	STRIPE_LIVE = True
	PLUTUS_STATUS = False
	


class DevelopmentConfig(Config):
	SECRET_KEY = 'x$ow].em(2mI$,c0g'
	ITS_DANGEROUS = "#f]F<<;9hp01nhJq*Z\\I2`teyf@J^=NK"#validate the security of itsdangerous
	FORGOT_PASSWORD_TOKEN_KEY = "iamforgotpasswordkey@#$%^&*("
	
	DEBUG = True
	SQLALCHEMY_DATABASE_URI = 'mysql://ubuntu:aFxLNrO8nMJT@sample.ci4tfrywi0ph.ap-southeast-1.rds.amazonaws.com/dev'
	SQLALCHEMY_BINDS = { 'settled': 'mysql://ubuntu:aFxLNrO8nMJT@sample.ci4tfrywi0ph.ap-southeast-1.rds.amazonaws.com/dev_settled'}	
	SQLALCHEMY_ECHO = False
	SETTELEMENT_SMS = '9908306120'
	PLUTUS_STATUS = False
	SALES = ['developers@aasaanpay']

class PruthviConfig(DevelopmentConfig):
	HANDLERS = ['src.utils.handlers.mail_handler',
				'src.utils.handlers.file_handler',
				#'src.utils.handlers.sentry_handler',
				]
	SENTRY_DSN = 'http://a856c4dabef141f1b283786deced6eb1:a2f00098977f4612befaf0f540b2846a@54.251.106.159/sentry/2?timeout=4'

	DEBUG = True
	TESTING = False
	SQLALCHEMY_DATABASE_URI = 'mysql://root:D3mo24@localhost/transactions'
	SQLALCHEMY_BINDS = {'settled' : 'mysql://root:D3mo24@localhost/settled'}
	SQLALCHEMY_ECHO = False
	KEY_TIER_URL = 'http://127.0.0.1/key/'
	STRIPE_LIVE = False

	RECAPTCHA_PUBLIC_KEY = '6LdrYvUSAAAAAE8V33LhpfG2IsTYWt7ZXuYUUNSF'
	RECAPTCHA_PRIVATE_KEY = '6LdrYvUSAAAAAMbXOZ3MxbNcX3lHujiDS3Q34tXR'
	SESSION_COOKIE_SECURE = False ##NOTE doesn't work with http connections

class TestingConfig(Config):
	SECRET_KEY = 'x$ow].em(2mI$,c0g'
	ITS_DANGEROUS = "#f]F<<;9hp01nhJq*Z\\I2`teyf@J^=NK"
	FORGOT_PASSWORD_TOKEN_KEY = "iamforgotpasswordkey@#$%^&*("
	
	DEBUG = True
	TESTING  = True
	SQLALCHEMY_DATABASE_URI = 'mysql://root:D3mo24@localhost/transactions1'
	SQLALCHEMY_BINDS = {'settled' : 'mysql://root:D3mo24@localhost/settled1'}
	SQLALCHEMY_ECHO = False

	#OTP Request Caps
	TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST = 4
	OTP_REQUEST_NUMTRIES_CAP = 2

	#POS Session Time
	POS_SESSION_TIME = 30
	SETTELEMENT_SMS = '9908306120'
	PLUTUS_STATUS = False

	#WTF-FORMS -> By default true. Works in normal environment. #TODO
	WTF_CSRF_ENABLED = False
	DEVELOPERS = ['pruthvi@aasaanpay.com']
	
class TejasConfig(DevelopmentConfig):
	DEBUG = True
	TESTING = False
	#SQLALCHEMY_DATABASE_URI = 'mysql://ubuntu:aFxLNrO8nMJT@sample.ci4tfrywi0ph.ap-southeast-1.rds.amazonaws.com/teja'	
	#SQLALCHEMY_BINDS = { 'settled': 'mysql://ubuntu:aFxLNrO8nMJT@sample.ci4tfrywi0ph.ap-southeast-1.rds.amazonaws.com/teja_settled'}	

	SQLALCHEMY_DATABASE_URI = 'mysql://root:asdf@localhost/test_transactions'
	SQLALCHEMY_BINDS = { 'settled': 'mysql://root:asdf@localhost/test_settled'}	
	
	SQLALCHEMY_ECHO = False
	SETTELEMENT_SMS = '9030997944'

	WTF_CSRF_ENABLED = False
	SESSION_COOKIE_SECURE = False

	TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST = 5 
	#SENTRY_DSN = None

class TejaIsTestingConfig(TestingConfig):

	SECRET_KEY = 'x$ow].em(2mI$,c0g'
	ITS_DANGEROUS = "#f]F<<;9hp01nhJq*Z\\I2`teyf@J^=NK"
	FORGOT_PASSWORD_TOKEN_KEY = "iamforgotpasswordkey@#$%^&*("

	DEBUG = True
	TESTING = True

	SQLALCHEMY_DATABASE_URI = 'mysql://root:asdf@localhost/test_transactions'
	SQLALCHEMY_BINDS = { 'settled': 'mysql://root:asdf@localhost/test_settled'}	
	SQLALCHEMY_ECHO = False

	#SQLALCHEMY_DATABASE_URI = 'mysql://ubuntu:aFxLNrO8nMJT@sample.ci4tfrywi0ph.ap-southeast-1.rds.amazonaws.com/test1'	
	#SQLALCHEMY_BINDS = { 'settled': 'mysql://ubuntu:aFxLNrO8nMJT@sample.ci4tfrywi0ph.ap-southeast-1.rds.amazonaws.com/test1_settled'}	
	
	#OTP Request Caps
	TIME_BETWEEN_SUCCESSIVE_OTP_REQUEST = 4
	OTP_REQUEST_NUMTRIES_CAP = 2

	#POS Session Time
	POS_SESSION_TIME = 90
	SETTELEMENT_SMS = '9030997944'
	PLUTUS_STATUS = False

	SENTRY_DSN = None

class DemoConfig(Config):
	SECRET_KEY = 'x$ow].em(2mI$,c0g'
	ITS_DANGEROUS = "#f]F<<;9hp01nhJq*Z\\I2`teyf@J^=NK"
	FORGOT_PASSWORD_TOKEN_KEY = "iamforgotpasswordkey@#$%^&*("
	
	DEBUG = True
	SQLALCHEMY_DATABASE_URI = 'mysql://ubuntu:aFxLNrO8nMJT@sample.ci4tfrywi0ph.ap-southeast-1.rds.amazonaws.com/demo'
	SQLALCHEMY_BINDS = { 'settled': 'mysql://ubuntu:aFxLNrO8nMJT@sample.ci4tfrywi0ph.ap-southeast-1.rds.amazonaws.com/demo_settled'}	
	SQLALCHEMY_ECHO = False
	SETTELEMENT_SMS = '9908306120'
	PLUTUS_STATUS = False
