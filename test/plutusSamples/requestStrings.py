#Using string formatting to generate XML requests. Since its a simple xml, efficiency would be better this way. Verify

purchase_request = '''<?xml version="1.0" encoding="UTF-8"?>
        <purchase-request>
	        <TransactionInput ID="ID00000_123k12j3lkasdfkasdflk">
		        <Card>
			        <IsManualEntry>true</IsManualEntry>
			        <CardNumber>%(cardNumber)s</CardNumber>
			        <ExpirationDate>
				        <MM>%(mm)s</MM>
				        <YY>%(yy)s</YY>
			        </ExpirationDate>
		        </Card>
		        <Amount>
			        <BaseAmount>%(amount)s</BaseAmount>
			        <CurrencyCode>INR</CurrencyCode>
		        </Amount>
		        <POS>
			        <POSReferenceNumber>%(posReference)s</POSReferenceNumber>
			        <TransactionTime>%(ttime)s</TransactionTime>
			        <TrackingNumber>%(trackingNumber)s</TrackingNumber>
		        </POS>
	        </TransactionInput>
        </purchase-request>'''
#track1 is optional
purchase_request1 = '''<?xml version="1.0" encoding="UTF-8"?>
        <purchase-request>
	        <TransactionInput ID="ID00000_123k12j3lkasdfkasdflk">
		        <Card>
			        <IsManualEntry>false</IsManualEntry>
					<Track1>%%B5176521005587557^SAINATH GUPTA K          /^10091010010000000772000000?</Track1>
					<Track2>;5176521005587557=10091010017720?</Track2>
		        </Card>
		        <Amount>
			        <BaseAmount>%(amount)s</BaseAmount>
			        <CurrencyCode>INR</CurrencyCode>
		        </Amount>
		        <POS>
			        <POSReferenceNumber>%(posReference)s</POSReferenceNumber>
			        <TransactionTime>%(ttime)s</TransactionTime>
   			        <TrackingNumber>%(trackingNumber)s</TrackingNumber>
		        </POS>
	        </TransactionInput>
        </purchase-request>'''
void_request = '''<?xml version="1.0"?>
        <purchase-request>
        	<TransactionInput ID="0ID000001">
		    <Amount>
				<BaseAmount>%(amount)s</BaseAmount>
				<CurrencyCode>INR</CurrencyCode>
			</Amount>
			<POS>
				<POSReferenceNumber>%(posReference)s</POSReferenceNumber>
				<TransactionTime>%(ttime)s</TransactionTime>
			        <TrackingNumber>%(trackingNumber)s</TrackingNumber>
			</POS>
		        <AdditionalInformation>
			        <AddlInfoField><ID>1011</ID><Value>%(acquirer)s</Value><Length>%(lenAcquirer)s</Length></AddlInfoField>
			        <AddlInfoField><ID>1000</ID><Value>%(invoice)s</Value><Length>%(lenInvoice)s</Length></AddlInfoField>
		        </AdditionalInformation>
		</TransactionInput>
	</purchase-request>'''

query_request = '''<?xml version="1.0"?>
	        <purchase-request>
		        <TransactionInput ID="ID000002">
			        <Amount>
					<BaseAmount>%(amount)s</BaseAmount>
					<CurrencyCode>INR</CurrencyCode>
				</Amount>
			        <POS>
					<POSReferenceNumber>%(posReference)s</POSReferenceNumber>
					<TransactionTime>%(ttime)s</TransactionTime>
				        <TrackingNumber>%(trackingNumber)s</TrackingNumber>
				</POS>
			        <AdditionalInformation>
				        <AddlInfoField><ID>1011</ID><Value>%(acquirer)s</Value><Length>%(lenAcquirer)s</Length></AddlInfoField>
				        <AddlInfoField><ID>1000</ID><Value>%(invoice)s</Value><Length>%(lenInvoice)s</Length></AddlInfoField>
			        </AdditionalInformation>
			</TransactionInput>
		</purchase-request>'''

settlement_report_request = '''<?xml version="1.0" encoding="utf-8" ?><settlement />'''

settlement_request = '''<Settlement-Request xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>'''

batch_report_request = '''<?xml version="1.0" encoding="utf-8" ?><purchase-request />'''

test_request = '''</xml version="1.0" encoding="utf-8"?><purchase-request />'''
