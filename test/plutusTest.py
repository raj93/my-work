#!/usr/bin/python
from suds.client import Client
import logging
#logging.basicConfig(level = logging.INFO)
#logging.getLogger('suds.client').setLevel(logging.DEBUG)

url = 'https://122.160.150.51:90/PlutusHub/bin/PlutusHub.dll?Handler=GenPlutusHubWSDL'
purchase_request = '''<?xml version="1.0" encoding="UTF-8"?>
	<purchase-request>
	<TransactionInput ID="ID000001">
	<Card>
	<IsManualEntry>True</IsManualEntry>
        <CardNumber>5176521005587557</CardNumber>
	<ExpirationDate>
	<MM>09</MM>
	<YY>10</YY>
	</ExpirationDate>
	</Card>
	<Amount>
	<BaseAmount>3999</BaseAmount>
	<CurrencyCode>INR</CurrencyCode>
	</Amount>
	</TransactionInput>
	</purchase-request>'''
purchase_icici_request = '''<?xml version="1.0" encoding="UTF-8"?>
	<purchase-request>
	<TransactionInput ID="ID000001">
	<Card>
	<IsManualEntry>True</IsManualEntry>
        <CardNumber>4182281234567890</CardNumber>
	<ExpirationDate>
	<MM>01</MM>
	<YY>10</YY>
	</ExpirationDate>
	</Card>
	<Amount>
	<BaseAmount>3999</BaseAmount>
	<CurrencyCode>INR</CurrencyCode>
	</Amount>
	</TransactionInput>
	</purchase-request>'''
void_request = 	'''<?xml version="1.0"?>
		<purchase-request xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
		<TransactionInput ID="0ID000001">
		<Amount><BaseAmount>4999</BaseAmount><CurrencyCode>INR</CurrencyCode></Amount>
		<POS><POSReferenceNumber>438</POSReferenceNumber><TransactionTime>2010-07-08T17:30:12.0Z</TransactionTime>
		<TrackingNumber>000037</TrackingNumber></POS>
		<AdditionalInformation>
		<AddlInfoField><ID>1011</ID><Value>HDFC</Value><Length>4</Length></AddlInfoField>
		<AddlInfoField><ID>1000</ID><Value>000194</Value><Length>6</Length></AddlInfoField>
		</AdditionalInformation></TransactionInput></purchase-request>'''
query_previous = '''<?xml version="1.0"?>
		<purchase-request xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
		<TransactionInput ID="ID000002">
		<Amount><BaseAmount>1276</BaseAmount><CurrencyCode>INR</CurrencyCode></Amount>
		<POS><POSReferenceNumber>438</POSReferenceNumber><TransactionTime>2010-07-08T17:30:12.0Z</TransactionTime>
		<TrackingNumber>002037</TrackingNumber></POS>
		<AdditionalInformation>
		<AddlInfoField><ID>1011</ID><Value>HDFC</Value><Length>4</Length></AddlInfoField>
		<AddlInfoField><ID>1000</ID><Value>000096</Value><Length>6</Length></AddlInfoField>
		</AdditionalInformation></TransactionInput></purchase-request>'''
test_request = '''<xml version="1.0" encoding="utf-8" ?>
	<purchase-request>
	</purchase-request>'''
settle_request = '''<?xml version="1.0" encoding="utf-8" ?>
<Settlement-Request xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" />'''
settle_report  = '''<?xml version="1.0" encoding="utf-8" ?>
		<settlement />'''
settle_report1  = '''<?xml version="1.0" encoding="utf-8" ?>
		<settlement>
		<AdditionalInformation>
		<AddlInfoField><ID>1011</ID><Value>HDFC</Value><Length>4</Length></AddlInfoField>
		</AdditionalInformation></settlement>'''
sale_complete = '''<?xml version="1.0" encoding="utf-8" ?>
	<purchase-request xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	<TransactionInput ID="0ID000001">
	<Amount><BaseAmount>122276</BaseAmount><CurrencyCode>INR</CurrencyCode></Amount>
	<POS><POSReferenceNumber>TL993812</POSReferenceNumber><TransactionTime>2010-07-08T17:30:12.0Z</TransactionTime>
	<TrackingNumber>000037</TrackingNumber></POS>
	<AdditionalInformation>
	<AddlInfoField><ID>1011</ID><Value>HDFC</Value><Length>4</Length></AddlInfoField>
	<AddlInfoField><ID>1000</ID><Value>000026</Value><Length>6</Length></AddlInfoField>
	</AdditionalInformation></TransactionInput></purchase-request>'''

tip = '''<?xml version="1.0" encoding="utf-8" ?>
	<purchase-request xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
	<TransactionInput ID="0ID000001">
	<Amount>
	<BaseAmount>1000</BaseAmount>
	<CurrencyCode>INR</CurrencyCode>
	</Amount>
	<POS><POSReferenceNumber>TL993812</POSReferenceNumber><TransactionTime>2010-07-08T17:30:12.0Z</TransactionTime>
	<TrackingNumber>000037</TrackingNumber></POS>
	<AdditionalInformation>
	<AddlInfoField><ID>1011</ID><Value>HDFC</Value><Length>4</Length></AddlInfoField>
	<AddlInfoField><ID>1000</ID><Value>000194</Value><Length>6</Length></AddlInfoField>
	</AdditionalInformation></TransactionInput></purchase-request>'''
batch_report = '''<?xml version="1.0" encoding="utf-8" ?><purchase-request />'''

preauth_request = purchase_request
refund_request = purchase_request
request_xml = purchase_request 
request_type = 0
sequence_code = 1030
client = Client(url)
result = client.service.PLHub_Exchange(78328, '{9F312C87-A5EF-4155-9593-84A7BE135CB0}' , request_type, request_xml, sequence_code) 
#f = open('result.xml','w')
#f.write(result.RESPONSE_XML)
print result.RESPONSE_XML
print repr(result.SEQUENCE_CODE)
#print "<root><request><request_type>"+repr(request_type)+"</request_type><request_xml>" + request_xml[40:]+ "</request_xml></request>" + result.RESPONSE_XML+"<seqCode>" + repr(result.SEQUENCE_CODE) + "</seqCode></root>"

PURCHASE = 0
VOID = 1
REFUND = 2
QUERY_PREVIOUS = 3
INITIALIZATION = 4
BATCH_SETTLEMENT = 5
PARAMETER_REQUEST = 6
TEST = 7
PRE_AUTHORIZE = 8
SALE_COMPLETE = 9
SETTLEMENT_REPORT = 16
TIP_REQUEST = 11
BATCH_REPORT = 14

