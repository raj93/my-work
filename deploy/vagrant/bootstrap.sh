#!/usr/bin/env bash

yum update
yum groupinstall -y 'Development Tools'
yum install -y httpd python-devel mysql-devel MySQL-python mod_wsgi python-pip openssl-devel libtiff-devel libjpeg-devel libzip-devel freetype-devel lcms2-devel libwebp-devel tcl-devel tk-devel git
