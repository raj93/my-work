def application(environ, start_response):
    status = '200 OK'
    import json
    output = 'Server is in maintenance. Maintenance period is from Monday 8PM to 11PM. Inconvenience regretted!'

    response_headers = [('Content-type', 'application/json'),
                        ('Content-Length', str(len(output)))]

    start_response(status, response_headers)
    return [output]
