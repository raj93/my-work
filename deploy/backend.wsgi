import sys
import os

os.environ['http_proxy']='http://proxy.aasaanpay.com:8080'
os.environ['https_proxy']='http://proxy.aasaanpay.com:8080'
os.environ['no_proxy']='127.0.0.1,localhost,169.254.169.254'

sys.path.insert(0, '/var/www/html/backend')

from app import app as application
