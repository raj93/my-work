#!/usr/bin/env python

from helpers import AppFactory
from config import *
from src.models import *
from src.mAuthenticate.common import errorMessage
from sqlalchemy.exc import IntegrityError,SQLAlchemyError
from flask import render_template, make_response, request, session, current_app
from flask.ext.login import current_user
import simplejson as json

app = AppFactory('PROJECT_SETTINGS').get_app(__name__)

from ext import db

mobileBlueprints = ['mServices','transactions']		# mobile requests
webBlueprints = ['adminPortal', 'basePortal', 'transactionPortal']# web requests

@app.errorhandler(403)
def access_forbidden(error):
	if request.blueprint in mobileBlueprints:
		return errorMessage('SERVER_ERROR')
	elif request.blueprint in webBlueprints:
		session['redirected_from'] = request.url
		return make_response("Access forbidden")
	else:
		session['redirected_from'] = request.url
		return make_response("Access forbidden")
	

@app.errorhandler(404)
def page_not_found(error):
	if request.blueprint in mobileBlueprints:
		return errorMessage('SERVER_ERROR')
	elif request.blueprint in webBlueprints:
		return render_template('error/404.html',current_user=current_user), 404
	else:
		return render_template('error/404.html',current_user=current_user), 404


#@app.errorhandler(Exception)#Disable during development for ease
#This probably should be removed even in production. Otherwise context of exceptions will be lost
@app.errorhandler(500)
def internal_server(error):
	current_app.logger.error(str(error))

	if request.blueprint in mobileBlueprints: 
		return errorMessage('SERVER_ERROR')
	elif request.blueprint in webBlueprints:
		return render_template('error/500.html',current_user=current_user), 500	
	else:
		return render_template('error/500.html',current_user=current_user), 500	
	

@app.errorhandler(502)
def special_exception_handler(error):
	current_app.logger.error(str(error))

	if request.blueprint in mobileBlueprints:
		return errorMessage('SERVER_ERROR')
	elif request.blueprint in webBlueprints:
		return render_template('error/special_exception_error.html',current_user=current_user)
	else:
		return render_template('error/special_exception_error.html',current_user=current_user)

	
@app.errorhandler(IntegrityError)
def integrity_exception_handler(error):
	current_app.logger.error(str(error))

	db.session.rollback()

	if request.blueprint in mobileBlueprints:
		return errorMessage('SERVER_ERROR')
	elif request.blueprint in webBlueprints:
		return render_template('error/integrity.html',current_user=current_user)
	else:
		return render_template('error/integrity.html',current_user=current_user)


@app.errorhandler(SQLAlchemyError)
def sqlalchemy_exception_handler(error):
	current_app.logger.error(str(error))
	db.session.rollback()

	if request.blueprint in mobileBlueprints:
		return errorMessage('SERVER_ERROR')
	elif request.blueprint in webBlueprints:
		return render_template('error/alchemyerror.html',current_user=current_user)
	else:
		return render_template('error/alchemyerror.html',current_user=current_user)


if __name__ == '__main__':
	app.run()
